<?php

use Illuminate\Support\Facades\Route;

Route::group(["prefix" => "route", "namespace" => "Route"], function () {
    Route::get('list', 'RouteController@list');
    Route::post('/', 'RouteController@store');
    Route::get('{route_id}', 'RouteController@get');
    Route::put('{route_id}', 'RouteController@update');
    Route::delete('{route_id}', 'RouteController@delete');

    Route::group(["prefix" => "{route_id}/version"], function () {
        Route::get('list', 'RouteVersionController@list');
        Route::post('/', 'RouteVersionController@store');
        Route::get('{version_id}', 'RouteVersionController@get');
        Route::get('{version_id}/publish', 'RouteVersionController@publish');

        Route::group(["prefix" => "{version_id}/tasks"], function () {
            Route::get('list', 'RouteVersionTaskController@list');
            Route::post('/', 'RouteVersionTaskController@store');
            Route::get('{task_id}', 'RouteVersionTaskController@get');
            Route::put('{task_id}', 'RouteVersionTaskController@update');
            Route::delete('{task_id}', 'RouteVersionTaskController@delete');
        });
    });

    Route::group(["prefix" => "{route_id}/gallery"], function () {
        Route::group(["prefix" => "category"], function () {
            Route::get('/list', 'RouteGalleryCategoryController@list');
            Route::post('/', 'RouteGalleryCategoryController@store');
            Route::get('{category_id}', 'RouteGalleryCategoryController@get');
        });

        Route::post('addFile', 'RouteGalleryController@addFile');
        Route::get('{gallery_id}', 'RouteGalleryController@get');
        Route::delete('{gallery_id}', 'RouteGalleryController@delete');
    });

    Route::group(["prefix" => "{route_id}/download"], function () {
        Route::get('list', 'RouteDownloadController@list');
        Route::get('{download_id}', 'RouteDownloadController@get');
        Route::post('/', 'RouteDownloadController@store');
        Route::delete('{download_id}', 'RouteDownloadController@delete');
    });

    Route::group(["prefix" => "{route_id}/session"], function () {
        Route::get('list', 'RouteSessionController@list');
        Route::get('{session_id}', 'RouteSessionController@get');
        Route::post('', 'RouteSessionController@store');
        Route::delete('{session_id}', 'RouteSessionController@delete');
    });
});
