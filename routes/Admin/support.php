<?php

use Illuminate\Support\Facades\Route;

Route::group(["prefix" => "support", "namespace" => "Support"], function () {

    Route::group(["prefix" => "category"], function () {
        Route::get('list', 'SupportCategoryController@list');
        Route::post('/', 'SupportCategoryController@store');
        Route::get('{category_id}', 'SupportCategoryController@get');
        Route::put('{category_id}', 'SupportCategoryController@update');
        Route::delete('{category_id}', 'SupportCategoryController@delete');
    });

    Route::group(["prefix" => "sector"], function () {
        Route::get('list', 'SupportSectorController@list');
        Route::post('/', 'SupportSectorController@store');
        Route::get('{sector_id}', 'SupportSectorController@get');
        Route::put('{sector_id}', 'SupportSectorController@update');
        Route::delete('{sector_id}', 'SupportSectorController@delete');
    });

    Route::group(["prefix" => "priority"], function () {
        Route::get('list', 'SupportTicketPriorityController@list');
        Route::post('/', 'SupportTicketPriorityController@store');
        Route::get('{priority_id}', 'SupportTicketPriorityController@get');
        Route::put('{priority_id}', 'SupportTicketPriorityController@update');
        Route::delete('{priority_id}', 'SupportTicketPriorityController@delete');
    });

    Route::group(["prefix" => "source"], function () {
        Route::get('list', 'SupportTicketSourceController@list');
        Route::post('/', 'SupportTicketSourceController@store');
        Route::get('{source_id}', 'SupportTicketSourceController@get');
        Route::put('{source_id}', 'SupportTicketSourceController@update');
        Route::delete('{source_id}', 'SupportTicketSourceController@delete');
    });

    Route::group(["prefix" => "status"], function () {
        Route::get('list', 'SupportTicketStatusController@list');
        Route::post('/', 'SupportTicketStatusController@store');
        Route::get('{status_id}', 'SupportTicketStatusController@get');
        Route::put('{status_id}', 'SupportTicketStatusController@update');
        Route::delete('{status_id}', 'SupportTicketStatusController@delete');
    });

    Route::group(["prefix" => "ticket"], function () {
        Route::get('list', 'SupportTicketController@list');
        Route::post('/', 'SupportTicketController@store');
        Route::get('{ticket_id}', 'SupportTicketController@get');
        Route::put('{ticket_id}', 'SupportTicketController@update');
        Route::get('{ticket_id}/close', 'SupportTicketController@close');
        Route::get('{ticket_id}/redeem', 'SupportTicketController@redeem');
        Route::delete('{ticket_id}', 'SupportTicketController@delete');

        Route::group(["prefix" => "{ticket_id}/chat"], function () {
            Route::get('/', 'SupportTicketConvController@list');
            Route::post('/', 'SupportTicketConvController@store');
            Route::delete('{conv_id}', 'SupportTicketConvController@delete');
        });
    });

});
