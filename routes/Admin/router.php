<?php

use Illuminate\Support\Facades\Route;

Route::group(["prefix" => "admin", "namespace" => "Api\Admin", "middleware" => ["admin"]], function () {
    include('blog.php');
    include('account.php');
    include('assets.php');
    include('route.php');
    include('tutoriel.php');
    include('settings.php');
    include('support.php');
    include('wiki.php');
});
