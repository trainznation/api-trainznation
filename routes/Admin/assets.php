<?php

use Illuminate\Support\Facades\Route;

Route::group(["prefix" => "asset", "namespace" => "Asset"], function () {
    Route::group(["prefix" => "category"], function () {
        Route::get('list', 'AssetCategoryController@list');
        Route::post('/', 'AssetCategoryController@store');
        Route::get('{id}', 'AssetCategoryController@get');
        Route::put('{id}', 'AssetCategoryController@update');
        Route::delete('{id}', 'AssetCategoryController@destroy');
    });

    Route::get('/', 'AssetController@all');
    Route::post('list', 'AssetController@list');
    Route::post('/', 'AssetController@store');
    Route::get('/{asset_id}', 'AssetController@get');
    Route::put('/{asset_id}', 'AssetController@update');
    Route::delete('/{asset_id}', 'AssetController@destroy');
    Route::get('/{asset_id}/publish', 'AssetController@publish');
    Route::get('/{asset_id}/dispublish', 'AssetController@dispublish');
    Route::post('{asset_id}/addSketchfab', 'AssetController@addSketchfab');
    Route::post('{asset_id}/addPrice', 'AssetController@addPrice');

    Route::group(["prefix" => "{asset_id}/kuid"], function () {
        Route::post('/', 'AssetKuidController@store');
        Route::delete('{asset_kuid_id}', 'AssetKuidController@destroy');
    });

    Route::group(["prefix" => "{asset_id}/tags"], function () {
        Route::post('/', 'AssetTagsController@store');
        Route::delete('{tag}', 'AssetTagsController@delete');
    });
});
