<?php

use Illuminate\Support\Facades\Route;

Route::group(["prefix" => "wiki", "namespace" => "Wiki"], function () {
    Route::group(["prefix" => "category"], function () {
        Route::get('list', 'WikiCategoryController@list');
        Route::post('search', 'WikiCategoryController@search');
        Route::post('/', 'WikiCategoryController@store');
        Route::delete('{category_id}', 'WikiCategoryController@delete');
    });

    Route::group(["prefix" => "sector"], function () {
        Route::get('list', 'WikiSectorController@list');
        Route::post('search', 'WikiSectorController@search');
        Route::post('/', 'WikiSectorController@store');
        Route::delete('{sector_id}', 'WikiSectorController@delete');
    });

    Route::get('list', 'WikiController@list');
    Route::post('search', 'WikiController@search');
    Route::post('/', 'WikiController@store');
    Route::get('{wiki_id}', 'WikiController@get');
    Route::put('{wiki_id}', 'WikiController@update');
    Route::delete('{wiki_id}', 'WikiController@delete');

    Route::get('{wiki_id}/publish', 'WikiController@publish');
    Route::get('{wiki_id}/unpublish', 'WikiController@unpublish');
});
