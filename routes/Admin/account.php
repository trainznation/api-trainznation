<?php

use Illuminate\Support\Facades\Route;

Route::group(["prefix" => "user", "namespace" => "User"], function () {
    Route::get('list', "UserController@list");
    Route::get('{user_id}', "UserController@get");
    Route::post('store', "UserController@store");

    Route::group(["prefix" => "{user_id}/notifications"], function () {
        Route::get('list', "UserNotificationsController@list");
        Route::get('unread', "UserNotificationsController@unread");
        Route::post('/', "UserNotificationsController@store");
        Route::get('{notification_id}/read', "UserNotificationsController@read");
        Route::delete('{notification_id}', "UserNotificationsController@delete");
        Route::delete('/', "UserNotificationsController@destroy");
    });
});
