<?php

use Illuminate\Support\Facades\Route;

Route::group(["prefix" => "blog", "namespace" => "Blog"], function () {
    Route::group(["prefix" => "category"], function () {
        Route::get('list', 'CategoryController@list');
        Route::post('/', 'CategoryController@store');
        Route::delete('{category_id}', 'CategoryController@delete');
    });

    Route::get('list', 'BlogController@list');
    Route::post('/', 'BlogController@store');
    Route::get('{blog_id}', 'BlogController@get');
    Route::put('{blog_id}', 'BlogController@update');
    Route::delete('{blog_id}', 'BlogController@delete');

    Route::get('{blog_id}/publish', 'BlogController@publish');
    Route::get('{blog_id}/dispublish', 'BlogController@dispublish');

    Route::group(["prefix" => "{blog_id}/comment"], function () {
        Route::get('{comment_id}/approve', 'CommentController@approve');
        Route::get('{comment_id}/disapprove', 'CommentController@disapprove');
    });
});
