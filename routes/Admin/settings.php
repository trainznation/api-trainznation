<?php

use Illuminate\Support\Facades\Route;

Route::group(["prefix" => "settings", "namespace" => "Settings"], function () {
    Route::group(["prefix" => "announcement"], function () {
        Route::get('list', 'AnnouncementController@list');
        Route::post('/', 'AnnouncementController@store');
        Route::delete('{announcement_id}', 'AnnouncementController@delete');
    });

    Route::group(["prefix" => "slideshow"], function () {
        Route::get('list', 'SlideshowController@list');
        Route::post('/', 'SlideshowController@store');
        Route::delete('{slide_id}', 'SlideshowController@delete');
    });
});
