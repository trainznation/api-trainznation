<?php

use Illuminate\Support\Facades\Route;

Route::group(["prefix" => "tutoriel", "namespace" => "Tutoriel"], function () {
    Route::group(["prefix" => "category"], function () {
        Route::get('list', 'TutorielCategoryController@list');
        Route::post('/', 'TutorielCategoryController@store');
        Route::get('{category_id}', 'TutorielCategoryController@get');
        Route::delete('{category_id}', 'TutorielCategoryController@delete');
    });

    Route::get('list', 'TutorielController@list');
    Route::post('/', 'TutorielController@store');
    Route::get('{tutoriel_id}', 'TutorielController@get');
    Route::put('{tutoriel_id}', 'TutorielController@update');
});
