<?php
use Illuminate\Support\Facades\Route;

Route::group(["prefix" => "checkout", "namespace" => "Api\Front\Checkout"], function () {
    Route::post('create', 'CheckoutController@store');
    Route::post('{order_id}/payment', 'CheckoutController@payment');
    Route::post('{order_id}/product', 'CheckoutController@product');

    Route::get('{order_id}', 'CheckoutController@get');
});
