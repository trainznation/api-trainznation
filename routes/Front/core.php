<?php
use Illuminate\Support\Facades\Route;

Route::group(["prefix" => "core", "namespace" => "Api\Front\Core"], function () {
    Route::get('announcement/list', 'CoreController@listAnnouncement');
    Route::get('slideshow/list', 'CoreController@listSlideshow');
});
