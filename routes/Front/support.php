<?php
use Illuminate\Support\Facades\Route;

Route::group(["prefix" => "support", "namespace" => "Api\Front\Support"], function () {
    Route::get('category/list', 'SupportTicketController@categoryList');
    Route::get('category/{category_id}/sector', 'SupportTicketController@sectorList');
    Route::group(["prefix" => "ticket"], function () {
        Route::get('/', 'SupportTicketController@list');
        Route::post('/', 'SupportTicketController@store');
    });
});
