<?php
use Illuminate\Support\Facades\Route;

Route::group(["prefix" => "route", "namespace" => "Api\Front\Route"], function () {
    Route::get('/list', 'RouteController@list');
    Route::get('{route_id}', 'RouteController@get');

    Route::get('{route_id}/gallery/category/{category_id}', 'RouteController@getGallery');
});
