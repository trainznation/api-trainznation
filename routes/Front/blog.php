<?php
use Illuminate\Support\Facades\Route;

Route::group(["prefix" => "blog", "namespace" => "Api\Front\Blog"], function () {
    Route::post('search', 'BlogController@search');
    Route::get('latest', 'BlogController@latest');
    Route::get('list', 'BlogController@list');
    Route::get('{blog_id}', 'BlogController@get');

    Route::post('{blog_id}/comment', 'BlogController@postComment');
    Route::delete('{blog_id}/comment/{comment_id}', 'BlogController@deleteComment');
});
