<?php
use Illuminate\Support\Facades\Route;

Route::group(["prefix" => "cart", "namespace" => "Api\Front\Cart"], function () {
    Route::post('/', 'CartController@store');
    Route::put('{cart_id}/update', 'CartController@update');
    Route::delete('{cart_id}/destroy', 'CartController@destroy');
});
