<?php
use Illuminate\Support\Facades\Route;

Route::group(["prefix" => "download", "namespace" => "Api\Front\Download"], function () {
    Route::group(["prefix" => "category"], function () {
        Route::get('/list', 'DownloadCategoryController@list');
    });

    Route::post('/search', 'DownloadController@search');
    Route::get('/list', 'DownloadController@list');
    Route::get('/{download_id}', 'DownloadController@get');
});
