<?php
use Illuminate\Support\Facades\Route;

Route::group(["prefix" => "notification", "namespace" => "Api\Front\Notification"], function () {
    Route::get('/', 'NotificationController@list');
    Route::post('/', 'NotificationController@store');
});
