<?php

use Illuminate\Support\Facades\Route;

include ("blog.php");
include ("assets.php");
include ("tutoriel.php");
include ("core.php");
include ("route.php");
include ("download.php");
include ("cart.php");
include ("checkout.php");
include ("notification.php");
include ("support.php");
include ("wiki.php");
