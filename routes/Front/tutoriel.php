<?php
use Illuminate\Support\Facades\Route;

Route::group(["prefix" => "tutoriel", "namespace" => "Api\Front\Tutoriel"], function () {
    Route::post('search', 'TutorielController@search');
    Route::get('latest', 'TutorielController@latest');
    Route::get('list', 'TutorielController@list');
    Route::get('{tutoriel_id}', 'TutorielController@get');

    Route::group(["prefix" => "category"], function () {
        Route::get('list', 'TutorielCategoryController@list');
    });
});
