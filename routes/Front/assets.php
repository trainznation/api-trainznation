<?php
use Illuminate\Support\Facades\Route;

Route::group(["prefix" => "assets", "namespace" => "Api\Front\Assets"], function () {
    Route::get('latest', 'AssetsController@latest');
});
