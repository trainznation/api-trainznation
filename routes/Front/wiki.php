<?php
use Illuminate\Support\Facades\Route;

Route::group(["prefix" => "wiki", "namespace" => "Api\Front\Wiki"], function () {
    Route::post('search', 'WikiController@search');
    Route::get('list', 'WikiController@list');
    Route::get('{article_id}', 'WikiController@get');

    Route::group(["prefix" => "category"], function () {
        Route::get('list', 'WikiCategoryController@list');
    });

    Route::group(["prefix" => "sector"], function () {
        Route::get('{category_id}', 'WikiSectorController@listByCategory');
        Route::get('{sector_id}/lists', 'WikiSectorController@get');
    });
});
