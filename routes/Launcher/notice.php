<?php

use Illuminate\Support\Facades\Route;

Route::group(["prefix" => "notice", "namespace" => "Notice"], function () {
    Route::post('/', 'NoticeController@noticing');
});
