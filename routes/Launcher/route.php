<?php

use Illuminate\Support\Facades\Route;

Route::group(["prefix" => "route", "namespace" => "Route"], function () {
    Route::get('list', 'RouteController@list');
    Route::get('allSession', 'RouteController@allSession');
    Route::get('{route_id}', 'RouteController@get');
});
