<?php

use Illuminate\Support\Facades\Route;

Route::group(["prefix" => "asset", "namespace" => "Asset"], function () {
    Route::group(["prefix" => "category"], function (){
        Route::get('list', 'AssetCategoryController@list');
        Route::get('{category_id}', 'AssetCategoryController@get');
    });
    Route::get('list', 'AssetController@list');
    Route::get('{asset_id}', 'AssetController@get');
    Route::get('{asset_id}/addCounter', 'AssetController@addCounter');
});