<?php

use Illuminate\Support\Facades\Route;

Route::group(["prefix" => "launcher", "namespace" => "Api\Launcher"], function () {
    include('blog.php');
    include('notice.php');
    include('asset.php');
    include('route.php');
});
