<?php

use Illuminate\Support\Facades\Route;

Route::group(["prefix" => "blog", "namespace" => "Blog"], function () {
    Route::post('list', 'BlogController@list');
    Route::get('{id}', 'BlogController@show');
});
