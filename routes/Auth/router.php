<?php

use Illuminate\Support\Facades\Route;

Route::prefix('auth')->namespace('Auth')->group(function () {
    Route::post('register', 'AuthController@register');
    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout')->middleware('api');
    Route::post('refresh', 'AuthController@refresh')->middleware('api');
    Route::post('/me', 'AuthController@me')->middleware('api');
    Route::get('/notif', 'AuthController@readNotif')->middleware('api');

    Route::post('/search', 'AuthController@searchUser');

    Route::put('editing', 'AuthController@editing');
    Route::delete('delete', 'AuthController@delete');
    Route::put('changePassword', "AuthController@changePassword");

    Route::group(["prefix" => "notification"], function () {
        Route::get('{notif_id}/read', 'NotificationController@read');
        Route::get('readAll', 'NotificationController@readAll');
    });

    Route::group(["prefix" => "order"], function () {
        Route::get('/', 'OrderController@list');
        Route::get('{order_id}', 'OrderController@get');
    });

    Route::group(["prefix" => "download"], function () {
        Route::get('/', 'DownloadController@list');
    });

    Route::get('/socialite/{provider}', 'AuthController@socialite');
    Route::get('/socialite/{provider}/auth', 'AuthController@socialize');

    Route::put('/premium/activate', 'PremiumController@activate');
    Route::put('/premium/desactivate', 'PremiumController@activate');

    Route::get('/account', 'MeController@account');
});
