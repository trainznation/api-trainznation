<?php

use App\Model\AssetCategory;
use App\Model\TutorielCategory;
use App\User;
use Illuminate\Database\Seeder;

class ProductionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AssetCategory::insert(["name" => "Locomotive Diesel"]);
        AssetCategory::insert(["name" => "Locomotive Electrique"]);
        AssetCategory::insert(["name" => "Locomotive Vapeur"]);
        AssetCategory::insert(["name" => "TGV"]);
        AssetCategory::insert(["name" => "Autorail / Automotrice Diesel"]);
        AssetCategory::insert(["name" => "Autorail / Automotrice Electrique"]);
        AssetCategory::insert(["name" => "RIO/RIB"]);
        AssetCategory::insert(["name" => "Tramway"]);
        AssetCategory::insert(["name" => "Voiture Voyageur"]);
        AssetCategory::insert(["name" => "Wagon marchandise"]);
        AssetCategory::insert(["name" => "Faune / Flore"]);
        AssetCategory::insert(["name" => "Décors"]);
        AssetCategory::insert(["name" => "Batiments"]);
        AssetCategory::insert(["name" => "Gares & Stations"]);
        AssetCategory::insert(["name" => "Industries"]);
        AssetCategory::insert(["name" => "Marchandises"]);
        AssetCategory::insert(["name" => "Objets de gare"]);
        AssetCategory::insert(["name" => "Objets de voie"]);
        AssetCategory::insert(["name" => "Ponts"]);
        AssetCategory::insert(["name" => "Tunnels"]);
        AssetCategory::insert(["name" => "Voies"]);
        AssetCategory::insert(["name" => "Autres"]);

        TutorielCategory::insert(["name" => "Concepteur", "slug" => "concepteur"]);
        TutorielCategory::insert(["name" => "Conducteur", "slug" => "conducteur"]);
        TutorielCategory::insert(["name" => "Options", "slug" => "options"]);
        TutorielCategory::insert(["name" => "Modélisation", "slug" => "modelisation"]);
        TutorielCategory::insert(["name" => "Configuration", "slug" => "configuration"]);
        TutorielCategory::insert(["name" => "Script", "slug" => "script"]);
        TutorielCategory::insert(["name" => "Terminologie", "slug" => "terminologie"]);
        TutorielCategory::insert(["name" => "Validation", "slug" => "validation"]);

        $this->call(SupportInitSeeder::class);
    }
}
