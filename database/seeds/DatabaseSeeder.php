<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $user = factory(User::class)->create([
            "name" => "Trainznation",
            "email" => "trainznation@gmail.com",
            "password" => Hash::make("1992_Maxime"),
            "admin" => true
        ]);
    }
}
