<?php

use App\Model\SupportCategory;
use App\Model\SupportSector;
use App\Model\SupportTicketPriority;
use App\Model\SupportTicketSource;
use App\Model\SupportTicketStatus;
use Illuminate\Database\Seeder;

class SupportInitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SupportCategory::insert(["name" => "Route"]);
        SupportCategory::insert(["name" => "Téléchargement"]);
        SupportCategory::insert(["name" => "Tutoriel"]);
        SupportCategory::insert(["name" => "Mon Compte"]);
        SupportCategory::insert(["name" => "Application"]);

        SupportSector::insert(["name" => "Pays de la Loire", "support_category_id" => 1]);
        SupportSector::insert(["name" => "Problème d'installation", "support_category_id" => 2]);
        SupportSector::insert(["name" => "Problème de maillage", "support_category_id" => 2]);
        SupportSector::insert(["name" => "J'ai acheter un objet", "support_category_id" => 2]);
        SupportSector::insert(["name" => "Vous avez une question", "support_category_id" => 3]);
        SupportSector::insert(["name" => "Idée de tutoriel", "support_category_id" => 3]);
        SupportSector::insert(["name" => "J'ai un problème avec mon compte", "support_category_id" => 4]);
        SupportSector::insert(["name" => "Facturation/abonnement", "support_category_id" => 4]);
        SupportSector::insert(["name" => "Problème Technique", "support_category_id" => 4]);
        SupportSector::insert(["name" => "Configurator", "support_category_id" => 5]);
        SupportSector::insert(["name" => "Launcher", "support_category_id" => 5]);

        SupportTicketPriority::insert(["designation" => "Basse", "color" => "success"]);
        SupportTicketPriority::insert(["designation" => "Moyenne", "color" => "warning"]);
        SupportTicketPriority::insert(["designation" => "Haute", "color" => "info"]);
        SupportTicketPriority::insert(["designation" => "Critique", "color" => "danger"]);

        SupportTicketSource::insert(["designation" => "Site web"]);
        SupportTicketSource::insert(["designation" => "Apps Configurator"]);
        SupportTicketSource::insert(["designation" => "Apps Launcher"]);
        SupportTicketSource::insert(["designation" => "Trainz"]);

        SupportTicketStatus::insert(["designation" => "Ouvert", "color" => "success", "icon" => "fas fa-unlock"]);
        SupportTicketStatus::insert(["designation" => "En attente de support", "color" => "warning", "icon" => "fas fa-transfer"]);
        SupportTicketStatus::insert(["designation" => "En attente de l'utilisateur", "color" => "warning", "icon" => "fas fa-transfer"]);
        SupportTicketStatus::insert(["designation" => "Cloturer", "color" => "danger", "icon" => "fas fa-lock"]);
    }
}
