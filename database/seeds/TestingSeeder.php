<?php

use App\Model\Asset;
use App\Model\AssetCategory;
use App\Model\AssetKuid;
use App\Model\Blog;
use App\Model\BlogCategory;
use App\Model\Route;
use App\Model\RouteDownload;
use App\Model\RouteRoadmap;
use App\Model\RouteRoadmapVersion;
use App\Model\RouteRoadmapVersionCategory;
use App\Model\RouteRoadmapVersionCategoryCard;
use App\Model\RouteRoadmapVersionCategoryCardTask;
use App\Model\RouteSession;
use App\Model\RouteVersion;
use App\Model\RouteVersionTask;
use App\Model\SupportCategory;
use App\Model\SupportSector;
use App\Model\SupportTicket;
use App\Model\SupportTicketPriority;
use App\Model\SupportTicketSource;
use App\Model\SupportTicketStatus;
use App\Model\Tutoriel;
use App\Model\TutorielCategory;
use App\Model\UserAccount;
use App\Model\UserSocial;
use App\Model\WikiArticle;
use App\Model\WikiCategory;
use App\Model\WikiSector;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class TestingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = new Faker\Generator();
        $user = factory(User::class)->create([
            "name" => "Trainznation",
            "email" => "trainznation@gmail.com",
            "password" => bcrypt('password'),
            "admin" => true
        ]);
        factory(User::class, 10)->create();
        $users = User::all();
        foreach ($users as $user) {
            factory(UserAccount::class)->create(["user_id" => $user->id]);
            factory(UserSocial::class)->create(["user_id" => $user->id]);
        }
        factory(BlogCategory::class, 5)->create();
        for($i = 1; $i <= 5; $i++) {
            factory(Blog::class, 5)->create([
                "blog_category_id" => $i
            ]);
        }

        $blogs = Blog::where('published', 1)->get();

        foreach ($blogs as $blog) {
            for($i = 0; $i < rand(1,15); $i++) {
                $usered = User::find(rand(1,10));
                $blog->commentAsUser($usered, 'Lorem...');
            }
        }

        AssetCategory::insert(["name" => "Locomotive Diesel"]);
        AssetCategory::insert(["name" => "Locomotive Electrique"]);
        AssetCategory::insert(["name" => "Locomotive Vapeur"]);
        AssetCategory::insert(["name" => "TGV"]);
        AssetCategory::insert(["name" => "Autorail / Automotrice Diesel"]);
        AssetCategory::insert(["name" => "Autorail / Automotrice Electrique"]);
        AssetCategory::insert(["name" => "RIO/RIB"]);
        AssetCategory::insert(["name" => "Tramway"]);
        AssetCategory::insert(["name" => "Voiture Voyageur"]);
        AssetCategory::insert(["name" => "Wagon marchandise"]);
        AssetCategory::insert(["name" => "Faune / Flore"]);
        AssetCategory::insert(["name" => "Décors"]);
        AssetCategory::insert(["name" => "Batiments"]);
        AssetCategory::insert(["name" => "Gares & Stations"]);
        AssetCategory::insert(["name" => "Industries"]);
        AssetCategory::insert(["name" => "Marchandises"]);
        AssetCategory::insert(["name" => "Objets de gare"]);
        AssetCategory::insert(["name" => "Objets de voie"]);
        AssetCategory::insert(["name" => "Ponts"]);
        AssetCategory::insert(["name" => "Tunnels"]);
        AssetCategory::insert(["name" => "Voies"]);
        AssetCategory::insert(["name" => "Autres"]);

        $categories = AssetCategory::all();
        foreach ($categories as $category) {
            factory(Asset::class, rand(1,15))->create([
                "asset_category_id" => $category->id
            ]);
        }

        $assets = Asset::all();
        foreach ($assets as $asset) {
            factory(AssetKuid::class, rand(1,5))->create([
                "published_at" => ($asset->published_at) ? $asset->published_at->addDays(rand(0,10)) : now(),
                "asset_id" => $asset->id
            ]);
        }

        // Route
        factory(Route::class, rand(1,5))->create();
        $routes = Route::all();

        foreach ($routes as $route) {
            factory(RouteVersion::class, rand(1,2))->create([
                "route_id" => $route->id
            ]);

            $versions = RouteVersion::query()->where('route_id', $route->id)->get();
            foreach ($versions as $version) {
                factory(RouteVersionTask::class, rand(1,10))->create([
                    "route_version_id" => $version->id,
                    "route_id" => $route->id
                ]);
            }

            factory(RouteDownload::class, rand(1,5))->create([
                "route_id" => $route->id
            ]);

            factory(RouteSession::class, rand(1,6))->create([
                "route_id" => $route->id
            ]);
        }


        // Tutoriel

        TutorielCategory::insert(["name" => "Concepteur", "slug" => "concepteur"]);
        TutorielCategory::insert(["name" => "Conducteur", "slug" => "conducteur"]);
        TutorielCategory::insert(["name" => "Options", "slug" => "options"]);
        TutorielCategory::insert(["name" => "Modélisation", "slug" => "modelisation"]);
        TutorielCategory::insert(["name" => "Configuration", "slug" => "configuration"]);
        TutorielCategory::insert(["name" => "Script", "slug" => "script"]);
        TutorielCategory::insert(["name" => "Terminologie", "slug" => "terminologie"]);
        TutorielCategory::insert(["name" => "Validation", "slug" => "validation"]);

        $categories = TutorielCategory::all();
        foreach ($categories as $category) {
            factory(Tutoriel::class, rand (1,10))->create([
                "tutoriel_category_id" => $category->id
            ]);
        }

        // Support
        factory(SupportCategory::class, rand(2,5))->create();
        $categories = SupportCategory::all();

        foreach ($categories as $category) {
            factory(SupportSector::class, rand(2,10))->create([
                "support_category_id" => $category->id
            ]);
        }

        factory(SupportTicketPriority::class, rand(2,6))->create();
        factory(SupportTicketSource::class, rand(2,6))->create();
        factory(SupportTicketStatus::class, rand(2,5))->create();

        $sectors = SupportSector::all();
        $priorities = SupportTicketPriority::all();
        $status = SupportTicketStatus::all();
        $sources = SupportTicketSource::all();

        for ($i=0; $i < rand(1,25); $i++) {
            factory(SupportTicket::class)->create([
                "support_category_id" => rand(1, count($categories)-1),
                "support_sector_id" => rand(1, count($sectors)-1),
                "support_ticket_priority_id" => rand(1, count($priorities)-1),
                "support_ticket_status_id" => rand(1, count($status)-1),
                "support_ticket_source_id" => rand(1, count($sources)-1),
                "requester_id" => $users->random()->id,
                "assign_to" => $user->id,
            ]);
        }

        // Wiki

        $categories = factory(WikiCategory::class, rand(1,10))->create([]);

        foreach ($categories as $category) {
            $sectors = factory(WikiSector::class, rand(1,10))->create([
                "wiki_category_id" => $category->id
            ]);

            foreach ($sectors as $sector) {
                factory(WikiArticle::class, rand(1,10))->create([
                    "wiki_category_id" => $category->id,
                    "wiki_sector_id" => $sector->id
                ]);
            }
        }
    }
}
