<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserSocialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_socials', function (Blueprint $table) {
            $table->id();
            $table->string('discord_id')->nullable();
            $table->string('discord_channel')->nullable();
            $table->string('discord_pseudo')->nullable();
            $table->string('google_pseudo')->nullable();
            $table->string('microsoft_pseudo')->nullable();
            $table->string('twitch_pseudo')->nullable();
            $table->string('youtube_pseudo')->nullable();
            $table->string('trainz_pseudo')->nullable();
            $table->string('twitter_pseudo')->nullable();
            $table->string('facebook_pseudo')->nullable();

            $table->foreignId('user_id')->constrained()->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_socials');
    }
}
