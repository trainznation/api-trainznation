<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRouteDownloadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('route_downloads', function (Blueprint $table) {
            $table->id();
            // TODO : Voir avec table roadmap
            $table->string('version');
            $table->string('build');
            $table->uuid('uuid');
            $table->longText('note')->nullable();
            $table->boolean('published')->default(true);
            $table->boolean('alpha')->default(false);
            $table->boolean('beta')->default(false);
            $table->boolean('release')->default(false);
            $table->timestamp('published_at')->nullable();
            $table->timestamps();

            $table->foreignId('route_id')->constrained()->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('route_downloads');
    }
}
