<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRouteVersionTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('route_version_tasks', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('description');
            $table->string('lieu')->nullable();
            $table->integer('etat')->default(0)->comment("0: Non Commencer |1: En cours... |2: Terminer");
            $table->integer('priority')->default(0)->comment("Priorité de la tache | Ex: 0 -> Bas, 1 -> Moyen, etc... (0 à 3)");
            $table->timestamp('started_at')->default(now());
            $table->timestamp('finished_at')->nullable();

            $table->foreignId('route_version_id')->constrained()->onDelete('cascade');
            $table->foreignId('route_id')->constrained()->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('route_version_tasks');
    }
}
