<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBugTackersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bug_tackers', function (Blueprint $table) {
            $table->id();
            $table->ipAddress('ip');
            $table->string('channel')->comment("Site, Launcher");
            $table->string('categorie')->comment("Accept, Decease, Suggest, Bugs");
            $table->string('sector')->comment("launcher, téléchargement, etc...");
            $table->longText('message');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bug_tackers');
    }
}
