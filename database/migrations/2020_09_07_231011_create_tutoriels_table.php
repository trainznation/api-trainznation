<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTutorielsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tutoriels', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('slug');
            $table->text('short_content');
            $table->longText('content');
            $table->boolean('published')->default(false);
            $table->boolean('source')->default(false);
            $table->boolean('premium')->default(false);
            $table->boolean('social')->default(false);
            $table->string('time')->nullable();
            $table->timestamp('published_at')->nullable();
            $table->timestamps();

            $table->foreignId('tutoriel_category_id')->constrained()->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tutoriels');
    }
}
