<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRouteVersionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('route_versions', function (Blueprint $table) {
            $table->id();
            $table->integer('version');
            $table->integer('build');
            $table->timestamp('published_at')->nullable();
            $table->integer('registered_task')->default(0);
            $table->integer('progress_task')->default(0);
            $table->integer('finish_task')->default(0);
            $table->integer('total_task')->default(0);
            $table->integer('percent_task')->default(0);
            $table->string('map_link')->nullable();
            $table->string('distance')->nullable();
            $table->string('station_start')->nullable();
            $table->string('station_end')->nullable();

            $table->foreignId('route_id')->constrained()->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('route_versions');
    }
}
