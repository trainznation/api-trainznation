<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSupportTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('support_tickets', function (Blueprint $table) {
            $table->id();
            $table->string('subject');

            $table->foreignId('support_category_id')->constrained()->onDelete('cascade');
            $table->foreignId('support_sector_id')->constrained()->onDelete('cascade');
            $table->foreignId('support_ticket_priority_id')->constrained()->onDelete('cascade');
            $table->foreignId('support_ticket_status_id')->constrained()->onDelete('cascade');
            $table->foreignId('support_ticket_source_id')->constrained()->onDelete('cascade');
            $table->foreignId('requester_id')->constrained('users')->onDelete('cascade');
            $table->foreignId('assign_to')->constrained('users')->onDelete('cascade');
            $table->integer('close')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('support_tickets');
    }
}
