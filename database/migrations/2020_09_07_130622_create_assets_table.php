<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assets', function (Blueprint $table) {
            $table->id();
            $table->string('designation');
            $table->text('short_description');
            $table->longText('description')->nullable();
            $table->boolean('social')->default(false);
            $table->boolean('published')->default(false);
            $table->boolean('meshes')->default(false);
            $table->boolean('pricing')->default(false);
            $table->decimal('price')->nullable();
            $table->timestamp('published_at')->nullable();
            $table->uuid('uuid');
            $table->integer('count_download')->default(0);
            $table->timestamps();

            $table->foreignId('asset_category_id')->constrained()->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assets');
    }
}
