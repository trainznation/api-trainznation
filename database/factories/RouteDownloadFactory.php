<?php

/** @var Factory $factory */

use App\Model\RouteDownload;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(RouteDownload::class, function (Faker $faker) {
    $alpha = rand(0,1);
    $beta = ($alpha == 0) ? rand(0,1) : 0;
    $release = ($alpha == 0 && $beta == 0) ? 1 : 0;
    return [
        "version" => 1,
        "build" => rand(100,9999),
        "uuid" => $faker->uuid,
        "note" => $faker->sentence(rand(10,50)),
        "published" => $faker->boolean,
        "alpha" => $alpha,
        "beta" => $beta,
        "release" => $release,
        "published_at" => $faker->dateTime,
        "route_id" => 1
    ];
});
