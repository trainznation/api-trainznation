<?php

/** @var Factory $factory */

use App\Model\WikiSector;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(WikiSector::class, function (Faker $faker) {
    return [
        "name" => $faker->sentence(rand(2,6))
    ];
});
