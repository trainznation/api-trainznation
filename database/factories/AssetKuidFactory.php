<?php

/** @var Factory $factory */

use App\Model\AssetKuid;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(AssetKuid::class, function (Faker $faker) {
    return [
        "kuid" => "kuid:400722:".rand(1,99999),
        "published_at" => now(),
        "asset_id" => 1
    ];
});
