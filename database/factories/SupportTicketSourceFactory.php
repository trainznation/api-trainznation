<?php

/** @var Factory $factory */

use App\Model\SupportTicketSource;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(SupportTicketSource::class, function (Faker $faker) {
    return [
        "designation" => $faker->word
    ];
});
