<?php

/** @var Factory $factory */

use App\Model;
use App\Model\Asset;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;
use Liior\Faker\Prices;

$factory->define(Asset::class, function (Faker $faker) {
    $faker->addProvider(new Prices($faker));
    $published = $faker->boolean;
    $pricing = $faker->boolean;
    return [
        "designation" => $faker->sentence,
        "short_description" => $faker->sentence(60, true),
        "description" => $faker->sentence(300, true),
        "social" => $faker->boolean,
        "published" => $published,
        "meshes" => $faker->boolean,
        "pricing" => $pricing,
        "price" => ($pricing == true) ? $faker->price(0, 25, true, true) : null,
        "published_at" => ($published == true) ? now()->subDays(rand(1, 50)) : null,
        "uuid" => $faker->uuid,
        "asset_category_id" => rand(1, 21)
    ];
});
