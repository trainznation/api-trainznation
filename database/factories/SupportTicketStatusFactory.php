<?php

/** @var Factory $factory */

use App\Model\SupportTicketStatus;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(SupportTicketStatus::class, function (Faker $faker) {
    $color = ["danger", "warning", "success", "info", "primary", "secondary", "default"];
    $icon = ["fas fa-check", "fas fa-warning", "fas fa-info", "fas fa-times", "fas fa-reload"];
    $rand_color = array_rand($color);
    $rand_icon = array_rand($icon);

    return [
        "designation" => $faker->word,
        "color" => $color[$rand_color],
        "icon" => $icon[$rand_icon]
    ];
});
