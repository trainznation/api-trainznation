<?php

/** @var Factory $factory */

use App\Model\SupportTicketPriority;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(SupportTicketPriority::class, function (Faker $faker) {
    $color = ["danger", "info", "warning", "success", "primary", "secondary"];
    return [
        "designation" => $faker->word,
        "color" => array_rand($color)
    ];
});
