<?php

/** @var Factory $factory */

use App\Model\Route;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Route::class, function (Faker $faker) {
    return [
        "name" => $faker->sentence,
        "description" => $faker->text,
        "published" => rand(0,1),
        "version" => 1,
        "build" => "2500",
        "route_kuid" => 'kuid:400722:'.rand(100,999999),
        "dependance_kuid" => 'kuid:400722:'.rand(100,999999),
    ];
});
