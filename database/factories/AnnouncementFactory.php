<?php

/** @var Factory $factory */

use App\Model;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Model\Announcement::class, function (Faker $faker) {
    $created_at = now()->subDays(rand(0,10));
    return [
        "message" => $faker->sentence(),
        "type" => 'success' || 'danger' || 'info' || 'warning',
        "created_at" => $created_at,
        "expiring_at" => $created_at->addDays(rand(1,5))
    ];
});
