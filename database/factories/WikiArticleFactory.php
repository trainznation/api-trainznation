<?php

/** @var Factory $factory */

use App\Model\WikiArticle;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(WikiArticle::class, function (Faker $faker) {
    $published = rand(0,1);
    return [
        "title" => $faker->sentence(rand(3,10)),
        "content" => $faker->realText(),
        "published" => $published,
        "published_at" => ($published == 1) ? now()->subDays(rand(1,120)) : null
    ];
});
