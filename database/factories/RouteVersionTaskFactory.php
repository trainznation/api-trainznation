<?php

/** @var Factory $factory */

use App\Model\RouteVersionTask;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(RouteVersionTask::class, function (Faker $faker) {
    $etat = rand(0,2);
    $started_at = now()->subDays(rand(0,99));
    return [
        "name" => $faker->sentence,
        "description" => $faker->sentence(rand(5,15)),
        "lieu" => $faker->city,
        "etat" => $etat,
        "priority" => rand(0,3),
        "started_at" => $started_at,
        "finished_at" => ($etat == 2) ? $started_at->addDays(rand(1,10)) : null
    ];
});
