<?php

/** @var Factory $factory */

use App\Model\RouteSession;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(RouteSession::class, function (Faker $faker) {
    $published = rand(0,1);
    return [
        "name" => $faker->sentence,
        "short_content" => $faker->sentence(20),
        "content" => $faker->text,
        "published" => $published,
        "published_at" => ($published == 1) ? now()->subDays(rand(0,60)) : null,
        "route_id" => 1,
        "kuid" => 'kuid:400722:'.rand(100,999999),
        "uuid" => $faker->uuid
    ];
});
