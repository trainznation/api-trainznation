<?php

/** @var Factory $factory */

use App\Model\WikiCategory;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(WikiCategory::class, function (Faker $faker) {
    return [
        "name" => $faker->sentence(rand(2,5))
    ];
});
