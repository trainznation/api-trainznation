<?php

/** @var Factory $factory */

use App\Model\Comment;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Comment::class, function (Faker $faker) {
    return [
        "username" => $faker->userName,
        "email" => $faker->email,
        "content" => $faker->sentence,
        "ip" => $faker->ipv4
    ];
});
