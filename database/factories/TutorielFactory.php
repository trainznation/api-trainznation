<?php

/** @var Factory $factory */

use App\Model;
use App\Model\Tutoriel;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;

$factory->define(Tutoriel::class, function (Faker $faker) {
    $title = $faker->sentence(rand(3,10));
    $published = $faker->boolean;
    $source = $faker->boolean;
    $premium = $faker->boolean;
    $social = $faker->boolean;
    return [
        "title" => $title,
        "slug" => Str::slug($title),
        "short_content" => $faker->realText(rand(10,255)),
        "content" => $faker->realText(),
        "published" => $published,
        "source" => $source,
        "premium" => $premium,
        "social" => $social,
        "time" => Carbon::createFromTime(rand(0,3), rand(0,59), rand(0,59))->format('H:i:s'),
        "published_at" => ($published == true) ? now()->subDays(rand(0,90)) : null,
        "tutoriel_category_id" => 1
    ];
});
