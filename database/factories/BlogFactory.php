<?php

/** @var Factory $factory */

use App\Model\Blog;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Blog::class, function (Faker $faker) {
    $published = rand(0,1);
    return [
        "blog_category_id" => 1,
        "title" => $faker->sentence(6),
        "slug" => $faker->slug(6),
        "short_content" => $faker->text,
        "content" => $faker->realText(),
        "published" => $published,
        "published_at" => ($published == 1) ? now()->subDays(rand(0,60)) : null,
        "social" => $faker->boolean
    ];
});
