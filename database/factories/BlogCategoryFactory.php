<?php

/** @var Factory $factory */

use App\Model\BlogCategory;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(BlogCategory::class, function (Faker $faker) {
    return [
        "name" => $faker->sentence(1)
    ];
});
