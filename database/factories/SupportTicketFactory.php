<?php

/** @var Factory $factory */

use App\Model\SupportTicket;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(SupportTicket::class, function (Faker $faker) {
    return [
        "subject" => $faker->sentence
    ];
});
