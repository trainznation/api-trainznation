<?php

/** @var Factory $factory */

use App\Model;
use App\Model\TutorielCategory;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(TutorielCategory::class, function (Faker $faker) {
    return [
        "name" => $faker->sentence,
        "slug" => $faker->slug
    ];
});
