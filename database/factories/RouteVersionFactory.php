<?php

/** @var Factory $factory */

use App\Model\RouteVersion;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(RouteVersion::class, function (Faker $faker) {
    $published = rand(0,1);
    return [
        "version" => 1,
        "build" => rand(1000,99999),
        "published_at" => ($published == 1) ? now()->subDays(rand(0,125)) : null,
        "map_link" => $faker->url,
        "distance" => number_format(rand(10,99), 2, ',', ' '),
        "station_start" => "Gare de départ",
        "station_end" => "Gare d'arrivée",
    ];
});
