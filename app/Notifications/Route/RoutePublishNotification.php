<?php

namespace App\Notifications\Route;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Log;
use NotificationChannels\FacebookPoster\FacebookPosterChannel;
use NotificationChannels\FacebookPoster\FacebookPosterPost;
use NotificationChannels\Twitter\Exceptions\CouldNotSendNotification;
use NotificationChannels\Twitter\TwitterChannel;
use NotificationChannels\Twitter\TwitterStatusUpdate;

class RoutePublishNotification extends Notification
{
    use Queueable;

    public $route;
    /**
     * @var string
     */
    public $text;

    /**
     * Create a new notification instance.
     *
     * @param $route
     */
    public function __construct($route)
    {
        //
        $this->route = $route;
        $this->text = $route->name." à été publier !";
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        if($this->blog->social == 1) {
            return [FacebookPosterChannel::class, TwitterChannel::class, 'database'];
        } else {
            return ['database'];
        }
    }

    public function toFacebookPoster($notifiable)
    {
        return (new FacebookPosterPost($this->text))
            ->withImage($this->route->image);
    }

    public function toTwitter($notifiable)
    {
        try {
            return new TwitterStatusUpdate($this->text);
        } catch (CouldNotSendNotification $e) {
            Log::error($e->getMessage());
            return $e->getMessage();
        }
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            "icon" => "fas fa-road",
            "stat" => "success",
            "title" => $this->text,
            "image" => $this->route->image
        ];
    }
}
