<?php

namespace App\Notifications\Route;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class RoutePublishDownloadNotification extends Notification
{
    use Queueable;

    private $download;
    /**
     * @var string
     */
    private $text;

    /**
     * Create a new notification instance.
     *
     * @param $download
     */
    public function __construct($download)
    {
        //
        $this->download = $download;
        $this->text = "Route $download->route->name: Un nouveau téléchargement est disponible !";
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            "icon" => "fas fa-download",
            "stat" => "success",
            "title" => $this->text,
            "image" => null
        ];
    }
}
