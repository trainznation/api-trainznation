<?php

namespace App\Notifications\Route;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class RoutePublishVersionNotification extends Notification
{
    use Queueable;

    private $version;
    private $route;
    /**
     * @var string
     */
    private $text;

    /**
     * Create a new notification instance.
     *
     * @param $version
     * @param $route
     */
    public function __construct($version, $route)
    {
        //
        $this->version = $version;
        $this->text = "Route $route->name: La version V.$version->version:$version->build à été publier";
        $this->route = $route;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }


    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            "icon" => "fas fa-code-branch",
            "stat" => "success",
            "title" => $this->text,
            "image" => $this->route->image
        ];
    }
}
