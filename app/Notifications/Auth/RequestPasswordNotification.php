<?php

namespace App\Notifications\Auth;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class RequestPasswordNotification extends Notification
{
    use Queueable;

    public $user;
    public $token;

    /**
     * Create a new notification instance.
     *
     * @param $user
     * @param $token
     */
    public function __construct($user, $token)
    {
        //
        $this->user = $user;
        $this->token = $token;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject("[Trainznation] - Requète de réinitialisation du mot de passe")
            ->from('auth-guard@trainznation.tk')
            ->greeting('Bonjour '.$this->user->name)
            ->line("Une demande de réinitialisation sur le site <strong>".env('APP_NAME')."</strong> à été émis.")
            ->line("Pour réinitialiser votre mot de passe, veuillez cliquer sur le bouton suivant.")
            ->action('Reinitialiser mon mot de passe', env('APP_FRONT_URI').'/auth/password/check')
            ->line("Si vous n'êtes pas à l'origine de cette demande, veuillez nous contacter dans les plus bref délai, car votre compte est peut être compromis.")
            ->level('info')
            ->salutation('Cordialement,');
    }

}
