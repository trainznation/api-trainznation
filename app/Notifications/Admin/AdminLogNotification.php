<?php

namespace App\Notifications\Admin;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class AdminLogNotification extends Notification
{
    use Queueable;

    public $icon;
    public $stat;
    public $message;
    /**
     * @var bool
     */
    private $mailable;
    /**
     * @var array
     */
    private $mail_object;

    /**
     * Create a new notification instance.
     *
     * @param $icon
     * @param $stat
     * @param $message
     * @param bool $mailable
     * @param array $mail_object
     */
    public function __construct($icon, $stat, $message, $mailable = false, $mail_object = [])
    {
        //
        $this->icon = $icon;
        $this->stat = $stat;
        $this->message = $message;
        $this->mailable = $mailable;
        $this->mail_object = $mail_object;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        if($this->mailable == true) {
            return ['mail', 'database'];
        } else {
            return ['database'];
        }
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable)
    {
        if($this->stat == 'success') {
            return (new MailMessage)
                ->from('log@trainznation.tk', "Système d'information Trainznation (SIT)")
                ->subject($this->mail_object['subject'])
                ->line($this->mail_object['text'])
                ->success();
        } elseif($this->stat == 'danger') {
            return (new MailMessage)
                ->from('log@trainznation.tk', "Système d'information Trainznation (SIT)")
                ->subject($this->mail_object['subject'])
                ->line($this->mail_object['text'])
                ->line($this->mail_object['error'])
                ->error();
        } else {
            return (new MailMessage)
                ->from('log@trainznation.tk', "Système d'information Trainznation (SIT)")
                ->subject($this->mail_object['subject'])
                ->line($this->mail_object['text']);
        }
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            "icon" => $this->icon,
            "stat" => $this->stat,
            "title" => $this->message
        ];
    }
}
