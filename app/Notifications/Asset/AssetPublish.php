<?php

namespace App\Notifications\Asset;

use App\DownloadFile;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use NotificationChannels\Facebook\Components\Button;
use NotificationChannels\Facebook\Components\Card;
use NotificationChannels\Facebook\Exceptions\CouldNotCreateMessage;
use NotificationChannels\Facebook\FacebookChannel;
use NotificationChannels\Facebook\FacebookMessage;
use NotificationChannels\FacebookPoster\FacebookPosterChannel;
use NotificationChannels\FacebookPoster\FacebookPosterPost;
use NotificationChannels\Twitter\Exceptions\CouldNotSendNotification;
use NotificationChannels\Twitter\TwitterChannel;
use NotificationChannels\Twitter\TwitterStatusUpdate;

class AssetPublish extends Notification
{
    use Queueable;

    private $asset;
    /**
     * @var string
     */
    private $text;
    /**
     * @var DownloadFile
     */
    private $downloadFile;

    /**
     * Create a new notification instance.
     *
     * @param $asset
     */
    public function __construct($asset)
    {
        $this->asset = $asset;
        $this->text = $this->asset->designation . " est maintenant disponible";
        $this->downloadFile = new DownloadFile();
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        if($this->asset->social == 1) {
            return [FacebookPosterChannel::class, TwitterChannel::class];
        } else {
            return [];
        }
    }

    public function toFacebookPoster($notifiable)
    {
        return (new FacebookPosterPost($this->text))
            ->withImage($this->asset->image);
    }

    public function toTwitter($notifiable)
    {
        try {
            return new TwitterStatusUpdate($this->text);
        } catch (CouldNotSendNotification $e) {
            Log::error($e->getMessage());
            return $e->getMessage();
        }
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            "icon" => "fa fa-cubes",
            "stat" => "success",
            "title" => $this->text,
            "image" => $this->asset->image
        ];
    }
}
