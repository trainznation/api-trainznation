<?php

namespace App\Notifications\Blog;

use App\DownloadFile;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Log;
use NotificationChannels\FacebookPoster\FacebookPosterChannel;
use NotificationChannels\FacebookPoster\FacebookPosterPost;
use NotificationChannels\Twitter\Exceptions\CouldNotSendNotification;
use NotificationChannels\Twitter\TwitterChannel;
use NotificationChannels\Twitter\TwitterStatusUpdate;

class BlogPublish extends Notification
{
    use Queueable;

    private $blog;
    private $text;
    /**
     * @var DownloadFile
     */
    private $downloadFile;

    /**
     * Create a new notification instance.
     *
     * @param $blog
     */
    public function __construct($blog)
    {
        //
        $this->blog = $blog;
        $this->text = $this->blog->designation;
        $this->downloadFile = new DownloadFile();
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        if($this->blog->social == 1) {
            return [FacebookPosterChannel::class, TwitterChannel::class, 'database'];
        } else {
            return ['database'];
        }
    }

    public function toFacebookPoster($notifiable)
    {
        return (new FacebookPosterPost($this->text))
            ->withImage($this->blog->image);
    }

    public function toTwitter($notifiable)
    {
        try {
            return new TwitterStatusUpdate($this->text);
        } catch (CouldNotSendNotification $e) {
            Log::error($e->getMessage());
            return $e->getMessage();
        }
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            "icon" => "fas fa-newspaper",
            "stat" => "success",
            "title" => $this->text,
            "image" => $this->blog->image
        ];
    }
}
