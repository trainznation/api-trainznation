<?php
declare(strict_types=1);
namespace App;

use App\Model\Comment;
use App\Model\Order;
use App\Model\SupportTicket;
use App\Model\SupportTicketConv;
use App\Model\UserAccount;
use App\Model\UserDownload;
use App\Model\UserNotification;
use App\Model\UserSocial;
use Assada\Achievements\Achiever;
use BeyondCode\Comments\Contracts\Commentator;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject,Commentator
{
    use Notifiable;
    use Achiever;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'admin', 'api_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'api_token'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * @return HasMany
     */
    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    /**
     * @return HasOne
     */
    public function account()
    {
        return $this->hasOne(UserAccount::class);
    }

    /**
     * @return HasOne
     */
    public function social()
    {
        return $this->hasOne(UserSocial::class);
    }

    public function notifications()
    {
        return $this->hasMany(UserNotification::class);
    }

    public function orders() {
        return $this->hasMany(Order::class);
    }

    public function downloads()
    {
        return $this->hasMany(UserDownload::class);
    }

    public function ticket_requesters()
    {
        return $this->hasMany(SupportTicket::class);
    }

    public function ticket_assignes()
    {
        return $this->hasMany(SupportTicket::class);
    }

    public function conversations()
    {
        return $this->hasMany(SupportTicketConv::class);
    }

    public function generateToken()
    {
        $this->api_token = Str::random(80);
        $this->save();

        return $this->api_token;
    }

    public function getJWTIdentifier()
    {
        // TODO: Implement getJWTIdentifier() method.
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        // TODO: Implement getJWTCustomClaims() method.
        return [];
    }

    public function setPasswordAttribute($value) {
        $this->attributes['password'] = Hash::make($value);
    }

    public function needsCommentApproval($model): bool
    {
        // TODO: Implement needsCommentApproval() method.
        return false;
    }
}
