<?php

namespace App;

use MadWeb\Initializer\Contracts\Runner;
use MadWeb\Initializer\Jobs\MakeCronTask;
use MadWeb\Initializer\Jobs\MakeEchoServerConfig;
use MadWeb\Initializer\Jobs\Supervisor\MakeQueueSupervisorConfig;

class Install
{
    public function production(Runner $run)
    {
        $run->external('composer', 'install', '--no-dev', '--prefer-dist', '--optimize-autoloader')
            ->artisan('key:generate', ['--force' => true])
            ->artisan('migrate', ['--force' => true])
            ->artisan('db:seed', ['--class=ProductionSeeder'])
            ->artisan('storage:link')
    //            ->dispatch(new MakeCronTask)
            ->external('npm', 'install', '--production')
            ->external('npm', 'run', 'production')
            ->artisan('route:cache')
            ->artisan('config:cache')
            ->artisan('event:cache')
            ->dispatch(new MakeCronTask)
            ->dispatch(new MakeEchoServerConfig(config('broadcasting.server')))
            ->dispatch(new MakeQueueSupervisorConfig([
                'command' => 'php artisan horizon',
            ]));
    }

    public function local(Runner $run)
    {
        $run->external('composer', 'install')
            ->artisan('key:generate')
            ->artisan('migrate')
            ->artisan('db:seed', ["--class=TestingSeeder"])
            ->artisan('storage:link')
            ->external('npm', 'install')
            ->external('npm', 'run', 'development')
            ->dispatch(new MakeCronTask)
            ->dispatch(new MakeEchoServerConfig(config('broadcasting.server')))
            ->dispatch(new MakeQueueSupervisorConfig([
                'command' => 'php artisan horizon',
            ]));
    }
}
