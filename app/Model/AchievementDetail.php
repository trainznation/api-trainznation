<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AchievementDetail extends Model
{
    protected $guarded = [];
    protected $table = 'achievement_details';
}
