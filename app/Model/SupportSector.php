<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SupportSector extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function category()
    {
        return $this->belongsTo(SupportCategory::class, 'support_category_id');
    }

    public function tickets()
    {
        return $this->hasMany(SupportTicket::class);
    }
}
