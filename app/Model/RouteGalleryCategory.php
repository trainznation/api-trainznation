<?php
declare(strict_types=1);
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RouteGalleryCategory extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function route() {
        return $this->belongsTo(Route::class);
    }

    public function galleries() {
        return $this->hasMany(RouteGallery::class);
    }
}
