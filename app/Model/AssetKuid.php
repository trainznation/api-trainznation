<?php
declare(strict_types=1);
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class AssetKuid extends Model
{
    protected $guarded = [];
    public $timestamps = false;
    protected $dates = [
        "published_at"
    ];

    /**
     * @return BelongsTo
     */
    public function asset()
    {
        return $this->belongsTo(Asset::class);
    }
}
