<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;

class UserDownload extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    protected $dates = [
        "unlocked_at"
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function asset()
    {
        return $this->belongsTo(Asset::class);
    }
}
