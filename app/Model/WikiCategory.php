<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class WikiCategory extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function wiki_sectors()
    {
        return $this->hasMany(WikiSector::class);
    }

    public function wiki_articles()
    {
        return $this->hasMany(WikiArticle::class);
    }
}
