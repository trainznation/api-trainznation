<?php
declare(strict_types=1);
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RouteSession extends Model
{
    protected $guarded = [];
    protected $dates = [
        "created_at", "updated_at", "published_at"
    ];

    public function route()
    {
        return $this->belongsTo(Route::class);
    }

    public static function scopeIsPublished($query)
    {
        return $query->where('published', 1);
    }
}
