<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class WikiSector extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function wiki_category()
    {
        return $this->belongsTo(WikiCategory::class, 'wiki_category_id');
    }

    public function wiki_articles()
    {
        return $this->hasMany(WikiArticle::class);
    }
}
