<?php
declare(strict_types=1);

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Route extends Model
{
    protected $guarded = [];

    public function sessions()
    {
        return $this->hasMany(RouteSession::class);
    }

    public function gallerycategories()
    {
        return $this->hasMany(RouteGalleryCategory::class);
    }

    public function galleries()
    {
        return $this->hasMany(RouteGallery::class);
    }

    public function downloads()
    {
        return $this->hasMany(RouteDownload::class);
    }

    public function versions()
    {
        return $this->hasMany(RouteVersion::class);
    }

    public function tasks()
    {
        return $this->hasMany(RouteVersionTask::class);
    }

    public function scopeIsPublished($query)
    {
        return $query->where('published', 1);
    }
}
