<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class WikiArticle extends Model
{
    protected $guarded = [];

    public function getDates()
    {
        return ["created_at", "updated_at", "published_at"];
    }

    public function wiki_category()
    {
        return $this->belongsTo(WikiCategory::class, 'wiki_category_id');
    }

    public function wiki_sector()
    {
        return $this->belongsTo(WikiSector::class, 'wiki_sector_id');
    }
}
