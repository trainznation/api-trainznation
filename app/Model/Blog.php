<?php
declare(strict_types=1);

namespace App\Model;

use BeyondCode\Comments\Traits\HasComments;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Support\Str;

class Blog extends Model
{
    use HasComments;

    protected $guarded = [];
    protected $dates = [
        "created_at", "updated_at", "published_at"
    ];

    protected $casts = [
        "is_publish" => "boolean",
        "publish_social" => "boolean"
    ];

    /**
     * @return BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(BlogCategory::class, 'blog_category_id');
    }

    /**
     * Vérifie que l'article est publier
     *
     * @param $query
     * @return mixed
     */
    public function scopeIsPublished($query)
    {
        return $query->where('published', true);
    }

    public function setSlugAttribute($title)
    {
        $this->attributes['slug'] = Str::slug($title);
    }

    public function publish($query)
    {
        return $query->update([
            "published" => true,
            "published_at" => now()
        ]);
    }

    public function dispublish($query)
    {
        return $query->update([
            "published" => false,
            "published_at" => null
        ]);
    }

}
