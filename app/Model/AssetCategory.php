<?php
declare(strict_types=1);
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class AssetCategory extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    /**
     * @return HasMany
     */
    public function assets()
    {
        return $this->hasMany(Asset::class);
    }
}
