<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RouteVersionTask extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    protected $dates = [
        "started_at", "finished_at"
    ];

    public function version()
    {
        return $this->belongsTo(RouteVersion::class);
    }

    public function route()
    {
        return $this->belongsTo(Route::class);
    }
}
