<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;

class SupportTicket extends Model
{
    protected $guarded = [];

    public function category()
    {
        return $this->belongsTo(SupportCategory::class, 'support_category_id');
    }

    public function sector()
    {
        return $this->belongsTo(SupportSector::class, 'support_sector_id');
    }

    public function priority()
    {
        return $this->belongsTo(SupportTicketPriority::class, 'support_ticket_priority_id');
    }

    public function status()
    {
        return $this->belongsTo(SupportTicketStatus::class, 'support_ticket_status_id');
    }

    public function source()
    {
        return $this->belongsTo(SupportTicketSource::class, 'support_ticket_source_id');
    }

    public function requester()
    {
        return $this->belongsTo(User::class, 'requester_id');
    }

    public function assign()
    {
        return $this->belongsTo(User::class, 'assign_to');
    }

    public function conversations()
    {
        return $this->hasMany(SupportTicketConv::class);
    }
}
