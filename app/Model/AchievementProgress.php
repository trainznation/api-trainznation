<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AchievementProgress extends Model
{
    protected $guarded = [];
    protected $table = 'achievement_progress';
    protected $dates = [
        "created_at", "updated_at", "unlocked_at"
    ];


}
