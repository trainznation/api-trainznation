<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RouteVersion extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    protected $dates = [
        "published_at"
    ];

    public function route()
    {
        return $this->belongsTo(Route::class);
    }

    public function tasks()
    {
        return $this->hasMany(RouteVersionTask::class);
    }
}
