<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

class Comment extends Model
{
    /**
     * @var string[]
     */
    protected static $commentable_for = ['Blog', 'Tutoriel'];
    protected $guarded = [];
    protected $hidden = ['email', 'ip'];
    protected $appends = ['email_md5', 'ip_md5'];

    /**
     * @return string
     */
    public function getEmailMD5Attribute()
    {
        return md5($this->attributes['email']);
    }

    /**
     * @return string
     */
    public function getIpMd5Attribute()
    {
        return md5($this->attributes['ip']);
    }

    /**
     * @param string $model
     * @param int $model_id
     * @return array
     */
    public static function allFor($model, $model_id)
    {
        $records = self::where(['commentable_id' => $model_id, "commentable_type" => $model])->orderBy('created_at', 'ASC')->get();
        $comments = [];
        $by_id = [];

        foreach ($records as $record) {
            if($record->reply) {
                $by_id[$record->reply]->attributes['replies'][] = $record;
            } else {
                $record->attributes['replies'] = [];
                $by_id[$record->id] = $record;
                $comments[] = $record;
            }
        }

        return array_reverse($comments);
    }

    /**
     * @param string $model
     * @param int $model_id
     * @return bool
     */
    public static function isCommentable($model, $model_id)
    {
        if(!in_array($model, self::$commentable_for)) {
            return false;
        } else {
            $model = "\\App\\Model\\$model";
            return $model::where(['id' => $model_id])->exists();
        }
    }
}
