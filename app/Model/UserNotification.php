<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;

class UserNotification extends Model
{
    protected $guarded = [];
    protected $dates = [
        "created_at", "updated_at", "read_at"
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
