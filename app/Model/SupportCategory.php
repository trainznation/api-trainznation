<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SupportCategory extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function sectors()
    {
        return $this->hasMany(SupportSector::class);
    }
}
