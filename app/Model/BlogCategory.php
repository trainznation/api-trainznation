<?php
declare(strict_types=1);
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema()
 *
 * Class BlogCategory
 * @package App\Model
 */

class BlogCategory extends Model
{
    /**
     * @OA\Property(type="integer", description="ID de la catégorie", default="1")
     * @var int
     */
    protected $id;
    /**
     * @OA\Property(type="string", description="Nom de la catégorie", default="Test d'une catégorie")
     * @var string
     */
    protected $name;


    protected $guarded = [];
    public $timestamps = false;

    /**
     * @return HasMany
     */
    public function blogs()
    {
        return $this->hasMany(Blog::class);
    }
}
