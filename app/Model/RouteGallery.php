<?php
declare(strict_types=1);
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RouteGallery extends Model
{
    protected $guarded = [];

    public function route() {
        return $this->belongsTo(Route::class);
    }

    public function category() {
        return $this->belongsTo(RouteGalleryCategory::class);
    }
}
