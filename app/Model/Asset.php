<?php
declare(strict_types=1);

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Str;
use Maher\Counters\Models\Counter;
use Maher\Counters\Traits\HasCounter;
use Spatie\Tags\HasTags;

class Asset extends Model
{
    use HasTags;
    use HasCounter;

    protected $guarded = [];

    protected $dates = [
        "created_at", "updated_at", "published_at"
    ];

    /**
     * @return BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(AssetCategory::class, 'asset_category_id');
    }

    /**
     * @return HasMany
     */
    public function kuids()
    {
        return $this->hasMany(AssetKuid::class);
    }

    public function orderproducts()
    {
        return $this->hasMany(OrderProduct::class);
    }

    public function downloads()
    {
        return $this->hasMany(UserDownload::class);
    }

    /**
     * Vérifie que l'objet est publier
     *
     * @param $query
     * @return mixed
     */
    public function scopeIsPublished($query)
    {
        return $query->where('published', true);
    }

    public function setUuidAttribute($value)
    {
        return $this->attributes['uuid'] = Str::uuid();
    }
}
