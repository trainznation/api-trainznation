<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SupportTicketStatus extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function tickets()
    {
        return $this->hasMany(SupportTicket::class);
    }
}
