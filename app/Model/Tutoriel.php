<?php
declare(strict_types=1);
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Tutoriel extends Model
{
    protected $guarded = [];
    protected $dates = [
        "created_at", "updated_at", "published_at"
    ];

    public function category()
    {
        return $this->belongsTo(TutorielCategory::class, 'tutoriel_category_id');
    }
}
