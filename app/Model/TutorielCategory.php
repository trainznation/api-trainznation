<?php
declare(strict_types=1);
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TutorielCategory extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function tutoriels()
    {
        return $this->hasMany(Tutoriel::class);
    }
}
