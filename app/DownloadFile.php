<?php


namespace App;


use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Support\Facades\Storage;

class DownloadFile
{
    public function downloadFile($uri)
    {
        $content = file_get_contents($uri);
        $name = substr($uri, strrpos($uri, '/') + 1);
        Storage::disk('public')->put('uploads/'.$name, $content);
        try {
            return Storage::disk('public')->get('uploads/' . $name);
        } catch (FileNotFoundException $e) {
            return $e->getMessage();
        }
    }
}
