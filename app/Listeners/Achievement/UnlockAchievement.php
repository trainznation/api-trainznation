<?php

namespace App\Listeners\Achievement;

use App\Helpers\Notificator;
use Assada\Achievements\Event\Unlocked;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Storage;

class UnlockAchievement
{
    /**
     * @var Notificator
     */
    private $notificator;
    /**
     * @var Filesystem
     */
    private $storage;
    /**
     * @var string
     */
    private $file;

    /**
     * Create the event listener.
     *
     * @param Notificator $notificator
     */
    public function __construct(Notificator $notificator)
    {
        //
        $this->notificator = $notificator;
        if (env('APP_ENV') == 'local') {
            $this->storage = Storage::disk('testing');
            $this->file = 'https://download.trainznation.io/';
        } else {
            $this->storage = Storage::disk('sftp');
            $this->file = 'https://download.trainznation.tk/';
        }
    }

    /**
     * Handle the event.
     *
     * @param Unlocked $event
     * @return void
     */
    public function handle(Unlocked $event)
    {
        $this->notificator->publish(
            $event->progress->details->description,
            "success",
            null,
            'fas fa-trophy',
            $this->file.'v3/achievement/'.$event->progress->details->name.'.png',
            true,
            $event->progress->achiever_id
        );
    }
}
