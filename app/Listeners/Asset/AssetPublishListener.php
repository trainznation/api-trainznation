<?php

namespace App\Listeners\Asset;

use App\Events\Asset\AssetPublishEvent;
use App\Model\Asset;
use App\Notifications\Asset\AssetPublish;
use App\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class AssetPublishListener
{
    /**
     * @var Asset
     */
    private $asset;

    /**
     * Create the event listener.
     *
     * @param Asset $asset
     */
    public function __construct(Asset $asset)
    {
        $this->asset = $asset;
    }

    /**
     * Handle the event.
     *
     * @param AssetPublishEvent $event
     * @return void
     */
    public function handle(AssetPublishEvent $event)
    {
        $users = User::all();
        foreach ($users as $user) {
            $user->notify(new AssetPublish($event->asset));
        }
    }
}
