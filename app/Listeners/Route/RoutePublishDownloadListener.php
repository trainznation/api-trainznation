<?php

namespace App\Listeners\Route;

use App\Model\RouteDownload;
use App\Notifications\Route\RoutePublishDownloadNotification;
use App\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class RoutePublishDownloadListener
{
    /**
     * @var RouteDownload
     */
    private $download;

    /**
     * Create the event listener.
     *
     * @param RouteDownload $download
     */
    public function __construct(RouteDownload $download)
    {
        //
        $this->download = $download;
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $users = User::all();
        foreach ($users as $user) {
            $user->notify(new RoutePublishDownloadNotification($event->download));
        }
    }
}
