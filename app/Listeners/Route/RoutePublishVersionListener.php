<?php

namespace App\Listeners\Route;

use App\Model\Route;
use App\Model\RouteRoadmapVersion;
use App\Notifications\Route\RoutePublishVersionNotification;
use App\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class RoutePublishVersionListener
{
    /**
     * @var RouteRoadmapVersion
     */
    private $version;
    /**
     * @var Route
     */
    private $route;

    /**
     * Create the event listener.
     *
     * @param RouteRoadmapVersion $version
     * @param Route $route
     */
    public function __construct(RouteRoadmapVersion $version, Route $route)
    {
        //
        $this->version = $version;
        $this->route = $route;
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $users = User::all();

        foreach ($users as $user) {
            $user->notify(new RoutePublishVersionNotification($event->version, $event->route));
        }
    }
}
