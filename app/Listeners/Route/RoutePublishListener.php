<?php

namespace App\Listeners\Route;

use App\Model\Route;
use App\Notifications\Route\RoutePublishNotification;
use App\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class RoutePublishListener
{
    /**
     * @var Route
     */
    private $route;

    /**
     * Create the event listener.
     *
     * @param Route $route
     */
    public function __construct(Route $route)
    {
        //
        $this->route = $route;
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $users = User::all();

        foreach ($users as $user) {
            $user->notify(new RoutePublishNotification($event->route));
        }
    }
}
