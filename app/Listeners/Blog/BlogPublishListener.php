<?php

namespace App\Listeners\Blog;

use App\Events\Blog\BlogPublishEvent;
use App\Model\Blog;
use App\Notifications\Blog\BlogPublish;
use App\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class BlogPublishListener
{
    /**
     * @var Blog
     */
    private $blog;

    /**
     * Create the event listener.
     *
     * @param Blog $blog
     */
    public function __construct(Blog $blog)
    {
        //
        $this->blog = $blog;
    }

    /**
     * Handle the event.
     *
     * @param BlogPublishEvent $event
     * @return void
     */
    public function handle(BlogPublishEvent $event)
    {
        $users = User::all();
        foreach ($users as $user) {
            $user->notify(new BlogPublish($event->blog));
        }
    }
}
