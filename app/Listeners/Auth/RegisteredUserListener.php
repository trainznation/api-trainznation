<?php

namespace App\Listeners\Auth;

use App\Events\Auth\RegisteredUser as RegisteredUserAlias;
use App\Notifications\Admin\Auth\AdminRegisteredUser;
use App\Notifications\Auth\RegisteredUser;
use App\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class RegisteredUserListener
{
    /**
     * @var User
     */
    private $user;

    /**
     * Create the event listener.
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        //
        $this->user = $user;
    }

    /**
     * Handle the event.
     *
     * @param RegisteredUserAlias $event
     * @return void
     */
    public function handle(RegisteredUserAlias $event)
    {
        //dd($event->user->name);
        $admins = User::query()->where('admin', 1)->get();
        foreach ($admins as $admin) {
            $admin->notify(new AdminRegisteredUser($event->user));
        }
        $event->user->notify(new RegisteredUser($event->user));
    }
}
