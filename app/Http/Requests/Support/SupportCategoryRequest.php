<?php

namespace App\Http\Requests\Support;

use Illuminate\Foundation\Http\FormRequest;
use OpenApi\Annotations as OA;

/**
 * Class SupportCategoryRequest
 * @package App\Http\Requests\Support
 *
 * @OA\Schema(schema="SupportCategoryRequest", required={"name"})
 * {
 *      @OA\Property(property="name", type="string", description="Nom de la catégorie", default="Téléchargement")
 * }
 */

class SupportCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => "required|string"
        ];
    }
}
