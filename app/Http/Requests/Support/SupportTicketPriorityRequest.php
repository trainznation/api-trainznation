<?php

namespace App\Http\Requests\Support;

use Illuminate\Foundation\Http\FormRequest;
use OpenApi\Annotations as OA;

/**
 * Class SupportTicketPriorityRequest
 * @package App\Http\Requests\Support
 *
 * @OA\Schema(schema="SupportTicketPriorityRequest", required={"designation", "color"})
 * {
 *      @OA\Property(property="designation", type="string", description="Nom de la priorité", default="Basse")
 *      @OA\Property(property="color", type="string", description="Couleur de la priorité", default="info")
 * }
 */

class SupportTicketPriorityRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "designation" => "required|string",
            "color" => "required|string"
        ];
    }
}
