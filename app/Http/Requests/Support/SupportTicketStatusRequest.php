<?php

namespace App\Http\Requests\Support;

use Illuminate\Foundation\Http\FormRequest;
use OpenApi\Annotations as OA;

/**
 * Class SupportTicketStatusRequest
 * @package App\Http\Requests\Support
 *
 * @OA\Schema(schema="SupportTicketStatusRequest", required={"designation", "color", "icon"})
 * {
 *      @OA\Property(property="designation", type="string", description="Nom du status", default="En attente de l'utilisateur")
 *      @OA\Property(property="color", type="string", description="couleur du status", default="danger")
 *      @OA\Property(property="icon", type="string", description="icon du status", default="fas fa-clock-o")
 * }
 */

class SupportTicketStatusRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "designation" => "required|string",
            "color" => "required|string",
            "icon" => "required|string",
        ];
    }
}
