<?php

namespace App\Http\Requests\Support;

use Illuminate\Foundation\Http\FormRequest;
use OpenApi\Annotations as OA;

/**
 * Class SupportTicketSourceRequest
 * @package App\Http\Requests\Support
 *
 * @OA\Schema(schema="SupportTicketSourceRequest", required={"designation"})
 * {
 *      @OA\Property(property="designation", type="string", description="Nom de la source", default="Web")
 * }
 */

class SupportTicketSourceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "designation" => "required|string"
        ];
    }
}
