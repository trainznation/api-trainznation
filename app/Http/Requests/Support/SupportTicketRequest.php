<?php

namespace App\Http\Requests\Support;

use Illuminate\Foundation\Http\FormRequest;
use OpenApi\Annotations as OA;

/**
 * Class SupportTicketRequest
 * @package App\Http\Requests\Support
 *
 * @OA\Schema(schema="SupportTicketRequest", required={"subject", "support_category_id", "support_sector_id"})
 * {
 *      @OA\Property(property="designation", type="string", description="Nom de l'objet", default="Maison en toit de chaume")
 * }
 */

class SupportTicketRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "subject" => "required|string",
            "support_category_id" => "required",
            "support_sector_id" => "required",
            "support_ticket_priority_id" => "required",
            "support_ticket_status_id" => "required",
            "support_ticket_source_id" => "required",
            "requester_id" => "required",
            "assign_to" => "required",
        ];
    }
}
