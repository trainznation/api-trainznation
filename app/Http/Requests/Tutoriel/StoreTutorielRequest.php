<?php

namespace App\Http\Requests\Tutoriel;

use Illuminate\Foundation\Http\FormRequest;
use OpenApi\Annotations as OA;

/**
 * Class StoreTutorielRequest
 * @package App\Http\Requests\Tutoriel
 *
 * @OA\Schema(schema="AdminTutorielRequest", required={"title", "short_content", "content", "tutoriel_category_id"})
 * {
 *      @OA\Property(property="title", type="string", description="Titre du tutoriel", default="Titre")
 *      @OA\Property(property="slug", type="string", description="Slug du Titre du tutoriel", default="Titre")
 *      @OA\Property(property="short_content", type="string", description="Courte description du tutoriel", default="Lorem max 255")
 *      @OA\Property(property="content", type="string", description="Contenue du tutoriel", default="Lorem Ipsum...")
 *      @OA\Property(property="published", type="boolean", description="Tutoriel publier ?", default="false")
 *      @OA\Property(property="source", type="boolean", description="Tutoriel a-t-il une source ?", default="false")
 *      @OA\Property(property="premium", type="boolean", description="Tutoriel est-il disponible uniquement au premium ?", default="false")
 *      @OA\Property(property="social", type="boolean", description="Tutoriel est-il publier sur les réseaux ?", default="false")
 *      @OA\Property(property="time", type="string", description="Durée de la vidéo", default="null")
 *      @OA\Property(property="published_at", type="string", description="Si publier quel est la date, peut être supérieur à la date actuel", default="2020-06-20 00:00:00")
 *      @OA\Property(property="tutoriel_category_id", type="integer", description="Catégory affilier", default="1")
 * }
 */

class StoreTutorielRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "title" => "required|string|max:255",
            "short_content" => "required|string|max:255",
            "content" => "required|string",
            "tutoriel_category_id" => "required|integer"
        ];
    }
}
