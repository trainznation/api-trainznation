<?php

namespace App\Http\Requests\Tutoriel;

use Illuminate\Foundation\Http\FormRequest;
use OpenApi\Annotations as OA;

/**
 * Class StoreTutorielCategoryRequest
 * @package App\Http\Requests\Tutoriel
 *
 * @OA\Schema(schema="AdminTutorielCategoryRequest", required={"name"})
 * {
 *      @OA\Property(property="name", type="string", description="Nom de la catégorie", default="Modélisation")
 * }
 */

class StoreTutorielCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name"  => "required|string"
        ];
    }
}
