<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;
use OpenApi\Annotations as OA;

/**
 * Class StoreNotificationsRequest
 * @package App\Http\Requests\User
 *
 * @OA\Schema(schema="StoreNotificationRequest", required={"title", "status", "user_id"})
 * {
 *      @OA\Property(property="title", type="string", description="Titre"),
 *      @OA\Property(property="description", type="string", description="Description"),
 *      @OA\Property(property="icon", type="string", description="Description"),
 *      @OA\Property(property="image", type="string", description="Description"),
 *      @OA\Property(property="status", type="string", description="Description"),
 *      @OA\Property(property="read_at", type="string", description="Description"),
 *      @OA\Property(property="user_id", type="string", description="Description"),
 * }
 */

class StoreNotificationsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "title" => "required|string",
            "status" => "required|string"
        ];
    }
}
