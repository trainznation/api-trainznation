<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;
use OpenApi\Annotations as OA;

/**
 * Class StoreUserRequest
 * @package App\Http\Requests\User
 *
 * @OA\Schema(schema="PostUserRequest", required={"name", "email", "password"})
 * {
 *      @OA\Property(property="name", type="string", description="Nom de l'utilisateur", default="John Doe"),
 *      @OA\Property(property="email", type="string", format="email", description="Email de l'utilisateur", default="john@doe.fr"),
 *      @OA\Property(property="password", type="string", format="password", description="Mot de passe", default="password"),
 * }
 */

class StoreUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => "required|string|min:3",
            "email" => "required|email|unique:users",
            "password" => "required|min:5"
        ];
    }
}
