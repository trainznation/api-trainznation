<?php

namespace App\Http\Requests\Asset;

use Illuminate\Foundation\Http\FormRequest;
use OpenApi\Annotations as OA;

/**
 * Class StoreAssetTagRequest
 * @package App\Http\Requests\Asset
 *
 * @OA\Schema(schema="PostAssetTagRequest", type="object", required={"tags"})
 * {
 *
 *      @OA\Property(property="tags", type="string", description="Le ou les tags à ajouter", default="test,over,tgv")
 * }
 */
class StoreAssetTagRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "tags" => "required|string"
        ];
    }
}
