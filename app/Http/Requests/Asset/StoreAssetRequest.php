<?php

namespace App\Http\Requests\Asset;

use Illuminate\Foundation\Http\FormRequest;
use OpenApi\Annotations as OA;

/**
 * Class StoreAssetRequest
 * @package App\Http\Requests\Asset
 *
 * @OA\Schema(schema="PostAssetRequest", required={"designation", "short_description", "description", "asset_category_id"})
 * {
 *      @OA\Property(property="designation", type="string", description="Nom de l'objet", default="Maison en toit de chaume")
 *      @OA\Property(property="short_description", type="string", description="Courte description de l'objet", default="Lorem à 120 caractères")
 *      @OA\Property(property="description", type="string", description="Description de l'objet", default="Lorem à 3200 caractères")
 *      @OA\Property(property="asset_category_id", type="integer", description="Identifiant de la catégorie de l'objet", default="6")
 *      @OA\Property(property="social", type="boolean", description="Publication de l'objet sur les réseau sociaux", default="false")
 *      @OA\Property(property="published", type="boolean", description="Publication de l'objet", default="false")
 *      @OA\Property(property="meshes", type="boolean", description="Meshes 3D disponible", default="false")
 *      @OA\Property(property="pricing", type="boolean", description="L'objet est-il payant ?", default="false")
 *      @OA\Property(property="price", type="string", description="Montant")
 *      @OA\Property(property="published_at", type="string", description="Date de publication")
 * }
 */
class StoreAssetRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "designation" => "required|string|max:255",
            "short_description" => "required|string",
            "description" => "required|string",
            "asset_category_id" => "required|integer"
        ];
    }
}
