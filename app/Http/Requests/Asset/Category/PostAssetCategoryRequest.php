<?php

namespace App\Http\Requests\Asset\Category;

use Illuminate\Foundation\Http\FormRequest;
use OpenApi\Annotations as OA;

/**
 * Class PostAssetCategoryRequest
 * @package App\Http\Requests\Asset\Category
 *
 * @OA\Schema(schema="PostAssetCategoryRequest", required={"name"})
 * {
 *      @OA\Property(property="name", type="string", description="Nom de la catégorie", default="TGV")
 * }
 */

class PostAssetCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => "required|string|min:3"
        ];
    }
}
