<?php

namespace App\Http\Requests\Asset;

use Illuminate\Foundation\Http\FormRequest;
use OpenApi\Annotations as OA;

/**
 * Class StoreAssetKuidRequest
 * @package App\Http\Requests\Asset
 *
 * @OA\Schema(schema="PostAssetKuidRequest", required={"kuid"})
 * {
 *      @OA\Property(property="kuid", type="string", description="Kuid de l'objet", example="kuid:400722:5200")
 *      @OA\Property(property="published_at", type="string", description="Date de publication du kuid", example="2020-09-03T00:00:00Z")
 * }
 */

class StoreAssetKuidRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "kuid" => "required"
        ];
    }
}
