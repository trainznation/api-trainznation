<?php

namespace App\Http\Requests\Route\Download;

use Illuminate\Foundation\Http\FormRequest;
use OpenApi\Annotations as OA;

/**
 * Class StoreRouteSessionRequest
 * @package App\Http\Requests\Route\Download
 *
 * @OA\Schema(schema="AdminRouteSessionRequest", required={"name", "short_content"})
 * {
 *      @OA\Property(property="name", type="string", description="Nom de la session", example="TER 856410 - Nantes / Pornic"),
 *      @OA\Property(property="short_content", type="string", description="Petit descriptif de la session", example="Lorem sur environ 120 caractères"),
 *      @OA\Property(property="content", type="string", description="Descriptif de la session", example="Lorem sur plus de 255 caractères"),
 *      @OA\Property(property="published", type="boolean", description="Publication de la session", example="true"),
 *      @OA\Property(property="published_at", type="string", description="Date de publication de la session", example="2020-07-10 03:00:00"),
 *      @OA\Property(property="kuid", type="string", description="Kuid de la session", example="<kuid:400722:120>>"),
 * }
 */

class StoreRouteSessionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => "required",
            "short_content" => "required"
        ];
    }
}
