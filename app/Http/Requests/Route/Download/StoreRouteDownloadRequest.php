<?php

namespace App\Http\Requests\Route\Download;

use Illuminate\Foundation\Http\FormRequest;
use OpenApi\Annotations as OA;

/**
 * Class StoreRouteDownloadRequest
 * @package App\Http\Requests\Route\Download
 *
 * @OA\Schema(schema="AdminRouteDownloadRequest", required={"version", "build"})
 * {
 *      @OA\Property(property="version", type="string", description="Version du téléchargement", default="1"),
 *      @OA\Property(property="build", type="string", description="Build du téléchargement", default="5300"),
 *      @OA\Property(property="description", type="string", description="Note sur le téléchargement", default="Lorem Ipsum..."),
 *      @OA\Property(property="alpha", type="boolean", description="Le téléchargement est-il en alpha", default="true"),
 *      @OA\Property(property="beta", type="boolean", description="Le téléchargement est-il en beta", default="false"),
 *      @OA\Property(property="release", type="boolean", description="Le téléchargement est-il en release", default="false"),
 *      @OA\Property(property="published", type="boolean", description="Le téléchargement est-il publier", default="true"),
 * }
 */

class StoreRouteDownloadRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "version" => "required",
            "build" => "required",
        ];
    }
}
