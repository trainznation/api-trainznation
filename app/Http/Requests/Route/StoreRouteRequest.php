<?php

namespace App\Http\Requests\Route;

use Illuminate\Foundation\Http\FormRequest;
use OpenApi\Annotations as OA;

/**
 * Class StoreRouteRequest
 * @package App\Http\Requests\Route
 *
 * @OA\Schema(schema="AdminPostRouteRequest", required={"name", "description"})
 * {
 *      @OA\Property(property="name", type="string", description="Nom de la version", default="Nantes")
 *      @OA\Property(property="description", type="string", description="Description de la version", default="Nantes")
 * }
 */

class StoreRouteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => "required|string",
            "description" => "required|string"
        ];
    }
}
