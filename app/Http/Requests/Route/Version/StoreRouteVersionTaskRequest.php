<?php

namespace App\Http\Requests\Route\Version;

use Illuminate\Foundation\Http\FormRequest;
use OpenApi\Annotations as OA;

/**
 * Class StoreRouteVersionTaskRequest
 * @package App\Http\Requests\Route\Version
 *
 * @OA\Schema(schema="AdminPostRouteVersionTask", required={"name", "description"})
 * {
 *      @OA\Property(property="route_id", type="integer", description="ID de la route", default="1")
 *      @OA\Property(property="version_id", type="integer", description="ID de la version", default="1")
 *      @OA\Property(property="name", type="integer", description="Nom de la tache ou anomalie", default="Test d'une tache")
 *      @OA\Property(property="description", type="integer", description="Description de la tache ou correction apporté", default="Ok pour envoie")
 * }
 */

class StoreRouteVersionTaskRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => "required|string",
            "description" => "required|string"
        ];
    }
}
