<?php

namespace App\Http\Requests\Route\Version;

use Illuminate\Foundation\Http\FormRequest;
use OpenApi\Annotations as OA;

/**
 * Class StoreRouteVersion
 * @package App\Http\Requests\Route\Version
 *
 * @OA\Schema(schema="AdminPostRouteVersion", required={"name", "description"})
 * {
 *      @OA\Property(property="route_id", type="integer", description="ID de la route", default="1")
 *      @OA\Property(property="version", type="integer", description="version", default="1")
 *      @OA\Property(property="build", type="integer", description="Build", default="26330")
 * }
 */

class StoreRouteVersion extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "version" => "required|integer",
            "build" => "required|integer"
        ];
    }
}
