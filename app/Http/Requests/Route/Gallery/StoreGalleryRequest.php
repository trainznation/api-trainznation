<?php

namespace App\Http\Requests\Route\Gallery;

use Illuminate\Foundation\Http\FormRequest;
use OpenApi\Annotations as OA;

/**
 * Class StoreGalleryRequest
 * @package App\Http\Requests\Route\Gallery
 *
 * @OA\Schema(schema="AdminPostGalleryRequest")
 * {
 *      @OA\Property(property="route_id", type="integer", description="ID de la route", example="4")
 *      @OA\Property(property="route_gallery_category_id", type="integer", description="ID de la category de la gallerie", example="3")
 * }
 */

class StoreGalleryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
