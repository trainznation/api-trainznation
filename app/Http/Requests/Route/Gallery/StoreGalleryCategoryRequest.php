<?php

namespace App\Http\Requests\Route\Gallery;

use Illuminate\Foundation\Http\FormRequest;
use OpenApi\Annotations as OA;

/**
 * Class StoreGalleryCategoryRequest
 * @package App\Http\Requests\Route\Gallery
 *
 * @OA\Schema(schema="AdminPostGalleryCategoryRequest", required={"name"})
 * {
 *      @OA\Property(property="name", type="string", description="Nom de la catégorie", default="Nantes")
 * }
 */

class StoreGalleryCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => "required|string|max:255"
        ];
    }
}
