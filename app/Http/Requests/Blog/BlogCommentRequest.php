<?php

namespace App\Http\Requests\Blog;

use Illuminate\Foundation\Http\FormRequest;
use OpenApi\Annotations as OA;

/**
 * Class BlogCommentRequest
 * @package App\Http\Requests\Blog
 *
 * @OA\Schema(schema="BlogCommentRequest", required={"comment"})
 * {
 *      @OA\Property(property="comment", type="string", description="Commentaire", default="Lorem")
 *      @OA\Property(property="user_id", type="integer", description="ID de l'utilisateur postant")
 * }
 */
class BlogCommentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "comment" => "required"
        ];
    }
}
