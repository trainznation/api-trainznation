<?php

namespace App\Http\Requests\Blog;

use Illuminate\Foundation\Http\FormRequest;
use OpenApi\Annotations as OA;

/**
 * Class BlogRequest
 * @package App\Http\Requests\Blog
 *
 * @OA\Schema(schema="PostBlogRequest", required={"title", "short_content", "content", "blog_category_id"})
 * {
 * @OA\Property(property="id", type="integer", description="ID de la l'article", example="1")
 * @OA\Property(property="title", type="string", description="Titre de la l'article", example="Nouveauté lors du test")
 * @OA\Property(property="slug", type="string", description="Sluggify de la l'article", example="nouveauté-lors-du-test")
 * @OA\Property(property="short_content", type="string", description="Contenue succins de la l'article", example="contenue à 120 caractères")
 * @OA\Property(property="content", type="string", description="Contenue de la l'article", example="contenue à 4500 caractères")
 * @OA\Property(property="published", type="boolean", description="Publication de l'article", default="false")
 * @OA\Property(property="published_at", type="string", description="Date de publication de l'article", example="2020-09-23 15:40:00")
 * @OA\Property(property="social", type="boolean", description="Publication de l'article sur les reseaux sociaux", default="false")
 * @OA\Property(property="blog_category_id", type="integer", description="ID de la catégorie affilié", example="1")
 * }
 */

class BlogRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "blog_category_id" => "required|int",
            "title" => "required|string|max:255",
            "short_content" => "required|string|max:255",
            "content" => "required|string"
        ];
    }
}
