<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use OpenApi\Annotations as OA;

/**
 * Class SupportSectorRequest
 * @package App\Http\Requests
 *
 * @OA\Schema(schema="SupportSectorRequest", required={"name", "support_category_id"})
 * {
 *      @OA\Property(property="name", type="string", description="Nom du secteur", default="Problème Visuel")
 *      @OA\Property(property="support_category_id", type="integer", description="ID de la catégorie associé", default="1")
 * }
 */

class SupportSectorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => "required|string",
            "support_category_id" => "required"
        ];
    }
}
