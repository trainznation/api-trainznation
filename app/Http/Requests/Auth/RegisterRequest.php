<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class RegisterRequest
 * @package App\Http\Requests\Auth
 *
 * @OA\Schema(schema="RegisterRequest")
 * {
 *      @OA\Property(property="name", type="string", description="Nom de l'utilisateur", example="Syltheron")
 *      @OA\Property(property="email", type="string", format="email", description="Email de l'utilisateur", example="syltheron@gmail.com")
 *      @OA\Property(property="password", type="string", format="password", description="Mot de passe de l'utilisateur", example="1992_Maxime")
 * }
 */
class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => "required|string|max:255",
            "email" => "required|string|email|unique:users",
            "password" => "required|string|min:6"
        ];
    }
}
