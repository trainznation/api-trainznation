<?php

namespace App\Http\Requests\Settings;

use Illuminate\Foundation\Http\FormRequest;
use OpenApi\Annotations as OA;

/**
 * Class StoreAnnouncementRequest
 * @package App\Http\Requests\Settings
 *
 * @OA\Schema(schema="StoreAnnouncementRequest", required={"message", "type", "expiring_at"})
 * {
 *      @OA\Property(property="message", type="string", description="Message")
 *      @OA\Property(property="type", type="string", description="Type")
 *      @OA\Property(property="expiring_at", type="string", description="Date d'expiration")
 * }
 */

class StoreAnnouncementRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "message" => "required|string",
            "type" => "required|string",
            "expiring_at" => "required|date"
        ];
    }
}
