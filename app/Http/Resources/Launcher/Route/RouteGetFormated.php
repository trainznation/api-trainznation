<?php


namespace App\Http\Resources\Launcher\Route;


use App\Model\RouteRoadmapVersion;
use App\Model\RouteRoadmapVersionCategory;
use App\Model\RouteRoadmapVersionCategoryCardTask;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class RouteGetFormated
{
    /**
     * @var RouteRoadmapVersion
     */
    private $roadmapVersion;
    /**
     * @var RouteRoadmapVersionCategory
     */
    private $versionCategory;
    /**
     * @var RouteRoadmapVersionCategoryCardTask
     */
    private $cardTasks;
    private $named_route;

    public function __construct()
    {
        $roadmapVersion = new RouteRoadmapVersion();
        $versionCategory = new RouteRoadmapVersionCategory();
        $this->roadmapVersion = $roadmapVersion;
        $this->versionCategory = $versionCategory;
        $cardTasks = new RouteRoadmapVersionCategoryCardTask();
        $this->cardTasks = $cardTasks;
        $this->named_route;
    }

    public function toArray($data)
    {
        $this->named_route = $data->name;
        if (env('APP_ENV') == 'local') {
            $storage = Storage::disk('testing');
            $file = 'https://download.trainznation.io/';
        } else {
            $storage = Storage::disk('sftp');
            $file = 'https://download.trainznation.tk/';
        }

        return [
            "id" => $data->id,
            "name" => $data->name,
            "description" => $data->description,
            "published" => $data->published,
            "updated_at" => $data->updated_at->format('Y-m-d H:i:s'),
            "roadmaps" => [
                "mapping" => [
                    "id" => $data->roadmap->id,
                    "created_at" => $data->roadmap->created_at->format('Y-m-d H:i:s'),
                    "updated_at" => $data->roadmap->created_at->format('Y-m-d H:i:s'),
                ],
                "versions" => $this->roadMapVersion($data->roadmap)
            ],
            "sessions" => $this->sessions($data->sessions),
            "image" => ($storage->exists('v3/route/' . $data->id . '/route.png') == true) ? $file . 'v3/route/' . $data->id . '/route.png' : 'https://via.placeholder.com/350x150',
            "version" => $data->version,
            "build" => $data->build,
            "route_kuid" => $data->route_kuid,
            "dependance_kuid" => $data->dependance_kuid
        ];
    }

    private function sessions($sessions)
    {
        $array = [];

        if (env('APP_ENV') == 'local') {
            $storage = Storage::disk('testing');
            $file = 'https://download.trainznation.io/';
        } else {
            $storage = Storage::disk('sftp');
            $file = 'https://download.trainznation.tk/';
        }

        foreach ($sessions as $session) {
            if ($session->published == 1) {
                $array[] = [
                    "id" => $session->id,
                    "name" => $session->name,
                    "short_content" => $session->short_content,
                    "published" => $session->published,
                    "published_at" => [
                        "normalize" => $session->published_at->format('d/m/Y à H:i'),
                        "human" => $session->published_at->diffForHumans()
                    ],
                    "updated_at" => $session->updated_at->format('Y-m-d H:i:s'),
                    "image" => ($storage->exists('v3/route/' . $session->route->id . '/session/' . $session->id . '/session.png') == true) ? $file . 'v3/route/' . $session->route->id . '/session/' . $session->id . '/session.png' : 'https://via.placeholder.com/200x200',
                    "kuid" => $session->kuid
                ];
            }
        }

        return $array;
    }

    private function roadMapVersion($roadmap)
    {
        $versions = $this->roadmapVersion->newQuery()->where('roadmap_id', $roadmap->id)->get()->load('categories');
        $array = [];

        foreach ($versions as $version) {
            $published = ($version->published_at != null) ? ["normalize" => $version->published_at->format('Y-m-d H:i:s'), "human" => $version->published_at->diffForHumans(), "serialize" => $version->published_at->isoFormat('LLLL')] : null;
            $array[] = [
                "id" => $version->id,
                "version" => $version->version,
                "build" => $version->build,
                "published_at" => $published,
                "categories" => $this->versionCategories($version->categories, $this->named_route)
            ];
        }

        return $array;
    }

    private function versionCategories($categories, $named_route)
    {
        $array = [];

        foreach ($categories as $category) {
            $array[] = [
                "id" => $category->id,
                "name" => $category->name,
                "slug" => Str::slug($named_route.'-'.$category->name),
                "roadmap_version_id" => $category->id,
                "cards" => $this->categoryCards($category->cards),
                "total_task" => $category->total_task,
                "waiting_task" => $category->waiting_task
            ];
        }

        return $array;
    }

    private function categoryCards($cards)
    {
        $array = [];

        foreach ($cards as $card) {
            $array[] = [
                "id" => $card->id,
                "name" => $card->name,
                "status" => $card->status,
                "description" => $card->description,
                "version_category_id" => $card->version_category_id,
                "tasks" => [
                    "count" => $this->formatedCardTask($card->id),
                    "tasks" => $card->tasks
                ]
            ];
        }

        return $array;
    }

    public function formatedCardTask($card_id)
    {
        $all = $this->cardTasks->newQuery()->where('version_category_card_id', $card_id)->count();
        if($all != 0) {
            $waiting = $this->cardTasks->newQuery()->where('version_category_card_id', $card_id)->where('status', "En attente")->count();
            $progress = $this->cardTasks->newQuery()->where('version_category_card_id', $card_id)->where('status', "En Cours")->count();
            $finish = $this->cardTasks->newQuery()->where('version_category_card_id', $card_id)->where('status', "Terminer")->count();

            return [
                "total_task" => $all,
                "waiting" => $waiting,
                "progress" => $progress,
                "finish" => $finish,
                "percent_dev" => (100*$finish)/$all
            ];
        } else {
            return [];
        }


    }

}
