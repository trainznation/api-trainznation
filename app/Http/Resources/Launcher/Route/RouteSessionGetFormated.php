<?php


namespace App\Http\Resources\Launcher\Route;


use App\Model\RouteRoadmapVersion;
use App\Model\RouteRoadmapVersionCategory;
use App\Model\RouteRoadmapVersionCategoryCardTask;
use Illuminate\Support\Facades\Storage;

class RouteSessionGetFormated
{
    public function toArray($data)
    {
        $array = [];

        foreach ($data as $item) {
            $array[] = [
                "id" => $item->id,
                "name" => $item->name,
                "route_id" => $item->route_id,
                "published_at" => $item->published_at->format('yy-m-d h:i:s'),
                "updated_at" => $item->updated_at->format('yy-m-d h:i:s'),
                "kuid" => $item->kuid
            ];
        }

        return $array;
    }
}