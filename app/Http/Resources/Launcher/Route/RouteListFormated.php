<?php


namespace App\Http\Resources\Launcher\Route;


use Illuminate\Support\Facades\Storage;

class RouteListFormated
{
    public function toArray($data)
    {
        $array = [];
        if(env('APP_ENV') == 'local') {
            $storage = Storage::disk('testing');
            $file = 'https://download.trainznation.io/';
        }else {
            $storage = Storage::disk('sftp');
            $file = 'https://download.trainznation.tk/';
        }
        //dd($storage->allDirectories());
        foreach ($data as $item) {
            $array[] = [
                "id" => $item->id,
                "name" => $item->name,
                "description" => $item->description,
                "published" => $item->published,
                "version" => $item->version,
                "build" => $item->build,
                "image" => ($storage->exists('v3/route/'.$item->id.'/route.png') == true) ? $file.'v3/route/'.$item->id.'/route.png' : 'https://via.placeholder.com/350x150',
                "updated_at" => $item->updated_at->format('Y-m-d H:i:s'),
                "route_kuid" => $item->route_kuid,
                "dependance_kuid" => $item->dependance_kuid
            ];
        }

        return $array;
    }
}
