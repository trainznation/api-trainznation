<?php


namespace App\Http\Resources\Launcher\Blog;


use Illuminate\Support\Facades\Storage;

class ListArticleFormated
{
    public function toArray($datas)
    {
        $array = [];
        if(env('APP_ENV') == 'local') {
            $storage = Storage::disk('testing');
            $file = 'https://download.trainznation.io/';
        }else {
            $storage = Storage::disk('sftp');
            $file = 'https://download.trainznation.tk/';
        }
        foreach ($datas as $data) {
            $array[] = [
                "id" => $data->id,
                "title" => $data->title,
                "slug" => $data->slug,
                "short_content" => $data->short_content,
                "content" => $data->content,
                "published" => $data->published == 1 ? 'true' : 'false',
                "published_at" => $data->published_at->diffForHumans(),
                "social" => $data->social == 1 ? 'true' : 'false',
                "category" => $data->category,
                "created_at" => $data->created_at,
                "updated_at" => $data->updated_at,
                "image" => ($storage->exists('v3/blog/'.$data->id.'.png') == true) ? $file.'v3/blog/'.$data->id.'.png' : 'https://via.placeholder.com/350x150',
            ];
        }

        return $array;
    }
}
