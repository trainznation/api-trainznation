<?php


namespace App\Http\Resources\Launcher\Asset;


use Illuminate\Support\Facades\Storage;

class GetAssetFormated
{
    public function toArray($data)
    {
        if (env('APP_ENV') == 'local') {
            $storage = Storage::disk('testing');
            $file = 'https://download.trainznation.io/';
        } else {
            $storage = Storage::disk('sftp');
            $file = 'https://download.trainznation.tk/';
        }

        return [
            "id" => $data->id,
            "designation" => $data->designation,
            "short_description" => $data->short_description,
            "description" => $data->description,
            "social" => $data->social,
            "published" => $data->published,
            "meshes" => $data->meshes,
            "pricing" => $data->pricing,
            "published_at" => [
                "normalize" => $data->published_at->format('d/m/Y à H:i'),
                "human" => $data->published_at->diffForHumans()
            ],
            "uuid" => $data->uuid,
            "count_download" => $data->count_download,
            "category" => $data->category,
            "kuids" => $data->kuids,
            "last_kuid" => $data->kuids->last()->kuid,
            "image" => ($storage->exists('v3/assets/'.$data->id.'.png') == true) ? $file.'v3/assets/'.$data->id.'.png' : 'https://via.placeholder.com/768x460',
            "sketchfab_uid" => $data->sketchfab_uid
        ];
    }
}
