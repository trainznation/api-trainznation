<?php


namespace App\Http\Resources\Admin\User;


use App\Http\Resources\Admin\Checkout\OrderList;
use App\Http\Resources\Auth\UserDownloadList;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Support\Facades\Storage;
use Laravolt\Avatar\Facade;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(schema="AdminUsersList")
 * {
 * @OA\Property(property="id", type="integer", description="ID de la catégorie", default="1")
 * }
 */

class UsersList
{
    public function __construct()
    {

    }

    public function toArray($datas)
    {
        $array = [];
        $order = new OrderList();
        $download = new UserDownloadList();
        foreach ($datas as $data) {
            $array[] = [
                "id" => $data->id,
                "name" => $data->name,
                "email" => $data->email,
                "updated_at" => $data->updated_at,
                "admin" => $data->admin,
                "account" => $this->meAccountFormated($data->account),
                "social" => $this->meSocialFormated($data->social),
                "avatar" => Facade::create($data->name)->toBase64(),
                "achievements" => $this->meAchievementFormated($data->achievements),
                "orders" => $order->toArray($data->orders),
                "downloads" => $download->toArray($data->downloads)
            ];
        }

        return $array;
    }

    public function meAccountFormated($account)
    {
        return [
            "last_ip" => $account->last_ip,
            "site_web" => $account->site_web,
            "point" => $account->point,
            "customer_id" => $account->customer_id,
            "last_login" => $account->last_login,
            "premium" => $account->premium,
            "premium_percent_shop" => $account->premium_percent_shop,
            "premium_asset_beta" => $account->premium_asset_beta
        ];
    }


    public function meSocialFormated($social)
    {
        return [
            "discord_id" => $social->discord_id,
            "discord_channel" => $social->discord_channel,
            "discord_pseudo" => $social->discord_pseudo,
            "google_pseudo" => $social->google_pseudo,
            "microsoft_pseudo" => $social->microsoft_pseudo,
            "twitch_pseudo" => $social->twitch_pseudo,
            "youtube_pseudo" => $social->youtube_pseudo,
            "trainz_pseudo" => $social->trainz_pseudo,
            "twitter_pseudo" => $social->twitter_pseudo,
            "facebook_pseudo" => $social->facebook_pseudo
        ];
    }

    public function meAchievementFormated($achievements)
    {
        $array = [];
        foreach ($achievements as $achievement) {
            $array[] = [
                "id" => $achievement->id,
                "achiever_id" => $achievement->achiever_id,
                "unlocked_at" => ($achievement->unlocked_at) ? [
                    "format" => $achievement->unlocked_at,
                    "normalize" => $achievement->unlocked_at->format('d/m/Y à H:i'),
                    "human" => $achievement->unlocked_at->diffForHumans()
                ] : [
                    "format" => null,
                    "normalize" => null,
                    "human" => null
                ],
                "details" => $achievement->details
            ];
        }

        return $array;
    }
}
