<?php


namespace App\Http\Resources\Admin\User;


use App\Http\Resources\Admin\Checkout\OrderList;
use App\Http\Resources\Auth\UserDownloadList;
use App\Http\Resources\TrainzResources;
use App\User;
use Laravolt\Avatar\Facade;
use OpenApi\Annotations as OA;

/**
 * Class UserGet
 * @package App\Http\Resources\Admin\User
 *
 * @OA\Schema(schema="AdminUserGet")
 * {
 *      @OA\Property(property="id", type="integer", description="ID de l'utilisateur", example="1")
 *      @OA\Property(property="name", type="string", description="Identité de l'utilisateur", example="Franck Rosse")
 *      @OA\Property(property="email", type="string", format="email", description="Email de l'utilisateur", example="f.rosse@gmail.com")
 *      @OA\Property(property="admin", type="boolean", description="L'utilisateut est-il administrateur", example="false")
 *      @OA\Property(property="account", type="object", description="Information de compte de l'utilisateur")
 *      @OA\Property(property="social", type="object", description="Information de compte social de l'utilisateur")
 *      @OA\Property(property="avatar", type="string", description="Lien de l'avatar générer")
 * }
 */
class UserGet implements TrainzResources
{

    public function __construct()
    {
    }

    public function toArray($data)
    {
        $user = User::find($data);
        $order = new OrderList();
        $download = new UserDownloadList();
        return [
            "id" => $user->id,
            "name" => $user->name,
            "email" => $user->email,
            "admin" => $user->admin,
            "account" => $this->meAccountFormated($user->account),
            "social" => $this->meSocialFormated($user->social),
            "avatar" => Facade::create($user->name)->toBase64(),
            "achievements" => $this->meAchievementFormated($user->achievements),
            "orders" => $order->toArray($user->orders),
            "downloads" => $download->toArray($user->downloads)
        ];
    }

    public function meAccountFormated($account)
    {
        return [
            "last_ip" => $account->last_ip,
            "site_web" => $account->site_web,
            "point" => $account->point,
            "customer_id" => $account->customer_id,
            "last_login" => $account->last_login,
            "premium" => $account->premium,
            "premium_percent_shop" => $account->premium_percent_shop,
            "premium_asset_beta" => $account->premium_asset_beta
        ];
    }


    public function meSocialFormated($social)
    {
        return [
            "discord_id" => $social->discord_id,
            "discord_channel" => $social->discord_channel,
            "discord_pseudo" => $social->discord_pseudo,
            "google_pseudo" => $social->google_pseudo,
            "microsoft_pseudo" => $social->microsoft_pseudo,
            "twitch_pseudo" => $social->twitch_pseudo,
            "youtube_pseudo" => $social->youtube_pseudo,
            "trainz_pseudo" => $social->trainz_pseudo,
            "twitter_pseudo" => $social->twitter_pseudo,
            "facebook_pseudo" => $social->facebook_pseudo
        ];
    }

    public function meAchievementFormated($achievements)
    {
        $array = [];
        foreach ($achievements as $achievement) {
            $array[] = [
                "id" => $achievement->id,
                "achiever_id" => $achievement->achiever_id,
                "unlocked_at" => ($achievement->unlocked_at) ? [
                    "format" => $achievement->unlocked_at,
                    "normalize" => $achievement->unlocked_at->format('d/m/Y à H:i'),
                    "human" => $achievement->unlocked_at->diffForHumans()
                ] : [
                    "format" => null,
                    "normalize" => null,
                    "human" => null
                ],
                "details" => $achievement->details
            ];
        }

        return $array;
    }
}
