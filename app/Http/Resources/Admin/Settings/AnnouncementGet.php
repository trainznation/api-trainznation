<?php

namespace App\Http\Resources\Admin\Settings;

use App\Http\Resources\TrainzResources;
use OpenApi\Annotations as OA;

/**
 * Class AnnouncementList
 *
 * @OA\Schema(schema="AdminAnnouncementGet", required={})
 * {
 * @OA\Property(property="message", type="string", description="Message de l'annonce")
 * @OA\Property(property="type", type="string", description="Type du Message de l'annonce")
 * @OA\Property(property="expiring_at", type="string", description="Date d'expiration du message")
 * @OA\Property(property="created_at", type="string", description="Date de création du message")
 * }
 */
class AnnouncementGet implements TrainzResources
{

    public function __construct()
    {
    }

    public function toArray($data)
    {
        return [
            "id" => $data->id,
            "message" => $data->message,
            "type" => $data->type,
            "expiring_at" => [
                "format" => $data->expiring_at,
                "normalize" => $data->expiring_at->format('d/m/Y à H:i')
            ],
            "created_at" => [
                "format" => $data->created_at,
                "normalize" => $data->created_at->format('d/m/Y à H:i')
            ],
        ];
    }
}
