<?php


namespace App\Http\Resources\Admin\Settings;


use App\Http\Resources\TrainzResources;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Support\Facades\Storage;

class SlideshowList implements TrainzResources
{

    /**
     * @var Filesystem
     */
    private $storage;
    /**
     * @var string
     */
    private $file;

    public function __construct()
    {
        if (env('APP_ENV') == 'local') {
            $this->storage = Storage::disk('testing');
            $this->file = 'https://download.trainznation.io/';
        } else {
            $this->storage = Storage::disk('sftp');
            $this->file = 'https://download.trainznation.tk/';
        }
    }

    public function toArray($datas)
    {
        $array = [];
        foreach ($datas as $data) {
            $array[] = [
                "id" => $data->id,
                "link" => $data->link,
                "image" => ($this->storage->exists('slideshow/' . $data->id . '.png') == true) ? $this->file . 'v3/slideshow/' . $data->id . '.png' : 'https://via.placeholder.com/1920x1080',
                "updated_at" => [
                    "format" => $data->updated_at,
                    "normalize" => $data->updated_at->format('d/m/Y à H:i'),
                    "human" => $data->updated_at->diffForHumans()
                ]
            ];
        }

        return $array;
    }
}
