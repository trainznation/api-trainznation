<?php
namespace App\Http\Resources\Admin\Tutoriel;

use App\Http\Resources\TrainzResources;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Support\Facades\Storage;
use OpenApi\Annotations as OA;

/**
 * Class TutorielCategoryList
 *
 * @OA\Schema(schema="AdminTutorielCategoryList", required={})
 * {
 *      @OA\Property(property="name", type="string", description="Nom de  la catégorie", default="Modélisation"),
 *      @OA\Property(property="slug", type="string", description="Slug du Nom  de la catégorie", default="modelisation"),
 *      @OA\Property(property="image", type="string", description="Lien de l'image de la catégorie", default="https://via.placeholder.com/32x32"),
 *      @OA\Property(property="tutoriels", type="object", description="Liste des tutoriels en rapport avec la catégorie"),
 * }
 */
class TutorielCategoryList implements TrainzResources
{

    /**
     * @var Filesystem
     */
    private $storage;
    /**
     * @var string
     */
    private $file;

    public function __construct()
    {
        if (env('APP_ENV') == 'local') {
            $this->storage = Storage::disk('testing');
            $this->file = 'https://download.trainznation.io/';
        } else {
            $this->storage = Storage::disk('sftp');
            $this->file = 'https://download.trainznation.tk/';
        }
    }

    public function toArray($datas)
    {
        $array = [];
        $format = new TutorielList();
        foreach ($datas as $data) {
            $array[] = [
                "id" => $data->id,
                "name" => $data->name,
                "slug" => $data->slug,
                "image" => ($this->storage->exists('tutoriel/category/'.$data->id.'.png') == true) ? $this->file.'v3/tutoriel/category/'.$data->id.'.png' : 'https://via.placeholder.com/32x32',
                "tutoriels" => $format->toArray($data->tutoriels)
            ];
        }

        return $array;
    }
}
