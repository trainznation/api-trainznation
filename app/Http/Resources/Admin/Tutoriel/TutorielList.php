<?php


namespace App\Http\Resources\Admin\Tutoriel;


use App\Http\Resources\TrainzResources;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Support\Facades\Storage;
use OpenApi\Annotations as OA;

/**
 * Class TutorielList
 * @package App\Http\Resources\Admin\Tutoriel
 *
 * @OA\Schema(schema="AdminTutorielList", required={})
 * {
 *      @OA\Property(property="id", type="integer", description="ID du tutoriel", default="1")
 *      @OA\Property(property="title", type="string", description="Titre du tutoriel", default="Comment ?")
 *      @OA\Property(property="slug", type="string", description="Slug du titre du tutoriel", default="comment-est")
 *      @OA\Property(property="short_content", type="string", description="Courte description du tutoriel", default="Lorem sur 255 caractères")
 *      @OA\Property(property="content", type="string", description="Description du tutoriel", default="Lorem Ipsum")
 *      @OA\Property(property="published", type="boolean", description="Tutoriel Publier", default="false")
 *      @OA\Property(property="source", type="boolean", description="Un fichier source est disponible", default="false")
 *      @OA\Property(property="premium", type="boolean", description="Tutoriel uniquement disponible pour les compte premium", default="false")
 *      @OA\Property(property="social", type="boolean", description="Tutoriel publier sur les reseaux sociaux", default="false")
 *      @OA\Property(property="time", type="string", description="Durée du tutoriel", default="00:05:32")
 *      @OA\Property(property="published_at", type="object", description="Tableau formater de la date de publication")
 *      @OA\Property(property="category", type="object", description="Tableau formater de la catégorie affilier")
 *      @OA\Property(property="image", type="string", description="Lien vers l'image du tutoriel")
 *      @OA\Property(property="video", type="string", description="Lien vers la vidéo du tutoriel")
 * }
 */

class TutorielList implements TrainzResources
{

    /**
     * @var Filesystem
     */
    private $storage;
    /**
     * @var string
     */
    private $file;

    public function __construct()
    {
        if (env('APP_ENV') == 'local') {
            $this->storage = Storage::disk('testing');
            $this->file = 'https://download.trainznation.io/';
        } else {
            $this->storage = Storage::disk('sftp');
            $this->file = 'https://download.trainznation.tk/';
        }
    }

    public function toArray($datas)
    {
        $format = new TutorielCategoryGet();
        $array = [];
        foreach ($datas as $data) {
            $array[] = [
                "id" => $data->id,
                "title" => $data->title,
                "slug" => $data->slug,
                "short_content" => $data->short_content,
                "content" => $data->content,
                "published" => $data->published,
                "source" => $data->source,
                "premium" => $data->premium,
                "social" => $data->social,
                "time" => $data->time,
                "published_at" => ($data->published == 1 && $data->published_at !== null) ? [
                    "format" => $data->published_at,
                    "normalize" => $data->published_at->format('d/m/Y à H:i'),
                    "human" => $data->published_at->diffForHumans(),
                ] : [
                    "format" => null,
                    "normalize" => null,
                    "human" => null,
                ],
                "category" => [
                    "id" => $data->category->id,
                    "name" => $data->category->name,
                    "slug" => $data->category->slug,
                ],
                "image" => ($this->storage->exists('tutoriel/'.$data->id.'/'.$data->id.'.png') == true) ? $this->file.'v3/tutoriel/'.$data->id.'/'.$data->id.'.png' : 'https://via.placeholder.com/768x420',
                "video" => ($this->storage->exists('tutoriel/'.$data->id.'/video.mp4') == true) ? $this->file.'v3/tutoriel/'.$data->id.'/video.mp4' : null
            ];
        }

        return $array;
    }
}
