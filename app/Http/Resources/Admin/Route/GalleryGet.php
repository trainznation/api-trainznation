<?php


namespace App\Http\Resources\Admin\Route;


use App\Http\Resources\TrainzResources;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Support\Facades\Storage;
use OpenApi\Annotations as OA;

/**
 * Class GalleryList
 * @package App\Http\Resources\Admin\Route
 *
 * @OA\Schema(schema="AdminRouteGalleryGet")
 * {
 *      @OA\Property(property="id", type="integer", description="ID de l'image", example="1")
 *      @OA\Property(property="image", type="string", description="Lien de l'image", example="https://via.placeholder.com/768x420")
 * }
 */

class GalleryGet implements TrainzResources
{

    /**
     * @var Filesystem
     */
    private $storage;
    /**
     * @var string
     */
    private $file;

    public function __construct()
    {
        if (env('APP_ENV') == 'local') {
            $this->storage = Storage::disk('testing');
            $this->file = 'https://download.trainznation.io/';
        } else {
            $this->storage = Storage::disk('sftp');
            $this->file = 'https://download.trainznation.tk/';
        }
    }

    public function toArray($data)
    {
        return [
            "id" => $data->id,
            "image" => ($this->storage->exists('/route/'.$data->route_id.'/gallery/'.$data->route_gallery_category_id.'/'.$data->id.'.png') == true)
                ? $this->file.'v3/route/'.$data->route_id.'/gallery/'.$data->route_gallery_category_id.'/'.$data->id.'.png'
                : 'https://via.placeholder.com/768x420',
            "route_id" => $data->route_id,
            "category" => $data->route_gallery_category_id
        ];
    }
}
