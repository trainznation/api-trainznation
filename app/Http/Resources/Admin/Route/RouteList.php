<?php
namespace App\Http\Resources\Admin\Route;

use App\Http\Resources\TrainzResources;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Support\Facades\Storage;
use OpenApi\Annotations as OA;

/**
 * Class RouteList
 *
 * @OA\Schema(schema="AdminRouteList")
 * {
 *      @OA\Property(property="id", type="integer", description="ID de la route", example="1")
 *      @OA\Property(property="name", type="string", description="Nom de la route", example="Pays de la loire")
 *      @OA\Property(property="description", type="string", description="Description de la route", example="Ceci est une description")
 *      @OA\Property(property="published", type="boolean", description="Publication de la route", example="true")
 *      @OA\Property(property="version", type="string", description="Version actuel de la route", example="1")
 *      @OA\Property(property="build", type="string", description="Build actuel de la route", example="2560")
 *      @OA\Property(property="route_kuid", type="string", description="kuid de la route", example="kuid:400722:100005")
 *      @OA\Property(property="dependance_kuid", type="string", description="dependance de la route", example="kuid:400722:9652000")
 *      @OA\Property(property="image", type="string", description="Lien de l'image", example="https://via.placeholder.com/768x420")
 *      @OA\Property(property="roadmap", type="object", description="Roadmap de la route")
 *      @OA\Property(property="sessions", type="object", description="Liste des sessions de la route")
 * }
 */

class RouteList implements TrainzResources
{

    /**
     * @var Filesystem
     */
    private $storage;
    /**
     * @var string
     */
    private $file;

    public function __construct()
    {
        if (env('APP_ENV') == 'local') {
            $this->storage = Storage::disk('testing');
            $this->file = 'https://download.trainznation.io/';
        } else {
            $this->storage = Storage::disk('sftp');
            $this->file = 'https://download.trainznation.tk/';
        }
    }

    public function toArray($datas)
    {
        $array = [];
        $formatVersions = new RouteVersionGet();
        foreach ($datas as $data) {
            $array[] = [
                "id" => $data->id,
                "name" => $data->name,
                "description" => $data->description,
                "published" => $data->published,
                "version" => $data->version,
                "build" => $data->build,
                "route_kuid" => $data->route_kuid,
                "dependance_kuid" => $data->dependance_kuid,
                "image" => ($this->storage->exists('/route/'.$data->id.'/route.png') == true) ? $this->file.'v3/route/'.$data->id.'/route.png' : 'https://via.placeholder.com/768x420',
                "versions" => $data->versions,
                "sessions" => $data->sessions
            ];
        }

        return $array;
    }
}
