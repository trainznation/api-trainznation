<?php
namespace App\Http\Resources\Admin\Route;

use App\Http\Resources\TrainzResources;
use OpenApi\Annotations as OA;

/**
 * Class GalleryCategoryList
 * @package App\Http\Resources\Admin\Route
 *
 * @OA\Schema(schema="AdminRouteGalleryCategoryList")
 * {
 *      @OA\Property(property="id", type="integer", description="ID de la catégorie", example="1")
 *      @OA\Property(property="name", type="string", description="Nom de la catégorie", example="Nantes")
 *      @OA\Property(property="galleries", type="array", @OA\Items(ref="#/components/schemas/AdminRouteGalleryList"))
 * }
 */

class GalleryCategoryList implements TrainzResources
{

    public function __construct()
    {
    }

    public function toArray($datas)
    {
        $array = [];
        $format = new GalleryList();
        foreach ($datas as $data) {
            $array[] = [
                "id" => $data->id,
                "name" => $data->name,
                "galleries" => $format->toArray($data->galleries),
                "route_id" => $data->route_id
            ];
        }

        return $array;
    }
}
