<?php


namespace App\Http\Resources\Admin\Route;


use App\Http\Resources\TrainzResources;

class VersionTaskList implements TrainzResources
{

    public function __construct()
    {
    }

    public function toArray($datas)
    {
        $array = [];
        $config = config('task');

        foreach ($datas as $data) {

            $array[] = [
                "id" => $data->id,
                "name" => $data->name,
                "description" => $data->description,
                "lieu" => $data->lieu,
                "etat" => $data->etat,
                "priority" => [
                    "value" => $data->priority,
                    "label" => $config['priority'][$data->priority]['name']
                ],
                "started_at" => [
                    "format" => $data->started_at,
                    "normalize" => $data->started_at->format('d/m/Y à H:i'),
                    "human" => $data->started_at->diffForHumans()
                ],
                "finished_at" => ($data->finished_at) ? [
                    "format" => $data->finished_at,
                    "normalize" => $data->finished_at->format('d/m/Y à H:i'),
                    "human" => $data->finished_at->diffForHumans()
                ] : [
                    "format" => null,
                    "normalize" => null,
                    "human" => null
                ],
                "version_id" => $data->route_version_id,
                "route_id" => $data->route_id
            ];
        }

        return $array;
    }
}
