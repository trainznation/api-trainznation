<?php


namespace App\Http\Resources\Admin\Route;


use App\Http\Resources\TrainzResources;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Support\Facades\Storage;
use OpenApi\Annotations as OA;

/**
 * Class RouteVersionGet
 * @package App\Http\Resources\Admin\Route
 *
 * @OA\Schema(schema="AdminRouteVersionGet", required={})
 * {
 *      @OA\Property(property="id", type="string", description="ID de la session", example="1"),
 *      @OA\Property(property="name", type="string", description="Nom de la session", example="TER 856410 - Nantes / Pornic"),
 *      @OA\Property(property="short_content", type="string", description="Petit descriptif de la session", example="Lorem sur environ 120 caractères"),
 *      @OA\Property(property="content", type="string", description="Descriptif de la session", example="Lorem sur plus de 255 caractères"),
 *      @OA\Property(property="published", type="boolean", description="Publication de la session", example="true"),
 *      @OA\Property(property="published_at", type="string", description="Date de publication de la session", example="2020-07-10 03:00:00"),
 *      @OA\Property(property="kuid", type="string", description="Kuid de la session", example="<kuid:400722:120>>"),
 *      @OA\Property(property="route", type="object", description="Route de la session"),
 * }
 */

class RouteVersionGet implements TrainzResources
{

    /**
     * @var Filesystem
     */
    private $storage;
    /**
     * @var string
     */
    private $file;

    public function __construct()
    {
        if (env('APP_ENV') == 'local') {
            $this->storage = Storage::disk('testing');
            $this->file = 'https://download.trainznation.io/';
        } else {
            $this->storage = Storage::disk('sftp');
            $this->file = 'https://download.trainznation.tk/';
        }
    }

    public function toArray($data)
    {
        $formatTask = new VersionTaskList();
        return [
            "id" => $data->id,
            "version" => $data->version,
            "build" => $data->build,
            "published_at" => ($data->published_at) ? [
                "format" => $data->published_at,
                "normalize" => $data->published_at->format('d/m/Y à H:i'),
                "human" => $data->published_at->diffForHumans()
            ] : [
                "format" => null,
                "normalize" => null,
                "human" => null
            ],
            "registered_task" => $data->registered_task,
            "progress_task" => $data->progress_task,
            "finish_task" => $data->finish_task,
            "total_task" => $data->total_task,
            "percent_task" => $data->percent_task,
            "map_link" => $data->map_link,
            "station_start" => $data->station_start,
            "station_end" => $data->station_end,
            "distance" => $data->distance,
            "route_id" => $data->route_id,
            "image" => ($this->storage->exists('route/version/'.$data->id.'/version.png') == true) ? $this->file.'v3/route/version/'.$data->id.'/version.png' : 'https://via.placeholder.com/768x420',
            "video" => ($this->storage->exists('route/version/'.$data->id.'/video.mp4') == true) ? $this->file.'v3/route/version/'.$data->id.'/video.mp4' : 'https://via.placeholder.com/768x420',
            "tasks" => $formatTask->toArray($data->tasks)
        ];
    }
}
