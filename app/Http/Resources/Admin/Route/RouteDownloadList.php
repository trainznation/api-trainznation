<?php


namespace App\Http\Resources\Admin\Route;

use App\Http\Resources\TrainzResources;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Support\Facades\Storage;
use OpenApi\Annotations as OA;

/**
 * Class RouteDownloadList
 * @package App\Http\Resources\Admin\Route
 *
 * @OA\Schema(schema="AdminRouteDownloadList")
 * {
 *      @OA\Property(property="id", type="integer", description="ID du téléchargement", default="1")
 *      @OA\Property(property="version", type="string", description="Version du téléchargement", default="1")
 *      @OA\Property(property="build", type="string", description="Build du téléchargement", default="560")
 *      @OA\Property(property="uuid", type="string", description="UUID du téléchargement", default="123e4567-e89b-12d3-a456-426614174000")
 *      @OA\Property(property="note", type="string", description="Note du téléchargement", default="Lorem Ispum...")
 *      @OA\Property(property="published", type="boolean", description="Publication du téléchargement", default="false")
 *      @OA\Property(property="alpha", type="boolean", description="téléchargement en ALPHA", default="false")
 *      @OA\Property(property="beta", type="boolean", description="téléchargement en BETA", default="false")
 *      @OA\Property(property="release", type="boolean", description="téléchargement en RELEASE CANDIDATE", default="false")
 *      @OA\Property(property="published_at", type="string", description="Date de publication du téléchargement", default="2020-09-27 01:23:00")
 *      @OA\Property(property="route_id", type="string", description="ID de la route", default="1")
 * }
 */

class RouteDownloadList implements TrainzResources
{

    /**
     * @var Filesystem
     */
    private $storage;
    /**
     * @var string
     */
    private $file;

    public function __construct()
    {
        if (env('APP_ENV') == 'local') {
            $this->storage = Storage::disk('testing');
            $this->file = 'https://download.trainznation.io/';
        } else {
            $this->storage = Storage::disk('sftp');
            $this->file = 'https://download.trainznation.tk/';
        }
    }

    public function toArray($datas)
    {
        $array = [];
        foreach ($datas as $data) {
            $array[] = [
                "id" => $data->id,
                "version" => $data->version,
                "build" => $data->build,
                "uuid" => $data->uuid,
                "note" => $data->note,
                "published" => $data->published,
                "alpha" => $data->alpha,
                "beta" => $data->beta,
                "release" => $data->release,
                "published_at" => ($data->published_at) ? [
                    "format" => $data->published_at,
                    "normalize" => $data->published_at->format('d/m/Y à H:i'),
                    "human" => $data->published_at->diffForHumans()
                ] : [
                    "format" => null,
                    "normalize" => null,
                    "human" => null
                ],
                "route_id" => $data->route_id,
                "link" => ($this->storage->exists('/route/'.$data->route_id.'/'.$data->uuid.'.zip') == true) ? $this->file.'v3/route/'.$data->route_id.'/'.$data->uuid.'.zip' : null,
            ];
        }

        return $array;
    }
}
