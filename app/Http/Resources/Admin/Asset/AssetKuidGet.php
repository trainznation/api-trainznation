<?php


namespace App\Http\Resources\Admin\Asset;


use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Support\Facades\Storage;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(schema="AdminAssetKuidGet")
 * {
 * @OA\Property(property="id", type="integer", description="Identifiant du Kuid", default="1")
 * @OA\Property(property="kuid", type="string", description="Nom du kuid format Trainz", default="kuid:400722:52000")
 * @OA\Property(property="published_at", type="string", description="Date de publication du kuid", default="2020-09-03 15:00:25")
 * @OA\Property(property="asset_id", type="integer", description="ID de l'objet affilier", default="1")
 * }
 */

class AssetKuidGet
{


    public function toArray($data)
    {
        return [
            "id" => $data->id,
            "kuid" => $data->kuid,
            "published_at" => $data->published_at,
            "asset_id" => $data->asset_id,
            "asset" => $data->asset
        ];
    }
}
