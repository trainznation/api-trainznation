<?php


namespace App\Http\Resources\Admin\Asset;


use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(schema="AdminAssetCategories")
 * {
 * @OA\Property(property="id", type="integer", description="ID de la catégorie", default="1")
 * @OA\Property(property="name", type="string", description="Nom de la catégorie", default="TGV")
 * @OA\Property(property="image", type="string", description="Image de la catégorie", default="https://via.placeholder.com/32x32")
 * @OA\Property(property="assets", type="array", description="Liste des objets de la catégorie", @OA\Items(ref="#/components/schemas/AdminAssets"))
 * }
 */

class AssetCategoryList
{
    /**
     * @var Filesystem
     */
    private $storage;
    /**
     * @var string
     */
    private $file;

    public function __construct()
    {
        if (env('APP_ENV') == 'local') {
            $this->storage = Storage::disk('testing');
            $this->file = 'https://download.trainznation.io/';
        } else {
            $this->storage = Storage::disk('sftp');
            $this->file = 'https://download.trainznation.tk/';
        }
    }

    public function toArray($datas)
    {
        $format = new AssetsList();
        $array = [];
        foreach ($datas as $data) {
            $array[] = [
                "id" => $data->id,
                "name" => $data->name,
                "slug" => Str::slug($data->name),
                "image" => ($this->storage->exists('/assets/category/'.$data->id.'.png') == true) ? $this->file.'v3/assets/category/'.$data->id.'.png' : 'https://via.placeholder.com/32x32',
                "assets" => $format->toArray($data->assets)
            ];
        }

        return $array;
    }
}
