<?php


namespace App\Http\Resources\Admin\Asset;


use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Support\Facades\Storage;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(schema="AdminAssets")
 * {
 * @OA\Property(property="id", type="integer", description="Identifiant de l'Objet", default="1")
 * @OA\Property(property="designation", type="string", description="Nom de l'objet", default="Maison au toit de chaume")
 * @OA\Property(property="short_description", type="string", description="Courte description de l'objet", default="Lorem ipsum sur 255 caractères")
 * @OA\Property(property="description", type="string", description="Description de l'objet", default="Lorem ipsum sur 3500 caractères")
 * @OA\Property(property="social", type="integer", description="Si l'objet est publier sur les réseau sociaux", default="0")
 * @OA\Property(property="published", type="integer", description="Si l'objet est publier sur le site", default="0")
 * @OA\Property(property="meshes", type="integer", description="Si l'objet à un mesh 3D de publier sur le serveur treeJs", default="0")
 * @OA\Property(property="pricing", type="integer", description="Si l'objet est payant", default="0")
 * @OA\Property(property="price", type="float", description="Montant de l'objet", default="0")
 * @OA\Property(property="published_at", type="string", description="Date de la publication de l'objet", default="2020-09-10 10:05:00")
 * @OA\Property(property="uuid", type="string", description="Identifiant Unique de l'objet", default="e2c26a21-3b0a-32ed-8e62-b6299f0b3f85")
 * @OA\Property(property="count_download", type="integer", description="Nombre de fois que l'objet à été télécharger", default="0")
 * @OA\Property(property="updated_at", type="string", description="Date de mise à jour de l'objet", default="2020-09-10 10:05:00")
 * @OA\Property(property="asset_category", type="array", description="Information sur la catégorie de l'objet", @OA\Items(
 *      @OA\Property(property="id", type="integer"),
 *      @OA\Property(property="name", type="string"),
 *      @OA\Property(property="image", type="string"),
 *     ))
 * @OA\Property(property="image", type="string", description="Image de l'objet (768x420)", default="https://via.placeholder.com/768x420")
 * @OA\Property(property="kuids", type="array", description="Liste des Kuids de l'objet", @OA\Items(ref="#/components/schemas/AdminAssetKuids"))
 * }
 */

class AssetsList
{
    /**
     * @var Filesystem
     */
    private $storage;
    /**
     * @var string
     */
    private $file;

    public function __construct()
    {
        if (env('APP_ENV') == 'local') {
            $this->storage = Storage::disk('testing');
            $this->file = 'https://download.trainznation.io/';
        } else {
            $this->storage = Storage::disk('sftp');
            $this->file = 'https://download.trainznation.tk/';
        }
    }

    public function toArray($datas)
    {
        $format = new AssetsKuidsList();
        $array = [];
        foreach ($datas as $data) {
            $array[] = [
                "id" => $data->id,
                "designation" => $data->designation,
                "short_description" => $data->short_description,
                "description" => $data->description,
                "social" => $data->social,
                "published" => $data->published,
                "meshes" => $data->meshes,
                "pricing" => $data->pricing,
                "price" => $data->price,
                "published_at" => ($data->published == 1) ? [
                    "format" => $data->published_at,
                    "normalize" => $data->published_at->format('d/m/Y à H:i'),
                    "human" => $data->published_at->diffForHumans()
                ] : [
                    "format" => null,
                    "normalize" => null,
                    "human" => null
                ],
                "uuid" => $data->uuid,
                "count_download" => $data->count_download,
                "updated_at" => [
                    "format" => $data->updated_at,
                    "normalize" => $data->updated_at->format('d/m/Y à H:i'),
                    "human" => $data->updated_at->diffForHumans()
                ],
                "tags" => $data->tags,
                "asset_category" => $this->getCategory($data->asset_category_id),
                "image" => ($this->storage->exists('/assets/'.$data->id.'/'.$data->id.'.png') == true) ? $this->file.'v3/assets/'.$data->id.'/'.$data->id.'.png' : 'https://via.placeholder.com/768x420',
                "asset" => ($this->storage->exists('/assets/'.$data->id.'/meshes/asset.glb') == true) ? $this->file.'v3/assets/'.$data->id.'/meshes/asset.glb' : null,
                "link_uuid" => ($this->storage->exists('/assets/'.$data->id.'/'.$data->uuid.'.zip') == true) ? $this->file.'v3/assets/'.$data->id.'/'.$data->uuid.'.zip' : null,
                "kuids" => $format->toArray($data->kuids),
                "sketchfab_uid" => $data->sketchfab_uid
            ];
        }

        return $array;
    }

    private function getCategory($category_id)
    {
        $category = \App\Model\AssetCategory::find($category_id);

        return [
            "id" => $category->id,
            "name" => $category->name,
            "image" => ($this->storage->exists('/assets/category/'.$category->id.'.png') == true) ? $this->file.'v3/assets/category/'.$category->id.'.png' : 'https://via.placeholder.com/32x32',
        ];
    }
}
