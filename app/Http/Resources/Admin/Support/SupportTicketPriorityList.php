<?php


namespace App\Http\Resources\Admin\Support;


use App\Http\Resources\TrainzResources;

class SupportTicketPriorityList implements TrainzResources
{

    public function __construct()
    {
    }

    public function toArray($datas)
    {
        $array = [];
        foreach ($datas as $data) {
            $array[] = [
                "id" => $data->id,
                "designation" => $data->designation,
                "color" => $data->color
            ];
        }

        return $array;
    }
}
