<?php


namespace App\Http\Resources\Admin\Support;


use App\Http\Resources\Auth\Me;
use App\Http\Resources\TrainzResources;

class SupportTicketConvList implements TrainzResources
{

    public function __construct()
    {
    }

    public function toArray($datas)
    {
        $array = [];
        $format_me = new Me();
        foreach ($datas as $data) {
            $array[] = [
                "message" => $data->message,
                "support_ticket_id" => $data->support_ticket_id,
                "user" => $format_me->toArray($data->user),
                "updated_at" => [
                    "format" => $data->updated_at,
                    "normalize" => $data->updated_at->format('d/m/Y à H:i'),
                    "human" => $data->updated_at->diffForHumans(),
                ]
            ];
        }

        return $array;
    }
}
