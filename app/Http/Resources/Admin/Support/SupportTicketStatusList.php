<?php


namespace App\Http\Resources\Admin\Support;


use App\Http\Resources\TrainzResources;

class SupportTicketStatusList implements TrainzResources
{

    public function __construct()
    {
    }

    public function toArray($datas)
    {
        $array = [];
        foreach ($datas as $data) {
            $array[] = [
                "designation" => $data->designation,
                "color" => $data->color,
                "icon" => $data->icon,
            ];
        }

        return $array;
    }
}
