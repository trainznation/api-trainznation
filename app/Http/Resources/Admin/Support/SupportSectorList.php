<?php


namespace App\Http\Resources\Admin\Support;


use App\Http\Resources\TrainzResources;

class SupportSectorList implements TrainzResources
{

    public function __construct()
    {
    }

    public function toArray($datas)
    {
        $array = [];
        foreach ($datas as $data) {
            $array[] = [
                "id" => $data->id,
                "name" => $data->name,
                "category" => $data->category
            ];
        }

        return $array;
    }
}
