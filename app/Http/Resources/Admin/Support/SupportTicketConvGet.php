<?php


namespace App\Http\Resources\Admin\Support;


use App\Http\Resources\Auth\Me;
use App\Http\Resources\TrainzResources;

class SupportTicketConvGet implements TrainzResources
{

    public function __construct()
    {
    }

    public function toArray($data)
    {
        $format_me = new Me();
        return [
            "message" => $data->message,
            "support_ticket_id" => $data->support_ticket_id,
            "user" => $format_me->toArray($data->user_id),
            "updated_at" => [
                "format" => $data->updated_at,
                "normalize" => $data->updated_at->format('d/m/Y à H:i'),
                "human" => $data->updated_at->diffForHumans(),
            ]
        ];
    }
}
