<?php


namespace App\Http\Resources\Admin\Support;


use App\Http\Resources\TrainzResources;

class SupportTicketPriorityGet implements TrainzResources
{

    public function __construct()
    {
    }

    public function toArray($data)
    {
        return [
            "id" => $data->id,
            "designation" => $data->designation,
            "color" => $data->color
        ];
    }
}
