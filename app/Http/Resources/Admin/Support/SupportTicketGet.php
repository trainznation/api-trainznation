<?php


namespace App\Http\Resources\Admin\Support;


use App\Http\Resources\Auth\Me;
use App\Http\Resources\TrainzResources;

class SupportTicketGet implements TrainzResources
{

    public function __construct()
    {
    }

    public function toArray($data)
    {
        $format_me = new Me();
        $format_conv = new SupportTicketConvList();
        return [
            "id" => $data->id,
            "subject" => $data->subject,
            "category" => $data->category,
            "sector" => $data->sector,
            "priority" => $data->priority,
            "status" => $data->status,
            "source" => $data->source,
            "requester" => $data->requester,
            "assign" => $data->assign,
            "created_at" => [
                "format" => $data->created_at,
                "normalize" => $data->created_at->format('d/m/Y H:i'),
                "human" => $data->created_at->diffForHumans()
            ],
            "updated_at" => [
                "format" => $data->updated_at,
                "normalize" => $data->updated_at->format('d/m/Y H:i'),
                "human" => $data->updated_at->diffForHumans()
            ],
            "conversations" => $format_conv->toArray($data->conversations)
        ];
    }
}
