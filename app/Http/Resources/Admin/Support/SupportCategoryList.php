<?php


namespace App\Http\Resources\Admin\Support;


use App\Http\Resources\TrainzResources;

class SupportCategoryList implements TrainzResources
{

    public function __construct()
    {
    }

    public function toArray($datas)
    {
        $array = [];
        foreach ($datas as $data) {
            $array[] = [
                "name" => $data->name
            ];
        }

        return $array;
    }
}
