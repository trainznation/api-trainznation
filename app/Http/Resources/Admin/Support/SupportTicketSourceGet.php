<?php


namespace App\Http\Resources\Admin\Support;


use App\Http\Resources\TrainzResources;

class SupportTicketSourceGet implements TrainzResources
{

    public function __construct()
    {
    }

    public function toArray($data)
    {
        return [
            "designation" => $data->designation
        ];
    }
}
