<?php


namespace App\Http\Resources\Admin\Support;


use App\Http\Resources\TrainzResources;

class SupportCategoryGet implements TrainzResources
{

    public function __construct()
    {
    }

    public function toArray($data)
    {
        return [
            "name" => $data->name
        ];
    }
}
