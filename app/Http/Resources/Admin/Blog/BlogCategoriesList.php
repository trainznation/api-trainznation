<?php


namespace App\Http\Resources\Admin\Blog;


use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(schema="AdminBlogCategoriesList")
 * {
 * @OA\Property(property="id", type="integer", description="ID de la catégorie", example="1")
 * @OA\Property(property="name", type="string", description="Nom de la catégorie", example="Test News")
 * @OA\Property(property="slug", type="string", description="Nom de la catégorie sluggify", example="test-news")
 * }
 */

class BlogCategoriesList
{
    public function __construct()
    {

    }

    public function toArray($datas)
    {
        $array = [];
        foreach ($datas as $data) {
            $array[] = [
                "id" => $data->id,
                "name" => $data->name,
                "slug" => Str::slug($data->name)
            ];
        }

        return $array;
    }
}
