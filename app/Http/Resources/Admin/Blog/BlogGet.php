<?php


namespace App\Http\Resources\Admin\Blog;


use App\Http\Resources\TrainzResources;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Support\Facades\Storage;
use OpenApi\Annotations as OA;

/**
 * Class BlogGet
 * @package App\Http\Resources\Admin\Blog
 *
 * @OA\Schema(schema="AdminBlogGet")
 * {
 * @OA\Property(property="id", type="integer", description="ID de la l'article", example="1")
 * @OA\Property(property="title", type="string", description="Titre de la l'article", example="Nouveauté lors du test")
 * @OA\Property(property="slug", type="string", description="Sluggify de la l'article", example="nouveauté-lors-du-test")
 * @OA\Property(property="short_content", type="string", description="Contenue succins de la l'article", example="contenue à 120 caractères")
 * @OA\Property(property="content", type="string", description="Contenue de la l'article", example="contenue à 4500 caractères")
 * @OA\Property(property="published", type="boolean", description="Publication de l'article", default="false")
 * @OA\Property(property="published_at", type="object", description="Date de publication de l'article", @OA\Items(
 *          @OA\Property(property="format", type="string", example="2020-09-21 23:00:00"),
 *          @OA\Property(property="normalize", type="string", example="21/09/2020 à 23:00"),
 *          @OA\Property(property="human", type="string", example="Il y a X temps"),
 *     ))
 * @OA\Property(property="social", type="boolean", description="Publication de l'article sur les reseaux sociaux", default="false"),
 * @OA\Property(property="image", type="string", description="Lien de l'image de l'article", example="https://via.placeholder.com/768x420"),
 * @OA\Property(property="category", type="object", description="Information sur la catégorie affilié", @OA\Items(
 *          @OA\Property(property="id", type="integer", example="1"),
 *          @OA\Property(property="name", type="string", example="Nouveauté"),
 *     )),
 * @OA\Property(property="comments", type="array", description="Liste des commentaires sur l'article", @OA\Items(ref="#/components/schemas/BlogCommentList")),
 * }
 */

class BlogGet implements TrainzResources
{

    /**
     * @var Filesystem
     */
    private $storage;
    /**
     * @var string
     */
    private $file;

    public function __construct()
    {
        if (env('APP_ENV') == 'local') {
            $this->storage = Storage::disk('testing');
            $this->file = 'https://download.trainznation.io/';
        } else {
            $this->storage = Storage::disk('sftp');
            $this->file = 'https://download.trainznation.tk/';
        }
    }

    public function toArray($data)
    {
        $commentFormat = new BlogCommentList();
        return [
            "id" => $data->id,
            "title" => $data->title,
            "slug" => $data->slug,
            "short_content" => $data->short_content,
            "content" => $data->content,
            "published" => $data->published,
            "published_at" => ($data->published == 1) ? [
                "format" => $data->published_at,
                "normalize" => $data->published_at->format('d/m/Y à H:i'),
                "human" => $data->published_at->diffForHumans()
            ] : null,
            "social" => $data->social,
            "updated_at" => [
                "format" => $data->updated_at,
                "normalize" => $data->updated_at->format('d/m/Y à H:i'),
                "human" => $data->updated_at->diffForHumans()
            ],
            "category" => $data->category,
            "comments" => $commentFormat->toArray($data->comments),
            "images" => ($this->storage->exists('blog/' . $data->id . '.png') == true) ? $this->file . 'v3/blog/' . $data->id . '.png' : 'https://via.placeholder.com/768x420',
        ];
    }

}
