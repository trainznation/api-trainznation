<?php


namespace App\Http\Resources\Admin\Blog;

use App\Http\Resources\Admin\User\UserGet;
use App\Http\Resources\TrainzResources;
use OpenApi\Annotations as OA;

/**
 * Class BlogCommentList
 * @package App\Http\Resources\Admin\Blog
 *
 * @OA\Schema(schema="BlogCommentList")
 * {
 *      @OA\Property(property="id", type="integer", description="ID du commentaire", example="1")
 *      @OA\Property(property="comment", type="string", description="Commentaire", example="Ceci est un commentaire")
 *      @OA\Property(property="approuved", type="boolean", description="Commentaire est approuvé ou non", example="true")
 *      @OA\Property(property="user", type="string", description="Information sur l'utilisateur du commentaire", example="Info")
 *      @OA\Property(property="updated_at", type="string", description="Date de mise à jour", example="2020-09-23 16:36:00")
 * }
 */
class BlogCommentList implements TrainzResources
{

    public function __construct()
    {
    }

    public function toArray($datas)
    {
        $userFormat = new UserGet();
        $array = [];
        foreach ($datas as $data) {
            $array[] = [
                "id" => $data->id,
                "comment" => $data->comment,
                "approuved" => $data->is_approved,
                "user" => $userFormat->toArray($data->user_id),
                "updated_at" => [
                    "format" => $data->updated_at,
                    "normalize" => $data->updated_at->format('d/m/Y à H:i'),
                    "human" => $data->updated_at->diffForHumans()
                ]
            ];
        }

        return $array;
    }
}
