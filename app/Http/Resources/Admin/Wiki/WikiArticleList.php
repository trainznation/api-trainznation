<?php


namespace App\Http\Resources\Admin\Wiki;


use App\Http\Resources\TrainzResources;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Support\Facades\Storage;

class WikiArticleList implements TrainzResources
{

    /**
     * @var Filesystem
     */
    private $storage;
    /**
     * @var string
     */
    private $file;

    public function __construct()
    {
        if (env('APP_ENV') == 'local') {
            $this->storage = Storage::disk('testing');
            $this->file = 'https://download.trainznation.io/';
        } else {
            $this->storage = Storage::disk('sftp');
            $this->file = 'https://download.trainznation.tk/';
        }
    }

    public function toArray($datas)
    {
        $array = [];

        foreach ($datas as $data) {
            $array[] = [
                "id" => $data->id,
                "title" => $data->title,
                "content" => $data->content,
                "published" => $data->published,
                "published_at" => ($data->published == 1 && $data->published_at != null) ? [
                    "format" => $data->published_at,
                    "normalize" => $data->published_at->format('d/m/Y à H:i'),
                    "human" => $data->published_at->diffForHumans()
                ] : [
                    "format" => null,
                    "normalize" => null,
                    "human" => null
                ],
                "updated_at" => [
                    "format" => $data->updated_at,
                    "normalize" => $data->updated_at->format('d/m/Y à H:i'),
                    "human" => $data->updated_at->diffForHumans()
                ],
                "category" => $data->wiki_category,
                "sector" => $data->wiki_sector,
                "images" => ($this->storage->exists('wiki/'.$data->id.'.png') == true) ? $this->file.'v3/wiki/'.$data->id.'.png': 'https://via.placeholder.com/768x420'
            ];
        }

        return $array;
    }
}
