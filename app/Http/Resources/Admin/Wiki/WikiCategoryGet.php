<?php


namespace App\Http\Resources\Admin\Wiki;


use App\Http\Resources\TrainzResources;

class WikiCategoryGet implements TrainzResources
{

    public function __construct()
    {
    }

    public function toArray($data)
    {
        return [
            "id" => $data->id,
            "name" => $data->name,
            "sectors" => $data->wiki_sectors,
            "articles" => $data->wiki_articles
        ];
    }
}
