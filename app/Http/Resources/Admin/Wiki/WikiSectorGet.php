<?php


namespace App\Http\Resources\Admin\Wiki;


use App\Http\Resources\TrainzResources;

class WikiSectorGet implements TrainzResources
{

    public function __construct()
    {
    }

    public function toArray($data)
    {
        $formatArticle = new WikiArticleList();
        return [
            "id" => $data->id,
            "name" => $data->name,
            "category" => $data->wiki_category,
            "articles" => $formatArticle->toArray($data->wiki_articles)
        ];
    }
}
