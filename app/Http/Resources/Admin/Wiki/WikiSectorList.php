<?php


namespace App\Http\Resources\Admin\Wiki;


use App\Http\Resources\TrainzResources;

class WikiSectorList implements TrainzResources
{

    public function __construct()
    {
    }

    public function toArray($datas)
    {
        $array = [];

        foreach ($datas as $data) {
            $array[] = [
                "id" => $data->id,
                "name" => $data->name,
                "category" => $data->wiki_category,
                "articles" => $data->wiki_articles
            ];
        }

        return $array;
    }
}
