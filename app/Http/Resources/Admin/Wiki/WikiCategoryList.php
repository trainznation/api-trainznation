<?php


namespace App\Http\Resources\Admin\Wiki;


use App\Http\Resources\TrainzResources;

class WikiCategoryList implements TrainzResources
{

    public function __construct()
    {
    }

    public function toArray($datas)
    {
        $array = [];

        foreach ($datas as $data) {
            $array[] = [
                "id" => $data->id,
                "name" => $data->name,
                "sectors" => $data->wiki_sectors,
                "articles" => $data->wiki_articles
            ];
        }

        return $array;
    }
}
