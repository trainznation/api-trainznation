<?php


namespace App\Http\Resources\Admin\Checkout;


use App\Http\Resources\TrainzResources;

class OrderPaymentList implements TrainzResources
{

    public function __construct()
    {
    }

    public function toArray($datas)
    {
        $array = [];
        foreach ($datas as $data) {
            $array[] = [
                "payment_id" => $data->payment_id,
                "order_id" => $data->order_id
            ];
        }

        return $array;
    }
}
