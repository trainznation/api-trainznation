<?php


namespace App\Http\Resources\Admin\Checkout;


use App\Http\Resources\TrainzResources;

class OrderPaymentGet implements TrainzResources
{

    public function __construct()
    {
    }

    public function toArray($data)
    {
        return [
            "payment_id" => $data->payment_id,
            "order_id" => $data->order_id,
            "updated_at" => [
                "format" => $data->updated_at,
                "normalize" => $data->updated_at->format('d/m/Y à H:i'),
                "human" => $data->updated_at->diffForHumans()
            ]
        ];
    }
}
