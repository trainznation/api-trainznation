<?php


namespace App\Http\Resources\Admin\Checkout;


use App\Http\Resources\Admin\Asset\AssetGet;
use App\Http\Resources\TrainzResources;

class OrderProductGet implements TrainzResources
{

    public function __construct()
    {
    }

    public function toArray($data)
    {
        $asset = new AssetGet();
        return [
            "id" => $data->id,
            "name" => $data->name,
            "total_price_gross" => $data->total_price_gross,
            "quantity" => $data->quantity,
            "order_id" => $data->order_id,
            "asset" => $asset->toArray($data->asset)
        ];
    }
}
