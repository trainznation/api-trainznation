<?php


namespace App\Http\Resources\Admin\Checkout;


use App\Http\Resources\Admin\Asset\AssetGet;
use App\Http\Resources\TrainzResources;

class OrderProductList implements TrainzResources
{

    public function __construct()
    {
    }

    public function toArray($datas)
    {
        $array = [];
        $asset = new AssetGet();
        foreach ($datas as $data) {
            $array[] = [
                "id" => $data->id,
                "name" => $data->name,
                "total_price_gross" => $data->total_price_gross,
                "quantity" => $data->quantity,
                "order_id" => $data->order_id,
                "asset" => $asset->toArray($data->asset)
            ];
        }

        return $array;
    }
}
