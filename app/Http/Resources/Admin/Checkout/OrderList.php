<?php


namespace App\Http\Resources\Admin\Checkout;


use App\Http\Resources\TrainzResources;

class OrderList implements TrainzResources
{

    public function __construct()
    {
    }

    public function toArray($datas)
    {
        $array = [];
        $products = new OrderProductList();
        $payment = new OrderPaymentList();
        foreach ($datas as $data) {
            $array[] = [
                "id" => $data->id,
                "reference" => $data->reference,
                "total" => $data->total,
                "invoice_id" => $data->invoice_id,
                "user_id" => $data->user_id,
                "status" => $data->status,
                "updated_at" => [
                    "format" => $data->updated_at,
                    "normalize" => $data->updated_at->format('d/m/Y à H:i'),
                    "human" => $data->updated_at->diffForHumans()
                ],
                "products" => $products->toArray($data->products),
                "payments" => $payment->toArray($data->payments)
            ];
        }

        return $array;
    }
}
