<?php


namespace App\Http\Resources\Admin\Checkout;


use App\Http\Resources\Admin\User\UserGet;
use App\Http\Resources\TrainzResources;

class OrderGet implements TrainzResources
{
    /**
     * @var bool
     */
    private $payment;

    /**
     * OrderGet constructor.
     * @param bool $payment
     */
    public function __construct($payment = false)
    {
        $this->payment = $payment;
    }

    public function toArray($data)
    {
        $products = new OrderProductList();
        $payment = new OrderPaymentGet();
        $user = new UserGet();

        if($payment == true) {
            return [
                "id" => $data->id,
                "reference" => $data->reference,
                "total" => $data->total,
                "invoice_id" => $data->invoice_id,
                "user_id" => $data->user_id,
                "status" => $data->status,
                "updated_at" => [
                    "format" => $data->updated_at,
                    "normalize" => $data->updated_at->format('d/m/Y à H:i'),
                    "human" => $data->updated_at->diffForHumans()
                ],
                "products" => $products->toArray($data->products),
                "payment" => (isset($data->payments[0])) ? $payment->toArray($data->payments[0]) : null,
                "user" => $user->toArray($data->user->id)
            ];
        } else {
            return [
                "id" => $data->id,
                "reference" => $data->reference,
                "total" => $data->total,
                "invoice_id" => $data->invoice_id,
                "user_id" => $data->user_id,
                "status" => $data->status,
                "updated_at" => [
                    "format" => $data->updated_at,
                    "normalize" => $data->updated_at->format('d/m/Y à H:i'),
                    "human" => $data->updated_at->diffForHumans()
                ],
                "products" => $products->toArray($data->products),
                "user" => $user->toArray($data->user->id)
            ];
        }

    }
}
