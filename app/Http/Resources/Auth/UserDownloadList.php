<?php


namespace App\Http\Resources\Auth;


use App\Http\Resources\Admin\Asset\AssetGet;
use App\Http\Resources\TrainzResources;

class UserDownloadList implements TrainzResources
{

    public function __construct()
    {
    }

    public function toArray($datas)
    {
        $array = [];
        $asset = new AssetGet();
        foreach ($datas as $data) {
            $array[] = [
                "user" => $data->user,
                "asset" => $asset->toArray($data->asset),
                "unlocked_at" => [
                    "format" => $data->unlocked_at,
                    "normalize" => $data->unlocked_at->format('d/m/Y à H:i'),
                    "human" => $data->unlocked_at->diffForHumans(),
                ]
            ];
        }

        return $array;
    }
}
