<?php


namespace App\Http\Resources\Auth;


use App\Http\Resources\Admin\Checkout\OrderList;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Support\Facades\Storage;
use Laravolt\Avatar\Facade;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(schema="Me")
 * {
 *      @OA\Property(property="id", type="integer", description="Identifiant de l'Utilisateur", default="1")
 *      @OA\Property(property="name", type="string", description="Nom ou pseudo de l'utilisateur", default="Marc Fosse")
 *      @OA\Property(property="email", type="string", format="email", description="Email de l'utilisateur", default="user1@user.com")
 *      @OA\Property(property="created_at", type="string", description="Date de création du compte", default="2020-09-03 12:00:00")
 *      @OA\Property(property="updated_at", type="string", description="Date de mise à jour du compte", default="2020-09-03 12:00:00")
 *      @OA\Property(property="admin", type="integer", description="Si l'utilisateur courant est un administrateur", default="0")
 *      @OA\Property(property="account", type="array", description="Information sur le compte de l'utilisateur", @OA\Items(ref="#/components/schemas/MeAccount"))
 *      @OA\Property(property="social", type="array", description="Information sur les comptes sociaux de l'utilisateur", @OA\Items(ref="#/components/schemas/MeSocial"))
 * }
 */

class Me
{
    /**
     * @var Filesystem
     */
    private $storage;
    /**
     * @var string
     */
    private $file;

    public function __construct()
    {
        if (env('APP_ENV') == 'local') {
            $this->storage = Storage::disk('testing');
            $this->file = 'https://download.trainznation.io/';
        } else {
            $this->storage = Storage::disk('sftp');
            $this->file = 'https://download.trainznation.tk/';
        }
    }

    public function toArray($data)
    {
        $order = new OrderList();
        $download = new UserDownloadList();
        return [
            "id" => $data->id,
            "name" => $data->name,
            "email" => $data->email,
            "created_at" => $data->created_at,
            "updated_at" => $data->updated_at,
            "admin" => $data->admin,
            "avatar" => Facade::create($data->name)->toBase64(),
            "account" => $this->meAccountFormated($data->account),
            "social" => $this->meSocialFormated($data->social),
            "achievements" => $this->meAchievementFormated($data->achievements),
            "notifications" => $data->notifications,
            "orders" => $order->toArray($data->orders),
            "downloads" => $download->toArray($data->downloads)
        ];
    }

    /**
     * @param $account
     * @return array
     *
     * @OA\Schema(schema="MeAccount")
     * {
     *      @OA\Property(property="last_ip", type="string", description="Adresse IP utiliser lors de la dernière connexion", example="127.0.0.1")
     *      @OA\Property(property="site_web", type="string", description="Adresse du site web de l'utilisateur", example="https://my-website.site")
     *      @OA\Property(property="point", type="integer", description="Nombre de point de l'utilisateur", example="1200")
     *      @OA\Property(property="customer_id", type="string", description="Identifiant de liaison Stripe", example="cus_rB785Po86")
     *      @OA\Property(property="last_login", type="string", description="Date de la dernière connexion", example="2020-09-03 12:00:00")
     * }
     */
    public function meAccountFormated($account)
    {
        return [
            "last_ip" => $account->last_ip,
            "site_web" => $account->site_web,
            "point" => $account->point,
            "customer_id" => $account->customer_id,
            "last_login" => $account->last_login,
            "premium" => $account->premium,
            "premium_percent_shop" => $account->premium_percent_shop,
            "premium_asset_beta" => $account->premium_asset_beta
        ];
    }


    /**
     * @param $social
     * @return array
     *
     * @OA\Schema(schema="MeSocial")
     * {
     *      @OA\Property(property="discord_id", type="string", description="ID du compte discord", example="6")
     *      @OA\Property(property="discord_channel", type="string", description="Channel du compte discord", example="TrainzAperture")
     *      @OA\Property(property="discord_pseudo", type="string", description="Pseudo du compte discord", example="Trainznation")
     *      @OA\Property(property="google_pseudo", type="string", description="Pseudo du compte Google", example="Trainznation Video")
     *      @OA\Property(property="microsoft_pseudo", type="string", description="Pseudo du compte Microsoft", example="Trainznation")
     *      @OA\Property(property="twitch_pseudo", type="string", description="Pseudo du compte Twitch", example="Trainznation")
     *      @OA\Property(property="youtube_pseudo", type="string", description="Pseudo du compte Youtube", example="Trainznation")
     *      @OA\Property(property="trainz_pseudo", type="string", description="Pseudo du compte Trainz", example="mookidu85100")
     *      @OA\Property(property="twitter_pseudo", type="string", description="Pseudo du compte Twitter", example="Trainznation")
     *      @OA\Property(property="facebook_pseudo", type="string", description="Pseudo du compte Facebook", example="Trainznation")
     * }
     */
    public function meSocialFormated($social)
    {
        return [
            "discord_id" => $social->discord_id,
            "discord_channel" => $social->discord_channel,
            "discord_pseudo" => $social->discord_pseudo,
            "google_pseudo" => $social->google_pseudo,
            "microsoft_pseudo" => $social->microsoft_pseudo,
            "twitch_pseudo" => $social->twitch_pseudo,
            "youtube_pseudo" => $social->youtube_pseudo,
            "trainz_pseudo" => $social->trainz_pseudo,
            "twitter_pseudo" => $social->twitter_pseudo,
            "facebook_pseudo" => $social->facebook_pseudo
        ];
    }

    public function meAchievementFormated($achievements)
    {
        $array = [];
        foreach ($achievements as $achievement) {
            $array[] = [
                "id" => $achievement->id,
                "achiever_id" => $achievement->achiever_id,
                "unlocked_at" => [
                    "format" => $achievement->unlocked_at,
                    "normalize" => $achievement->unlocked_at->format('d/m/Y à H:i'),
                    "human" => $achievement->unlocked_at->diffForHumans()
                ],
                "details" => $achievement->details
            ];
        }

        return $array;
    }
}
