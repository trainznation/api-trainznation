<?php

namespace App\Http\Controllers;

use App\Http\Resources\Admin\Blog\BlogCommentList;
use App\Http\Resources\Admin\Blog\BlogGet;
use App\Model\AchievementDetail;
use App\Model\Asset;
use App\Model\Blog;
use App\Model\Order;
use App\Model\RouteRoadmapVersion;
use App\Model\RouteVersion;
use App\Model\TutorielCategory;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Spatie\Tags\Tag;

class TestController extends Controller
{
    public function test()
    {
        $version = RouteVersion::find(2)->load('tasks');

        dd($version);
    }
}
