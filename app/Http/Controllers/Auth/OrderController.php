<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Api\BaseController;
use App\Http\Controllers\Controller;
use App\Http\Resources\Admin\Checkout\OrderGet;
use App\Http\Resources\Admin\Checkout\OrderList;
use App\Model\Order;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use OpenApi\Annotations as OA;

class OrderController extends BaseController
{
    /**
     * @var Order
     */
    private $order;

    /**
     * OrderController constructor.
     * @param Order $order
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * @return JsonResponse
     *
     * @OA\Get(
     *     path="/api/auth/order",
     *     tags={"User Order"},
     *     security={{"api_key": {}}},
     *     summary="Liste des commandes de l'utilisateur",
     *      @OA\Response(
     *          response="200",
     *          description="Tableau des commandes",
     *          @OA\JsonContent(
     *              type="object",
     *          )
     *      )
     * )
     */
    public function list()
    {
        try {
            $order = $this->order->newQuery()->where('user_id', auth()->user()->id)->get();
        }catch (Exception $exception) {
            return $this->sendError($exception->getMessage(), null, 500);
        }

        $format = new OrderList();
        return $this->sendResponse($format->toArray($order), null, 200);
    }

    /**
     * @param $order_id
     * @return JsonResponse
     *
     * @OA\Get(
     *     path="/api/auth/order/{order_id}",
     *     tags={"User Order"},
     *     security={{"api_key": {}}},
     *     summary="Information sur une commande",
     *      @OA\Parameter(name="order_id", in="path", description="ID de la commande", required=true),
     *      @OA\Response(
     *          response="200",
     *          description="Tableau des informations de la commande",
     *          @OA\JsonContent(
     *              type="object",
     *          )
     *      )
     * )
     */
    public function get($order_id)
    {
        try {
            $order = $this->order->newQuery()->findOrFail($order_id);
        }catch (Exception $exception) {
            return $this->sendError($exception->getMessage(), null, 500);
        }

        $format = new OrderGet();
        return $this->sendResponse($format->toArray($order), null, 200);
    }
}
