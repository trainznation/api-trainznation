<?php

namespace App\Http\Controllers\Auth;

use App\Achievements\UserPremium;
use App\Http\Controllers\Api\BaseController;
use App\Model\UserAccount;
use Exception;
use Illuminate\Http\JsonResponse;
use OpenApi\Annotations as OA;

class PremiumController extends BaseController
{
    /**
     * @var UserAccount
     */
    private $userAccount;

    /**
     * PremiumController constructor.
     * @param UserAccount $userAccount
     */
    public function __construct(UserAccount $userAccount)
    {
        $this->userAccount = $userAccount;
    }

    /**
     * @return JsonResponse
     *
     * @OA\Put(
     *     tags={"Authentification"},
     *     path="/api/auth/premium/activate",
     *     security={{"api_key": {}}},
     *     summary="Active le mode premium",
     *     @OA\Parameter(name="", in="path", description="", required=true),
     *     @OA\Response(
     *      response="200",
     *      description="NULL",
     *      @OA\JsonContent(
     *        type="null",
     *      )
     *     ),
     * )
     */
    public function activate()
    {
        switch (request('plan')) {
            case 'mensuel':
                $premium_percent_shop = 5;
                $premium_asset_beta = 0;
                break;

            case 'trimestriel':
                $premium_percent_shop = 10;
                $premium_asset_beta = 0;
                break;

            case 'semestriel':
                $premium_percent_shop = 25;
                $premium_asset_beta = 1;
                break;

            case 'annuel':
                $premium_percent_shop = 35;
                $premium_asset_beta = 1;
                break;

            default:
                $premium_percent_shop = null;
                $premium_asset_beta = 0;
                break;
        }
        try {
            $this->userAccount->newQuery()->where('user_id', auth()->user()->id)->first()->update([
                "premium" => 1,
                "premium_percent_shop" => $premium_percent_shop,
                "premium_asset_beta" => $premium_asset_beta
            ]);
        }catch (Exception $exception) {
            return $this->sendError($exception->getMessage());
        }

        auth()->user()->unlock(new UserPremium());

        return $this->sendResponse(null, null, 200);
    }

    /**
     * @return JsonResponse
     *
     * @OA\Put(
     *     tags={"Authentification"},
     *     path="/api/auth/premium/desactivate",
     *     security={{"api_key": {}}},
     *     summary="Désactive le mode premium",
     *     @OA\Parameter(name="", in="path", description="", required=true),
     *     @OA\Response(
     *      response="200",
     *      description="NULL",
     *      @OA\JsonContent(
     *        type="null",
     *      )
     *     ),
     * )
     */
    public function desactivate()
    {
        try {
            $this->userAccount->newQuery()->where('user_id', auth()->user()->id)->first()->update([
                "premium" => 0,
                "premium_percent_shop" => null,
                "premium_asset_beta" => 0
            ]);
        }catch (Exception $exception) {
            return $this->sendError($exception->getMessage());
        }

        return $this->sendResponse(null, null, 200);
    }
}
