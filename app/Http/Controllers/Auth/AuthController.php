<?php


namespace App\Http\Controllers\Auth;


use App\Achievements\UserAsRegistered;
use App\Events\Auth\RegisteredUser;
use App\Helpers\Notificator;
use App\Http\Controllers\Api\BaseController;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\RegisterRequest;
use App\Http\Resources\Auth\Me;
use App\Mail\Auth\Registered;
use App\Model\UserAccount;
use App\Model\UserSocial;
use App\Notifications\Auth\RequestPasswordNotification;
use App\User;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Laravel\Socialite\Facades\Socialite;
use OpenApi\Annotations as OA;

class AuthController extends BaseController
{
    /**
     * @var User
     */
    private $user;
    /**
     * @var UserAccount
     */
    private $userAccount;
    /**
     * @var UserSocial
     */
    private $userSocial;
    /**
     * @var Notification
     */
    private $notification;
    /**
     * @var Notificator
     */
    private $notificator;
    /**
     * @var Socialite
     */
    private $socialite;

    /**
     * AuthController constructor.
     * @param User $user
     * @param UserAccount $userAccount
     * @param UserSocial $userSocial
     * @param Notification $notification
     * @param Notificator $notificator
     * @param Socialite $socialite
     */
    public function __construct(User $user, UserAccount $userAccount, UserSocial $userSocial, Notification $notification, Notificator $notificator, Socialite $socialite)
    {
        $this->middleware('auth:api', ['except' => ['login', 'register', 'searchUser', 'socialize']]);
        $this->user = $user;
        $this->userAccount = $userAccount;
        $this->userSocial = $userSocial;
        $this->notification = $notification;
        $this->notificator = $notificator;
        $this->socialite = $socialite;
    }

    /**
     * @return JsonResponse
     *
     * @OA\Post(
     *     path="/api/auth/login",
     *     summary="Connexion de l'utilisateur",
     *     description="Connexion au service Trainznation",
     *     operationId="authLogin",
     *     tags={"Authentification"},
     *      @OA\RequestBody(
     *          required=true,
     *          description="Information de connexion de l'utilisateur",
     *          @OA\JsonContent(
     *              required={"email", "password"},
     *              @OA\Property(property="email", type="string", format="email", example="syltheron@gmail.com"),
     *              @OA\Property(property="password", type="string", format="password", example="1992_Maxime"),
     *          ),
     *      ),
     *      @OA\Response(
     *          response="422",
     *          description="Identification incorrect",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Désolé, l'email ou le mot de passe semble faux. Veuillez réessayer !")
     *          )
     *      ),
     *      @OA\Response(
     *          response="401",
     *          description="Non Authorisé",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="L'accès est interdit")
     *          )
     *      ),
     *     @OA\Response(
     *          response="200",
     *          description="Connexion effectuer",
     *          @OA\JsonContent(
     *              @OA\Property(property="access_token", type="string", example="Token d'accès"),
     *              @OA\Property(property="token_type", type="string", example="Type de token", default="bearer"),
     *              @OA\Property(property="expire_in", type="integer", example="Temps de validation du token", default="3600"),
     *          )
     *      )
     * )
     */
    public function login()
    {
        $credentials = request(['email', 'password']);
        if (!$token = auth()->setTTl(86400)->attempt($credentials)) {
            return response()->json(['error' => "Desole, l'email ou le mot de passe semble faux. Veuillez reessayer !"], 422);
        }

        $user = $this->userAccount->newQuery()->where('user_id', auth()->user()->id)->first();
        $user->update([
            "last_ip" => request()->ip(),
            "last_login" => now()
        ]);
        $this->notificator->publishAsAdmin("Connexion de l'utilisateur <strong>" . $user->name . "</strong>", "success", null, "fas fa-user-lock");

        return $this->respondWithToken($token);
    }

    /**
     * @return JsonResponse
     * @OA\Post(
     *     path="/api/auth/me",
     *     summary="Information de l'utilisateur",
     *     tags={"Authentification"},
     *     security={{"api_key": {}}},
     *     description="Affiche les informations de l'utilisateur connecter",
     *     @OA\Response(
     *          response="401",
     *          description="Non Authorisé",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="L'accès est interdit")
     *          )
     *      ),
     *     @OA\Response(
     *          response="200",
     *          description="Information de l'utilisateur",
     *          @OA\JsonContent(
     *              @OA\Items(ref="#/components/schemas/Me")
     *          )
     *      )
     * )
     */
    public function me()
    {
        $format = new Me();
        $user = User::find(auth()->user()->id);
        return response()->json($format->toArray($user));
    }

    /**
     * @return JsonResponse
     * @OA\Post(
     *     path="/api/auth/logout",
     *     tags={"Authentification"},
     *     security={{"api_key": {}}},
     *     description="Deconnecte l'utilisateur",
     *     summary="Deconnexion de l'utilisateur",
     *     @OA\Response(
     *          response="401",
     *          description="Non Authorisé",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="L'accès est interdit")
     *          )
     *      ),
     *     @OA\Response(
     *          response="200",
     *          description="Message",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Successfully Logged out !")
     *          )
     *      )
     * )
     */
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully Logged out !']);
    }

    /**
     * @return JsonResponse
     *
     * @OA\Post(
     *     path="/api/auth/refresh",
     *     tags={"Authentification"},
     *     security={{"api_key": {}}},
     *     description="Rafraichit le token d'accès",
     *     summary="Rafraichissement du Token d'accès",
     *     @OA\Response(
     *          response="401",
     *          description="Non Authorisé",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="L'accès est interdit")
     *          )
     *      ),
     *     @OA\Response(
     *          response="200",
     *          description="Message",
     *          @OA\JsonContent(
     *              @OA\Property(property="access_token", type="string", example="Token d'accès"),
     *              @OA\Property(property="token_type", type="string", example="Type de token", default="bearer"),
     *              @OA\Property(property="expire_in", type="integer", example="Temps de validation du token", default="3600"),
     *          )
     *      )
     * )
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    protected function respondWithToken($token)
    {

        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }

    /**
     * @param RegisterRequest $request
     * @return JsonResponse
     *
     * @OA\Post(
     *     tags={"Authentification"},
     *     path="/api/auth/register",
     *     summary="Création d'un compte utilisateur",
     *     @OA\Response(
     *      response="200",
     *      description="Retourne l'utilisateur créer'",
     *      @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref="#/components/schemas/Me")
     *      )
     *     ),
     *     @OA\RequestBody(
     *          description="Création de l'utilisateur",
     *          required=true,
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(ref="#/components/schemas/RegisterRequest")
     *          )
     *     )
     * )
     */
    public function register(RegisterRequest $request)
    {
        $format = new Me();
        $user = $this->user->newQuery()->create($request->all());
        $account = $this->userAccount->newQuery()->create([
            "user_id" => $user->id,
            "customer_id" => $request->get('customers_id')
        ]);
        $social = $this->userSocial->newQuery()->create([
            "user_id" => $user->id
        ]);

        $this->notificator->publishAsAdmin("Enregistrement de l'utilisateur <strong>" . $user->name . "</strong>", "success", null, "fas fa-user-plus");
        $this->notificator->publish("Bienvenue " . $user->name, "success", "Bienvenue sur le site de trainznation", "fas fa-user", null, true, $user->id);
        $user->unlock(new UserAsRegistered());
        Mail::to($user)->send(new Registered($user));

        return response()->json($format->toArray($user), 200);
    }

    public function searchUser(Request $request)
    {
        try {
            switch ($request->get('type')) {
                case 'name':
                    $user = $this->user->newQuery()->where('name', $request->get('name'))->first();
                    break;
                case 'email':
                    $user = $this->user->newQuery()->where('email', $request->get('email'))->first();
                    break;
                default:
                    $user = [];
            }
        } catch (Exception $exception) {
            Log::error($exception);
            return $this->sendError($exception->getMessage(), null, 510);
        }
        if ($user) {
            try {
                $token = Str::random();
                $user->reset_password_token = $token;
                $user->save();
            } catch (Exception $exception) {
                Log::error($exception);
                return $this->sendError($exception->getMessage(), null, 510);
            }
            $user->notify(new RequestPasswordNotification($user, $token));
            return $this->sendResponse(1, null, 200);
        } else {
            return $this->sendResponse(0, null, 200);
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     *
     * @OA\Put(
     *     tags={"Authentification"},
     *     path="/api/auth/editing",
     *     security={{"api_key": {}}},
     *     summary="Edition des informations d'un utilisateur",
     *     @OA\Response(
     *      response="200",
     *      description="NULL",
     *      @OA\JsonContent(
     *        type="object",
     *      )
     *     ),
     *     @OA\RequestBody(
     *          description="Requete",
     *          required=true,
     *          @OA\MediaType(
     *              mediaType="application/json",
     *          ),
     *          @OA\MediaType(
     *              mediaType="application/x-www-form-urlencoded",
     *          )
     *     )
     * )
     */
    public function editing(Request $request)
    {
        $user = $this->user->newQuery()->findOrFail(auth()->user()->id);
        $account = $this->userAccount->newQuery()->findOrFail(auth()->user()->id);
        try {
            $user->update([
                "name" => $request->get('name'),
                "email" => $request->get('email')
            ]);
        }catch (Exception $exception) {
            return $this->sendError($exception->getMessage());
        }

        try {
            $account->update([
                "site_web" => $request->get('site_web'),
                "premium" => $request->get('premium')
            ]);
        }catch (Exception $exception) {
            return $this->sendError($exception->getMessage());
        }

        return $this->sendResponse(null, null, 200);
    }

    /**
     * @return JsonResponse
     *
     * @OA\Delete(
     *     path="/api/auth/delete",
     *     tags={"Authentification"},
     *     security={{"api_key": {}}},
     *     summary="Supprime le compte",
     *      @OA\Response(
     *          response="200",
     *          description="Null",
     *          @OA\JsonContent(
     *              type="object"
     *          )
     *      ),
     * )
     */
    public function delete()
    {
        $user = $this->user->newQuery()->findOrFail(auth()->user()->id);

        try {
            $user->delete();
        }catch (Exception $exception) {
            return $this->sendError($exception->getMessage());
        }

        return $this->sendResponse(null, null, 200);
    }

    /**
     * @param $provider
     * @return mixed
     *
     * @OA\Get(
     *     path="/api/auth/socialite/{provider}",
     *     tags={"Authentification"},
     *     security={{"api_key": {}}},
     *     summary="Socialite",
     *      @OA\Parameter(name="provider", in="path", description="ID de la catégorie", required=true),
     *      @OA\Response(
     *          response="200",
     *          description="URI",
     *          @OA\JsonContent(
     *              type="object",
     *          )
     *      )
     * )
     */
    public function socialite($provider) {
        return Socialite::driver($provider)->stateless()->redirect()->getTargetUrl();
    }

    /**
     * @param $provider
     * @return \Laravel\Socialite\Contracts\User
     *
     * @OA\Get(
     *     path="/api/auth/socialite/{provider}/auth",
     *     tags={"Authentification"},
     *     security={{"api_key": {}}},
     *     summary="",
     *      @OA\Parameter(name="provider", in="path", description="ID de la catégorie", required=true),
     *      @OA\Response(
     *          response="200",
     *          description="Info Social",
     *          @OA\JsonContent(
     *              type="object",
     *          )
     *      )
     * )
     */
    public function socialize($provider)
    {
        return Socialite::driver($provider)->stateless()->user();
    }

    /**
     * @param Request $request
     * @return JsonResponse
     *
     * @OA\Put(
     *     tags={"Authentification"},
     *     path="/api/auth/changePassword",
     *     security={{"api_key": {}}},
     *     summary="Changement de mot de passe",
     *     @OA\Response(
     *      response="200",
     *      description="NULL",
     *      @OA\JsonContent(
     *        type="string",
     *      )
     *     ),
     *     @OA\RequestBody(
     *          description="Requete",
     *          required=true,
     *          @OA\MediaType(
     *              mediaType="application/json",
     *          ),
     *          @OA\MediaType(
     *              mediaType="application/x-www-form-urlencoded",
     *          )
     *     )
     * )
     */
    public function changePassword(Request $request) {
        $user = $this->user->newQuery()->findOrFail(auth()->user()->id);
        try {
            $user->update([
                "password" => Hash::make($request->get('password'))
            ]);
        }catch (Exception $exception) {
            return $this->sendError($exception->getMessage());
        }

        return $this->sendResponse(null, null, 200);
    }
}
