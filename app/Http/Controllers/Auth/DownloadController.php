<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Api\BaseController;
use App\Http\Controllers\Controller;
use App\Http\Resources\Auth\UserDownloadList;
use App\Model\UserDownload;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use OpenApi\Annotations as OA;

class DownloadController extends BaseController
{
    /**
     * @var UserDownload
     */
    private $download;

    /**
     * DownloadController constructor.
     * @param UserDownload $download
     */
    public function __construct(UserDownload $download)
    {
        $this->download = $download;
    }

    public function list()
    {
        try {
            $lists = $this->download->newQuery()->where('user_id', auth()->user()->id)->get();
        }catch (Exception $exception) {
            Log::error($exception);
            return $this->sendError($exception->getMessage(), null, 500);
        }

        $format = new UserDownloadList();
        return $this->sendResponse($format->toArray($lists), null, 200);
    }
}
