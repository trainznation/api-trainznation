<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Api\BaseController;
use App\Http\Controllers\Controller;
use App\Model\UserNotification;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use OpenApi\Annotations as OA;

class NotificationController extends BaseController
{
    /**
     * @var UserNotification
     */
    private $userNotification;

    /**
     * NotificationController constructor.
     * @param UserNotification $userNotification
     */
    public function __construct(UserNotification $userNotification)
    {
        $this->userNotification = $userNotification;
    }

    /**
     * @param $notif_id
     * @return JsonResponse
     *
     * @OA\Get(
     *     path="/api/auth/notification/{notif_id}/read",
     *     tags={"User Notification"},
     *     security={{"api_key": {}}},
     *     summary="Lit une notification",
     *      @OA\Parameter(name="notif_id", in="path", description="ID de la notification", required=true),
     *      @OA\Response(
     *          response="200",
     *          description="NULL",
     *          @OA\JsonContent(
     *              type="string",
     *          )
     *      )
     * )
     */
    public function read($notif_id)
    {
        $notif = $this->userNotification->newQuery()->findOrFail($notif_id);
        try {
            $notif->update([
                "read_at" => now()
            ]);
        }catch (Exception $exception) {
            Log::error($exception->getMessage());
            return $this->sendError($exception->getMessage());
        }

        return $this->sendResponse(null, null, 200);
    }

    /**
     * @return JsonResponse
     *
     * @OA\Get(
     *     path="/api/auth/notification/readAll",
     *     tags={"User Notification"},
     *     security={{"api_key": {}}},
     *     summary="Lit toutes les notifications",
     *      @OA\Response(
     *          response="200",
     *          description="NULL",
     *          @OA\JsonContent(
     *              type="string",
     *          )
     *      )
     * )
     */
    public function readAll()
    {
        $notifs = $this->userNotification->newQuery()->where('user_id', auth()->user()->id)->get();
        foreach ($notifs as $notif) {
            try {
                $notif->update([
                    "read_at" => now()
                ]);
            }catch (Exception $exception) {
                Log::error($exception->getMessage());
                return $this->sendError($exception->getMessage());
            }
        }

        return $this->sendResponse(null, null, 200);
    }
}
