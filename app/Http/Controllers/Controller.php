<?php
declare(strict_types=1);
namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use OpenApi\Annotations as OA;

/**
 * @OA\Info(
 *     version="2.0.0",
 *     title="API Trainznation",
 *     description="Documentation de l'API Trainznation"
 * )
 *
 * @OA\Server(
 *     url=L5_SWAGGER_CONST_HOST,
 *     description="Serveur Principal"
 * )
 *
 * @OA\Tag(name="Authentification", description="Authentification de l'utilisateur"),
 * @OA\Tag(name="Admin Asset", description="Gestion des Objets (Administration)"),
 * @OA\Tag(name="Admin User", description="Gestion des Utilisateurs (Administration)"),
 * @OA\Tag(name="Admin User Notification", description="Gestion des notifications Utilisateurs (Administration)"),
 * @OA\Tag(name="Admin Blog Category", description="Gestion des catégorie d'article"),
 * @OA\Tag(name="Admin Blog", description="Gestion des article"),
 * @OA\Tag(name="Admin Route", description="Gestion des routes"),
 * @OA\Tag(name="Admin Route Gallery", description="Gestion des gallerie des routes"),
 * @OA\Tag(name="Admin Route Version", description="Gestion des versions des routes"),
 * @OA\Tag(name="Admin Route Task", description="Gestion des taches d'une carte"),
 * @OA\Tag(name="Admin Route Download", description="Gestion des téléchargement des routes"),
 * @OA\Tag(name="Admin Route Sessions", description="Gestion des sessions des routes"),
 * @OA\Tag(name="Admin Tutoriel Category", description="Gestion des catégories de tutoriel"),
 * @OA\Tag(name="Admin Tutoriel", description="Gestion des tutoriels"),
 * @OA\Tag(name="Admin Settings Announcement", description="Gestion des Annonces de diffusion"),
 * @OA\Tag(name="Admin Settings Slideshow", description="Gestion des Slideshow"),
 * @OA\Tag(name="Admin Support Category", description="Gestion des catégorie de support"),
 * @OA\Tag(name="Admin Support Sector", description="Gestion des secteurs de support"),
 * @OA\Tag(name="Admin Support Priority", description="Gestion des priorités de support"),
 * @OA\Tag(name="Admin Support Source", description="Gestion des sources de support"),
 * @OA\Tag(name="Admin Support Status", description="Gestion des status de support"),
 * @OA\Tag(name="Admin Support Ticket", description="Gestion des Tickets de support"),
 * @OA\Tag(name="Blog", description="Blog"),
 * @OA\Tag(name="Assets", description="Assets"),
 * @OA\Tag(name="Tutoriel", description="Tutoriel"),
 * @OA\Tag(name="Route", description="Route"),
 * @OA\Tag(name="Download", description="Téléchargement"),
 * @OA\Tag(name="User Notification", description="Notification de l'utilisateur"),
 * @OA\Tag(name="User Order", description="Commande de l'utilisateur"),
 * @OA\Tag(name="User Download", description="Téléchargement de l'utilisateur"),
 * @OA\Tag(name="Cart", description="Gestion du panier"),
 * @OA\Tag(name="Checkout", description="Gestion des commandes"),
 * @OA\Tag(name="Notification", description="Gestion des notifications"),
 * @OA\Tag(name="Announcement", description="Gestion des Annonces"),
 * @OA\Tag(name="Slideshow", description="Slideshow"),
 *
 * @OA\SecurityScheme(
 *     type="http",
 *     securityScheme="api_key",
 *     name="Password Based",
 *     scheme="bearer",
 *     bearerFormat="JWT",
 *     description="Utilisez votre identifiant et mot de passe pour obtenir un jeton d'accès",
 *     in="header",
 *     @OA\Flow(
 *      flow="password",
 *      authorizationUrl="/api/auth/login",
 *      refreshUrl="/api/auth/refresh",
 *      scopes={}
 *  )
 * )
 */

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
