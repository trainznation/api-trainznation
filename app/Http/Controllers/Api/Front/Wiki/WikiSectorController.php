<?php

namespace App\Http\Controllers\Api\Front\Wiki;

use App\Http\Controllers\Api\BaseController;
use App\Http\Controllers\Controller;
use App\Http\Resources\Admin\Wiki\WikiSectorGet;
use App\Http\Resources\Admin\Wiki\WikiSectorList;
use App\Model\WikiSector;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class WikiSectorController extends BaseController
{
    /**
     * @var WikiSector
     */
    private $sector;

    /**
     * WikiSectorController constructor.
     * @param WikiSector $sector
     */
    public function __construct(WikiSector $sector)
    {
        $this->sector = $sector;
    }

    public function listByCategory($category_id)
    {
        $format = new WikiSectorList();
        try {
            $lists = $this->sector->newQuery()->where('wiki_category_id', $category_id)->get();
        }catch (Exception $exception) {
            Log::error($exception);
            return $this->sendError($exception->getMessage());
        }

        return $this->sendResponse($format->toArray($lists), null, 200);
    }

    public function get($sector_id)
    {
        $format = new WikiSectorGet();
        try {
            $lists = $this->sector->newQuery()->find($sector_id);
        }catch (Exception $exception) {
            Log::error($exception);
            return $this->sendError($exception->getMessage());
        }

        return $this->sendResponse($format->toArray($lists), null, 200);
    }
}
