<?php

namespace App\Http\Controllers\Api\Front\Wiki;

use App\Http\Controllers\Api\BaseController;
use App\Http\Controllers\Controller;
use App\Http\Resources\Admin\Wiki\WikiArticleGet;
use App\Http\Resources\Admin\Wiki\WikiArticleList;
use App\Model\WikiArticle;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class WikiController extends BaseController
{
    /**
     * @var WikiArticle
     */
    private $article;

    /**
     * WikiController constructor.
     * @param WikiArticle $article
     */
    public function __construct(WikiArticle $article)
    {
        $this->article = $article;
    }

    public function search(Request $request)
    {
        $format = new WikiArticleList();

        try {
            $articles = $this->article->newQuery()->where('title', 'LIKE', '%'.$request->get('q').'%')
                ->orWhere('content', 'LIKE', '%'.$request->get('q').'%')
                ->where('published', 1)
                ->orderBy('published_at', 'desc')
                ->get();
        }catch (Exception $exception) {
            Log::error($exception);
            return $this->sendError($exception->getMessage());
        }

        return $this->sendResponse($format->toArray($articles), null, 200);
    }

    public function list()
    {
        $format = new WikiArticleList();

        try {
            $articles = $this->article->newQuery()
                ->where('published', 1)
                ->orderBy('published_at', 'desc')
                ->get();
        }catch (Exception $exception) {
            Log::error($exception);
            return $this->sendError($exception->getMessage());
        }

        return $this->sendResponse($format->toArray($articles), null, 200);
    }

    public function get($article_id)
    {
        $format = new WikiArticleGet();

        try {
            $article = $this->article->newQuery()->find($article_id);
        }catch (Exception $exception) {
            Log::error($exception);
            return $this->sendError($exception->getMessage());
        }

        return $this->sendResponse($format->toArray($article), null, 200);
    }
}
