<?php

namespace App\Http\Controllers\Api\Front\Wiki;

use App\Http\Controllers\Api\BaseController;
use App\Http\Controllers\Controller;
use App\Http\Resources\Admin\Wiki\WikiCategoryGet;
use App\Http\Resources\Admin\Wiki\WikiCategoryList;
use App\Model\WikiCategory;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class WikiCategoryController extends BaseController
{
    /**
     * @var WikiCategory
     */
    private $wikiCategory;

    /**
     * WikiCategoryController constructor.
     * @param WikiCategory $wikiCategory
     */
    public function __construct(WikiCategory $wikiCategory)
    {
        $this->wikiCategory = $wikiCategory;
    }

    public function list()
    {
        $format = new WikiCategoryList();

        try {
            $lists = $this->wikiCategory->newQuery()->get();
        }catch (Exception $exception) {
            Log::error($exception);
            return $this->sendError($exception->getMessage());
        }

        return $this->sendResponse($format->toArray($lists), null, 200);
    }

    public function get($category_id)
    {
        $format = new WikiCategoryGet();

        try {
            $category = $this->wikiCategory->newQuery()->find($category_id);
        }catch (Exception $exception) {
            Log::error($exception);
            return $this->sendError($exception->getMessage());
        }

        return $this->sendResponse($format->toArray($category), null, 200);
    }
}
