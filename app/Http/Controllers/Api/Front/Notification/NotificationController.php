<?php

namespace App\Http\Controllers\Api\Front\Notification;

use App\Helpers\Notificator;
use App\Http\Controllers\Api\BaseController;
use App\Http\Controllers\Controller;
use App\Model\UserAccount;
use App\Model\UserNotification;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class NotificationController extends BaseController
{
    /**
     * @var Notificator
     */
    private $notificator;
    /**
     * @var UserAccount
     */
    private $userAccount;
    /**
     * @var UserNotification
     */
    private $userNotification;

    /**
     * NotificationController constructor.
     * @param Notificator $notificator
     * @param UserAccount $userAccount
     * @param UserNotification $userNotification
     */
    public function __construct(Notificator $notificator, UserAccount $userAccount, UserNotification $userNotification)
    {
        $this->notificator = $notificator;
        $this->userAccount = $userAccount;
        $this->userNotification = $userNotification;
    }

    /**
     * @return JsonResponse
     *
     * @OA\Get(
     *     path="/api/notification",
     *     tags={"Notification"},
     *     security={{"api_key": {}}},
     *     summary="Liste des notification d'un utilisateur",
     *      @OA\Response(
     *          response="200",
     *          description="Liste",
     *          @OA\JsonContent(
     *              type="object",
     *          )
     *      )
     * )
     */
    public function list()
    {
        try {
            $notifications = $this->userNotification->newQuery()->where('user_id', \request('user_id'))->get();
        }catch (Exception $exception) {
            return $this->sendError($exception->getMessage(), null, 500);
        }

        return $this->sendResponse($notifications, null, 200);
    }

    /**
     * Generate Front Notification
     *
     * @OA\Post(
     *     tags={"Notification"},
     *     path="/api/notification",
     *     security={{"api_key": {}}},
     *     summary="Poste d'une nouvelle notification",
     *     @OA\Response(
     *      response="200",
     *      description="NULL",
     *      @OA\JsonContent(
     *        type="null",
     *      )
     *     ),
     *     @OA\RequestBody(
     *          description="Request",
     *          required=true,
     *          @OA\MediaType(
     *              mediaType="application/json",
     *          ),
     *          @OA\MediaType(
     *              mediaType="application/x-www-form-urlencoded",
     *          )
     *     )
     * )
     */
    public function store()
    {
        if(\request()->get('admin') == true) {
            $this->notificator->publishAsAdmin(request()->get('title'), \request('status'), \request('description'), \request('icon'), \request('image'));
        } else {

            try {
                $user = $this->userAccount->newQuery()->where('customer_id', \request('customer_id'))->first();
            }catch (Exception $exception) {
                Log::error($exception);
            }

            $this->notificator->publish(
                \request('title'),
                \request('status'),
                \request('description'),
                \request('icon'),
                \request('image'),
                true,
                $user->user_id
            );
        }
    }
}
