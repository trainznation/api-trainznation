<?php

namespace App\Http\Controllers\Api\Front\Checkout;

use App\Achievements\UserFifteenPurchase;
use App\Achievements\UserFirstPurchase;
use App\Achievements\UserTenPurchase;
use App\Achievements\UserThirtyPurchase;
use App\Achievements\UserTwentyPurchase;
use App\Helpers\Notificator;
use App\Http\Controllers\Api\BaseController;
use App\Http\Controllers\Controller;
use App\Http\Resources\Admin\Checkout\OrderGet;
use App\Http\Resources\Admin\Checkout\OrderPaymentGet;
use App\Http\Resources\Admin\Checkout\OrderProductGet;
use App\Mail\Checkout\CreateOrder;
use App\Model\Order;
use App\Model\OrderProduct;
use App\Model\Payment;
use App\Model\UserDownload;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use OpenApi\Annotations as OA;

class CheckoutController extends BaseController
{
    /**
     * @var Order
     */
    private $order;
    /**
     * @var OrderProduct
     */
    private $product;
    /**
     * @var Payment
     */
    private $payment;
    /**
     * @var Notificator
     */
    private $notificator;
    /**
     * @var UserDownload
     */
    private $userDownload;

    /**
     * CheckoutController constructor.
     * @param Order $order
     * @param OrderProduct $product
     * @param Payment $payment
     * @param Notificator $notificator
     * @param UserDownload $userDownload
     */
    public function __construct(Order $order, OrderProduct $product, Payment $payment, Notificator $notificator, UserDownload $userDownload)
    {
        $this->order = $order;
        $this->product = $product;
        $this->payment = $payment;
        $this->notificator = $notificator;
        $this->userDownload = $userDownload;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     *
     * @OA\Post(
     *     tags={"Checkout"},
     *     path="/api/checkout",
     *     security={{"api_key": {}}},
     *     summary="Création d'une commande",
     *     @OA\Response(
     *      response="200",
     *      description="Retourne la commande",
     *      @OA\JsonContent(
     *        type="object",
     *      )
     *     ),
     *     @OA\RequestBody(
     *          description="Requete",
     *          required=true,
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Property(property="reference", example="00000000"),
     *              @OA\Property(property="total", example="12.30"),
     *              @OA\Property(property="user_id", example="1"),
     *          ),
     *          @OA\MediaType(
     *              mediaType="application/x-www-form-urlencoded",
     *              @OA\Property(property="reference", example="00000000"),
     *              @OA\Property(property="total", example="12.30"),
     *              @OA\Property(property="user_id", example="1"),
     *          )
     *     )
     * )
     */
    public function store(Request $request)
    {
        try {
            $order = $this->order->newQuery()->create([
                "reference" => $request->get('reference'),
                "total" => $request->get('total'),
                "user_id" => $request->get('user_id'),
                "status" => 0
            ]);
        }catch (Exception $exception) {
            Log::error($exception->getMessage());
            return $this->sendError($exception->getMessage());
        }

        $this->notificator->publishAsAdmin('Nouvelle commande', 'success', 'La commande N°'.$order->reference.' à été créer par '.$order->user->name, 'fas fa-shopping-cart');
        $this->notificator->publish('Nouvelle Commande', 'success', 'Votre commande N°'.$order->reference.' à été créer', 'fas fa-shopping-cart', null, true, auth()->user()->id);

        $format = new OrderGet();
        return $this->sendResponse($format->toArray($order), null, 200);
    }

    /**
     * @param Request $request
     * @param $order_id
     * @return JsonResponse
     *
     * @OA\Post(
     *     tags={"Checkout"},
     *     path="/api/checkout/{order_id}/payment",
     *     security={{"api_key": {}}},
     *     summary="Ajout d'un paiement pour une commande",
     *     @OA\Parameter(name="order_id", in="path", description="ID de la commande", required=true),
     *     @OA\Response(
     *      response="200",
     *      description="Information sur le paiement",
     *      @OA\JsonContent(
     *        type="object",
     *      )
     *     ),
     *     @OA\RequestBody(
     *          description="Requete",
     *          required=true,
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Property(property="payment_id", example="pid_4522102245")
     *          ),
     *          @OA\MediaType(
     *              mediaType="application/x-www-form-urlencoded",
     *              @OA\Property(property="payment_id", example="pid_4522102245")
     *          )
     *     )
     * )
     */
    public function payment(Request $request, $order_id)
    {
        try {
            $order = $this->order->newQuery()->findOrFail($order_id);
            $payment = $this->payment->newQuery()->create([
                "payment_id" => $request->get('payment_id'),
                "order_id" => $order_id
            ]);
            $order->update([
                "status" => 1
            ]);

            $this->addUserDownload($order->products);
        }catch (Exception $exception) {
            Log::error($exception->getMessage());
            return $this->sendError($exception->getMessage());
        }

        switch (count(auth()->user()->orders)) {
            case 1: auth()->user()->unlock(new UserFirstPurchase()); break;
            case 10: auth()->user()->unlock(new UserTenPurchase()); break;
            case 15: auth()->user()->unlock(new UserFifteenPurchase()); break;
            case 20: auth()->user()->unlock(new UserTwentyPurchase()); break;
            case 30: auth()->user()->unlock(new UserThirtyPurchase()); break;
        }

        $this->notificator->publishAsAdmin('Nouveau Paiement', 'success', 'Le paiement N°'.$payment->payment_id.' à été exécuter', 'fas fa-euro-sign');
        $this->notificator->publish('Nouveau Paiement', 'success', 'Le paiement de la commande N°'.$payment->order->reference.' à été exécuter avec succès', 'fas fa-euro-sign', null, true, auth()->user()->id);

        $format = new OrderPaymentGet();
        return $this->sendResponse($format->toArray($payment), null, 200);
    }

    /**
     * @param Request $request
     * @param $order_id
     * @return JsonResponse
     *
     * @OA\Post(
     *     tags={"Checkout"},
     *     path="/api/checkout/{order_id}/product",
     *     security={{"api_key": {}}},
     *     summary="Ajout d'un produit sur une commande",
     *     @OA\Parameter(name="order_id", in="path", description="ID de la commande", required=true),
     *     @OA\Response(
     *      response="200",
     *      description="NULL",
     *      @OA\JsonContent(
     *        type="object",
     *      )
     *     ),
     *     @OA\RequestBody(
     *          description="",
     *          required=true,
     *          @OA\MediaType(
     *              mediaType="application/json",
     *
     *          ),
     *          @OA\MediaType(
     *              mediaType="application/x-www-form-urlencoded",
     *
     *          )
     *     )
     * )
     */
    public function product(Request $request, $order_id)
    {
        try {
            $product = $this->product->newQuery()->create([
                "name" => $request->get('name'),
                "total_price_gross" => $request->get('total_price_gross'),
                "quantity" => $request->get('quantity'),
                "order_id" => $order_id,
                "asset_id" => $request->get('asset_id')
            ]);
        }catch (Exception $exception) {
            return $this->sendError($exception->getMessage());
        }

        $format = new OrderProductGet();
        return $this->sendResponse($format->toArray($product), null, 200);
    }

    /**
     * @param $order_id
     * @return JsonResponse
     *
     * @OA\Get(
     *     path="/api/checkout/{order_id}",
     *     tags={"Checkout"},
     *     security={{"api_key": {}}},
     *     summary="Information sur une facture",
     *      @OA\Parameter(name="order_id", in="path", description="ID de la commande", required=true),
     *      @OA\Response(
     *          response="200",
     *          description="Information",
     *          @OA\JsonContent(
     *              type="object",
     *          )
     *      )
     * )
     */
    public function get($order_id)
    {
        try {
            $order = $this->order->newQuery()->findOrFail($order_id);
        }catch (Exception $exception) {
            return $this->sendError($exception->getMessage());
        }

        $format = new OrderGet(true);
        return $this->sendResponse($format->toArray($order), null, 200);
    }

    private function addUserDownload($products)
    {
        foreach ($products as $product) {
            try {
                $this->userDownload->newQuery()->create([
                    "asset_id" => $product->asset->id,
                    "user_id" => $product->order->user_id,
                    "unlocked_at" => now()
                ]);
            }catch (Exception $exception) {
                return $exception;
            }
        }

        return null;
    }
}
