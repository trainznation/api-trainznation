<?php

namespace App\Http\Controllers\Api\Front\Cart;

use App\Http\Controllers\Api\BaseController;
use App\Http\Controllers\Controller;
use App\Model\Asset;
use Darryldecode\Cart\Cart;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use OpenApi\Annotations as OA;

class CartController extends BaseController
{
    /**
     * @var Cart
     */
    private $cart;
    /**
     * @var Asset
     */
    private $asset;

    /**
     * CartController constructor.
     * @param Cart $cart
     * @param Asset $asset
     */
    public function __construct(Cart $cart, Asset $asset)
    {
        $this->cart = $cart;
        $this->asset = $asset;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     *
     * @OA\Post(
     *     tags={"Cart"},
     *     path="/api/cart",
     *     security={{"api_key": {}}},
     *     summary="Création d'un panier",
     *     @OA\Response(
     *      response="200",
     *      description="NULL",
     *      @OA\JsonContent(
     *        type="string",
     *      )
     *     ),
     *     @OA\RequestBody(
     *          description="REQUETE",
     *          required=true,
     *          @OA\MediaType(
     *              mediaType="application/json",
     *          ),
     *          @OA\MediaType(
     *              mediaType="application/x-www-form-urlencoded",
     *          )
     *     )
     * )
     */
    public function store(Request $request)
    {
        try {
            $asset = $this->asset->newQuery()->findOrFail($request->get('id'));
            $this->cart->add([
                "id" => $asset->id,
                "name" => $asset->designation,
                "price" => $asset->price,
                "quantity" => 1,
                "attributes" => [],
                "associatedModel" => $asset
            ]);
        }catch (Exception $exception) {
            return $this->sendError($exception->getMessage(), null, 500);
        }

        return $this->sendResponse(['cartCount' => $this->cart->getTotalQuantity(), 'cartTotal' => $this->cart->getTotal()], null, 200);
    }
}
