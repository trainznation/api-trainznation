<?php

namespace App\Http\Controllers\Api\Front\Tutoriel;

use App\Http\Controllers\Api\BaseController;
use App\Http\Controllers\Controller;
use App\Http\Resources\Admin\Tutoriel\TutorielCategoryList;
use App\Model\TutorielCategory;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use OpenApi\Annotations as OA;

class TutorielCategoryController extends BaseController
{
    /**
     * @var TutorielCategory
     */
    private $category;

    /**
     * TutorielCategoryController constructor.
     * @param TutorielCategory $category
     */
    public function __construct(TutorielCategory $category)
    {
        $this->category = $category;
    }

    /**
     * @return JsonResponse
     *
     * @OA\Get(
     *     path="/api/tutoriel/category/list",
     *     tags={"Tutoriel"},
     *     security={{"api_key": {}}},
     *     summary="Liste des catégories de tutoriel",
     *      @OA\Response(
     *          response="200",
     *          description="Tableau catégorie formater",
     *          @OA\JsonContent(
     *              type="array",
     *              @OA\Items(ref="#/components/schemas/AdminTutorielCategoryList")
     *          )
     *      )
     * )
     */
    public function list()
    {
        try {
            $lists = $this->category->newQuery()->get();
        }catch (Exception $exception) {
            return $this->sendError($exception->getMessage(), null, 510);
        }

        $format = new TutorielCategoryList();
        return $this->sendResponse($format->toArray($lists), null, 200);
    }
}
