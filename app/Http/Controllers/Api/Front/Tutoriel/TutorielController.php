<?php

namespace App\Http\Controllers\Api\Front\Tutoriel;

use App\Http\Controllers\Api\BaseController;
use App\Http\Controllers\Controller;
use App\Http\Resources\Admin\Tutoriel\TutorielGet;
use App\Http\Resources\Admin\Tutoriel\TutorielList;
use App\Model\Tutoriel;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class TutorielController extends BaseController
{
    /**
     * @var Tutoriel
     */
    private $tutoriel;

    /**
     * TutorielController constructor.
     * @param Tutoriel $tutoriel
     */
    public function __construct(Tutoriel $tutoriel)
    {
        $this->tutoriel = $tutoriel;
    }

    /**
     * @return JsonResponse
     *
     * @OA\Post(
     *     tags={"Tutoriel"},
     *     path="/api/tutoriel/search",
     *     security={{"api_key": {}}},
     *     summary="Recherche d'un ou plusieurs tutoriels par terme",
     *     @OA\Response(
     *      response="200",
     *      description="Tableau des tutoriels",
     *      @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref="#/components/schemas/AdminTutorielList")
     *      )
     *     ),
     *     @OA\RequestBody(
     *          description="Requete",
     *          required=true,
     *          @OA\MediaType(
     *              mediaType="application/json",
     *          ),
     *          @OA\MediaType(
     *              mediaType="application/x-www-form-urlencoded",
     *          )
     *     )
     * )
     */
    public function search()
    {
        try {
            $tutoriels = $this->tutoriel->newQuery()->where('title', 'LIKE', '%'.\request('q').'%')
                ->where('published', 1)
                ->orderBy('published_at', 'desc')
                ->get();
        }catch (Exception $exception) {
            return $this->sendError($exception->getMessage(), null, 500);
        }

        $format = new TutorielList();
        return $this->sendResponse($format->toArray($tutoriels), null, 200);
    }

    /**
     * @return JsonResponse
     *
     * @OA\Get(
     *     path="/api/tutoriel/latest",
     *     tags={"Tutoriel"},
     *     security={{"api_key": {}}},
     *     summary="Liste des derniers tutoriel",
     *      @OA\Response(
     *          response="200",
     *          description="Tableau des tutoriel",
     *          @OA\JsonContent(
     *              type="array",
     *              @OA\Items(ref="#/components/schemas/AdminTutorielList")
     *          )
     *      )
     * )
     */
    public function latest()
    {
        try {
            $latests = $this->tutoriel->newQuery()->where('published', 1)->orderBy('published_at', 'desc')->limit(5)->get();
        }catch (Exception $exception) {
            return $this->sendError($exception->getMessage(), null, 510);
        }

        $format = new TutorielList();
        return $this->sendResponse($format->toArray($latests), null, 200);
    }

    /**
     * @return JsonResponse
     *
     * @OA\Get(
     *     path="/api/tutoriel/list",
     *     tags={"Tutoriel"},
     *     security={{"api_key": {}}},
     *     summary="Liste de tous les tutoriels publier",
     *      @OA\Response(
     *          response="200",
     *          description="Tableau tuto formater",
     *          @OA\JsonContent(
     *              type="array",
     *              @OA\Items(ref="#/components/schemas/AdminTutorielList")
     *          )
     *      )
     * )
     */
    public function list()
    {
        try {
            $lists = $this->tutoriel->newQuery()->where('published', 1)->orderBy('published_at', 'desc')->get();
        }catch (Exception $exception) {
            return $this->sendError($exception->getMessage(), null, 510);
        }

        $format = new TutorielList();
        return $this->sendResponse($format->toArray($lists), null, 200);
    }

    /**
     * @param $tutoriel_id
     * @return JsonResponse
     *
     * @OA\Get(
     *     path="/api/tutoriel/{tutoriel_id}",
     *     tags={"Tutoriel"},
     *     security={{"api_key": {}}},
     *     summary="Information tutoriel",
     *      @OA\Parameter(name="tutoriel_id", in="path", description="ID du tutoriel", required=true),
     *      @OA\Response(
     *          response="200",
     *          description="Tableau des infos",
     *          @OA\JsonContent(
     *              type="array",
     *              @OA\Items(ref="#/components/schemas/AdminTutorielGet")
     *          )
     *      )
     * )
     */
    public function get($tutoriel_id)
    {
        try {
            $tutoriel = $this->tutoriel->newQuery()->findOrFail($tutoriel_id);
        }catch (Exception $exception) {
            return $this->sendError($exception->getMessage(), null, 510);
        }

        $format = new TutorielGet();
        return $this->sendResponse($format->toArray($tutoriel), null, 200);
    }
}
