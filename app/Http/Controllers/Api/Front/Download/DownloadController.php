<?php

namespace App\Http\Controllers\Api\Front\Download;

use App\Http\Controllers\Api\BaseController;
use App\Http\Controllers\Controller;
use App\Http\Resources\Admin\Asset\AssetGet;
use App\Http\Resources\Admin\Asset\AssetsList;
use App\Model\Asset;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use OpenApi\Annotations as OA;

class DownloadController extends BaseController
{
    /**
     * @var Asset
     */
    private $asset;

    /**
     * DownloadController constructor.
     * @param Asset $asset
     */
    public function __construct(Asset $asset)
    {
        $this->asset = $asset;
    }

    /**
     * @return JsonResponse
     *
     * @OA\Post(
     *     tags={"Download"},
     *     path="/api/download/search",
     *     security={{"api_key": {}}},
     *     summary="Recherche d'un ou plusieurs assets",
     *     @OA\Response(
     *      response="200",
     *      description="Tableau des objets",
     *      @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref="#/components/schemas/AdminAssets")
     *      )
     *     ),
     *     @OA\RequestBody(
     *          description="REQUETE",
     *          required=true,
     *          @OA\MediaType(
     *              mediaType="application/json",
     *          ),
     *          @OA\MediaType(
     *              mediaType="application/x-www-form-urlencoded",
     *          )
     *     )
     * )
     */
    public function search()
    {
        try {
            $assets = $this->asset->newQuery()->where('designation', 'LIKE', '%'.\request('q').'%')
                ->where('published', 1)
                ->orderBy('published_at', 'desc')
                ->get();
        }catch (Exception $exception) {
            return $this->sendError($exception->getMessage(), null, 200);
        }

        $format = new AssetsList();
        return $this->sendResponse($format->toArray($assets), null, 200);
    }

    /**
     * @return JsonResponse
     *
     * @OA\Get(
     *     path="/api/download/list",
     *     tags={"Download"},
     *     security={{"api_key": {}}},
     *     summary="Liste des téléchargements disponible",
     *      @OA\Response(
     *          response="200",
     *          description="Tableau",
     *          @OA\JsonContent(
     *              type="array",
     *              @OA\Items(ref="#/components/schemas/AdminAssets")
     *          )
     *      )
     * )
     */
    public function list()
    {
        try {
            $lists = $this->asset->newQuery()->where('published', 1)->orderBy('published_at', 'desc')->get();
        }catch (Exception $exception) {
            return $this->sendError($exception->getMessage(), null, 510);
        }

        $format = new AssetsList();
        return  $this->sendResponse($format->toArray($lists), null, 200);
    }

    /**
     * @param $download_id
     * @return JsonResponse
     *
     * @OA\Get(
     *     path="/api/download/{download_id}",
     *     tags={"Download"},
     *     security={{"api_key": {}}},
     *     summary="Information sur Asset",
     *      @OA\Parameter(name="download_id", in="path", description="ID de l'asset", required=true),
     *      @OA\Response(
     *          response="200",
     *          description="Tableau",
     *          @OA\JsonContent(
     *              type="array",
     *              @OA\Items(ref="#/components/schemas/AdminAssetGet")
     *          )
     *      )
     * )
     */
    public function get($download_id)
    {
        try {
            $asset = $this->asset->newQuery()->find($download_id);
        }catch (Exception $exception) {
            return $this->sendError($exception->getMessage());
        }

        $format = new AssetGet();
        return  $this->sendResponse($format->toArray($asset), null, 200);
    }
}
