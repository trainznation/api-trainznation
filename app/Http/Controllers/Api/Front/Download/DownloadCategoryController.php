<?php

namespace App\Http\Controllers\Api\Front\Download;

use App\Http\Controllers\Api\BaseController;
use App\Http\Controllers\Controller;
use App\Http\Resources\Admin\Asset\AssetCategoryList;
use App\Model\AssetCategory;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use OpenApi\Annotations as OA;

class DownloadCategoryController extends BaseController
{
    /**
     * @var AssetCategory
     */
    private $category;

    /**
     * DownloadCategoryController constructor.
     * @param AssetCategory $category
     */
    public function __construct(AssetCategory $category)
    {
        $this->category = $category;
    }

    /**
     * @return JsonResponse
     *
     * @OA\Get(
     *     path="/api/download/category/list",
     *     tags={"Download"},
     *     security={{"api_key": {}}},
     *     summary="Liste des catégories",
     *      @OA\Response(
     *          response="200",
     *          description="Tableau",
     *          @OA\JsonContent(
     *              type="array",
     *              @OA\Items(ref="#/components/schemas/AdminAssetCategories")
     *          )
     *      )
     * )
     */
    public function list()
    {
        try {
            $lists = $this->category->newQuery()->get();
        }catch (Exception $exception) {
            return $this->sendError($exception->getMessage(), null, 510);
        }

        $format = new AssetCategoryList();
        return $this->sendResponse($format->toArray($lists), null, 200);
    }
}
