<?php

namespace App\Http\Controllers\Api\Front\Assets;

use App\Http\Controllers\Api\BaseController;
use App\Http\Controllers\Controller;
use App\Http\Resources\Admin\Asset\AssetsList;
use App\Model\Asset;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class AssetsController extends BaseController
{
    /**
     * @var Asset
     */
    private $asset;

    /**
     * AssetsController constructor.
     * @param Asset $asset
     */
    public function __construct(Asset $asset)
    {
        $this->asset = $asset;
    }

    /**
     * @return JsonResponse
     *
     * @OA\Get(
     *     path="/api/assets/latest",
     *     tags={"Assets"},
     *     security={{"api_key": {}}},
     *     summary="Liste des derniers téléchargements disponible",
     *      @OA\Response(
     *          response="200",
     *          description="Tableau des objets",
     *          @OA\JsonContent(
     *              type="array",
     *              @OA\Items(ref="#/components/schemas/AdminAssets")
     *          )
     *      )
     * )
     */
    public function latest()
    {
        try {
            $latest = $this->asset->newQuery()->where('published', 1)->orderBy('published_at', 'desc')->limit(5)->get();
        }catch (Exception $exception) {
            return $this->sendError($exception->getMessage(), null, 510);
        }

        $format = new AssetsList();
        return $this->sendResponse($format->toArray($latest), null, 200);
    }
}
