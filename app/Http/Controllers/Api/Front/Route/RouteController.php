<?php

namespace App\Http\Controllers\Api\Front\Route;

use App\Http\Controllers\Api\BaseController;
use App\Http\Controllers\Controller;
use App\Http\Resources\Admin\Route\GalleryCategoryGet;
use App\Http\Resources\Admin\Route\RouteGet;
use App\Http\Resources\Admin\Route\RouteList;
use App\Model\Route;
use App\Model\RouteGalleryCategory;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use OpenApi\Annotations as OA;

class RouteController extends BaseController
{
    /**
     * @var Route
     */
    private $route;
    /**
     * @var RouteGalleryCategory
     */
    private $routeGalleryCategory;

    /**
     * RouteController constructor.
     * @param Route $route
     * @param RouteGalleryCategory $routeGalleryCategory
     */
    public function __construct(Route $route, RouteGalleryCategory $routeGalleryCategory)
    {
        $this->route = $route;
        $this->routeGalleryCategory = $routeGalleryCategory;
    }

    /**
     * @return JsonResponse
     *
     * @OA\Get(
     *     path="/api/route/list",
     *     tags={"Route"},
     *     security={{"api_key": {}}},
     *     summary="Liste des route publier",
     *      @OA\Response(
     *          response="200",
     *          description="Tableau des route",
     *          @OA\JsonContent(
     *              type="array",
     *              @OA\Items(ref="#/components/schemas/AdminRouteList")
     *          )
     *      )
     * )
     */
    public function list()
    {
        try {
            $route = $this->route->newQuery()->where('published', 1)->get();
        }catch (Exception $exception ) {
            return $this->sendError($exception->getMessage());
        }

        $format = new RouteList();
        return $this->sendResponse($format->toArray($route), null, 200);
    }

    /**
     * @param $route_id
     * @return JsonResponse
     *
     * @OA\Get(
     *     path="/api/route/{route_id}",
     *     tags={"Route"},
     *     security={{"api_key": {}}},
     *     summary="Fiche d'une route",
     *      @OA\Parameter(name="route_id", in="path", description="ID de la route", required=true),
     *      @OA\Response(
     *          response="200",
     *          description="Tableau de la route",
     *          @OA\JsonContent(
     *              type="array",
     *              @OA\Items(ref="#/components/schemas/AdminRouteGet")
     *          )
     *      )
     * )
     */
    public function get($route_id)
    {
        try {
            $route = $this->route->newQuery()->findOrFail($route_id);
        }catch (Exception $exception) {
            return $this->sendError($exception->getMessage());
        }

        $format = new RouteGet();
        return $this->sendResponse($format->toArray($route), null, 200);
    }

    /**
     * @param $route_id
     * @param $category_id
     * @return JsonResponse
     *
     * @OA\Get(
     *     path="/api/route/{route_id}/gallery/category/{category_id}",
     *     tags={"Route"},
     *     security={{"api_key": {}}},
     *     summary="Liste de la gallery",
     *      @OA\Parameter(name="route_id", in="path", description="ID de la route", required=true),
     *      @OA\Parameter(name="category_id", in="path", description="ID de la category", required=true),
     *      @OA\Response(
     *          response="200",
     *          description="Tableau",
     *          @OA\JsonContent(
     *              type="array",
     *              @OA\Items(ref="#/components/schemas/AdminRouteGalleryCategoryGet")
     *          )
     *      )
     * )
     */
    public function getGallery($route_id, $category_id)
    {
        try {
            $category = $this->routeGalleryCategory->newQuery()->findOrFail($category_id);
        }catch (Exception $exception) {
            return $this->sendError($exception->getMessage());
        }

        $format = new GalleryCategoryGet();
        return $this->sendResponse($format->toArray($category), null, 200);
    }
}
