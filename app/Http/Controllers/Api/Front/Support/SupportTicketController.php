<?php

namespace App\Http\Controllers\Api\Front\Support;

use App\Http\Controllers\Api\BaseController;
use App\Http\Controllers\Controller;
use App\Http\Requests\Support\SupportTicketRequest;
use App\Http\Resources\Admin\Support\SupportTicketGet;
use App\Http\Resources\Admin\Support\SupportTicketList;
use App\Model\SupportCategory;
use App\Model\SupportSector;
use App\Model\SupportTicket;
use App\Model\SupportTicketConv;
use App\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class SupportTicketController extends BaseController
{
    /**
     * @var SupportTicket
     */
    private $ticket;
    /**
     * @var SupportTicketConv
     */
    private $conv;
    /**
     * @var SupportCategory
     */
    private $category;
    /**
     * @var SupportSector
     */
    private $sector;

    /**
     * SupportTicketController constructor.
     * @param SupportTicket $ticket
     * @param SupportTicketConv $conv
     * @param SupportCategory $category
     * @param SupportSector $sector
     */
    public function __construct(SupportTicket $ticket, SupportTicketConv $conv, SupportCategory $category, SupportSector $sector)
    {
        $this->ticket = $ticket;
        $this->conv = $conv;
        $this->category = $category;
        $this->sector = $sector;
    }

    public function list()
    {
        try {
            $tickets = $this->ticket->newQuery()->where('requester_id', auth()->user()->id)->get();
        } catch (Exception $exception) {
            Log::error($exception);
            return $this->sendError($exception->getMessage());
        }

        $format = new SupportTicketList();
        return $this->sendResponse($format->toArray($tickets), null, 200);
    }

    public function store(SupportTicketRequest $request)
    {
        try {
            $user = User::all()->random()->where('admin', 1)->first();
            $ticket = $this->ticket->newQuery()->create([
                "subject" => $request->get('subject'),
                "support_category_id" => $request->get('support_category_id'),
                "support_sector_id" => $request->get('support_sector_id'),
                "support_ticket_priority_id" => $this->definingPriority($request->get('support_sector_id')),
                "support_ticket_status_id" => 1,
                "support_ticket_source_id" => $request->get('support_ticket_source_id'),
                "requester_id" => $request->get('requester_id'),
                "assign_to" => $user->id
            ]);

            $message = $this->conv->newQuery()->create([
                "message" => $request->get('message'),
                "support_ticket_id" => $ticket->id,
                "user_id" => $request->get('requester_id')
            ]);
        }catch (Exception $exception) {
            Log::error($exception);
            return $this->sendError($exception->getMessage());
        }

        $format = new SupportTicketGet();
        return $this->sendResponse($format->toArray($ticket), null, 200);
    }

    public function categoryList()
    {
        try {
            $categories = $this->category->newQuery()->get();
        }catch (Exception $exception) {
            return $this->sendError($exception->getMessage());
        }

        return $this->sendResponse($categories, null, 200);
    }

    public function sectorList($category_id)
    {
        try {
            $sectors = $this->sector->newQuery()->where('support_category_id', $category_id)->get();
        }catch (Exception $exception) {
            return $this->sendError($exception->getMessage());
        }

        return $this->sendResponse($sectors, null, 200);
    }

    private function definingPriority($support_sector_id)
    {
        switch ($support_sector_id)
        {
            case 1: return 2;
            case 2: return 1;
            case 3: return 1;
            case 4: return 3;
            case 5: return 1;
            case 6: return 1;
            case 7: return 3;
            case 8: return 4;
            case 9: return 3;
            case 10: return 2;
            case 11: return 2;
        }
    }
}
