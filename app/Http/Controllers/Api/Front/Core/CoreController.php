<?php

namespace App\Http\Controllers\Api\Front\Core;

use App\Http\Controllers\Api\BaseController;
use App\Http\Controllers\Controller;
use App\Http\Resources\Admin\Settings\AnnouncementGet;
use App\Http\Resources\Admin\Settings\AnnouncementList;
use App\Http\Resources\Admin\Settings\SlideshowList;
use App\Model\Announcement;
use App\Model\Slideshow;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class CoreController extends BaseController
{
    /**
     * @var Announcement
     */
    private $announcement;
    /**
     * @var Slideshow
     */
    private $slideshow;

    /**
     * CoreController constructor.
     * @param Announcement $announcement
     * @param Slideshow $slideshow
     */
    public function __construct(Announcement $announcement, Slideshow $slideshow)
    {
        $this->announcement = $announcement;
        $this->slideshow = $slideshow;
    }

    /**
     * @return JsonResponse
     *
     * @OA\Get(
     *     path="/api/core/announcement/list",
     *     tags={"Announcement"},
     *     security={{"api_key": {}}},
     *     summary="Liste des annonces",
     *      @OA\Response(
     *          response="200",
     *          description="Tableau des annonces",
     *          @OA\JsonContent(
     *              type="array",
     *              @OA\Items(ref="#/components/schemas/AdminAnnouncementList")
     *          )
     *      )
     * )
     */
    public function listAnnouncement()
    {
        try {
            $lists = $this->announcement->newQuery()->where('expiring_at', '>=', now())->orderBy('expiring_at', 'asc')->get();
        }catch (Exception $exception) {
            return $this->sendError($exception->getMessage(), null, 510);
        }

        $format = new AnnouncementList();
        return $this->sendResponse($format->toArray($lists), null, 200);
    }

    /**
     * @return JsonResponse
     *
     * @OA\Get(
     *     path="/api/core/slideshow/list",
     *     tags={"Slideshow"},
     *     security={{"api_key": {}}},
     *     summary="Liste des slides",
     *      @OA\Response(
     *          response="200",
     *          description="Tableau des slides",
     *          @OA\JsonContent(
     *              type="object",
     *          )
     *      )
     * )
     */
    public function listSlideshow()
    {
        try {
            $lists = $this->slideshow->newQuery()
                ->orderBy('updated_at', 'desc')
                ->get();
        }catch (Exception $exception) {
            return $this->sendError($exception->getMessage(), null, 500);
        }

        $format = new SlideshowList();
        return $this->sendResponse($format->toArray($lists), null, 200);
    }
}
