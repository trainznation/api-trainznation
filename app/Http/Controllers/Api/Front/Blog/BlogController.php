<?php

namespace App\Http\Controllers\Api\Front\Blog;

use App\Http\Controllers\Api\BaseController;
use App\Http\Controllers\Controller;
use App\Http\Requests\Blog\BlogCommentRequest;
use App\Http\Resources\Admin\Blog\BlogCommentGet;
use App\Http\Resources\Admin\Blog\BlogGet;
use App\Http\Resources\Admin\Blog\BlogList;
use App\Model\Blog;
use App\User;
use BeyondCode\Comments\Comment;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class BlogController extends BaseController
{
    /**
     * @var Blog
     */
    private $blog;

    /**
     * BlogController constructor.
     * @param Blog $blog
     */
    public function __construct(Blog $blog)
    {
        $this->blog = $blog;
    }

    /**
     * @return JsonResponse
     *
     * @OA\Post(
     *     tags={"Blog"},
     *     path="/api/blog/search",
     *     security={{"api_key": {}}},
     *     summary="Recherche d'un ou plusieurs articles",
     *     @OA\Response(
     *      response="200",
     *      description="Tableau des articles",
     *      @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref="#/components/schemas/AdminBlogList")
     *      )
     *     ),
     *     @OA\RequestBody(
     *          description="REQUETE",
     *          required=true,
     *          @OA\MediaType(
     *              mediaType="application/json",
     *          ),
     *          @OA\MediaType(
     *              mediaType="application/x-www-form-urlencoded",
     *          )
     *     )
     * )
     */
    public function search()
    {
        try {
            $blogs = $this->blog->newQuery()->where('title', 'LIKE', '%'.\request('q').'%')
                ->where('published', 1)
                ->orderBy('published_at', 'desc')
                ->get();
        }catch (Exception $exception) {
            return $this->sendError($exception->getMessage(), null, 500);
        }

        $format = new BlogList();
        return $this->sendResponse($format->toArray($blogs), null, 200);
    }

    /**
     * @return JsonResponse
     *
     * @OA\Get(
     *     path="/api/blog/latest",
     *     tags={"Blog"},
     *     security={{"api_key": {}}},
     *     summary="Liste des articles",
     *      @OA\Response(
     *          response="200",
     *          description="Tableau formater",
     *          @OA\JsonContent(
     *              type="array",
     *              @OA\Items(ref="#/components/schemas/AdminBlogList")
     *          )
     *      )
     * )
     */
    public function latest()
    {
        try {
            $blogs = $this->blog->newQuery()->where('published', 1)->orderBy('published_at', 'desc')->limit(5)->get();
        }catch (Exception $exception) {
            return $this->sendError($exception->getMessage(), null, 510);
        }

        $format = new BlogList();
        return  $this->sendResponse($format->toArray($blogs), null, 200);
    }

    /**
     * @return JsonResponse
     *
     * @OA\Get(
     *     path="/api/blog/list",
     *     tags={"Blog"},
     *     security={{"api_key": {}}},
     *     summary="Liste des articles publier (No Limit)",
     *      @OA\Response(
     *          response="200",
     *          description="Tableau des articles",
     *          @OA\JsonContent(
     *              type="array",
     *              @OA\Items(ref="#/components/schemas/AdminBlogList")
     *          )
     *      )
     * )
     */
    public function list()
    {
        try {
            $blogs = $this->blog->newQuery()->where('published', 1)->orderBy('published_at', 'desc')->get();
        }catch (Exception $exception) {
            return $this->sendError($exception->getMessage(), null, 510);
        }

        $format = new BlogList();
        return $this->sendResponse($format->toArray($blogs), null, 200);
    }

    /**
     * @param $blog_id
     * @return JsonResponse
     *
     * @OA\Get(
     *     path="/api/blog/{blog_id}",
     *     tags={"Blog"},
     *     security={{"api_key": {}}},
     *     summary="Information d'un article",
     *      @OA\Parameter(name="blog_id", in="path", description="ID de l'article", required=true),
     *      @OA\Response(
     *          response="200",
     *          description="Tableau de l'article choisie",
     *          @OA\JsonContent(
     *              type="array",
     *              @OA\Items(ref="#/components/schemas/AdminBlogGet")
     *          )
     *      )
     * )
     */
    public function get($blog_id)
    {
        try {
            $blog = $this->blog->newQuery()->findOrFail($blog_id);
        }catch (Exception $exception) {
            return $this->sendError($exception->getMessage());
        }

        $format = new BlogGet();
        return $this->sendResponse($format->toArray($blog), null, 200);
    }

    /**
     * @param BlogCommentRequest $request
     * @param $blog_id
     * @return JsonResponse
     *
     * @OA\Post(
     *     tags={"Blog"},
     *     path="/api/blog/{blog_id}/comment",
     *     security={{"api_key": {}}},
     *     summary="Poste d'un commentaire",
     *     @OA\Response(
     *      response="200",
     *      description="Commentaire créer",
     *      @OA\JsonContent(
     *        type="object",
     *      )
     *     ),
     *     @OA\RequestBody(
     *          description="Création d'un commentaire",
     *          required=true,
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(ref="#/components/schemas/BlogCommentRequest")
     *          ),
     *          @OA\MediaType(
     *              mediaType="application/x-www-form-urlencoded",
     *              @OA\Schema(ref="#/components/schemas/BlogCommentRequest")
     *          )
     *     )
     * )
     */
    public function postComment(BlogCommentRequest $request, $blog_id)
    {
        try {
            $comment = $this->blog->newQuery()->findOrFail($blog_id);
            $user = User::find($request->get('user_id'));
            $comment->commentAsUser($user, $request->get('comment'));
        }catch (Exception $exception) {
            return $this->sendError($exception->getMessage());
        }

        $last = $comment->comments()->approved()->orderBy('id', 'desc')->first();

        $format = new BlogCommentGet();
        return $this->sendResponse($format->toArray($last), null, 200);
    }

    /**
     * @param $blog_id
     * @param $comment_id
     * @return JsonResponse
     *
     * @OA\Delete(
     *     path="/api/blog/{blog_id}/comment/{comment_id}",
     *     tags={"Blog"},
     *     security={{"api_key": {}}},
     *     summary="Supprime un commentaire",
     *      @OA\Parameter(name="blog_id", in="path", description="ID de l'article", required=true),
     *      @OA\Parameter(name="comment_id", in="path", description="ID du commentaire", required=true),
     *      @OA\Response(
     *          response="200",
     *          description="Null",
     *      ),
     *      @OA\Response(
     *          response="510",
     *          description="Error",
     *          @OA\JsonContent(
     *              @OA\Property(property="Message", type="string", example="Exception")
     *          )
     *      )
     * )
     */
    public function deleteComment($blog_id, $comment_id)
    {
        try {
            $comment = Comment::find($comment_id);
            $comment->delete();
        }catch (Exception $exception) {
            return $this->sendError($exception->getMessage());
        }

        return $this->sendResponse(null, null, 200);
    }
}
