<?php

namespace App\Http\Controllers\Api\Admin\Route;

use App\Events\Route\RoutePublishDownloadEvent;
use App\Helpers\Notificator;
use App\Http\Controllers\Api\BaseController;
use App\Http\Controllers\Controller;
use App\Http\Requests\Route\Download\StoreRouteDownloadRequest;
use App\Http\Resources\Admin\Route\RouteDownloadGet;
use App\Http\Resources\Admin\Route\RouteDownloadList;
use App\Model\RouteDownload;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use OpenApi\Annotations as OA;

class RouteDownloadController extends BaseController
{
    /**
     * @var RouteDownload
     */
    private $download;
    /**
     * @var Notificator
     */
    private $notificator;

    /**
     * RouteDownloadController constructor.
     * @param RouteDownload $download
     * @param Notificator $notificator
     */
    public function __construct(RouteDownload $download, Notificator $notificator)
    {
        $this->download = $download;
        $this->notificator = $notificator;
    }

    /**
     * @param $route_id
     * @return JsonResponse
     *
     * @OA\Get(
     *     path="/api/admin/route/{route_id}/download/list",
     *     tags={"Admin Route Download"},
     *     security={{"api_key": {}}},
     *     summary="Liste des téléchargements d'une route",
     *      @OA\Parameter(name="route_id", in="path", description="ID de la route", required=true),
     *      @OA\Response(
     *          response="200",
     *          description="Retourne la liste des téléchargements",
     *          @OA\JsonContent(
     *              type="array",
     *              @OA\Items(ref="#/components/schemas/AdminRouteDownloadList")
     *          )
     *      )
     * )
     */
    public function list($route_id)
    {
        $downloads = $this->download->newQuery()->where('route_id', $route_id)->get();

        $format = new RouteDownloadList();
        return $this->sendResponse($format->toArray($downloads), null, 200);
    }

    /**
     * @param StoreRouteDownloadRequest $request
     * @param $route_id
     * @return JsonResponse
     *
     * @OA\Post(
     *     tags={"Admin Route Download"},
     *     path="/api/route/{route_id}/download",
     *     security={{"api_key": {}}},
     *     summary="Création d'un téléchargement de route",
     *     @OA\Parameter(name="route_id", in="path", description="ID de la route", required=true),
     *     @OA\Response(
     *      response="200",
     *      description="Retourne la route créer",
     *      @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref="#/components/schemas/AdminRouteDownloadGet")
     *      )
     *     ),
     *     @OA\RequestBody(
     *          description="Création d'un téléchargement",
     *          required=true,
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(ref="#/components/schemas/AdminRouteDownloadRequest")
     *          ),
     *          @OA\MediaType(
     *              mediaType="application/x-www-form-urlencoded",
     *              @OA\Schema(ref="#/components/schemas/AdminRouteDownloadRequest")
     *          )
     *     )
     * )
     */
    public function store(StoreRouteDownloadRequest $request, $route_id)
    {
        $download = $this->download->newQuery()->create([
            "route_id" => $route_id,
            "version" => $request->get('version'),
            "build" => $request->get('build'),
            "uuid" => Str::uuid(),
            "note" => $request->get('note'),
            "alpha" => $request->get('alpha'),
            "beta" => $request->get('beta'),
            "release" => $request->get('release'),
            "published" => $request->get('published'),
            "published_at" => ($request->get('published') == 1) ? now() : null
        ]);

        if ($request->get('published') == 1) {
            try {
                $this->publish($route_id, $download->id, 1);
            }catch (Exception $exception) {
                return $this->sendError($exception->getMessage(), null, 510);
            }
        } else {
            try {
                $this->publish($route_id, $download->id, 0);
            }catch (Exception $exception) {
                return $this->sendError($exception->getMessage(), null, 510);
            }
        }

        $format = new RouteDownloadGet();
        return $this->sendResponse($format->toArray($download), null, 200);
    }

    /**
     * @param $route_id
     * @param $download_id
     * @return JsonResponse
     *
     * @OA\Get(
     *     path="/api/admin/route/{route_id}/download/{download_id}",
     *     tags={"Admin Route Download"},
     *     security={{"api_key": {}}},
     *     summary="Affiche les informations d'un téléchargement",
     *      @OA\Parameter(name="route_id", in="path", description="ID de la routes", required=true),
     *      @OA\Parameter(name="download_id", in="path", description="ID du téléchargement", required=true),
     *      @OA\Response(
     *          response="200",
     *          description="Affiche le téléchargement",
     *          @OA\JsonContent(
     *              type="array",
     *              @OA\Items(ref="#/components/schemas/AdminRouteDownloadGet")
     *          )
     *      )
     * )
     */
    public function get($route_id, $download_id)
    {
        $download = $this->download->newQuery()->find($download_id);

        $format = new RouteDownloadGet();
        return $this->sendResponse($format->toArray($download), null, 200);
    }

    /**
     * @param $route_id
     * @param $download_id
     * @return JsonResponse
     *
     * @OA\Delete(
     *     path="/api/route/{route_id}/download/{download_id}",
     *     tags={"Admin Route Download"},
     *     security={{"api_key": {}}},
     *     summary="Supprime un téléchargement",
     *      @OA\Parameter(name="route_id", in="path", description="ID de la route", required=true),
     *      @OA\Parameter(name="download_id", in="path", description="ID du téléchargement", required=true),
     *      @OA\Response(
     *          response="200",
     *          description="Retourne un message",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Téléchargement supprimer")
     *          )
     *      ),
     *      @OA\Response(
     *          response="510",
     *          description="Retourne une exception",
     *          @OA\JsonContent(
     *              @OA\Property(property="error", type="string", example="Exception")
     *          )
     *      )
     * )
     */
    public function delete($route_id, $download_id)
    {
        $download = $this->download->newQuery()->find($download_id);
        try {
            $download->delete();
            return $this->sendResponse(["message" => "Ce téléchargement à été supprimer avec succès"], null, 200);
        }catch (Exception $exception) {
            return $this->sendError("Erreur Système", ["error" => $exception], 510);
        }
    }

    public function publish($route_id, $download_id, $requested)
    {
        $download = $this->download->newQuery()->findOrFail($download_id);
        try {
            $download->update([
                "published" => $requested,
                "published_at" => ($requested == 1) ? now() : null
            ]);

            if($requested == 1 && $download->published_at <= now()) {
                $this->notificator->publish("Route ".$download->route->name.": un nouveau téléchargement est disponible", "success", null, "fas fa-download");
            }

            return $download;
        }catch (Exception $exception) {
            return $exception;
        }
    }
}
