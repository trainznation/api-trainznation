<?php

namespace App\Http\Controllers\Api\Admin\Route;

use App\Http\Controllers\Api\BaseController;
use App\Http\Controllers\Controller;
use App\Http\Resources\Admin\Route\GalleryGet;
use App\Model\RouteGallery;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class RouteGalleryController extends BaseController
{
    /**
     * @var RouteGallery
     */
    private $gallery;

    /**
     * RouteGalleryController constructor.
     * @param RouteGallery $gallery
     */
    public function __construct(RouteGallery $gallery)
    {
        $this->gallery = $gallery;
    }

    /**
     * @param $route_id
     * @param $gallery_id
     * @return JsonResponse
     *
     * @OA\Get(
     *     path="/api/admin/{route_id}/gallery/{gallery_id}",
     *     tags={"Admin Route Gallery"},
     *     security={{"api_key": {}}},
     *     summary="Affiche les information d'une image",
     *      @OA\Parameter(name="route_id", in="path", description="ID de la route", required=true),
     *      @OA\Parameter(name="gallery_id", in="path", description="ID de l'image", required=true),
     *      @OA\Response(
     *          response="200",
     *          description="Info de l'image",
     *          @OA\JsonContent(
     *              type="array",
     *              @OA\Items(ref="#/components/schemas/AdminRouteGalleryGet")
     *          )
     *      )
     * )
     */
    public function get($route_id, $gallery_id)
    {
        $format = new GalleryGet();
        $gallery = $this->gallery->newQuery()->findOrFail($gallery_id);

        return $this->sendResponse($format->toArray($gallery), null, 200);
    }

    /**
     * @param $route_id
     * @param $gallery_id
     * @return JsonResponse
     *
     * @OA\Delete(
     *     path="/api/admin/route/{route_id}/gallery/{gallery_id}",
     *     tags={"Admin Route Gallery"},
     *     security={{"api_key": {}}},
     *     summary="Supprime une image de la base",
     *      @OA\Parameter(name="route_id", in="path", description="ID de la route", required=true),
     *      @OA\Parameter(name="gallery_id", in="path", description="ID de l'image", required=true),
     *      @OA\Response(
     *          response="200",
     *          description="Message",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="L'image à été supprimé")
     *          )
     *      ),
     *      @OA\Response(
     *          response="510",
     *          description="Exception",
     *          @OA\JsonContent(
     *              @OA\Property(property="error", type="string", example="Exception")
     *          )
     *      )
     * )
     */
    public function delete($route_id, $gallery_id)
    {
        $gallery = $this->gallery->newQuery()->findOrFail($gallery_id);
        try {
            $gallery->delete();
            return $this->sendResponse(["message" => "L'image à été supprimé"], null, 200);
        }catch (Exception $exception) {
            return $this->sendError("Erreur Système", ["error" => $exception], 510);
        }
    }

    /**
     * @param Request $request
     * @param $route_id
     * @return JsonResponse
     *
     * @OA\Post(
     *     tags={"Admin Route Gallery"},
     *     path="/api/admin/route/{route_id}/gallery/addFile",
     *     security={{"api_key": {}}},
     *     summary="Ajoute une image à la gallerie",
     *     @OA\Response(
     *      response="200",
     *      description="Retourne les informations de l'image ajouter",
     *      @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref="#/components/schemas/AdminRouteGalleryGet")
     *      )
     *     ),
     *     @OA\RequestBody(
     *          description="Création de l'image",
     *          required=true,
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(ref="#/components/schemas/AdminPostGalleryRequest")
     *          ),
     *          @OA\MediaType(
     *              mediaType="application/x-www-form-urlencoded",
     *              @OA\Schema(ref="#/components/schemas/AdminPostGalleryRequest")
     *          )
     *     )
     * )
     */
    public function addFile(Request $request, $route_id)
    {
        $format = new GalleryGet();
        try {
            $gallery = $this->gallery->newQuery()->create([
                "route_id" => $route_id,
                "route_gallery_category_id" => $request->get('category_id')
            ]);
            return $this->sendResponse($format->toArray($gallery), null, 200);
        }catch (Exception $exception) {
            return $this->sendError("Erreur Système", ["error" => $exception], 510);
        }
    }
}
