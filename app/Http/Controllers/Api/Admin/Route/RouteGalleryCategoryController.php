<?php

namespace App\Http\Controllers\Api\Admin\Route;

use App\Http\Controllers\Api\BaseController;
use App\Http\Controllers\Controller;
use App\Http\Requests\Route\Gallery\StoreGalleryCategoryRequest;
use App\Http\Resources\Admin\Route\GalleryCategoryGet;
use App\Http\Resources\Admin\Route\GalleryCategoryList;
use App\Model\RouteGalleryCategory;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class RouteGalleryCategoryController extends BaseController
{
    /**
     * @var RouteGalleryCategory
     */
    private $galleryCategory;

    /**
     * RouteGalleryCategoryController constructor.
     * @param RouteGalleryCategory $galleryCategory
     */
    public function __construct(RouteGalleryCategory $galleryCategory)
    {
        $this->galleryCategory = $galleryCategory;
    }

    /**
     * @param Request $request
     * @param $route_id
     * @return JsonResponse
     *
     * @OA\Get(
     *     path="/api/admin/route/{route_id}/gallery/category/list",
     *     tags={"Admin Route Gallery"},
     *     security={{"api_key": {}}},
     *     summary="Affiche les catégories de la gallerie et les images correspondante",
     *      @OA\Parameter(name="route_id", in="path", description="ID de la route", required=true),
     *      @OA\Parameter(in="query", name="q", description="Terme de la recherche", required=false),
     *      @OA\Response(
     *          response="200",
     *          description="Affiche les éléments",
     *          @OA\JsonContent(
     *              type="array",
     *              @OA\Items(ref="#/components/schemas/AdminRouteGalleryCategoryList")
     *          )
     *      )
     * )
     */
    public function list(Request $request, $route_id)
    {
        $format = new GalleryCategoryList();
        if($request->has('q') == true) {
            $categories = $this->galleryCategory->newQuery()->where('route_id', $route_id)->where('name', 'LIKE', '%'.$request->get('q').'%')->get();
        } else {
            $categories = $this->galleryCategory->newQuery()->where('route_id', $route_id)->get();
        }

        return $this->sendResponse($format->toArray($categories), null, 200);
    }

    /**
     * @param StoreGalleryCategoryRequest $request
     * @param $route_id
     * @return JsonResponse
     *
     * @OA\Post(
     *     tags={"Admin Route Gallery"},
     *     path="/api/admin/route/{route_id}/gallery/category",
     *     security={{"api_key": {}}},
     *     summary="Création d'une catégorie",
     *     @OA\Response(
     *      response="200",
     *      description="Retourne la catégorie créer",
     *      @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref="#/components/schemas/AdminRouteGalleryCategoryGet")
     *      )
     *     ),
     *     @OA\RequestBody(
     *          description="Création de la catégorie",
     *          required=true,
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(ref="#/components/schemas/AdminPostGalleryCategoryRequest")
     *          ),
     *          @OA\MediaType(
     *              mediaType="application/x-www-form-urlencoded",
     *              @OA\Schema(ref="#/components/schemas/AdminPostGalleryCategoryRequest")
     *          )
     *     )
     * )
     */
    public function store(StoreGalleryCategoryRequest $request, $route_id)
    {
        $format = new GalleryCategoryGet();
        try {
            $category = $this->galleryCategory->newQuery()->create([
                "route_id" => $route_id,
                "name" => $request->get('name')
            ]);
        }catch (Exception $exception) {
            return $this->sendError("Erreru Système", ["error" => $exception], 500);
        }

        return $this->sendResponse($format->toArray($category), null, 200);
    }

    /**
     * @param $route_id
     * @param $category_id
     * @return JsonResponse
     *
     * @OA\Get(
     *     path="/api/admin/route/{route_id}/gallery/category/{category_id}",
     *     tags={"Admin Route Gallery"},
     *     security={{"api_key": {}}},
     *     summary="Retourne les information de la catégorie",
     *      @OA\Parameter(name="route_id", in="path", description="ID de la route", required=true),
     *      @OA\Parameter(name="category_id", in="path", description="ID de la catégorie", required=true),
     *      @OA\Response(
     *          response="200",
     *          description="",
     *          @OA\JsonContent(
     *              type="array",
     *              @OA\Items(ref="#/components/schemas/AdminRouteGalleryCategoryGet")
     *          )
     *      )
     * )
     */
    public function get($route_id, $category_id)
    {
        $format = new GalleryCategoryGet();
        $category = $this->galleryCategory->newQuery()->find($category_id);

        return $this->sendResponse($format->toArray($category), null, 200);
    }
}
