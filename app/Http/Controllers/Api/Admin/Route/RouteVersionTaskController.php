<?php

namespace App\Http\Controllers\Api\Admin\Route;

use App\Http\Controllers\Api\BaseController;
use App\Http\Controllers\Controller;
use App\Http\Requests\Route\Version\StoreRouteVersionTaskRequest;
use App\Http\Resources\Admin\Route\VersionTaskGet;
use App\Http\Resources\Admin\Route\VersionTaskList;
use App\Model\Route;
use App\Model\RouteVersion;
use App\Model\RouteVersionTask;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class RouteVersionTaskController extends BaseController
{
    /**
     * @var RouteVersionTask
     */
    private $versionTask;

    /**
     * RouteVersionTaskController constructor.
     * @param RouteVersionTask $versionTask
     */
    public function __construct(RouteVersionTask $versionTask)
    {
        $this->versionTask = $versionTask;
    }

    /**
     * @param $route_id
     * @param $version_id
     * @return JsonResponse
     *
     * @OA\Get(
     *     path="/api/admin/route/{route_id}/version/{version_id}/tasks/list",
     *     tags={"Admin Route Task"},
     *     security={{"api_key": {}}},
     *     summary="Liste des taches de la version de la route selectionner",
     *      @OA\Parameter(name="route_id", in="path", description="ID de la route", required=true),
     *      @OA\Parameter(name="route_version_id", in="path", description="ID de la version", required=true),
     *      @OA\Response(
     *          response="200",
     *          description="Retourne la liste des taches",
     *          @OA\JsonContent(
     *              type="object",
     *          )
     *      )
     * )
     */

    public function list($route_id, $version_id)
    {
        $format = new VersionTaskList();

        if (\request()->has('q') == true) {
            $lists = $this->versionTask->newQuery()->where('route_version_id', $version_id)
                ->where('name', 'LIKE', '%' . \request()->get('q') . '%')
                ->where('description', 'LIKE', '%' . \request()->get('q') . '%')
                ->get();
        } else {
            $lists = $this->versionTask->newQuery()->where('route_version_id', $version_id)
                ->get();
        }

        return $this->sendResponse($format->toArray($lists), null, 200);
    }

    /**
     * @param StoreRouteVersionTaskRequest $request
     * @param $route_id
     * @param $version_id
     * @return JsonResponse
     *
     * @OA\Post(
     *     tags={"Admin Route Task"},
     *     path="/api/admin/route/{route_id}/version/{version_id}/tasks",
     *     security={{"api_key": {}}},
     *     summary="Création d'une tache",
     *     @OA\Response(
     *      response="200",
     *      description="Tableau de la tache créer",
     *      @OA\JsonContent(
     *        type="object",
     *      )
     *     ),
     *     @OA\RequestBody(
     *          description="",
     *          required=true,
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(ref="#/components/schemas/AdminPostRouteVersionTask")
     *          ),
     *          @OA\MediaType(
     *              mediaType="application/x-www-form-urlencoded",
     *              @OA\Schema(ref="#/components/schemas/AdminPostRouteVersionTask")
     *          )
     *     )
     * )
     */
    public function store(StoreRouteVersionTaskRequest $request, $route_id, $version_id)
    {
        $format = new VersionTaskGet();
        try {
            $config = config('task');
            $task = $this->versionTask->newQuery()->create([
                "name" => $request->get("name"),
                "description" => $request->get('description'),
                "lieu" => $request->get('lieu'),
                "etat" => $request->get('etat'),
                "priority" => $request->get('priority'),
                "started_at" => $request->get('started_at'),
                "finished_at" => $request->get('finished_at'),
                "route_version_id" => $request->get('route_version_id'),
                "route_id" => $request->get('route_id')
            ]);

            try {
                switch ($request->get('etat')) {
                    case 0:
                        $this->IncreaseTotalTask($version_id);
                        $this->IncreaseRegisteredTask($version_id);
                        break;

                    case 1:
                        $this->IncreaseTotalTask($version_id);
                        $this->IncreaseProgressTask($version_id);
                        break;

                    case 2:
                        $this->IncreaseTotalTask($version_id);
                        $this->IncreaseFinishTask($version_id);
                        $this->IncreaseBuildNumber($version_id, $config['priority'][$request->get('priority')]['point']);
                        $this->updatePercentTask($version_id);
                        break;

                    default: return $this->sendError("Erreur", ["error" => "Erreur de mise à jour d'information de build"], 520);
                }
            }catch (Exception $exception) {
                return $this->sendError($exception->getMessage());
            }
        } catch (Exception $exception) {
            return $this->sendError($exception->getMessage());
        }

        return $this->sendResponse($format->toArray($task), null, 200);
    }

    /**
     * @param $route_id
     * @param $version_id
     * @param $task_id
     * @return JsonResponse
     *
     * @OA\Get(
     *     path="/api/admin/route/{route_id}/version/{version_id}/tasks/{task_id}",
     *     tags={"Admin Route Task"},
     *     security={{"api_key": {}}},
     *     summary="Affiche les informations d'une tache",
     *      @OA\Parameter(name="category_id", in="path", description="ID de la catégorie", required=true),
     *      @OA\Response(
     *          response="200",
     *          description="Tableau des Informations",
     *          @OA\JsonContent(
     *              type="object",
     *          )
     *      )
     * )
     */
    public function get($route_id, $version_id, $task_id)
    {
        try {
            $task = $this->versionTask->newQuery()->findOrFail($task_id);
        }catch (Exception $exception) {
            return $this->sendError($exception->getMessage());
        }

        $format = new VersionTaskGet();
        return $this->sendResponse($format->toArray($task), null, 200);
    }

    /**
     * @param Request $request
     * @param $route_id
     * @param $version_id
     * @param $task_id
     * @return JsonResponse
     *
     * @OA\Put(
     *     tags={"Admin Route Task"},
     *     path="/api/admin/route/{route_id}/version/{version_id}/tasks/{task_id}",
     *     security={{"api_key": {}}},
     *     summary="Met à jour une tache",
     *     @OA\Parameter(name="route_id", in="path", description="ID de la route", required=true),
     *     @OA\Response(
     *      response="200",
     *      description="Tableau de la tache modifier",
     *      @OA\JsonContent(
     *        type="object",
     *      )
     *     ),
     *     @OA\RequestBody(
     *          description="Info pour la modification",
     *          required=true,
     *          @OA\MediaType(
     *              mediaType="application/json",
     *          ),
     *          @OA\MediaType(
     *              mediaType="application/x-www-form-urlencoded",
     *          )
     *     )
     * )
     */
    public function update(Request $request, $route_id, $version_id, $task_id)
    {
        //dd($request->all());
        $config = config('task');
        $format = new VersionTaskGet();
        try {
            $task = $this->versionTask->newQuery()->findOrFail($task_id);
        }catch (Exception $exception) {
            return $this->sendError($exception->getMessage());
        }

        if($request->get('etat') != $task->etat) {
            Log::debug("Decrease Systeme task");
            switch ($task->etat) {
                case 0:
                    $this->DecreaseTotalTask($version_id);
                    $this->DecreaseRegisteredTask($version_id);
                    break;

                case 1:
                    $this->DecreaseTotalTask($version_id);
                    $this->DecreaseProgressTask($version_id);
                    break;

                case 2:
                    $this->DecreaseTotalTask($version_id);
                    $this->DecreaseFinishTask($version_id);
                    $this->DecreaseBuildNumber($version_id, $config['priority'][$task->priority]['point']);
                    $this->updatePercentTask($version_id);
                    break;

                default:Log::error("Erreur de mise à jour d'information de build"); return $this->sendError("Erreur", ["error" => "Erreur de mise à jour d'information de build"], 520);
            }

            Log::debug("Increase Systeme task");
            switch ($request->get('etat')) {
                case 0:
                    $this->IncreaseTotalTask($version_id);
                    $this->IncreaseRegisteredTask($version_id);
                    break;

                case 1:
                    $this->IncreaseTotalTask($version_id);
                    $this->IncreaseProgressTask($version_id);
                    break;

                case 2:
                    $this->IncreaseTotalTask($version_id);
                    $this->IncreaseFinishTask($version_id);
                    $this->IncreaseBuildNumber($version_id, $config['priority'][$request->get('priority')]['point']);
                    $this->updatePercentTask($version_id);
                    break;

                default:Log::error("Erreur de mise à jour d'information de build"); return $this->sendError("Erreur", ["error" => "Erreur de mise à jour d'information de build"], 520);
            }
        }

        try {
            $task->update([
                "name" => $request->get("name"),
                "description" => $request->get('description'),
                "lieu" => $request->get('lieu'),
                "etat" => $request->get('etat'),
                "priority" => $request->get('priority'),
                "started_at" => $request->get('started_at'),
                "finished_at" => $request->get('finished_at'),
                "route_version_id" => $request->get('route_version_id'),
                "route_id" => $request->get('route_id')
            ]);
        }catch (Exception $exception) {
            Log::error($exception->getMessage());
            return $this->sendError($exception->getMessage());
        }
        Log::debug("Mise à jour de la tache Terminer");
        return $this->sendResponse($format->toArray($task), null, 200);
    }

    /**
     * @param $route_id
     * @param $version_id
     * @param $task_id
     * @return JsonResponse
     *
     * @OA\Delete(
     *     path="/api/admin/route/{route_id}/version/{version_id}/tasks/{task_id}",
     *     tags={"Admin Route Task"},
     *     security={{"api_key": {}}},
     *     summary="Supprime une tache",
     *      @OA\Parameter(name="route_id", in="path", description="ID de la route", required=true),
     *      @OA\Parameter(name="version_id", in="path", description="ID de la version", required=true),
     *      @OA\Parameter(name="task_id", in="path", description="ID de la tache", required=true),
     *      @OA\Response(
     *          response="200",
     *          description="NULL",
     *          @OA\JsonContent(
     *              @OA\Property(property="NULL", type="string", example="NULL")
     *          )
     *      ),
     * )
     */
    public function delete($route_id, $version_id, $task_id)
    {
        $config = config('task');
        try {
            $task = $this->versionTask->newQuery()->findOrFail($task_id);
        }catch (Exception $exception) {
            return $this->sendError($exception->getMessage());
        }

        try {
            switch ($task->etat) {
                case 0:
                    $this->DecreaseTotalTask($version_id);
                    $this->DecreaseRegisteredTask($version_id);
                    break;

                case 1:
                    $this->DecreaseTotalTask($version_id);
                    $this->DecreaseProgressTask($version_id);
                    break;

                case 2:
                    $this->DecreaseTotalTask($version_id);
                    $this->DecreaseFinishTask($version_id);
                    $this->DecreaseBuildNumber($version_id, $config['priority'][$task->priority]['point']);
                    $this->updatePercentTask($version_id);
                    break;

                default:return $this->sendError("Erreur", ["error" => "Erreur de mise à jour d'information de build"], 520);
            }
        }catch (Exception $exception) {
            return $this->sendError($exception->getMessage());
        }

        try {
            $task->delete();
        }catch (Exception $exception) {
            return  $this->sendError($exception->getMessage());
        }

        return $this->sendResponse(null, null, 200);
    }

    private function IncreaseTotalTask($id)
    {
        $version = RouteVersion::find($id);
        if($version->total_task !== 0) {
            $version->update([
                "total_task" => $version->total_task+1
            ]);
        } else {
            $version->update([
                "total_task" => $version->total_task+1
            ]);
        }
    }

    private function DecreaseTotalTask($id)
    {
        $version = RouteVersion::find($id);
        if($version->total_task !== 0) {
            $version->update([
                "total_task" => $version->total_task-1
            ]);
        } else {
            $version->update([
                "total_task" => 1
            ]);
        }
    }

    private function IncreaseRegisteredTask($id)
    {
        $version = RouteVersion::find($id);
        $version->update([
            "registered_task" => $version->registered_task+1
        ]);
    }

    private function DecreaseRegisteredTask($id)
    {
        $version = RouteVersion::find($id);
        $version->update([
            "registered_task" => $version->registered_task-1
        ]);
    }

    private function IncreaseProgressTask($id)
    {
        $version = RouteVersion::find($id);
        $version->update([
            "progress_task" => $version->progress_task+1
        ]);
    }

    private function DecreaseProgressTask($id)
    {
        $version = RouteVersion::find($id);
        $version->update([
            "progress_task" => $version->progress_task-1
        ]);
    }

    private function IncreaseFinishTask($id)
    {
        $version = RouteVersion::find($id);
        $version->update([
            "finish_task" => $version->finish_task+1
        ]);
    }

    private function DecreaseFinishTask($id)
    {
        $version = RouteVersion::find($id);
        $version->update([
            "finish_task" => $version->finish_task-1
        ]);
    }

    private function updatePercentTask($id)
    {
        $version = RouteVersion::find($id);
        $percent = (100 * $version->finish_task)/$version->total_task;
        $version->update([
            "percent_task" => $percent
        ]);
    }

    private function IncreaseBuildNumber($id, $priority_point)
    {
        $version = RouteVersion::find($id);
        $route = Route::find($version->route_id);

        $new_build = round($version->build+$priority_point);

        $version->update(["build" => $new_build]);
        $route->update(["build" => $new_build]);
    }

    private function DecreaseBuildNumber($id, $priority_point)
    {
        $version = RouteVersion::find($id);
        $route = Route::find($version->route_id);

        $new_build = round($version->build-$priority_point);

        $version->update(["build" => $new_build]);
        $route->update(["build" => $new_build]);
    }
}
