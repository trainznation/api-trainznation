<?php

namespace App\Http\Controllers\Api\Admin\Route;

use App\Http\Controllers\Api\BaseController;
use App\Http\Controllers\Controller;
use App\Http\Requests\Route\Version\StoreRouteVersion;
use App\Http\Resources\Admin\Route\RouteVersionGet;
use App\Http\Resources\Admin\Route\RouteVersionList;
use App\Model\RouteVersion;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class RouteVersionController extends BaseController
{
    /**
     * @var RouteVersion
     */
    private $version;

    /**
     * RouteVersionController constructor.
     * @param RouteVersion $version
     */
    public function __construct(RouteVersion $version)
    {
        $this->version = $version;
    }


    /**
     * @param $route_id
     * @return JsonResponse
     *
     * @OA\Get(
     *     path="/api/admin/route/{route_id}/version/list",
     *     tags={"Admin Route Version"},
     *     security={{"api_key": {}}},
     *     summary="Liste des version de la route selectionner",
     *      @OA\Parameter(name="route_id", in="path", description="ID de la route", required=true),
     *      @OA\Response(
     *          response="200",
     *          description="Retourne la liste des versions",
     *          @OA\JsonContent(
     *              type="object",
     *          )
     *      )
     * )
     */
    public function list($route_id)
    {
        $format = new RouteVersionList();

        if (\request()->has('q') == true) {
            $versions = $this->version->newQuery()->where('route_id', $route_id)
                ->where('version', 'LIKE', '%' . request()->get('q') . '%')
                ->orWhere('build', 'LIKE', '%' . request()->get('q') . '%')
                ->get();
        } else {
            $versions = $this->version->newQuery()->where('route_id', $route_id)->get();
        }

        return $this->sendResponse($format->toArray($versions), null, 200);
    }

    /**
     * @param $route_id
     * @param $version_id
     * @return JsonResponse
     *
     * @OA\Get(
     *     path="/api/admin/route/{route_id}/version/{version_id}",
     *     tags={"Admin Route Version"},
     *     security={{"api_key": {}}},
     *     summary="Affiche les informations d'une version",
     *      @OA\Parameter(name="route_id", in="path", description="ID de la route", required=true),
     *      @OA\Parameter(name="version_id", in="path", description="ID de la version", required=true),
     *      @OA\Response(
     *          response="200",
     *          description="Tableau des info",
     *          @OA\JsonContent(
     *              type="object",
     *          )
     *      )
     * )
     */
    public function get($route_id, $version_id)
    {
        $format = new RouteVersionGet();

        try {
            $version = $this->version->newQuery()->find($version_id);
        }catch (Exception $exception) {
            return $this->sendError($exception->getMessage(), null, 500);
        }

        return $this->sendResponse($format->toArray($version), null, 200);
    }

    /**
     * @param StoreRouteVersion $request
     * @param $route_id
     * @return JsonResponse
     *
     * @OA\Post(
     *     tags={"Admin Route Version"},
     *     path="/api/admin/route/{route_id}/version",
     *     security={{"api_key": {}}},
     *     summary="Création d'une version",
     *      @OA\Parameter(name="route_id", in="path", description="ID de la route", required=true),
     *     @OA\Response(
     *      response="200",
     *      description="Tableau de la version créer",
     *      @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref="#/components/schemas/AdminRouteVersionGet")
     *      )
     *     ),
     *     @OA\RequestBody(
     *          description="Création",
     *          required=true,
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(ref="#/components/schemas/AdminPostRouteVersion")
     *          ),
     *          @OA\MediaType(
     *              mediaType="application/x-www-form-urlencoded",
     *              @OA\Schema(ref="#/components/schemas/AdminPostRouteVersion")
     *          )
     *     )
     * )
     */
    public function store(StoreRouteVersion $request, $route_id)
    {
        $format = new RouteVersionGet();
        try {
            $version = $this->version->newQuery()->create([
                "route_id" => $route_id,
                "version" => $request->get('version'),
                "build" => $request->get('build'),
                "map_link" => $request->get('map_link'),
                "distance" => $request->get('distance'),
                "station_start" => $request->get('station_start'),
                "station_end" => $request->get('station_end'),
            ]);
        }catch (Exception $exception) {
            return $this->sendError($exception->getMessage());
        }

        return $this->sendResponse($format->toArray($version), null, 200);
    }

    /**
     * @param $route_id
     * @param $version_id
     * @return JsonResponse
     *
     * @OA\Get(
     *     path="/api/admin/route/{route_id}/version/{version_id}/publish",
     *     tags={"Admin Route Version"},
     *     security={{"api_key": {}}},
     *     summary="Publier une version",
     *      @OA\Parameter(name="category_id", in="path", description="ID de la catégorie", required=true),
     *      @OA\Response(
     *          response="200",
     *          description="NULL",
     *          @OA\JsonContent(
     *              type="object",
     *          )
     *      )
     * )
     */
    public function publish($route_id, $version_id)
    {
        try {
            $this->version->newQuery()->findOrFail($version_id)
                ->update([
                    "published_at" => now()
                ]);
        }catch (Exception $exception) {
            return $this->sendError($exception->getMessage());
        }

        return $this->sendResponse(null, null, 200);
    }
}
