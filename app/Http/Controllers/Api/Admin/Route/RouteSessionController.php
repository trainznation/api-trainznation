<?php

namespace App\Http\Controllers\Api\Admin\Route;

use App\Http\Controllers\Api\BaseController;
use App\Http\Controllers\Controller;
use App\Http\Requests\Route\Download\StoreRouteSessionRequest;
use App\Http\Resources\Admin\Route\RouteSessionGet;
use App\Http\Resources\Admin\Route\RouteSessionList;
use App\Model\RouteSession;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class RouteSessionController extends BaseController
{
    /**
     * @var RouteSession
     */
    private $routeSession;

    /**
     * RouteSessionController constructor.
     * @param RouteSession $routeSession
     */
    public function __construct(RouteSession $routeSession)
    {
        $this->routeSession = $routeSession;
    }

    /**
     * @param $route_id
     * @return JsonResponse
     *
     * @OA\Get(
     *     path="/api/admin/route/{route_id}/session/list",
     *     tags={"Admin Route Sessions"},
     *     security={{"api_key": {}}},
     *     summary="Liste des sessions",
     *      @OA\Parameter(name="route_id", in="path", description="ID de la route", required=true),
     *      @OA\Response(
     *          response="200",
     *          description="Liste des sessions",
     *          @OA\JsonContent(
     *              type="array",
     *              @OA\Items(ref="#/components/schemas/AdminRouteSessionList")
     *          )
     *      )
     * )
     */
    public function list($route_id)
    {
        $sessions = $this->routeSession->newQuery()->where('route_id', $route_id)->get();

        $format = new RouteSessionList();
        return $this->sendResponse($format->toArray($sessions), null, 200);
    }

    /**
     * @param $route_id
     * @param $session_id
     * @return JsonResponse
     *
     * @OA\Get(
     *     path="/api/admin/route/{route_id}/session/{session_id}",
     *     tags={"Admin Route Sessions"},
     *     security={{"api_key": {}}},
     *     summary="Information sur une session",
     *      @OA\Parameter(name="route_id", in="path", description="ID de la route", required=true),
     *      @OA\Parameter(name="session_id", in="path", description="ID de la session", required=true),
     *      @OA\Response(
     *          response="200",
     *          description="Tableau de la session",
     *          @OA\JsonContent(
     *              type="array",
     *              @OA\Items(ref="#/components/schemas/AdminRouteSessionGet")
     *          )
     *      )
     * )
     */
    public function get($route_id, $session_id)
    {
        $session = $this->routeSession->newQuery()->find($session_id);

        $format = new RouteSessionGet();
        return $this->sendResponse($format->toArray($session), null, 200);
    }

    /**
     * @param StoreRouteSessionRequest $request
     * @param $route_id
     * @return JsonResponse
     *
     * @OA\Post(
     *     tags={"Admin Route Sessions"},
     *     path="/api/admin/route/{route_id}/session",
     *     security={{"api_key": {}}},
     *     summary="Création d'une session",
     *      @OA\Parameter(name="route_id", in="path", description="ID de la route", required=true),
     *     @OA\Response(
     *      response="200",
     *      description="Tableau de la session créer",
     *      @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref="#/components/schemas/AdminRouteSessionGet")
     *      )
     *     ),
     *     @OA\RequestBody(
     *          description="Création",
     *          required=true,
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(ref="#/components/schemas/AdminRouteSessionRequest")
     *          ),
     *          @OA\MediaType(
     *              mediaType="application/x-www-form-urlencoded",
     *              @OA\Schema(ref="#/components/schemas/AdminRouteSessionRequest")
     *          )
     *     )
     * )
     */
    public function store(StoreRouteSessionRequest $request, $route_id)
    {
        $session = $this->routeSession->newQuery()->create([
            "route_id" => $route_id,
            "name" => $request->get('name'),
            "short_content" => $request->get('short_content'),
            "content" => $request->get('content'),
            "published" => $request->get('published'),
            "published_at" => ($request->get('published') == 1) ?
                ($request->has('published_at')) ? $request->get('published_at') : now()
                : '',
            "kuid" => $request->get('kuid'),
            "uuid" => Str::uuid()
        ]);

        $format = new RouteSessionGet();
        return $this->sendResponse($format->toArray($session), null, 200);
    }

    /**
     * @param $route_id
     * @param $session_id
     * @return JsonResponse
     *
     * @OA\Delete(
     *     path="/api/admin/{route_id}/session/{session_id}",
     *     tags={"Admin Route Sessions"},
     *     security={{"api_key": {}}},
     *     summary="Supprime une session",
     *      @OA\Parameter(name="route_id", in="path", description="ID de la route", required=true),
     *      @OA\Parameter(name="session_id", in="path", description="ID de la session", required=true),
     *      @OA\Response(
     *          response="200",
     *          description="Retourne un message",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="La session à été supprimé")
     *          )
     *      ),
     *      @OA\Response(
     *          response="510",
     *          description="Message d'erreur",
     *          @OA\JsonContent(
     *              @OA\Property(property="error", type="string", example="Exception")
     *          )
     *      )
     * )
     */
    public function delete($route_id, $session_id)
    {
        $session = $this->routeSession->newQuery()->find($session_id);

        try {
            $session->delete();
        }catch (Exception $exception) {
            return $this->sendError("Erreur Système", ['error' => $exception], 510);
        }

        return $this->sendResponse(['message' => "La session à été supprimer"], null, 200);
    }
}
