<?php

namespace App\Http\Controllers\Api\Admin\Route;

use App\Events\Route\RoutePublishEvent;
use App\Helpers\Notificator;
use App\Http\Controllers\Api\BaseController;
use App\Http\Controllers\Controller;
use App\Http\Requests\Route\StoreRouteRequest;
use App\Http\Resources\Admin\Route\RouteGet;
use App\Http\Resources\Admin\Route\RouteList;
use App\Model\Route;
use App\Model\RouteRoadmap;
use App\Model\RouteVersion;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class RouteController extends BaseController
{
    /**
     * @var Route
     */
    private $route;
    /**
     * @var Notificator
     */
    private $notificator;
    /**
     * @var RouteVersion
     */
    private $routeVersion;

    /**
     * RouteController constructor.
     * @param Route $route
     * @param RouteVersion $routeVersion
     * @param Notificator $notificator
     */
    public function __construct(Route $route, RouteVersion $routeVersion, Notificator $notificator)
    {
        $this->route = $route;
        $this->notificator = $notificator;
        $this->routeVersion = $routeVersion;
    }

    /**
     * @return JsonResponse
     *
     * @OA\Get(
     *     path="/api/admin/route/list",
     *     tags={"Admin Route"},
     *     security={{"api_key": {}}},
     *     summary="Liste des routes",
     *      @OA\Response(
     *          response="200",
     *          description="Retourne la liste des routes",
     *          @OA\JsonContent(
     *              type="array",
     *              @OA\Items(ref="#/components/schemas/AdminRouteList")
     *          )
     *      )
     * )
     */
    public function list()
    {
        $format = new RouteList();
        $route = $this->route->newQuery()->get();

        return $this->sendResponse($format->toArray($route), null, 200);
    }

    /**
     * @param StoreRouteRequest $request
     * @return JsonResponse
     *
     * @OA\Post(
     *     tags={"Admin Route"},
     *     path="/api/admin/route",
     *     security={{"api_key": {}}},
     *     summary="Création d'une route et de sa roadmap",
     *     @OA\Response(
     *      response="200",
     *      description="Retourne les informations de la route créer",
     *      @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref="#/components/schemas/AdminRouteGet")
     *      )
     *     ),
     *     @OA\RequestBody(
     *          description="Information de la création de la route",
     *          required=true,
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(ref="#/components/schemas/AdminPostRouteRequest")
     *          ),
     *          @OA\MediaType(
     *              mediaType="application/x-www-form-urlencoded",
     *              @OA\Schema(ref="#/components/schemas/AdminPostRouteRequest")
     *          )
     *     )
     * )
     */
    public function store(StoreRouteRequest $request)
    {
        $format = new RouteGet();
        $route = $this->route->newQuery()->create([
            "name" => $request->get('name'),
            "description" => $request->get('description'),
            "published" => $request->get('published'),
            "version" => $request->get('version'),
            "build" => $request->get('build'),
            "route_kuid" => $request->get('route_kuid'),
            "dependance_kuid" => $request->get('dependance_kuid')
        ]);

        if($request->has('version') == true && $request->has('build') == true) {
            $this->routeVersion->newQuery()->create([
                "version" => $request->get('version'),
                "build" => $request->get('build'),
                "route_id" => $route->id
            ]);
        }

        return $this->sendResponse($format->toArray($route), null, 200);

    }

    /**
     * @param $route_id
     * @return JsonResponse
     *
     * @OA\Get(
     *     path="/api/admin/route/{route_id}",
     *     tags={"Admin Route"},
     *     security={{"api_key": {}}},
     *     summary="Informations relative à une route",
     *      @OA\Parameter(name="route_id", in="path", description="ID de la route", required=true),
     *      @OA\Response(
     *          response="200",
     *          description="Retourne les informations d'une route",
     *          @OA\JsonContent(
     *              type="array",
     *              @OA\Items(ref="#/components/schemas/AdminRouteGet")
     *          )
     *      )
     * )
     */
    public function get($route_id)
    {
        $format = new RouteGet();
        $route = $this->route->newQuery()->findOrFail($route_id)->load('versions', 'sessions', 'galleries', 'downloads', 'tasks');

        return $this->sendResponse($format->toArray($route), null, 200);
    }

    /**
     * @param StoreRouteRequest $request
     * @param $route_id
     * @return JsonResponse
     *
     * @OA\Put(
     *     tags={"Admin Route"},
     *     path="/api/admin/route/{route_id}",
     *     security={{"api_key": {}}},
     *     summary="Met à jour les informations de la route",
     *     @OA\Parameter(name="route_id", in="path", description="ID de la route", required=true),
     *     @OA\Response(
     *      response="200",
     *      description="Retourne les informations de la route mis à jour",
     *      @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref="#/components/schemas/AdminRouteGet")
     *      )
     *     ),
     *     @OA\RequestBody(
     *          description="Informations à mettre à jour",
     *          required=true,
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(ref="#/components/schemas/AdminPostRouteRequest")
     *          ),
     *          @OA\MediaType(
     *              mediaType="application/x-www-form-urlencoded",
     *              @OA\Schema(ref="#/components/schemas/AdminPostRouteRequest")
     *          )
     *     )
     * )
     */
    public function update(StoreRouteRequest $request, $route_id)
    {
        $format = new RouteGet();
        $route = $this->route->newQuery()->findOrFail($route_id);

        try {
            $route->update([
                "name" => $request->get('name'),
                "description" => $request->get('description'),
                "published" => $request->get('published'),
                "version" => $request->get('version'),
                "build" => $request->get('build'),
                "route_kuid" => $request->get('route_kuid'),
                "dependance_kuid" => $request->get('dependance_kuid')
            ]);

            if($request->get('published') == 1) {
                $this->notificator->publish("La route <strong></strong> à été publier", "success", null, "fas fa-road", $route->image);
            }
        }catch (Exception $exception) {
            return $this->sendError("Erreur Système", ["error" => $exception->getMessage()], 510);
        }

        return  $this->sendResponse($format->toArray($route), null, 200);
    }

    /**
     * @param $route_id
     * @return JsonResponse
     *
     * @OA\Delete(
     *     path="/api/admin/route/{route_id}",
     *     tags={"Admin Route"},
     *     security={{"api_key": {}}},
     *     summary="Suppression de la route",
     *      @OA\Parameter(name="route_id", in="path", description="ID de la route", required=true),
     *      @OA\Response(
     *          response="200",
     *          description="Message indiquant la bonne suppression de la route",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="La route à été supprimé")
     *          )
     *      ),
     *      @OA\Response(
     *          response="510",
     *          description="Message d'erreur",
     *          @OA\JsonContent(
     *              @OA\Property(property="error", type="string", example="Exception")
     *          )
     *      )
     * )
     */
    public function delete($route_id)
    {
        $route = $this->route->newQuery()->findOrFail($route_id);

        try {
            $route->delete();
        }catch (Exception $exception) {
            return $this->sendError("Erreur Système", ["error" => $exception->getMessage()], 510);
        }

        return $this->sendResponse(['message' => "La route à été supprimer"], null, 200);
    }
}
