<?php

namespace App\Http\Controllers\Api\Admin\Blog;

use App\Events\Blog\BlogPublishEvent;
use App\Helpers\Notificator;
use App\Http\Controllers\Api\BaseController;
use App\Http\Controllers\Controller;
use App\Http\Requests\Blog\BlogRequest;
use App\Http\Resources\Admin\Blog\BlogGet;
use App\Http\Resources\Admin\Blog\BlogList;
use App\Model\Blog;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Broadcast;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

use OpenApi\Annotations as OA;

class BlogController extends BaseController
{
    /**
     * @var Blog
     */
    private $blog;
    /**
     * @var Notificator
     */
    private $notificator;

    /**
     * BlogController constructor.
     * @param Blog $blog
     * @param Notificator $notificator
     */
    public function __construct(Blog $blog, Notificator $notificator)
    {
        $this->blog = $blog;
        $this->notificator = $notificator;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     *
     * @OA\Get(
     *     path="/api/admin/blog/list",
     *     tags={"Admin Blog"},
     *     security={{"api_key": {}}},
     *     summary="Liste des articles",
     *     @OA\Parameter(in="query", name="q", description="Terme de la recherche", required=false),
     *      @OA\Response(
     *          response="200",
     *          description="Liste des articles",
     *          @OA\JsonContent(
     *              type="array",
     *              @OA\Items(ref="#/components/schemas/AdminBlogList")
     *          )
     *      )
     * )
     */
    public function list(Request $request)
    {
        $format = new BlogList();
        if($request->has('q') == true) {
            $blogs = $this->blog->newQuery()->where('title', 'LIKE', '%'.$request->get('q').'%')->get();
        } else {
            $blogs = $this->blog->newQuery()->get();
        }

        return $this->sendResponse($format->toArray($blogs), null, 200);
    }

    /**
     * @param BlogRequest $request
     * @return JsonResponse
     *
     * @OA\Post(
     *     tags={"Admin Blog"},
     *     path="/api/admin/blog",
     *     security={{"api_key": {}}},
     *     summary="Création d'un article",
     *     @OA\Response(
     *      response="200",
     *      description="Retourne l'article créer",
     *      @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref="#/components/schemas/AdminBlogGet")
     *      )
     *     ),
     *     @OA\RequestBody(
     *          description="Création d'un article",
     *          required=true,
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(ref="#/components/schemas/PostBlogRequest")
     *          ),
     *          @OA\MediaType(
     *              mediaType="application/x-www-form-urlencoded",
     *              @OA\Schema(ref="#/components/schemas/PostBlogRequest")
     *          )
     *     )
     * )
     */
    public function store(BlogRequest $request)
    {
        $format = new BlogGet();

        try {
            $blog = $this->blog->newQuery()->create([
                "title" => $request->get('title'),
                "slug" => Str::slug($request->get('title')),
                "short_content" => $request->get('short_content'),
                "content" => $request->get('content'),
                "published" => $request->get('published'),
                "published_at" => ($request->get('published') == 1) ? $request->get('published_at') : null,
                "social" => $request->get('social'),
                "blog_category_id" => $request->get('blog_category_id')
            ]);
            return $this->sendResponse($format->toArray($blog), null, 200);
            // @codeCoverageIgnoreStart
        }catch (Exception $exception) {
            Log::critical($exception->getMessage(), ["secteur" => "Admin Blog"]);
            return $this->sendError("Erreur Système", ['error' => $exception], 510);
        }
        // @codeCoverageIgnoreEnd
    }

    /**
     * @param $blog_id
     * @return JsonResponse
     *
     * @OA\Get(
     *     path="/api/admin/blog/{blog_id}",
     *     tags={"Admin Blog"},
     *     security={{"api_key": {}}},
     *     summary="Information sur un article",
     *      @OA\Parameter(name="blog_id", in="path", description="ID de l'article", required=true),
     *      @OA\Response(
     *          response="200",
     *          description="Retourne les informations d'un article",
     *          @OA\JsonContent(
     *              type="array",
     *              @OA\Items(ref="#/components/schemas/AdminBlogGet")
     *          )
     *      )
     * )
     */
    public function get($blog_id)
    {
        $format = new BlogGet();
        try {
            $blog = $this->blog->newQuery()->findOrFail($blog_id);
            return $this->sendResponse($format->toArray($blog), null, 200);
            // @codeCoverageIgnoreStart
        }catch (Exception $exception) {
            return $this->sendError("Erreur Système", ['error' => $exception], 510);
        }
        // @codeCoverageIgnoreEnd
    }

    /**
     * @param BlogRequest $request
     * @param $blog_id
     * @return JsonResponse
     *
     * @OA\Put(
     *     tags={"Admin Blog"},
     *     path="/api/admin/blog/{blog_id}",
     *     security={{"api_key": {}}},
     *     summary="Mettre à jour les informations d'un article",
     *     @OA\Parameter(name="blog_id", in="path", description="ID de l'article", required=true),
     *     @OA\Response(
     *      response="200",
     *      description="Retourne l'article mis à jour",
     *      @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref="#/components/schemas/AdminBlogGet")
     *      )
     *     ),
     *     @OA\RequestBody(
     *          description="Mettre à jour les informations d'un article",
     *          required=true,
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(ref="#/components/schemas/PostBlogRequest")
     *          ),
     *          @OA\MediaType(
     *              mediaType="application/x-www-form-urlencoded",
     *              @OA\Schema(ref="#/components/schemas/PostBlogRequest")
     *          )
     *     )
     * )
     */
    public function update(BlogRequest $request, $blog_id)
    {
        $format = new BlogGet();
        $blog = $this->blog->newQuery()->findOrFail($blog_id);

        try {
            $blog->update([
                "title" => $request->get('title'),
                "slug" => Str::slug($request->get('title')),
                "short_content" => $request->get('short_content'),
                "content" => $request->get('content'),
                "published" => $request->get('published'),
                "published_at" => ($request->get('published') == 1) ? $request->get('published_at') : null,
                "social" => $request->get('social'),
                "blog_category_id" => $request->get('blog_category_id')
            ]);
            return $this->sendResponse($format->toArray($blog), null, 200);
            // @codeCoverageIgnoreStart
        }catch (Exception $exception) {
            Log::critical($exception->getMessage(), ['secteur' => "Admin Blog"]);
            return $this->sendError("Erreur Système", ['error' => $exception], 510);
        }
        // @codeCoverageIgnoreEnd
    }


    /**
     * @param $blog_id
     * @return JsonResponse
     *
     * @OA\Delete(
     *     path="/api/admin/blog/{blog_id}",
     *     tags={"Admin Blog"},
     *     security={{"api_key": {}}},
     *     summary="Supprime un article",
     *      @OA\Parameter(name="blog_id", in="path", description="ID de l'article", required=true),
     *      @OA\Response(
     *          response="200",
     *          description="Retourne un message indiquant la suppression de l'article",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="L'article à été supprimer")
     *          )
     *      ),
     *      @OA\Response(
     *          response="510",
     *          description="Erreur de suppression",
     *          @OA\JsonContent(
     *              @OA\Property(property="error", type="string", example="Exception")
     *          )
     *      )
     * )
     */
    public function delete($blog_id)
    {
        $blog = $this->blog->newQuery()->findOrFail($blog_id);
        try {
            $blog->delete();
            return $this->sendResponse(['message' => "L'article à été supprimer"], null, 200);
            // @codeCoverageIgnoreStart
        }catch (Exception $exception) {
            Log::critical($exception->getMessage(), ['secteur' => "Admin Blog"]);
            return $this->sendError("Erreur Système", ['error' => $exception], 510);
        }
        // @codeCoverageIgnoreEnd
    }

    /**
     * @param $blog_id
     * @return JsonResponse
     *
     * @OA\Get(
     *     path="/api/admin/blog/{blog_id}/publish",
     *     tags={"Admin Blog"},
     *     security={{"api_key": {}}},
     *     summary="Publication de l'article",
     *      @OA\Parameter(name="blog_id", in="path", description="ID de l'article", required=true),
     *      @OA\Response(
     *          response="200",
     *          description="Message indiquant la publication de l'article",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="L'article à été publier")
     *          )
     *      ),
     *     @OA\Response(
     *          response="510",
     *          description="Message Erreur",
     *          @OA\JsonContent(
     *              @OA\Property(property="error", type="string", example="Exception")
     *          )
     *      )
     * )
     */
    public function publish($blog_id)
    {
        $blog = $this->blog->newQuery()->findOrFail($blog_id);

        try {
            $blog->update([
                "published" => true,
                "published_at" => now()
            ]);

            $this->notificator->publish("L'article <strong>".$blog->title."</strong> à été publier", "success", null, "fas fa-newspaper", $blog->images);

            return $this->sendResponse(['message' => "L'article à été publier"], null, 200);
            // @codeCoverageIgnoreStart
        }catch (Exception $exception) {
            Log::critical($exception->getMessage(), ["secteur" ]);
            return $this->sendError("Erreur système", ["error" => $exception], 510);
        }
        // @codeCoverageIgnoreEnd
    }

    /**
     * @param $blog_id
     * @return JsonResponse
     *
     * @OA\Get(
     *     path="/api/admin/blog/{blog_id}/dispublish",
     *     tags={"Admin Blog"},
     *     security={{"api_key": {}}},
     *     summary="Dépublication de l'article",
     *      @OA\Parameter(name="blog_id", in="path", description="ID de l'article", required=true),
     *      @OA\Response(
     *          response="200",
     *          description="Message indiquant la dépublication de l'article",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="L'article à été dépublier")
     *          )
     *      ),
     *     @OA\Response(
     *          response="510",
     *          description="Message Erreur",
     *          @OA\JsonContent(
     *              @OA\Property(property="error", type="string", example="Exception")
     *          )
     *      )
     * )
     */
    public function dispublish($blog_id)
    {
        $blog = $this->blog->newQuery()->findOrFail($blog_id);

        try {
            $blog->update([
                "published" => false,
                "published_at" => null
            ]);
            return $this->sendResponse(['message' => "L'article à été dépublier"], null, 200);
            // @codeCoverageIgnoreStart
        }catch (Exception $exception) {
            return $this->sendError("Erreur système", ["error" => $exception], 510);
        }
        // @codeCoverageIgnoreEnd
    }

}
