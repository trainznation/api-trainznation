<?php

namespace App\Http\Controllers\Api\Admin\Blog;

use App\Http\Controllers\Api\BaseController;
use App\Http\Controllers\Controller;
use App\Model\Blog;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use OpenApi\Annotations\Property;

class CommentController extends BaseController
{
    /**
     * @var Blog
     */
    private $blog;

    /**
     * CommentController constructor.
     * @param Blog $blog
     */
    public function __construct(Blog $blog)
    {
        $this->blog = $blog;
    }

    /**
     * @param $blog_id
     * @param $comment_id
     * @return JsonResponse
     *
     * @OA\Get(
     *     path="/api/admin/blog/{blog_id}/comment/{comment_id}/approve",
     *     tags={"Admin Blog"},
     *     security={{"api_key": {}}},
     *     summary="Approuve un commentaire",
     *      @OA\Parameter(name="blog_id", in="path", description="ID de l'article", required=true),
     *      @OA\Parameter(name="comment_id", in="path", description="ID du commentaire", required=true),
     *      @OA\Response(
     *          response="200",
     *          description="Message indiquant l'approuvation d'un commentaire",
     *          @OA\JsonContent(
     *              @Property(property="message", type="string", example="Le commentaire à été approuvé")
     *          )
     *      ),
     *     @OA\Response(
     *          response="510",
     *          description="Message Exception",
     *          @OA\JsonContent(
     *              @Property(property="error", type="string", example="Exception")
     *          )
     *      )
     * )
     */
    public function approve($blog_id, $comment_id)
    {
        $blog = $this->blog->newQuery()->findOrFail($blog_id);
        $comment = $blog->comments()->find($comment_id);

        try {
            $comment->approve();
            return $this->sendResponse(['message' => "Le commentaire ".$comment->id." à été approuvé"], null, 200);
        }catch (Exception $exception) {
            return $this->sendError("Erreur Système", ["error" => $exception], 510);
        }
    }

    /**
     * @param $blog_id
     * @param $comment_id
     * @return JsonResponse
     *
     * @OA\Get(
     *     path="/api/admin/blog/{blog_id}/comment/{comment_id}/disapprove",
     *     tags={"Admin Blog"},
     *     security={{"api_key": {}}},
     *     summary="Désapprouve un commentaire",
     *      @OA\Parameter(name="blog_id", in="path", description="ID de l'article", required=true),
     *      @OA\Parameter(name="comment_id", in="path", description="ID du commentaire", required=true),
     *      @OA\Response(
     *          response="200",
     *          description="Message indiquant la désapprouvation d'un commentaire",
     *          @OA\JsonContent(
     *              @Property(property="message", type="string", example="Le commentaire à été désapprouvé")
     *          )
     *      ),
     *     @OA\Response(
     *          response="510",
     *          description="Message Exception",
     *          @OA\JsonContent(
     *              @Property(property="error", type="string", example="Exception")
     *          )
     *      )
     * )
     */
    public function disapprove($blog_id, $comment_id)
    {
        $blog = $this->blog->newQuery()->findOrFail($blog_id);
        $comment = $blog->comments()->find($comment_id);

        try {
            $comment->disapprove();
            return $this->sendResponse(['message' => "Le commentaire ".$comment->id." à été désapprouvé"], null, 200);
        }catch (Exception $exception) {
            return $this->sendError("Erreur Système", ["error" => $exception], 510);
        }
    }
}
