<?php

namespace App\Http\Controllers\Api\Admin\Blog;

use App\Http\Controllers\Api\BaseController;
use App\Http\Controllers\Controller;
use App\Http\Requests\Blog\BlogCategoryRequest;
use App\Http\Resources\Admin\Blog\BlogCategoriesList;
use App\Model\BlogCategory;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use OpenApi\Annotations as OA;

class CategoryController extends BaseController
{
    /**
     * @var BlogCategory
     */
    private $blogCategory;

    /**
     * CategoryController constructor.
     * @param BlogCategory $blogCategory
     */
    public function __construct(BlogCategory $blogCategory)
    {
        $this->blogCategory = $blogCategory;
    }


    /**
     * @param Request $request
     * @return JsonResponse
     *
     * @OA\Get(
     *     path="/api/admin/blog/category/list",
     *     tags={"Admin Blog Category"},
     *     security={{"api_key": {}}},
     *     summary="Liste des catégories",
     *     @OA\Parameter(in="query", name="q", description="Terme de la recherche", required=false),
     *      @OA\Response(
     *          response="200",
     *          description="Retourne la liste des catégories",
     *          @OA\JsonContent(
     *              type="array",
     *              @OA\Items(ref="#/components/schemas/AdminBlogCategoriesList")
     *          )
     *      )
     * )
     */
    public function list(Request $request)
    {
        $format = new BlogCategoriesList();
        if($request->has('q') == true) {
            $categories = $this->blogCategory->newQuery()->where('name', 'LIKE', '%'.$request->get('q').'%')->get();
        }else {
            $categories = $this->blogCategory->newQuery()->get();
        }

        return $this->sendResponse($format->toArray($categories), null, 200);
    }


    /**
     * @param BlogCategoryRequest $request
     * @return JsonResponse
     *
     * @OA\Post(
     *     tags={"Admin Blog Category"},
     *     path="/api/admin/blog/category",
     *     security={{"api_key": {}}},
     *     summary="Création d'une catégorie",
     *     @OA\Response(
     *      response="200",
     *      description="Retourne la catégorie construite",
     *      @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref="#/components/schemas/AdminBlogCategory")
     *      )
     *     ),
     *     @OA\RequestBody(
     *          description="Création d'une catégorie",
     *          required=true,
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(ref="#/components/schemas/PostBlogCategoryRequest")
     *          ),
     *          @OA\MediaType(
     *              mediaType="application/x-www-form-urlencoded",
     *              @OA\Schema(ref="#/components/schemas/PostBlogCategoryRequest")
     *          )
     *     )
     * )
     */
    public function store(BlogCategoryRequest $request)
    {
        $format = new \App\Http\Resources\Admin\Blog\BlogCategory();
        $category = $this->blogCategory->newQuery()->create([
            "name" => $request->get('name')
        ]);

        return $this->sendResponse($format->toArray($category), null, 200);
    }

    /**
     * @param $category_id
     * @return JsonResponse
     *
     * @OA\Delete(
     *     path="/api/admin/blog/category/{category_id}",
     *     tags={"Admin Blog Category"},
     *     security={{"api_key": {}}},
     *     summary="Supprime une catégorie",
     *      @OA\Parameter(name="category_id", in="path", description="ID de la catégorie", required=true),
     *      @OA\Response(
     *          response="200",
     *          description="Retourne un message indiquant la suppression de la catégorie",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="La catégorie à été supprimer")
     *          )
     *      ),
     *      @OA\Response(
     *          response="510",
     *          description="Retourne un message d'erreur",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Erreur lors de la suppression de la catégorie")
     *          )
     *      )
     * )
     */
    public function delete($category_id)
    {
        $category = $this->blogCategory->newQuery()->findOrFail($category_id);
        try {
            $category->delete();
            // @codeCoverageIgnoreStart
        }catch (Exception $exception) {
            return $this->sendError("Erreur Système", ['error' => $exception], 510);
        }
        // @codeCoverageIgnoreEnd
        return $this->sendResponse(['message' => "La catégorie à été supprimer"], null, 200);
    }
}
