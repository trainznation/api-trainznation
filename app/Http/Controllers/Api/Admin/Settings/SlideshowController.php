<?php

namespace App\Http\Controllers\Api\Admin\Settings;

use App\Http\Controllers\Api\BaseController;
use App\Http\Controllers\Controller;
use App\Http\Resources\Admin\Settings\SlideshowGet;
use App\Http\Resources\Admin\Settings\SlideshowList;
use App\Model\Slideshow;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use OpenApi\Annotations as OA;

class SlideshowController extends BaseController
{
    /**
     * @var Slideshow
     */
    private $slideshow;

    /**
     * SlideshowController constructor.
     * @param Slideshow $slideshow
     */
    public function __construct(Slideshow $slideshow)
    {
        $this->slideshow = $slideshow;
    }

    /**
     * @return JsonResponse
     *
     * @OA\Get(
     *     path="/api/admin/settings/slideshow/list",
     *     tags={"Admin Settings Slideshow"},
     *     security={{"api_key": {}}},
     *     summary="Liste des Slides",
     *      @OA\Response(
     *          response="200",
     *          description="Tableau des slides",
     *          @OA\JsonContent(
     *              type="object",
     *          )
     *      )
     * )
     */
    public function list()
    {
        try {
            $lists = $this->slideshow->newQuery()->orderBy('updated_at', 'desc')->get();
        } catch (Exception $exception) {
            return $this->sendError($exception->getMessage(), null, 200);
        }

        $format = new SlideshowList();
        return $this->sendResponse($format->toArray($lists), null, 200);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     *
     * @OA\Post(
     *     tags={"Admin Settings Slideshow"},
     *     path="/api/admin/settings/slideshow",
     *     security={{"api_key": {}}},
     *     summary="Création d'un slide",
     *     @OA\Response(
     *      response="200",
     *      description="NULL",
     *      @OA\JsonContent(
     *        type="null",
     *      )
     *     ),
     *     @OA\RequestBody(
     *          description="Requete",
     *          required=true,
     *          @OA\MediaType(
     *              mediaType="application/json",
     *          ),
     *          @OA\MediaType(
     *              mediaType="application/x-www-form-urlencoded",
     *          )
     *     )
     * )
     */
    public function store(Request $request)
    {
        try {
            $slide = $this->slideshow->newQuery()->create([
                "link" => $request->get('link')
            ]);
        }catch (Exception $exception) {
            return $this->sendError($exception->getMessage(), null, 500);
        }

        return $this->sendResponse($slide, null, 200);
    }

    /**
     * @param $slide_id
     * @return JsonResponse
     *
     * @OA\Delete(
     *     path="/api/admin/settings/slideshow/{slide_id}",
     *     tags={"Admin Settings Slideshow"},
     *     security={{"api_key": {}}},
     *     summary="Supprime un slide",
     *      @OA\Parameter(name="slide_id", in="path", description="ID du slide", required=true),
     *      @OA\Response(
     *          response="200",
     *          description="NULL",
     *      ),
     * )
     */
    public function delete($slide_id)
    {
        try {
            $this->slideshow->newQuery()->findOrFail($slide_id)->delete();
        }catch (Exception $exception) {
            return $this->sendError($exception->getMessage(), null, 500);
        }

        return $this->sendResponse(null, null, 200);
    }
}
