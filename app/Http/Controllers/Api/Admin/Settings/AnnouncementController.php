<?php

namespace App\Http\Controllers\Api\Admin\Settings;

use App\Http\Controllers\Api\BaseController;
use App\Http\Controllers\Controller;
use App\Http\Requests\Settings\StoreAnnouncementRequest;
use App\Http\Resources\Admin\Settings\AnnouncementGet;
use App\Http\Resources\Admin\Settings\AnnouncementList;
use App\Model\Announcement;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class AnnouncementController extends BaseController
{
    /**
     * @var Announcement
     */
    private $announcement;

    /**
     * AnnouncementController constructor.
     * @param Announcement $announcement
     */
    public function __construct(Announcement $announcement)
    {
        $this->announcement = $announcement;
    }

    /**
     * @return JsonResponse
     *
     * @OA\Get(
     *     path="/api/admin/settings/announcement/list",
     *     tags={"Admin Settings Announcement"},
     *     security={{"api_key": {}}},
     *     summary="Liste des annonces à diffuser",
     *      @OA\Response(
     *          response="200",
     *          description="Tableau des annonces",
     *          @OA\JsonContent(
     *              type="array",
     *              @OA\Items(ref="#/components/schemas/AdminAnnouncementList")
     *          )
     *      )
     * )
     */
    public function list()
    {
        try {
            $lists = $this->announcement->newQuery()->get();
        }catch (Exception $exception) {
            return $this->sendError($exception->getMessage(), null, 510);
        }

        $format = new AnnouncementList();
        return $this->sendResponse($format->toArray($lists), null, 200);
    }

    /**
     * @param StoreAnnouncementRequest $request
     * @return JsonResponse
     *
     * @OA\Post(
     *     tags={"Admin Settings Announcement"},
     *     path="/api/admin/settings/announcement",
     *     security={{"api_key": {}}},
     *     summary="Création d'une annonce",
     *     @OA\Response(
     *      response="200",
     *      description="Tableau de l'annonce",
     *      @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref="#/components/schemas/AdminAnnouncementGet")
     *      )
     *     ),
     *     @OA\RequestBody(
     *          description="Create",
     *          required=true,
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(ref="#/components/schemas/StoreAnnouncementRequest")
     *          ),
     *          @OA\MediaType(
     *              mediaType="application/x-www-form-urlencoded",
     *              @OA\Schema(ref="#/components/schemas/StoreAnnouncementRequest")
     *          )
     *     )
     * )
     */
    public function store(StoreAnnouncementRequest $request)
    {
        try {
            $ann = $this->announcement->newQuery()->create([
                "message" => $request->get('message'),
                "type" => $request->get('type'),
                "expiring_at" => $request->get('expiring_at')
            ]);
        }catch (Exception $exception) {
            return $this->sendError($exception->getMessage(), null, 510);
        }

        $format = new AnnouncementGet();
        return $this->sendResponse($format->toArray($ann), null, 200);
    }

    /**
     * @param $announcement_id
     * @return JsonResponse
     *
     * @OA\Delete(
     *     path="/api/admin/settings/announcement/{announcement_id}",
     *     tags={"Admin Settings Announcement"},
     *     security={{"api_key": {}}},
     *     summary="Supprime une annonce",
     *      @OA\Parameter(name="announcement_id", in="path", description="ID de l'annonce", required=true),
     *      @OA\Response(
     *          response="200",
     *          description="null",
     *          @OA\JsonContent(
     *              @OA\Property(property="null", type="string", example="null")
     *          )
     *      ),
     *      @OA\Response(
     *          response="510",
     *          description="Error",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Exception")
     *          )
     *      )
     * )
     */
    public function delete($announcement_id)
    {
        try {
            $annonce = $this->announcement->newQuery()->findOrFail($announcement_id);
            $annonce->delete();
        }catch (Exception $exception) {
            return $this->sendError($exception->getMessage(), null, 510);
        }

        return $this->sendResponse(null, null, 200);
    }
}
