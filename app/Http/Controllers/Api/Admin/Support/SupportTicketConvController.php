<?php

namespace App\Http\Controllers\Api\Admin\Support;

use App\Helpers\Notificator;
use App\Http\Controllers\Api\BaseController;
use App\Http\Requests\Support\SupportTicketMessageRequest;
use App\Http\Resources\Admin\Support\SupportTicketConvGet;
use App\Http\Resources\Admin\Support\SupportTicketConvList;
use App\Model\SupportTicket;
use App\Model\SupportTicketConv;
use App\User;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;
use OpenApi\Annotations as OA;


class SupportTicketConvController extends BaseController
{
    /**
     * @var SupportTicketConv
     */
    private $conv;
    /**
     * @var Notificator
     */
    private $notificator;

    /**
     * SupportTicketConvController constructor.
     * @param SupportTicketConv $conv
     * @param Notificator $notificator
     */
    public function __construct(SupportTicketConv $conv, Notificator $notificator)
    {
        $this->conv = $conv;
        $this->notificator = $notificator;
    }

    /**
     * @param $ticket_id
     * @return JsonResponse
     *
     * @OA\Get(
     *     path="/api/admin/support/ticket/{ticket_id}/chat",
     *     tags={"Admin Support Ticket"},
     *     security={{"api_key": {}}},
     *     summary="Liste des messages d'un ticket",
     *      @OA\Parameter(name="ticket_id", in="path", description="ID du ticket", required=true),
     *      @OA\Response(
     *          response="200",
     *          description="Tableau des messages",
     *          @OA\JsonContent(
     *              type="object",
     *          )
     *      )
     * )
     */
    public function list($ticket_id)
    {
        try {
            $convs = $this->conv->newQuery()->where('support_ticket_id', $ticket_id)->get();
        }catch (Exception $exception) {
            Log::error($exception);
            return $this->sendError($exception);
        }

        $format = new SupportTicketConvList();
        return $this->sendResponse($format->toArray($convs), null, 200);
    }

    /**
     * @param SupportTicketMessageRequest $request
     * @param $ticket_id
     * @return JsonResponse
     *
     * @OA\Post(
     *     tags={"Admin Support Ticket"},
     *     path="/api/admin/support/ticket/{ticket_id}/chat",
     *     security={{"api_key": {}}},
     *     summary="Nouveau message pour un ticket",
     *      @OA\Parameter(name="ticket_id", in="path", description="ID du ticket", required=true),
     *     @OA\Response(
     *      response="200",
     *      description="Tableau du message créer",
     *      @OA\JsonContent(
     *        type="object",
     *      )
     *     ),
     *     @OA\RequestBody(
     *          description="Information sur le message à créer",
     *          required=true,
     *          @OA\MediaType(
     *              mediaType="application/json",
     *          ),
     *          @OA\MediaType(
     *              mediaType="application/x-www-form-urlencoded",
     *          )
     *     )
     * )
     */
    public function store(SupportTicketMessageRequest $request, $ticket_id)
    {
        try {
            $user = User::find(auth()->user()->id);
            $message = $this->conv->newQuery()->create([
                "message" => $request->get('message'),
                "support_ticket_id" => $ticket_id,
                "user_id" => $user->id
            ]);
            if($request->get('acting') == 'admin') {
                SupportTicket::find($ticket_id)->update([
                    "support_ticket_status_id" => 3,
                    "updated_at" => now()
                ]);
            }else{
                SupportTicket::find($ticket_id)->update([
                    "support_ticket_status_id" => 1,
                    "updated_at" => now()
                ]);
            }
        }catch (Exception $exception) {
            Log::error($exception);
            return $this->sendError($exception->getMessage());
        }

        $this->notificator->publish('Nouvelle réponse à votre ticket de support', 'info', 'Une nouvelle réponse à été posté pour le ticket N°'.$ticket_id, 'fas fa-ticket-alt', null, true, $user->id);
        return $this->sendResponse($message, null, 200);
    }

    /**
     * @param $ticket_id
     * @param $conv_id
     * @return JsonResponse
     *
     * @OA\Delete(
     *     path="/api/admin/support/ticket/{ticket_id}/chat/{conv_id}",
     *     tags={"Admin Support Ticket"},
     *     security={{"api_key": {}}},
     *     summary="Supprime un message",
     *      @OA\Parameter(name="ticket_id", in="path", description="ID du ticket", required=true),
     *      @OA\Parameter(name="conv_id", in="path", description="ID du message", required=true),
     *      @OA\Response(
     *          response="200",
     *          description="NULL",
     *          @OA\JsonContent(
     *              @OA\Property(property="NULL", type="string", example="NULL")
     *          )
     *      ),
     * )
     */
    public function delete($ticket_id, $conv_id)
    {
        try {
            $this->conv->newQuery()->findOrFail($conv_id)->delete();
        }catch (Exception $exception) {
            Log::error($exception);
            return $this->sendError($exception->getMessage());
        }

        return $this->sendResponse(null, null, 200);
    }
}
