<?php

namespace App\Http\Controllers\Api\Admin\Support;

use App\Http\Controllers\Api\BaseController;
use App\Http\Controllers\Controller;
use App\Http\Requests\Support\SupportTicketPriorityRequest;
use App\Http\Requests\SupportSectorRequest;
use App\Model\SupportTicketPriority;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;
use OpenApi\Annotations as OA;

class SupportTicketPriorityController extends BaseController
{
    /**
     * @var SupportTicketPriority
     */
    private $priority;


    /**
     * SupportTicketPriorityController constructor.
     * @param SupportTicketPriority $priority
     */
    public function __construct(SupportTicketPriority $priority)
    {
        $this->priority = $priority;
    }

    /**
     * @return JsonResponse
     *
     * @OA\Get(
     *     path="/api/admin/support/priority/list",
     *     tags={"Admin Support Priority"},
     *     security={{"api_key": {}}},
     *     summary="Liste des priorité",
     *      @OA\Response(
     *          response="200",
     *          description="Tableau de la liste des priorité",
     *          @OA\JsonContent(
     *              type="object",
     *          )
     *      )
     * )
     */
    public function list()
    {
        try {
            $priorities = $this->priority->newQuery()->get();
        }catch (Exception $exception) {
            Log::error($exception);
            return $this->sendError($exception->getMessage());
        }

        return $this->sendResponse($priorities, null, 200);
    }

    /**
     * @param SupportTicketPriorityRequest $request
     * @return JsonResponse
     *
     * @OA\Post(
     *     tags={"Admin Support Priority"},
     *     path="/api/admin/support/priority",
     *     security={{"api_key": {}}},
     *     summary="Création d'une priorité",
     *     @OA\Response(
     *      response="200",
     *      description="Information sur la priorité créer",
     *      @OA\JsonContent(
     *        type="object",
     *      )
     *     ),
     *     @OA\RequestBody(
     *          description="Information util à la création d'une priorité",
     *          required=true,
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(ref="#/components/schemas/SupportTicketPriorityRequest")
     *          ),
     *          @OA\MediaType(
     *              mediaType="application/x-www-form-urlencoded",
     *              @OA\Schema(ref="#/components/schemas/SupportTicketPriorityRequest")
     *          )
     *     )
     * )
     */
    public function store(SupportTicketPriorityRequest $request)
    {
        try {
            $priority = $this->priority->newQuery()->create($request->all());
        }catch (Exception $exception) {
            Log::error($exception);
            return $this->sendError($exception->getMessage());
        }

        return $this->sendResponse($priority, null, 200);
    }

    /**
     * @param $priority_id
     * @return JsonResponse
     *
     * @OA\Get(
     *     path="/api/admin/support/priority/{priority_id}",
     *     tags={"Admin Support Priority"},
     *     security={{"api_key": {}}},
     *     summary="Information sur une priorité particuliere",
     *      @OA\Parameter(name="priority_id", in="path", description="ID de la priorité", required=true),
     *      @OA\Response(
     *          response="200",
     *          description="Tableau des informations",
     *          @OA\JsonContent(
     *              type="object",
     *          )
     *      )
     * )
     */
    public function get($priority_id)
    {
        try {
            $priority = $this->priority->newQuery()->findOrFail($priority_id);
        }catch (Exception $exception) {
            Log::error($exception);
            return $this->sendError($exception->getMessage());
        }

        return $this->sendResponse($priority, null, 200);
    }

    /**
     * @param SupportTicketPriorityRequest $request
     * @param $priority_id
     * @return JsonResponse
     *
     * @OA\Put(
     *     tags={"Admin Support Priority"},
     *     path="/api/admin/support/priority/{priority_id}",
     *     security={{"api_key": {}}},
     *     summary="Mise à jour d'une priorité",
     *     @OA\Parameter(name="priority_id", in="path", description="ID de la priorité", required=true),
     *     @OA\Response(
     *      response="200",
     *      description="Information sur la priorité mise à jour",
     *      @OA\JsonContent(
     *        type="object",
     *      )
     *     ),
     *     @OA\RequestBody(
     *          description="Information pour la mise à jour de la priorité",
     *          required=true,
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(ref="#/components/schemas/SupportTicketPriorityRequest")
     *          ),
     *          @OA\MediaType(
     *              mediaType="application/x-www-form-urlencoded",
     *              @OA\Schema(ref="#/components/schemas/SupportTicketPriorityRequest")
     *          )
     *     )
     * )
     */
    public function update(SupportTicketPriorityRequest $request, $priority_id)
    {
        try {
            $priority = $this->priority->newQuery()->findOrFail($priority_id);
            $priority->update($request->all());
        }catch (Exception $exception) {
            Log::error($exception);
            return $this->sendError($exception->getMessage());
        }

        return $this->sendResponse($priority, null, 200);
    }

    /**
     * @param $priority_id
     * @return JsonResponse
     *
     * @OA\Delete(
     *     path="/api/admin/support/priority/{priority_id}",
     *     tags={"Admin Support Priority"},
     *     security={{"api_key": {}}},
     *     summary="Supprime une priorité",
     *      @OA\Parameter(name="priority_id", in="path", description="ID de la priorité", required=true),
     *      @OA\Response(
     *          response="200",
     *          description="NULL",
     *          @OA\JsonContent(
     *              @OA\Property(property="NULL", type="string", example="NULL")
     *          )
     *      ),
     * )
     */
    public function delete($priority_id)
    {
        try {
            $priority = $this->priority->newQuery()->findOrFail($priority_id);
            $priority->delete();
        }catch (Exception $exception) {
            Log::error($exception);
            return $this->sendError($exception->getMessage());
        }

        return $this->sendResponse(null, null, 200);
    }
}
