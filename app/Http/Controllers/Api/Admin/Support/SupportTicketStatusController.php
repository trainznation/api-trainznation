<?php

namespace App\Http\Controllers\Api\Admin\Support;

use App\Http\Controllers\Api\BaseController;
use App\Http\Requests\Support\SupportTicketSourceRequest;
use App\Http\Requests\Support\SupportTicketStatusRequest;
use App\Model\SupportTicketStatus;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;
use OpenApi\Annotations as OA;


class SupportTicketStatusController extends BaseController
{
    /**
     * @var SupportTicketStatus
     */
    private $status;

    /**
     * SupportTicketStatusController constructor.
     * @param SupportTicketStatus $status
     */
    public function __construct(SupportTicketStatus $status)
    {
        $this->status = $status;
    }

    /**
     * @return JsonResponse
     *
     * @OA\Get(
     *     path="/api/admin/support/status/list",
     *     tags={"Admin Support Status"},
     *     security={{"api_key": {}}},
     *     summary="Liste des status",
     *      @OA\Response(
     *          response="200",
     *          description="Tableau de la liste des status",
     *          @OA\JsonContent(
     *              type="object",
     *          )
     *      )
     * )
     */
    public function list()
    {
        try {
            $statuses = $this->status->newQuery()->get();
        }catch (Exception $exception) {
            Log::error($exception);
            return $this->sendError($exception->getMessage());
        }

        return $this->sendResponse($statuses, null, 200);
    }

    /**
     * @param SupportTicketStatusRequest $request
     * @return JsonResponse
     *
     * @OA\Post(
     *     tags={"Admin Support Status"},
     *     path="/api/admin/support/status",
     *     security={{"api_key": {}}},
     *     summary="Création d'un status",
     *     @OA\Response(
     *      response="200",
     *      description="Information sur le status créer",
     *      @OA\JsonContent(
     *        type="object",
     *      )
     *     ),
     *     @OA\RequestBody(
     *          description="Information util à la création d'un status",
     *          required=true,
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(ref="#/components/schemas/SupportTicketStatusRequest")
     *          ),
     *          @OA\MediaType(
     *              mediaType="application/x-www-form-urlencoded",
     *              @OA\Schema(ref="#/components/schemas/SupportTicketStatusRequest")
     *          )
     *     )
     * )
     */
    public function store(SupportTicketStatusRequest $request)
    {
        try {
            $status = $this->status->newQuery()->create($request->all());
        }catch (Exception $exception) {
            Log::error($exception);
            return $this->sendError($exception->getMessage());
        }

        return $this->sendResponse($status, null, 200);
    }

    /**
     * @param $status_id
     * @return JsonResponse
     *
     * @OA\Get(
     *     path="/api/admin/support/status/{status_id}",
     *     tags={"Admin Support Status"},
     *     security={{"api_key": {}}},
     *     summary="Information sur un status particuliere",
     *      @OA\Parameter(name="status_id", in="path", description="ID du status", required=true),
     *      @OA\Response(
     *          response="200",
     *          description="Tableau des informations",
     *          @OA\JsonContent(
     *              type="object",
     *          )
     *      )
     * )
     */
    public function get($status_id)
    {
        try {
            $status = $this->status->newQuery()->findOrFail($status_id);
        }catch (Exception $exception) {
            Log::error($exception);
            return $this->sendError($exception->getMessage());
        }

        return $this->sendResponse($status, null, 200);
    }

    /**
     * @param SupportTicketStatusRequest $request
     * @param $status_id
     * @return JsonResponse
     *
     * @OA\Put(
     *     tags={"Admin Support Status"},
     *     path="/api/admin/support/status/{status_id}",
     *     security={{"api_key": {}}},
     *     summary="Mise à jour d'un status",
     *     @OA\Parameter(name="status_id", in="path", description="ID du status", required=true),
     *     @OA\Response(
     *      response="200",
     *      description="Information sur le status mise à jour",
     *      @OA\JsonContent(
     *        type="object",
     *      )
     *     ),
     *     @OA\RequestBody(
     *          description="Information pour la mise à jour du status",
     *          required=true,
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(ref="#/components/schemas/SupportTicketStatusRequest")
     *          ),
     *          @OA\MediaType(
     *              mediaType="application/x-www-form-urlencoded",
     *              @OA\Schema(ref="#/components/schemas/SupportTicketStatusRequest")
     *          )
     *     )
     * )
     */
    public function update(SupportTicketStatusRequest $request, $status_id)
    {
        try {
            $status = $this->status->newQuery()->findOrFail($status_id);
            $status->update($request->all());
        }catch (Exception $exception) {
            Log::error($exception);
            return $this->sendError($exception->getMessage());
        }

        return $this->sendResponse($status, null, 200);
    }

    /**
     * @param $status_id
     * @return JsonResponse
     *
     * @OA\Delete(
     *     path="/api/admin/support/status/{status_id}",
     *     tags={"Admin Support Status"},
     *     security={{"api_key": {}}},
     *     summary="Supprime un status",
     *      @OA\Parameter(name="status_id", in="path", description="ID du status", required=true),
     *      @OA\Response(
     *          response="200",
     *          description="NULL",
     *          @OA\JsonContent(
     *              @OA\Property(property="NULL", type="string", example="NULL")
     *          )
     *      ),
     * )
     */
    public function delete($status_id)
    {
        try {
            $status = $this->status->newQuery()->findOrFail($status_id);
            $status->delete();
        }catch (Exception $exception) {
            Log::error($exception);
            return $this->sendError($exception->getMessage());
        }

        return $this->sendResponse(null, null, 200);
    }
}
