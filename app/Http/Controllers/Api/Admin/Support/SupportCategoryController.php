<?php

namespace App\Http\Controllers\Api\Admin\Support;

use App\Http\Controllers\Api\BaseController;
use App\Http\Controllers\Controller;
use App\Http\Requests\Support\SupportCategoryRequest;
use App\Model\SupportCategory;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use OpenApi\Annotations as OA;


class SupportCategoryController extends BaseController
{
    /**
     * @var SupportCategory
     */
    private $category;

    /**
     * SupportCategoryController constructor.
     * @param SupportCategory $category
     */
    public function __construct(SupportCategory $category)
    {
        $this->category = $category;
    }

    /**
     * @return JsonResponse
     *
     * @OA\Get(
     *     path="/api/admin/support/category/list",
     *     tags={"Admin Support Category"},
     *     security={{"api_key": {}}},
     *     summary="Affiche la liste des catégories",
     *      @OA\Parameter(name="category_id", in="path", description="ID de la catégorie", required=true),
     *      @OA\Response(
     *          response="200",
     *          description="Tableau des catégorie",
     *          @OA\JsonContent(
     *              type="object",
     *          )
     *      )
     * )
     */
    public function list()
    {
        try {
            $categories = $this->category->newQuery()->get();
        }catch (Exception $exception) {
            Log::error($exception);
            return $this->sendError($exception->getMessage());
        }

        return $this->sendResponse($categories, null, 200);
    }

    /**
     * @param SupportCategoryRequest $request
     * @return JsonResponse
     *
     * @OA\Post(
     *     tags={"Admin Support Category"},
     *     path="/api/admin/support/category",
     *     security={{"api_key": {}}},
     *     summary="Création d'une catégorie",
     *     @OA\Response(
     *      response="200",
     *      description="Retourne la catégorie créer",
     *      @OA\JsonContent(
     *        type="object",
     *      )
     *     ),
     *     @OA\RequestBody(
     *          description="Création de la catégorie",
     *          required=true,
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(ref="#/components/schemas/SupportCategoryRequest")
     *          ),
     *          @OA\MediaType(
     *              mediaType="application/x-www-form-urlencoded",
     *              @OA\Schema(ref="#/components/schemas/SupportCategoryRequest")
     *          )
     *     )
     * )
     */

    public function store(SupportCategoryRequest $request)
    {
        try {
            $category = $this->category->newQuery()->create($request->all());
        }catch (Exception $exception) {
            Log::error($exception);
            return $this->sendError($exception->getMessage());
        }

        return $this->sendResponse($category, null, 200);
    }

    /**
     * @param $category_id
     * @return JsonResponse
     *
     * @OA\Get(
     *     path="/api/admin/support/category/{category_id}",
     *     tags={"Admin Support Category"},
     *     security={{"api_key": {}}},
     *     summary="Information sur une categorie",
     *      @OA\Parameter(name="category_id", in="path", description="ID de la catégorie", required=true),
     *      @OA\Response(
     *          response="200",
     *          description="Tableau de la catégorie",
     *          @OA\JsonContent(
     *              type="object",
     *          )
     *      )
     * )
     */
    public function get($category_id)
    {
        try {
            $category = $this->category->newQuery()->findOrFail($category_id);
        }catch (Exception $exception) {
            Log::error($exception);
            return $this->sendError($exception->getMessage());
        }

        return $this->sendResponse($category, null, 200);
    }

    /**
     * @param SupportCategoryRequest $request
     * @param $category_id
     * @return JsonResponse
     *
     * @OA\Put(
     *     tags={"Admin Support Category"},
     *     path="/api/admin/support/category/{category_id}",
     *     security={{"api_key": {}}},
     *     summary="Met à jour une catégorie",
     *     @OA\Parameter(name="category_id", in="path", description="ID de la catégorie", required=true),
     *     @OA\Response(
     *      response="200",
     *      description="NULL",
     *      @OA\JsonContent(
     *        type="null",
     *      )
     *     ),
     *     @OA\RequestBody(
     *          description="Information de mise à jour",
     *          required=true,
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(ref="#/components/schemas/SupportCategoryRequest")
     *          ),
     *          @OA\MediaType(
     *              mediaType="application/x-www-form-urlencoded",
     *              @OA\Schema(ref="#/components/schemas/SupportCategoryRequest")
     *          )
     *     )
     * )
     */
    public function update(SupportCategoryRequest $request, $category_id)
    {
        try {
            $this->category->newQuery()->findOrFail($category_id)
                ->update($request->all());
        }catch (Exception $exception) {
            Log::error($exception);
            return $this->sendError($exception->getMessage());
        }

        return $this->sendResponse(null, null, 200);
    }

    /**
     * @param $category_id
     * @return JsonResponse
     *
     * @OA\Delete(
     *     path="/api/admin/support/category/{category_id}",
     *     tags={"Admin Support Category"},
     *     security={{"api_key": {}}},
     *     summary="Supprime une categorie",
     *      @OA\Parameter(name="category_id", in="path", description="ID de la catégorie", required=true),
     *      @OA\Response(
     *          response="200",
     *          description="null",
     *          @OA\JsonContent(
     *              @OA\Property(property="null", type="string", example="null")
     *          )
     *      ),
     * )
     */
    public function delete($category_id)
    {
        try {
            $this->category->newQuery()->findOrFail($category_id)
                ->delete();
        }catch (Exception $exception) {
            Log::error($exception);
            return $this->sendError($exception->getMessage());
        }

        return $this->sendResponse(null, null, 200);
    }
}
