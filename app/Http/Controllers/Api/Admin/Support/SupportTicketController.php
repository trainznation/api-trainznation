<?php

namespace App\Http\Controllers\Api\Admin\Support;

use App\Http\Controllers\Api\BaseController;
use App\Http\Controllers\Controller;
use App\Http\Requests\Support\SupportTicketRequest;
use App\Http\Resources\Admin\Support\SupportTicketGet;
use App\Http\Resources\Admin\Support\SupportTicketList;
use App\Model\SupportTicket;
use App\Model\SupportTicketConv;
use App\User;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use OpenApi\Annotations as OA;

class SupportTicketController extends BaseController
{
    /**
     * @var SupportTicket
     */
    private $ticket;
    /**
     * @var SupportTicketConv
     */
    private $conv;

    /**
     * SupportTicketController constructor.
     * @param SupportTicket $ticket
     * @param SupportTicketConv $conv
     */
    public function __construct(SupportTicket $ticket, SupportTicketConv $conv)
    {
        $this->ticket = $ticket;
        $this->conv = $conv;
    }

    /**
     * @return JsonResponse
     *
     * @OA\Get(
     *     path="/api/admin/support/ticket/list",
     *     tags={"Admin Support Ticket"},
     *     security={{"api_key": {}}},
     *     summary="Liste des tickets",
     *      @OA\Parameter(name="category_id", in="path", description="ID de la catégorie", required=true),
     *      @OA\Response(
     *          response="200",
     *          description="Tableau de la liste des tickets",
     *          @OA\JsonContent(
     *              type="object",
     *          )
     *      )
     * )
     */
    public function list()
    {
        try {
            $tickets = $this->ticket->newQuery()->get();
        }catch (Exception $exception) {
            Log::error($exception);
            return $this->sendError($exception->getMessage());
        }

        $format = new SupportTicketList();
        return $this->sendResponse($format->toArray($tickets), null, 200);
    }

    /**
     * @param SupportTicketRequest $request
     * @return JsonResponse
     *
     * @OA\Post(
     *     tags={"Admin Support Ticket"},
     *     path="/api/admin/support/ticket",
     *     security={{"api_key": {}}},
     *     summary="Création d'un ticket",
     *     @OA\Response(
     *      response="200",
     *      description="Information sur le ticket créer",
     *      @OA\JsonContent(
     *        type="object",
     *      )
     *     ),
     *     @OA\RequestBody(
     *          description="Information pour créer le ticket",
     *          required=true,
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(ref="#/components/schemas/SupportTicketRequest")
     *          ),
     *          @OA\MediaType(
     *              mediaType="application/x-www-form-urlencoded",
     *              @OA\Schema(ref="#/components/schemas/SupportTicketRequest")
     *          )
     *     )
     * )
     */
    public function store(SupportTicketRequest $request)
    {
        try {
            $user = User::all()->random()->where('admin', 1)->first();
            $ticket = $this->ticket->newQuery()->create([
                "subject" => $request->get('subject'),
                "support_category_id" => $request->get('support_category_id'),
                "support_sector_id" => $request->get('support_sector_id'),
                "support_ticket_priority_id" => $this->definingPriority($request->get('support_sector_id')),
                "support_ticket_status_id" => $request->get('support_ticket_status_id'),
                "support_ticket_source_id" => $request->get('support_ticket_source_id'),
                "requester_id" => $request->get('requester_id'),
                "assign_to" => $user->id
            ]);

            $message = $this->conv->newQuery()->create([
                "message" => $request->get('message'),
                "support_ticket_id" => $ticket->id,
                "user_id" => $request->get('requester_id')
            ]);
        }catch (Exception $exception) {
            Log::error($exception);
            return $this->sendError($exception->getMessage());
        }

        $format = new SupportTicketGet();
        return $this->sendResponse($format->toArray($ticket), null, 200);
    }

    /**
     * @param $ticket_id
     * @return JsonResponse
     *
     * @OA\Get(
     *     path="/api/admin/support/ticket/{ticket_id}",
     *     tags={"Admin Support Ticket"},
     *     security={{"api_key": {}}},
     *     summary="Information sur un ticket",
     *      @OA\Parameter(name="category_id", in="path", description="ID de la catégorie", required=true),
     *      @OA\Response(
     *          response="200",
     *          description="Tableau des informations d'un ticket",
     *          @OA\JsonContent(
     *              type="object",
     *          )
     *      )
     * )
     */
    public function get($ticket_id)
    {
        try {
            $ticket = $this->ticket->newQuery()->findOrFail($ticket_id);
        }catch (Exception $exception) {
            Log::error($exception);
            return $this->sendError($exception);
        }

        $format = new SupportTicketGet();
        return $this->sendResponse($format->toArray($ticket), null, 200);
    }

    /**
     * @param Request $request
     * @param $ticket_id
     * @return JsonResponse
     *
     * @OA\Put(
     *     tags={"Admin Support Ticket"},
     *     path="/api/admin/support/ticket/{ticket_id}",
     *     security={{"api_key": {}}},
     *     summary="Mise à jour d'un ticket",
     *     @OA\Parameter(name="ticket_id", in="path", description="ID du ticket", required=true),
     *     @OA\Response(
     *      response="200",
     *      description="Information sur le ticket mis à jour",
     *      @OA\JsonContent(
     *        type="object",
     *      )
     *     ),
     *     @OA\RequestBody(
     *          description="Information à mettre à jour",
     *          required=true,
     *          @OA\MediaType(
     *              mediaType="application/json",
     *          ),
     *          @OA\MediaType(
     *              mediaType="application/x-www-form-urlencoded",
     *          )
     *     )
     * )
     */
    public function update(Request $request, $ticket_id)
    {
        try {
            $ticket = $this->ticket->newQuery()->findOrFail($ticket_id);
            $ticket->update([
                "subject" => $request->get('subject'),
                "support_category_id" => $request->get('support_category_id'),
                "support_sector_id" => $request->get('support_sector_id'),
                "support_ticket_priority_id" => $this->definingPriority($request->get('support_sector_id')),
                "support_ticket_status_id" => $request->get('support_ticket_status_id'),
                "support_ticket_source_id" => $request->get('support_ticket_source_id'),
                "requester_id" => $request->get('requester_id'),
                "assign_to" => $user->id
            ]);
        }catch (Exception $exception) {
            Log::error($exception);
            return $this->sendError($exception->getMessage());
        }

        $format = new SupportTicketGet();
        return $this->sendResponse($format->toArray($ticket), null, 200);
    }

    /**
     * @param $ticket_id
     * @return JsonResponse
     *
     * @OA\Get(
     *     path="/api/admin/support/ticket/{ticket_id}/close",
     *     tags={"Admin Support Ticket"},
     *     security={{"api_key": {}}},
     *     summary="Cloturer un ticket",
     *      @OA\Parameter(name="ticket_id", in="path", description="ID du ticket", required=true),
     *      @OA\Response(
     *          response="200",
     *          description="NULL",
     *          @OA\JsonContent(
     *              type="object",
     *          )
     *      )
     * )
     */
    public function close($ticket_id)
    {
        try {
            $ticket = $this->ticket->newQuery()->findOrFail($ticket_id);
            $ticket->update([
                "close" => 1
            ]);
        }catch (Exception $exception) {
            Log::error($exception);
            return $this->sendError($exception->getMessage());
        }

        return $this->sendResponse(null, null, 200);
    }


    /**
     * @param $ticket_id
     * @return JsonResponse
     *
     * @OA\Get(
     *     path="/api/admin/support/ticket/{ticket_id}/redeem",
     *     tags={"Admin Support Ticket"},
     *     security={{"api_key": {}}},
     *     summary="Rouvrir un ticket",
     *      @OA\Parameter(name="ticket_id", in="path", description="ID du ticket", required=true),
     *      @OA\Response(
     *          response="200",
     *          description="NULL",
     *          @OA\JsonContent(
     *              type="object",
     *          )
     *      )
     * )
     */
    public function redeem($ticket_id)
    {
        try {
            $ticket = $this->ticket->newQuery()->findOrFail($ticket_id);
            $ticket->update([
                "close" => 0
            ]);
        }catch (Exception $exception) {
            Log::error($exception);
            return $this->sendError($exception->getMessage());
        }

        return $this->sendResponse(null, null, 200);
    }

    /**
     * @param $ticket_id
     * @return JsonResponse
     *
     * @OA\Delete(
     *     path="/api/admin/support/ticket/{ticket_id}",
     *     tags={"Admin Support Ticket"},
     *     security={{"api_key": {}}},
     *     summary="Supprime un ticket",
     *      @OA\Parameter(name="ticket_id", in="path", description="ID du ticket", required=true),
     *      @OA\Response(
     *          response="200",
     *          description="NULL",
     *          @OA\JsonContent(
     *              @OA\Property(property="NULL", type="string", example="NULL")
     *          )
     *      ),
     * )
     */
    public function delete($ticket_id)
    {
        try {
            $ticket = $this->ticket->newQuery()->findOrFail($ticket_id);
            $ticket->delete();
        }catch (Exception $exception) {
            Log::error($exception);
            return $this->sendError($exception->getMessage());
        }

        return $this->sendResponse(null, null, 200);
    }

    private function definingPriority($support_sector_id)
    {
        switch ($support_sector_id)
        {
            case 1: return 2;
            case 2: return 1;
            case 3: return 1;
            case 4: return 3;
            case 5: return 1;
            case 6: return 1;
            case 7: return 3;
            case 8: return 4;
            case 9: return 3;
            case 10: return 2;
            case 11: return 2;
        }
    }
}
