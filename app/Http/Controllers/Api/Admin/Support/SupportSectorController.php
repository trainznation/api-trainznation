<?php

namespace App\Http\Controllers\Api\Admin\Support;

use App\Http\Controllers\Api\BaseController;
use App\Http\Controllers\Controller;
use App\Http\Requests\SupportSectorRequest;
use App\Http\Resources\Admin\Support\SupportSectorGet;
use App\Http\Resources\Admin\Support\SupportSectorList;
use App\Model\SupportSector;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use OpenApi\Annotations as OA;

class SupportSectorController extends BaseController
{
    /**
     * @var SupportSector
     */
    private $sector;

    /**
     * SupportSectorController constructor.
     * @param SupportSector $sector
     */
    public function __construct(SupportSector $sector)
    {
        $this->sector = $sector;
    }

    /**
     * @return JsonResponse
     *
     * @OA\Get(
     *     path="/api/admin/support/sector/list",
     *     tags={"Admin Support Sector"},
     *     security={{"api_key": {}}},
     *     summary="Liste des secteurs",
     *      @OA\Response(
     *          response="200",
     *          description="Tableau de la liste des secteurs",
     *          @OA\JsonContent(
     *              type="object",
     *          )
     *      )
     * )
     */
    public function list()
    {
        try {
            $sectors = $this->sector->newQuery()->get();
        }catch (Exception $exception) {
            Log::error($exception);
            return $this->sendError($exception->getMessage());
        }

        $format = new SupportSectorList();
        return $this->sendResponse($format->toArray($sectors), null, 200);
    }

    /**
     * @param SupportSectorRequest $request
     * @return JsonResponse
     *
     * @OA\Post(
     *     tags={"Admin Support Sector"},
     *     path="/api/admin/support/sector",
     *     security={{"api_key": {}}},
     *     summary="Création d'un secteur",
     *     @OA\Response(
     *      response="200",
     *      description="Information sur le secteur créer",
     *      @OA\JsonContent(
     *        type="object",
     *      )
     *     ),
     *     @OA\RequestBody(
     *          description="Information util à la création d'un secteur",
     *          required=true,
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(ref="#/components/schemas/SupportSectorRequest")
     *          ),
     *          @OA\MediaType(
     *              mediaType="application/x-www-form-urlencoded",
     *              @OA\Schema(ref="#/components/schemas/SupportSectorRequest")
     *          )
     *     )
     * )
     */
    public function store(SupportSectorRequest $request)
    {
        try {
            $sector = $this->sector->newQuery()->create($request->all());
        }catch (Exception $exception) {
            Log::error($exception);
            return $this->sendError($exception->getMessage());
        }

        return $this->sendResponse($sector, null, 200);
    }

    /**
     * @param $sector_id
     * @return JsonResponse
     *
     * @OA\Get(
     *     path="/api/admin/support/sector/{sector_id}",
     *     tags={"Admin Support Sector"},
     *     security={{"api_key": {}}},
     *     summary="Information sur un secteur particulier",
     *      @OA\Parameter(name="sector_id", in="path", description="ID du secteur", required=true),
     *      @OA\Response(
     *          response="200",
     *          description="Tableau des informations",
     *          @OA\JsonContent(
     *              type="object",
     *          )
     *      )
     * )
     */
    public function get($sector_id)
    {
        try {
            $sector = $this->sector->newQuery()->findOrFail($sector_id);
        }catch (Exception $exception) {
            Log::error($exception);
            return $this->sendError($exception->getMessage());
        }

        $format = new SupportSectorGet();
        return $this->sendResponse($format->toArray($sector), null, 200);
    }

    /**
     * @param SupportSectorRequest $request
     * @param $sector_id
     * @return JsonResponse
     *
     * @OA\Put(
     *     tags={"Admin Support Sector"},
     *     path="/api/admin/support/sector/{sector_id}",
     *     security={{"api_key": {}}},
     *     summary="Mise à jour d'un secteur",
     *     @OA\Parameter(name="sector_id", in="path", description="ID du secteur", required=true),
     *     @OA\Response(
     *      response="200",
     *      description="Information sur le secteur mis à jour",
     *      @OA\JsonContent(
     *        type="object",
     *      )
     *     ),
     *     @OA\RequestBody(
     *          description="Information pour la mise à jour du secteur",
     *          required=true,
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(ref="#/components/schemas/SupportSectorRequest")
     *          ),
     *          @OA\MediaType(
     *              mediaType="application/x-www-form-urlencoded",
     *              @OA\Schema(ref="#/components/schemas/SupportSectorRequest")
     *          )
     *     )
     * )
     */
    public function update(SupportSectorRequest $request, $sector_id)
    {
        try {
            $sector = $this->sector->newQuery()->findOrFail($sector_id);
            $sector->update($request->all());
        }catch (Exception $exception) {
            Log::error($exception);
            return $this->sendError($exception->getMessage());
        }

        return $this->sendResponse($sector, null, 200);
    }

    /**
     * @param $sector_id
     * @return JsonResponse
     *
     * @OA\Delete(
     *     path="/api/admin/support/sector/{sector_id}",
     *     tags={"Admin Support Sector"},
     *     security={{"api_key": {}}},
     *     summary="Supprime un secteur",
     *      @OA\Parameter(name="sector_id", in="path", description="ID du secteur", required=true),
     *      @OA\Response(
     *          response="200",
     *          description="NULL",
     *          @OA\JsonContent(
     *              @OA\Property(property="NULL", type="string", example="NULL")
     *          )
     *      ),
     * )
     */
    public function delete($sector_id)
    {
        try {
            $sector = $this->sector->newQuery()->findOrFail($sector_id);
            $sector->delete();
        }catch (Exception $exception) {
            Log::error($exception);
            return $this->sendError($exception->getMessage());
        }

        return $this->sendResponse(null, null, 200);
    }
}
