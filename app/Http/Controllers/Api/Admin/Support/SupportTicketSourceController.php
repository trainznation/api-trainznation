<?php

namespace App\Http\Controllers\Api\Admin\Support;

use App\Http\Controllers\Api\BaseController;
use App\Http\Requests\Support\SupportTicketPriorityRequest;
use App\Http\Requests\Support\SupportTicketSourceRequest;
use App\Model\SupportTicketPriority;
use App\Model\SupportTicketSource;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;

class SupportTicketSourceController extends BaseController
{
    /**
     * @var SupportTicketSource
     */
    private $source;

    /**
     * SupportTicketSourceController constructor.
     * @param SupportTicketSource $source
     */
    public function __construct(SupportTicketSource $source)
    {
        $this->source = $source;
    }

    /**
     * @return JsonResponse
     *
     * @OA\Get(
     *     path="/api/admin/support/source/list",
     *     tags={"Admin Support Source"},
     *     security={{"api_key": {}}},
     *     summary="Liste des sources",
     *      @OA\Response(
     *          response="200",
     *          description="Tableau de la liste des sources",
     *          @OA\JsonContent(
     *              type="object",
     *          )
     *      )
     * )
     */
    public function list()
    {
        try {
            $sources = $this->source->newQuery()->get();
        }catch (Exception $exception) {
            Log::error($exception);
            return $this->sendError($exception->getMessage());
        }

        return $this->sendResponse($sources, null, 200);
    }

    /**
     * @param SupportTicketSourceRequest $request
     * @return JsonResponse
     *
     * @OA\Post(
     *     tags={"Admin Support Source"},
     *     path="/api/admin/support/source",
     *     security={{"api_key": {}}},
     *     summary="Création d'une source",
     *     @OA\Response(
     *      response="200",
     *      description="Information sur la source créer",
     *      @OA\JsonContent(
     *        type="object",
     *      )
     *     ),
     *     @OA\RequestBody(
     *          description="Information util à la création d'une source",
     *          required=true,
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(ref="#/components/schemas/SupportTicketSourceRequest")
     *          ),
     *          @OA\MediaType(
     *              mediaType="application/x-www-form-urlencoded",
     *              @OA\Schema(ref="#/components/schemas/SupportTicketSourceRequest")
     *          )
     *     )
     * )
     */
    public function store(SupportTicketSourceRequest $request)
    {
        try {
            $source = $this->source->newQuery()->create($request->all());
        }catch (Exception $exception) {
            Log::error($exception);
            return $this->sendError($exception->getMessage());
        }

        return $this->sendResponse($source, null, 200);
    }

    /**
     * @param $source_id
     * @return JsonResponse
     *
     * @OA\Get(
     *     path="/api/admin/support/source/{source_id}",
     *     tags={"Admin Support Source"},
     *     security={{"api_key": {}}},
     *     summary="Information sur une source particuliere",
     *      @OA\Parameter(name="source_id", in="path", description="ID de la source", required=true),
     *      @OA\Response(
     *          response="200",
     *          description="Tableau des informations",
     *          @OA\JsonContent(
     *              type="object",
     *          )
     *      )
     * )
     */
    public function get($source_id)
    {
        try {
            $source = $this->source->newQuery()->findOrFail($source_id);
        }catch (Exception $exception) {
            Log::error($exception);
            return $this->sendError($exception->getMessage());
        }

        return $this->sendResponse($source, null, 200);
    }

    /**
     * @param SupportTicketSourceRequest $request
     * @param $source_id
     * @return JsonResponse
     *
     * @OA\Put(
     *     tags={"Admin Support Source"},
     *     path="/api/admin/support/source/{source_id}",
     *     security={{"api_key": {}}},
     *     summary="Mise à jour d'une source",
     *     @OA\Parameter(name="source_id", in="path", description="ID de la source", required=true),
     *     @OA\Response(
     *      response="200",
     *      description="Information sur la source mise à jour",
     *      @OA\JsonContent(
     *        type="object",
     *      )
     *     ),
     *     @OA\RequestBody(
     *          description="Information pour la mise à jour de la source",
     *          required=true,
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(ref="#/components/schemas/SupportTicketSourceRequest")
     *          ),
     *          @OA\MediaType(
     *              mediaType="application/x-www-form-urlencoded",
     *              @OA\Schema(ref="#/components/schemas/SupportTicketSourceRequest")
     *          )
     *     )
     * )
     */
    public function update(SupportTicketSourceRequest $request, $source_id)
    {
        try {
            $source = $this->source->newQuery()->findOrFail($source_id);
            $source->update($request->all());
        }catch (Exception $exception) {
            Log::error($exception);
            return $this->sendError($exception->getMessage());
        }

        return $this->sendResponse($source, null, 200);
    }

    /**
     * @param $source_id
     * @return JsonResponse
     *
     * @OA\Delete(
     *     path="/api/admin/support/source/{source_id}",
     *     tags={"Admin Support Source"},
     *     security={{"api_key": {}}},
     *     summary="Supprime une source",
     *      @OA\Parameter(name="source_id", in="path", description="ID de la source", required=true),
     *      @OA\Response(
     *          response="200",
     *          description="NULL",
     *          @OA\JsonContent(
     *              @OA\Property(property="NULL", type="string", example="NULL")
     *          )
     *      ),
     * )
     */
    public function delete($source_id)
    {
        try {
            $source = $this->source->newQuery()->findOrFail($source_id);
            $source->delete();
        }catch (Exception $exception) {
            Log::error($exception);
            return $this->sendError($exception->getMessage());
        }

        return $this->sendResponse(null, null, 200);
    }
}
