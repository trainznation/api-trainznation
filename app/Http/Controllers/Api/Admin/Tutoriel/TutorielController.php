<?php

namespace App\Http\Controllers\Api\Admin\Tutoriel;

use App\Http\Controllers\Api\BaseController;
use App\Http\Controllers\Controller;
use App\Http\Requests\Tutoriel\StoreTutorielRequest;
use App\Http\Resources\Admin\Tutoriel\TutorielGet;
use App\Http\Resources\Admin\Tutoriel\TutorielList;
use App\Model\Tutoriel;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class TutorielController extends BaseController
{
    /**
     * @var Tutoriel
     */
    private $tutoriel;

    /**
     * TutorielController constructor.
     * @param Tutoriel $tutoriel
     */
    public function __construct(Tutoriel $tutoriel)
    {
        $this->tutoriel = $tutoriel;
    }

    /**
     * @return JsonResponse
     *
     * @OA\Get(
     *     path="/api/admin/tutoriel/list",
     *     tags={"Admin Tutoriel"},
     *     security={{"api_key": {}}},
     *     summary="Liste des tutoriels",
     *      @OA\Response(
     *          response="200",
     *          description="Tableau des tutoriels",
     *          @OA\JsonContent(
     *              type="array",
     *              @OA\Items(ref="#/components/schemas/AdminTutorielList")
     *          )
     *      )
     * )
     */
    public function list()
    {
        try {
            $tutoriel = $this->tutoriel->newQuery()->get();
        }catch (Exception $exception) {
            return $this->sendError($exception->getMessage(), null, 510);
        }

        $format = new TutorielList();
        return $this->sendResponse($format->toArray($tutoriel), null, 200);
    }

    /**
     * @param StoreTutorielRequest $request
     * @return JsonResponse
     *
     * @OA\Post(
     *     tags={"Admin Tutoriel"},
     *     path="/api/admin/tutoriel",
     *     security={{"api_key": {}}},
     *     summary="Création d'un tutoriel",
     *     @OA\Response(
     *      response="200",
     *      description="Tableau du tutoriel créer",
     *      @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref="#/components/schemas/AdminTutorielGet")
     *      )
     *     ),
     *     @OA\RequestBody(
     *          description="Information pour la création d'un tutoriel",
     *          required=true,
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(ref="#/components/schemas/AdminTutorielRequest")
     *          ),
     *          @OA\MediaType(
     *              mediaType="application/x-www-form-urlencoded",
     *              @OA\Schema(ref="#/components/schemas/AdminTutorielRequest")
     *          )
     *     )
     * )
     */
    public function store(StoreTutorielRequest $request)
    {
        try {
            $tutoriel = $this->tutoriel->newQuery()->create([
                "tutoriel_category_id" => $request->get('tutoriel_category_id'),
                "title" => $request->get('title'),
                "slug" => Str::slug($request->get('title')),
                "short_content" => $request->get('short_content'),
                "content" => $request->get('content'),
                "published" => $request->get('published'),
                "source" => $request->get('source'),
                "premium" => $request->get('premium'),
                "social" => $request->get('social'),
                "time" => $request->get('time'),
                "published_at" => ($request->get('published') == 1) ? $request->get('published_at') : null
            ]);
        }catch (Exception $exception) {
            return $this->sendError($exception->getMessage(), null, 510);
        }

        $format = new TutorielGet();
        return $this->sendResponse($format->toArray($tutoriel), null, 200);
    }

    /**
     * @param $tutoriel_id
     * @return JsonResponse
     *
     * @OA\Get(
     *     path="/api/admin/tutoriel/{tutoriel_id}",
     *     tags={"Admin Tutoriel"},
     *     security={{"api_key": {}}},
     *     summary="Information sur un tutoriel",
     *      @OA\Parameter(name="category_id", in="path", description="ID de la catégorie", required=true),
     *      @OA\Response(
     *          response="200",
     *          description="Tableau d'information sur le tutoriel",
     *          @OA\JsonContent(
     *              type="array",
     *              @OA\Items(ref="#/components/schemas/AdminTutorielGet")
     *          )
     *      )
     * )
     */
    public function get($tutoriel_id)
    {
        try {
            $tutoriel = $this->tutoriel->newQuery()->findOrFail($tutoriel_id);
        }catch (Exception $exception) {
            return $this->sendError($exception->getMessage(), null, 510);
        }

        $format = new TutorielGet();
        return $this->sendResponse($format->toArray($tutoriel), null, 200);
    }

    /**
     * @param Request $request
     * @param $tutoriel_id
     * @return JsonResponse
     *
     * @OA\Put(
     *     tags={"Admin Tutoriel"},
     *     path="/api/admin/tutoriel/{tutoriel_id}",
     *     security={{"api_key": {}}},
     *     summary="Met a jour un tutoriel",
     *     @OA\Parameter(name="tutoriel_id", in="path", description="ID du tutoriel", required=true),
     *     @OA\Response(
     *      response="200",
     *      description="Tableau du tutoriel mis à jour",
     *      @OA\JsonContent(
     *        type="object",
     *      )
     *     ),
     *     @OA\RequestBody(
     *          description="Requete de mise à jour",
     *          required=true,
     *          @OA\MediaType(
     *              mediaType="application/json",
     *          ),
     *          @OA\MediaType(
     *              mediaType="application/x-www-form-urlencoded",
     *          )
     *     )
     * )
     */
    public function update(Request $request, $tutoriel_id)
    {
        try {
            $tutoriel = $this->tutoriel->newQuery()->findOrFail($tutoriel_id);
            $tutoriel->update($request->all());
        }catch (Exception $exception) {
            return $this->sendError($exception->getMessage(), null, 510);
        }

        $format = new TutorielGet();
        return $this->sendResponse($format->toArray($tutoriel), null, 200);
    }
}
