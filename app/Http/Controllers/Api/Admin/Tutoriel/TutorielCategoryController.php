<?php

namespace App\Http\Controllers\Api\Admin\Tutoriel;

use App\Http\Controllers\Api\BaseController;
use App\Http\Controllers\Controller;
use App\Http\Requests\Tutoriel\StoreTutorielCategoryRequest;
use App\Http\Resources\Admin\Tutoriel\TutorielCategoryGet;
use App\Http\Resources\Admin\Tutoriel\TutorielCategoryList;
use App\Model\TutorielCategory;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use OpenApi\Annotations as OA;

class TutorielCategoryController extends BaseController
{
    /**
     * @var TutorielCategory
     */
    private $category;

    /**
     * TutorielCategoryController constructor.
     * @param TutorielCategory $category
     */
    public function __construct(TutorielCategory $category)
    {
        $this->category = $category;
    }

    /**
     * @return JsonResponse
     *
     * @OA\Get(
     *     path="/api/admin/tutoriel/category/list",
     *     tags={"Admin Tutoriel Category"},
     *     security={{"api_key": {}}},
     *     summary="Liste des catégories de tutoriel",
     *      @OA\Response(
     *          response="200",
     *          description="Tableau des catégories",
     *          @OA\JsonContent(
     *              type="array",
     *              @OA\Items(ref="#/components/schemas/AdminTutorielCategoryList")
     *          )
     *      )
     * )
     */
    public function list()
    {
        try {
            $categories = $this->category->newQuery()->get();
        }catch (Exception $exception) {
            return $this->sendError($exception->getMessage(), null, 510);
        }

        $format = new TutorielCategoryList();
        return $this->sendResponse($format->toArray($categories), null, 200);
    }

    /**
     * @param StoreTutorielCategoryRequest $request
     * @return JsonResponse
     *
     * @OA\Post(
     *     tags={"Admin Tutoriel Category"},
     *     path="/api/admin/tutoriel/category",
     *     security={{"api_key": {}}},
     *     summary="Création d'une catégorie",
     *     @OA\Response(
     *      response="200",
     *      description="Tableau des informations de la catégorie créer",
     *      @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref="#/components/schemas/AdminTutorielCategoryGet")
     *      )
     *     ),
     *     @OA\RequestBody(
     *          description="Création de la catégorie",
     *          required=true,
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(ref="#/components/schemas/AdminTutorielCategoryRequest")
     *          ),
     *          @OA\MediaType(
     *              mediaType="application/x-www-form-urlencoded",
     *              @OA\Schema(ref="#/components/schemas/AdminTutorielCategoryRequest")
     *          )
     *     )
     * )
     */
    public function store(StoreTutorielCategoryRequest $request)
    {
        try {
            $category = $this->category->newQuery()->create([
                "name" => $request->get('name'),
                "slug" => Str::slug($request->get('name'))
            ]);
        }catch (Exception $exception) {
            return $this->sendError($exception->getMessage(), 510);
        }

        $format = new TutorielCategoryGet();
        return $this->sendResponse($format->toArray($category), null, 200);
    }

    /**
     * @param $category_id
     * @return JsonResponse
     *
     * @OA\Get(
     *     path="/api/admin/tutoriel/category/{category_id}",
     *     tags={"Admin Tutoriel Category"},
     *     security={{"api_key": {}}},
     *     summary="Information sur une catégorie",
     *      @OA\Parameter(name="category_id", in="path", description="ID de la catégorie", required=true),
     *      @OA\Response(
     *          response="200",
     *          description="Tableau d'information",
     *          @OA\JsonContent(
     *              type="array",
     *              @OA\Items(ref="#/components/schemas/AdminTutorielCategoryGet")
     *          )
     *      )
     * )
     */
    public function get($category_id)
    {
        try {
            $category = $this->category->newQuery()->find($category_id);
        }catch (Exception $exception) {
            return $this->sendError($exception->getMessage(), null, 510);
        }

        $format = new TutorielCategoryGet();
        return  $this->sendResponse($format->toArray($category), null, 200);
    }

    /**
     * @param $category_id
     * @return JsonResponse
     *
     * @OA\Delete(
     *     path="/api/admin/tutoriel/category/{category_id}",
     *     tags={"Admin Tutoriel Category"},
     *     security={{"api_key": {}}},
     *     summary="Supprime une catégorie de tutoriel",
     *      @OA\Parameter(name="category_id", in="path", description="ID de la catégorie", required=true),
     *      @OA\Response(
     *          response="200",
     *          description="Message de succès",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Catégorie supprimer")
     *          )
     *      ),
     *      @OA\Response(
     *          response="510",
     *          description="Message d'exception",
     *          @OA\JsonContent(
     *              @OA\Property(property="error", type="string", example="Exception")
     *          )
     *      )
     * )
     */
    public function delete($category_id)
    {
        try {
            $this->category->newQuery()->find($category_id)->delete();
        }catch (Exception $exception) {
            return $this->sendError($exception->getMessage(), null, 510);
        }

        return  $this->sendResponse(["message" => "La catégorie de tutoriel à été supprimé"], null, 200);
    }
}
