<?php

namespace App\Http\Controllers\Api\Admin\Asset;

use App\Helpers\Notificator;
use App\Http\Controllers\Api\BaseController;
use App\Http\Controllers\Controller;
use App\Http\Requests\Asset\StoreAssetKuidRequest;
use App\Http\Resources\Admin\Asset\AssetKuidGet;
use App\Model\AssetKuid;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class AssetKuidController extends BaseController
{
    /**
     * @var AssetKuid
     */
    private $assetKuid;
    /**
     * @var Notificator
     */
    private $notificator;

    /**
     * AssetKuidController constructor.
     * @param AssetKuid $assetKuid
     * @param Notificator $notificator
     */
    public function __construct(AssetKuid $assetKuid, Notificator $notificator)
    {
        $this->assetKuid = $assetKuid;
        $this->notificator = $notificator;
    }

    /**
     * @param StoreAssetKuidRequest $request
     * @param $asset_id
     * @return JsonResponse
     *
     * @OA\Post(
     *     tags={"Admin Asset"},
     *     path="/api/admin/asset/{asset_id}/kuid",
     *     security={{"api_key": {}}},
     *     summary="Création d'un kuid à un asset particulier",
     *     @OA\Parameter(name="asset_id", in="path", description="ID de l'objet", required=true),
     *     @OA\Response(
     *      response="200",
     *      description="Retourne le kuid créer",
     *      @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref="#/components/schemas/AdminAssetKuidGet")
     *      )
     *     ),
     *     @OA\RequestBody(
     *          description="Création du Kuid",
     *          required=true,
     *          @OA\MediaType(
     *              mediaType="application/x-www-form-urlencoded",
     *              @OA\Schema(ref="#/components/schemas/PostAssetKuidRequest")
     *          ),
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(ref="#/components/schemas/PostAssetKuidRequest")
     *          )
     *     )
     * )
     */
    public function store(StoreAssetKuidRequest $request, $asset_id)
    {
        $format = new AssetKuidGet();
        try {
            $kuid = $this->assetKuid->newQuery()->create([
                "kuid" => $request->get('kuid'),
                "published_at" => ($request->has('published_at')) ? $request->get('published_at') : null,
                "asset_id" => $asset_id
            ]);

            if($request->has('published_at')) {
                $this->notificator->publish($kuid->asset->designation.": Une nouvelle version est disponible en téléchargement", "success", null, "fas fa-cubes", $kuid->asset->image);
            }
        }catch (Exception $exception) {
            Log::error($exception->getMessage());
            return $this->sendError($exception->getMessage());
        }

        return $this->sendResponse($format->toArray($kuid), null, 200);
    }

    /**
     * @param $asset_id
     * @param $asset_kuid_id
     * @return JsonResponse
     *
     * @OA\Delete(
     *     path="/api/admin/asset/{asset_id}/kuid/{asset_kuid_id}",
     *     tags={"Admin Asset"},
     *     security={{"api_key": {}}},
     *     summary="Suppression d'un kuid",
     *      @OA\Parameter(name="asset_id", in="path", description="ID de l'objet'", required=true),
     *      @OA\Parameter(name="asset_kuid_id", in="path", description="ID du kuid'", required=true),
     *      @OA\Response(
     *          response="200",
     *          description="Retourne un message indiquant la bonne suppression du kuid",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Le kuid à été supprimer")
     *          )
     *      ),
     *      @OA\Response(
     *          response="510",
     *          description="Suppression impossible / Erreur de suppression",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Message d'exception")
     *          )
     *      )
     * )
     */
    public function destroy($asset_id, $asset_kuid_id)
    {
        $kuid = $this->assetKuid->newQuery()->findOrFail($asset_kuid_id);
        try {
            $kuid->delete();
            return $this->sendResponse(["message" => "Le kuid $kuid->id à été supprimer"], null, 200);
        }catch (Exception $exception) {
            Log::error($exception->getMessage());
            return $this->sendError("Erreur Système", ["error" => $exception->getMessage()], 510);
        }
    }
}
