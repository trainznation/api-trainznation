<?php

namespace App\Http\Controllers\Api\Admin\Asset;

use App\Http\Controllers\Api\BaseController;
use App\Model\Asset;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use OpenApi\Annotations as OA;

class AssetTagsController extends BaseController
{
    /**
     * @var Asset
     */
    private $asset;

    /**
     * AssetTagsController constructor.
     * @param Asset $asset
     */
    public function __construct(Asset $asset)
    {
        $this->asset = $asset;
    }


    /**
     * @param Request $request
     * @param $asset_id
     *
     * @OA\Post(
     *     tags={"Admin Asset"},
     *     path="/api/admin/asset/{asset_id}/tags",
     *     security={{"api_key": {}}},
     *     summary="Création d'un tag",
     *     @OA\Parameter(name="asset_id", in="path", description="ID de l'objet", required=true),
     *      @OA\Response(
     *          response="200",
     *          description="Retourne un message indiquant la bonne insertion d'un tag'",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Le ou les tags ont été ajouté")
     *          )
     *      ),
     *      @OA\Response(
     *          response="510",
     *          description="Erreur d'insertion",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Message d'exception")
     *          )
     *      ),
     *     @OA\RequestBody(
     *          description="Création du ou des tags",
     *          required=true,
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(ref="#/components/schemas/PostAssetTagRequest")
     *          ),
     *          @OA\MediaType(
     *              mediaType="application/x-www-form-urlencoded",
     *              @OA\Schema(ref="#/components/schemas/PostAssetTagRequest")
     *          )
     *     )
     * )
     * @return JsonResponse
     */
    public function store(Request $request, $asset_id)
    {
        $tags = explode(',', $request->get('tags'));
        $asset = $this->asset->newQuery()->findOrFail($asset_id);

        try {
            foreach ($tags as $tag) {
                $asset->attachTag($tag);
            }
            return $this->sendResponse(['message' => "Le ou les tags ont été ajoutés avec succès"], null, 200);
        }catch (Exception $exception) {
            Log::error($exception->getMessage());
            return  $this->sendError("Erreur système", ["error" => $exception], 510);
        }

    }

    /**
     * @param $asset_id
     * @param $tag
     * @return JsonResponse
     *
     * @OA\Delete(
     *     path="/api/admin/asset/{asset_id}/tags/{tag}",
     *     tags={"Admin Asset"},
     *     security={{"api_key": {}}},
     *     summary="Suppression d'un tag",
     *      @OA\Parameter(name="asset_id", in="path", description="ID de l'objet", required=true),
     *      @OA\Parameter(name="tag", in="path", description="Désignation du tag", required=true),
     *      @OA\Response(
     *          response="200",
     *          description="Retourne un message indiquant la bonne suppression du tag",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Le tag à été supprimer")
     *          )
     *      ),
     *      @OA\Response(
     *          response="510",
     *          description="Suppression impossible / Erreur de suppression",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Message d'exception")
     *          )
     *      )
     * )
     */
    public function delete($asset_id, $tag)
    {
        $asset = $this->asset->newQuery()->findOrFail($asset_id);

        try {
            $asset->detachTag($tag);
            return $this->sendResponse(['message' => "Le tag à été supprimer avec succès"], null, 200);
        }catch (Exception $exception) {
            Log::error($exception->getMessage());
            return $this->sendError("Erreur System", ['error' => $exception], 510);
        }
    }
}
