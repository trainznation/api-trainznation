<?php

namespace App\Http\Controllers\Api\Admin\Asset;

use App\Events\Asset\AssetPublishEvent;
use App\Helpers\Notificator;
use App\Http\Controllers\Api\BaseController;
use App\Http\Controllers\Controller;
use App\Http\Requests\Asset\StoreAssetRequest;
use App\Http\Resources\Admin\Asset\AssetGet;
use App\Http\Resources\Admin\Asset\AssetsList;
use App\Model\Asset;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use OpenApi\Annotations as OA;

/**
 * Class AssetController
 * @package App\Http\Controllers\Api\Admin\Asset
 * @OA\Tag(name="Admin Asset")
 */
class AssetController extends BaseController
{
    /**
     * @var Asset
     */
    private $asset;
    /**
     * @var Notificator
     */
    private $notificator;

    /**
     * AssetController constructor.
     * @param Asset $asset
     * @param Notificator $notificator
     */
    public function __construct(Asset $asset, Notificator $notificator)
    {
        $this->asset = $asset;
        $this->notificator = $notificator;
    }

    /**
     * @return JsonResponse
     * @OA\Get(
     *     path="/api/admin/asset",
     *     security={{"api_key": {}}},
     *     summary="Liste des objets",
     *     tags={"Admin Asset"},
     *      @OA\Response(
     *          response="200",
     *          description="Retourne la liste des objets",
     *          @OA\JsonContent(
     *              type="array",
     *              @OA\Items(ref="#/components/schemas/AdminAssets")
     *          )
     *      )
     * )
     */
    public function all()
    {
        $format = new AssetsList();
        $assets = $this->asset->newQuery()->get();

        return $this->sendResponse($format->toArray($assets), null, 200);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     *
     * @OA\Post(
     *     tags={"Admin Asset"},
     *     path="/api/admin/asset/list",
     *     security={{"api_key": {}}},
     *     summary="Liste des objets avec une recherche (filter)",
     *     @OA\Parameter(name="query", in="query", description="Terme de la recherche", example="Maison à toit de chaume"),
     *     @OA\Response(
     *      response="200",
     *      description="Retourne la liste des objets rechercher",
     *      @OA\JsonContent(
     *        type="array",
     *              @OA\Items(ref="#/components/schemas/AdminAssets")
     *      )
     *     )
     * )
     */
    public function list(Request $request)
    {
        $format = new AssetsList();
        if($request->get('query') !== null) {
            $assets = $this->asset->newQuery()->where('designation', 'LIKE', '%'.$request->get('query').'%')
                ->orWhere('short_description', 'LIKE', '%'.$request->get('query').'%')
                ->orWhere('description', 'LIKE', '%'.$request->get('query').'%')
                ->get();
        } else {
            $assets = $this->asset->newQuery()->get();
        }

        return $this->sendResponse($format->toArray($assets), null, 200);
    }

    /**
     * @param StoreAssetRequest $request
     * @return JsonResponse
     *
     * @OA\Post(
     *     tags={"Admin Asset"},
     *     path="/api/admin/asset",
     *     security={{"api_key": {}}},
     *     summary="Création d'un objet",
     *     @OA\Response(
     *      response="200",
     *      description="Retourne l'objet créer",
     *      @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref="#/components/schemas/AdminAssetGet")
     *      )
     *     ),
     *     @OA\RequestBody(
     *          description="Création de l'objet",
     *          required=true,
     *          @OA\MediaType(
     *              mediaType="application/x-www-form-urlencoded",
     *              @OA\Schema(ref="#/components/schemas/PostAssetRequest")
     *          )
     *     )
     * )
     */
    public function store(StoreAssetRequest $request)
    {
        $format = new AssetGet();

        try {
            $asset = $this->asset->newQuery()->create([
                "designation" => $request->get('designation'),
                "short_description" => $request->get('short_description'),
                "description" => $request->get('description'),
                "uuid" => Str::uuid(),
                "asset_category_id" => $request->get('asset_category_id'),
                "sketchfab_uid" => $request->get('sketchfab_uid')
            ]);
        }catch (Exception $exception) {
            Log::critical($exception->getMessage(), ["secteur" => "Admin Asset"]);
            return $this->sendError($exception->getMessage());
        }

        return $this->sendResponse($format->toArray($asset), null, 200);
    }

    /**
     * @param $asset_id
     * @return JsonResponse
     *
     * @OA\Get(
     *     path="/api/admin/asset/{asset_id}",
     *     tags={"Admin Asset"},
     *     security={{"api_key": {}}},
     *     summary="Information d'un objet",
     *      @OA\Parameter(name="asset_id", in="path", description="ID de l'objet", required=true),
     *      @OA\Response(
     *          response="200",
     *          description="Retourne l'objet avec les informations correspondantes'",
     *          @OA\JsonContent(
     *              type="array",
     *              @OA\Items(ref="#/components/schemas/AdminAssetGet")
     *          )
     *      )
     * )
     */
    public function get($asset_id)
    {
        $format = new AssetGet();
        $asset = $this->asset->newQuery()->findOrFail($asset_id);

        return $this->sendResponse($format->toArray($asset), null, 200);
    }

    /**
     * @param Request $request
     * @param $asset_id
     * @return JsonResponse
     *
     * @OA\Put(
     *     tags={"Admin Asset"},
     *     path="/api/admin/asset/{asset_id}",
     *     security={{"api_key": {}}},
     *     summary="Edition d'un objet",
     *     @OA\Parameter(name="asset_id", in="path", description="ID de l'objet", required=true),
     *     @OA\Response(
     *      response="200",
     *      description="Retourne l'objet mise à jour",
     *      @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref="#/components/schemas/AdminAssetGet")
     *      )
     *     ),
     *     @OA\RequestBody(
     *          description="Mise à jour de la catégorie",
     *          required=true,
     *          @OA\MediaType(
     *              mediaType="application/x-www-form-urlencoded",
     *              @OA\Schema(
     *                  type="object",
     *                  @OA\Property(property="designation", type="string", default="Maison en toit de chaume"),
     *                  @OA\Property(property="short_description", type="string", default="lorem à 150 caractère"),
     *                  @OA\Property(property="description", type="string", default="lorem à 3200 caractère"),
     *                  @OA\Property(property="social", type="boolean", default="false"),
     *                  @OA\Property(property="published", type="boolean", default="false"),
     *                  @OA\Property(property="meshes", type="boolean", default="false"),
     *                  @OA\Property(property="pricing", type="boolean", default="false"),
     *                  @OA\Property(property="price", type="string", default="0,00"),
     *                  @OA\Property(property="published_at", type="string", format="date-time"),
     *                  @OA\Property(property="asset_category_id", type="integer"),
     *              )
     *          )
     *     )
     * )
     */
    public function update(Request $request, $asset_id)
    {
        $format = new AssetGet();
        try {
            $asset = $this->asset->newQuery()->findOrFail($asset_id);
            $asset->update($request->all());
        }catch (Exception $exception) {
            Log::critical($exception->getMessage(), ["secteur" => "Admin Asset"]);
            return $this->sendError($exception->getMessage());
        }

        return $this->sendResponse($format->toArray($asset), null, 200);
    }

    /**
     * @param $asset_id
     * @return JsonResponse
     *
     * @OA\Delete(
     *     path="/api/admin/asset/{asset_id}",
     *     tags={"Admin Asset"},
     *     security={{"api_key": {}}},
     *     summary="Suppression d'un objet",
     *      @OA\Parameter(name="asset_id", in="path", description="ID de la catégorie", required=true),
     *      @OA\Response(
     *          response="200",
     *          description="Retourne un message indiquant la bonne suppression de l'objet",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="L'objet {asset_id} à été supprimer")
     *          )
     *      ),
     *      @OA\Response(
     *          response="510",
     *          description="Suppression impossible / Erreur de suppression",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Message d'exception")
     *          )
     *      )
     * )
     */
    public function destroy($asset_id)
    {
        $asset = $this->asset->newQuery()->findOrFail($asset_id);
        try {
            $asset->delete();
            return $this->sendResponse(["message" => "L'objet $asset->id à été supprimer"], null, 200);
        }catch (Exception $exception) {
            Log::critical($exception->getMessage(), ['secteur' => "Admin Asset"]);
            return  $this->sendError("Erreru Système", ["error" => $exception], 510);
        }
    }

    /**
     * @param $asset_id
     * @return JsonResponse
     *
     * @OA\Get(
     *     path="/api/admin/asset/{asset_id}/publish",
     *     tags={"Admin Asset"},
     *     security={{"api_key": {}}},
     *     summary="Publication d'un objet",
     *      @OA\Parameter(name="asset_id", in="path", description="ID de l'objet", required=true),
     *      @OA\Response(
     *          response="200",
     *          description="Retourne un message indiquant la bonne publication de l'objet",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="L'objet {asset_id} à été publier")
     *          )
     *      ),
     *     @OA\Response(
     *          response="510",
     *          description="Erreur Serveur",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Message d'exception")
     *          )
     *      )
     * )
     */
    public function publish($asset_id)
    {
        $asset = $this->asset->newQuery()->findOrFail($asset_id);
        try {
            $asset->update([
                'published' => 1,
                'published_at' => now()
            ]);
            $this->notificator->publish($asset->designation." est disponible en téléchargement", "success", null, "fas fa-cubes", $asset->image);
            return $this->sendResponse(['message' => "L'objet $asset->id à été publier"], null, 200);
        }catch (Exception $exception) {
            Log::critical($exception->getMessage(), ['secteur' => "Admin Asset"]);
            return  $this->sendError("Erreur Système", ["error" => $exception], 510);
        }
    }

    /**
     * @param $asset_id
     * @return JsonResponse
     *
     * @OA\Get(
     *     path="/api/admin/asset/{asset_id}/dispublish",
     *     tags={"Admin Asset"},
     *     security={{"api_key": {}}},
     *     summary="Dépublication d'un objet",
     *      @OA\Parameter(name="asset_id", in="path", description="ID de l'objet", required=true),
     *      @OA\Response(
     *          response="200",
     *          description="Retourne un message indiquant la bonne dépublication de l'objet",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="L'objet {asset_id} à été dépublier")
     *          )
     *      ),
     *     @OA\Response(
     *          response="510",
     *          description="Erreur Serveur",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Message d'exception")
     *          )
     *      )
     * )
     */
    public function dispublish($asset_id)
    {
        $asset = $this->asset->newQuery()->findOrFail($asset_id);
        try {
            $asset->update([
                'published' => 0,
                'published_at' => null
            ]);
            return $this->sendResponse(['message' => "L'objet $asset->id à été dépublier"], null, 200);
        }catch (Exception $exception) {
            Log::critical($exception->getMessage(), ["secteur" => "Admin Asset"]);
            return  $this->sendError("Erreur Système", ["error" => $exception], 510);
        }
    }

    /**
     * @param Request $request
     * @param $asset_id
     * @return JsonResponse
     *
     * @OA\Post(
     *     tags={"Admin Asset"},
     *     path="/api/admin/asset/{asset_id}/addSketchfab",
     *     security={{"api_key": {}}},
     *     summary="Mise à jour de l'UID Sketchfab",
     *     @OA\Response(
     *      response="200",
     *      description="null",
     *      @OA\JsonContent(
     *        type="object"
     *      )
     *     ),
     *     @OA\RequestBody(
     *          description="Request",
     *          required=true,
     *          @OA\MediaType(
     *              mediaType="application/json",
     *          ),
     *          @OA\MediaType(
     *              mediaType="application/x-www-form-urlencoded",
     *          )
     *     )
     * )
     */
    public function addSketchfab(Request $request, $asset_id)
    {
        $asset = $this->asset->newQuery()->findOrFail($asset_id);

        try {
            $asset->update([
                "sketchfab_uid" => $request->get('sketchfab_uid')
            ]);
        }catch (Exception $exception) {
            Log::error($exception->getMessage());
            return $this->sendError($exception->getMessage());
        }

        return $this->sendResponse(null, null, 200);
    }

    /**
     * @param Request $request
     * @param $asset_id
     * @return JsonResponse
     *
     * @OA\Post(
     *     tags={"Admin Asset"},
     *     path="/api/admin/asset/{asset_id}/addPrice",
     *     security={{"api_key": {}}},
     *     summary="Mise à jour du prix de l'asset",
     *     @OA\Response(
     *      response="200",
     *      description="null",
     *      @OA\JsonContent(
     *        type="object",
     *      )
     *     ),
     *     @OA\RequestBody(
     *          description="Requete",
     *          required=true,
     *          @OA\MediaType(
     *              mediaType="application/json",
     *          ),
     *          @OA\MediaType(
     *              mediaType="application/x-www-form-urlencoded",
     *          )
     *     )
     * )
     */
    public function addPrice(Request $request, $asset_id)
    {
        $asset = $this->asset->newQuery()->findOrFail($asset_id);

        try {
            $asset->update([
                "price" => $request->get('price')
            ]);
        }catch (Exception $exception) {
            Log::error($exception->getMessage());
            return $this->sendError($exception->getMessage());
        }

        return $this->sendResponse(null, null, 200);
    }
}
