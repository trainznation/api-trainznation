<?php

namespace App\Http\Controllers\Api\Admin\Asset;

use App\Http\Controllers\Api\BaseController;
use App\Http\Controllers\Controller;
use App\Http\Requests\Asset\Category\PostAssetCategoryRequest;
use App\Http\Requests\Asset\Category\UpdateAssetCategoryRequest;
use App\Http\Resources\Admin\Asset\AssetCategoryList;
use App\Model\AssetCategory;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use OpenApi\Annotations as OA;

class AssetCategoryController extends BaseController
{
    /**
     * @var AssetCategory
     */
    private $assetCategory;

    /**
     * AssetCategoryController constructor.
     * @param AssetCategory $assetCategory
     */
    public function __construct(AssetCategory $assetCategory)
    {
        $this->assetCategory = $assetCategory;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     *
     * @OA\Get(
     *     path="/api/admin/asset/category/list",
     *     tags={"Admin Asset"},
     *     security={{"api_key": {}}},
     *     summary="Liste des catégories",
     *     @OA\Parameter(in="query", name="q", description="Terme de la recherche", required=false),
     *      @OA\Response(
     *          response="200",
     *          description="Retourne la liste des Catégories d'objets avec la liste des assets correspondant",
     *          @OA\JsonContent(
     *              type="array",
     *              @OA\Items(ref="#/components/schemas/AdminAssetCategories")
     *          )
     *      )
     * )
     */
    public function list(Request $request)
    {
        $format = new AssetCategoryList();
        if($request->has('q') == true) {
            $categories = $this->assetCategory->newQuery()->where('name', 'LIKE', '%'.$request->get('q').'%')->get()->load('assets');
        } else {
            $categories = $this->assetCategory->newQuery()->get()->load('assets');
        }

        return $this->sendResponse($format->toArray($categories), null, 200);
    }

    /**
     * @param PostAssetCategoryRequest $request
     * @return JsonResponse
     *
     * @OA\Post(
     *     tags={"Admin Asset"},
     *     path="/api/admin/asset/category",
     *     security={{"api_key": {}}},
     *     summary="Création d'une catégorie",
     *     @OA\Response(
     *      response="200",
     *      description="Retourne la catégorie créer",
     *      @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref="#/components/schemas/AdminAssetCategory")
     *      )
     *     ),
     *     @OA\RequestBody(
     *          description="Création de la catégorie",
     *          required=true,
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(ref="#/components/schemas/PostAssetCategoryRequest")
     *          )
     *     )
     * )
     */
    public function store(PostAssetCategoryRequest $request)
    {
        $format = new \App\Http\Resources\Admin\Asset\AssetCategory();
        try {
            $category = $this->assetCategory->newQuery()->create($request->all())->load('assets');
        }catch (Exception $exception) {
            Log::critical($exception->getMessage(), ["secteur" => "Admin Asset Category"]);
            return $this->sendError($exception->getMessage());
        }

        return $this->sendResponse($format->toArray($category), null, 200);

    }

    /**
     * @param $category_id
     * @return JsonResponse
     *
     * @OA\Get(
     *     path="/api/admin/asset/category/{category_id}",
     *     tags={"Admin Asset"},
     *     security={{"api_key": {}}},
     *     summary="Liste des catégories",
     *      @OA\Parameter(name="category_id", in="path", description="ID de la catégorie", required=true),
     *      @OA\Response(
     *          response="200",
     *          description="Retourne la catégorie avec la liste des objets correspondantes",
     *          @OA\JsonContent(
     *              type="array",
     *              @OA\Items(ref="#/components/schemas/AdminAssetCategory")
     *          )
     *      )
     * )
     */
    public function get($category_id)
    {
        $format = new \App\Http\Resources\Admin\Asset\AssetCategory();
        $category = $this->assetCategory->newQuery()->findOrFail($category_id)->load('assets');

        return $this->sendResponse($format->toArray($category), null, 200);
    }

    /**
     * @param UpdateAssetCategoryRequest $request
     * @param $category_id
     * @return JsonResponse
     *
     * @OA\Put(
     *     tags={"Admin Asset"},
     *     path="/api/admin/asset/category/{category_id}",
     *     security={{"api_key": {}}},
     *     summary="Edition d'une catégorie",
     *     @OA\Parameter(name="category_id", in="path", description="ID de la catégorie", required=true),
     *     @OA\Response(
     *      response="200",
     *      description="Retourne la catégorie mise à jour",
     *      @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref="#/components/schemas/AdminAssetCategory")
     *      )
     *     ),
     *     @OA\RequestBody(
     *          description="Mise à jour de la catégorie",
     *          required=true,
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(ref="#/components/schemas/UpdateAssetCategoryRequest")
     *          )
     *     )
     * )
     */
    public function update(UpdateAssetCategoryRequest $request, $category_id)
    {
        $format = new \App\Http\Resources\Admin\Asset\AssetCategory();
        try {
            $category = $this->assetCategory->newQuery()->findOrFail($category_id);
            $category->update($request->all());
        }catch (Exception $exception) {
            Log::critical($exception->getMessage(), ["secteur" => "Admin Asset Category"]);
            return $this->sendError($exception->getMessage());
        }

        return $this->sendResponse($format->toArray($category), null, 200);
    }

    /**
     * @param $category_id
     * @return JsonResponse
     *
     * @OA\Delete(
     *     path="/api/admin/asset/category/{category_id}",
     *     tags={"Admin Asset"},
     *     security={{"api_key": {}}},
     *     summary="Suppression d'une catégorie",
     *      @OA\Parameter(name="category_id", in="path", description="ID de la catégorie", required=true),
     *      @OA\Response(
     *          response="200",
     *          description="Retourne un message indiquant la bonne suppression de la catégorie",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="La catégorie à été supprimer")
     *          )
     *      ),
     *      @OA\Response(
     *          response="510",
     *          description="Suppression impossible / Erreur de suppression",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Message d'exception")
     *          )
     *      )
     * )
     */
    public function destroy($category_id)
    {
        $category = $this->assetCategory->newQuery()->findOrFail($category_id);
        try {
            $category->delete();
            return $this->sendResponse(["message" => "La catégorie $category->id à été supprimer"], null, 200);
        }catch (Exception $exception) {
            Log::critical($exception->getMessage(), ["secteur" => "Admin Asset Category"]);
            return $this->sendError("Erreur Système", ["error" => $exception->getMessage()], 510);
        }
    }
}
