<?php

namespace App\Http\Controllers\Api\Admin\User;

use App\Http\Controllers\Api\BaseController;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\StoreNotificationsRequest;
use App\Model\UserNotification;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use OpenApi\Annotations as OA;

class UserNotificationsController extends BaseController
{
    /**
     * @var UserNotification
     */
    private $notification;

    /**
     * UserNotificationsController constructor.
     * @param UserNotification $notification
     */
    public function __construct(UserNotification $notification)
    {
        $this->notification = $notification;
    }

    /**
     * @return JsonResponse
     *
     * @OA\Get(
     *     path="/api/admin/user/{user_id}/notifications/list",
     *     tags={"Admin User Notification"},
     *     security={{"api_key": {}}},
     *     summary="Liste des notification d'un utilisateur",
     *      @OA\Parameter(name="user_id", in="path", description="ID de l'utilisateur", required=true),
     *      @OA\Response(
     *          response="200",
     *          description="Tableau de notification",
     *          @OA\JsonContent(
     *              type="object",
     *          )
     *      )
     * )
     */
    public function list($user_id)
    {
        try {
            $lists = $this->notification->newQuery()->where('user_id', auth()->user()->id)->get();
        } catch (Exception $exception) {
            return $this->sendError($exception->getMessage(), null, 510);
        }

        return $this->sendResponse($lists, null, 200);
    }

    /**
     * @return JsonResponse
     *
     * @OA\Get(
     *     path="/api/admin/user/{user_id}/notifications/unread",
     *     tags={"Admin User Notification"},
     *     security={{"api_key": {}}},
     *     summary="Liste des notifications non lu",
     *      @OA\Parameter(name="user_id", in="path", description="ID de l'utilisateur", required=true),
     *      @OA\Response(
     *          response="200",
     *          description="Liste des notifications non lu",
     *          @OA\JsonContent(
     *              type="object",
     *          )
     *      )
     * )
     */
    public function unread($user_id)
    {
        try {
            $lists = $this->notification->newQuery()->where('user_id', auth()->user()->id)->where('read_at', null)->get();
        } catch (Exception $exception) {
            return $this->sendError($exception->getMessage(), null, 510);
        }

        return $this->sendResponse($lists, null, 200);
    }

    /**
     * @param StoreNotificationsRequest $request
     * @return JsonResponse
     *
     * @OA\Post(
     *     tags={"Admin User Notification"},
     *     path="/api/admin/user/{user_id}/notifications",
     *     security={{"api_key": {}}},
     *     summary="Création d'une notification",
     *     @OA\Response(
     *      response="200",
     *      description="Retourne la notification créer",
     *      @OA\JsonContent(
     *        type="object",
     *      )
     *     ),
     *     @OA\RequestBody(
     *          description="Création de la notification",
     *          required=true,
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(ref="#/components/schemas/StoreNotificationRequest")
     *          ),
     *          @OA\MediaType(
     *              mediaType="application/x-www-form-urlencoded",
     *              @OA\Schema(ref="#/components/schemas/StoreNotificationRequest")
     *          )
     *     )
     * )
     */
    public function store($user_id, StoreNotificationsRequest $request)
    {
        try {
            $notification = $this->notification->newQuery()->create([
                "user_id" => auth()->user()->id,
                "title" => $request->get('title'),
                "description" => ($request->has('description')) ? $request->get('description') : null,
                "icon" => ($request->has('icon')) ? $request->get('icon') : null,
                "image" => ($request->has('image')) ? $request->get('image') : null,
                "status" => $request->get('status')
            ]);
        } catch (Exception $exception) {
            return $this->sendError($exception->getMessage(), null, 510);
        }

        return $this->sendResponse($notification, null, 200);
    }

    /**
     * @param $user_id
     * @param $notification_id
     * @return JsonResponse
     *
     * @OA\Get(
     *     tags={"Admin User Notification"},
     *     path="/api/admin/user/{user_id}/notifications/{notification_id}/read",
     *     security={{"api_key": {}}},
     *     summary="Lit une notification",
     *     @OA\Parameter(name="user_id", in="path", description="ID de l'utilisateur", required=true),
     *     @OA\Response(
     *      response="200",
     *      description="Null",
     *      @OA\JsonContent(
     *        type="null",
     *      )
     *     ),
     * )
     */
    public function read($user_id, $notification_id)
    {
        try {
            $notification = $this->notification->newQuery()->find($notification_id);
            $notification->update([
                "read_at" => now()
            ]);
        }catch (Exception $exception) {
            return $this->sendError($exception->getMessage(), null, 510);
        }

        return $this->sendResponse(null, null, 200);
    }

    /**
     * @param $notification_id
     * @return JsonResponse
     */
    public function delete($notification_id)
    {
        try {
            $notification = $this->notification->newQuery()->findOrFail($notification_id);
            $notification->delete();
        }catch (Exception $exception) {
            return $this->sendError($exception->getMessage(), null, 510);
        }

        return $this->sendResponse(null, null, 200);
    }

    public function destroy()
    {
        try {
            $notifications = $this->notification->newQuery()->get();
            foreach ($notifications as $notification) {
                $notification->delete();
            }
        }catch (Exception $exception) {
            return $this->sendError($exception->getMessage(), null, 510);
        }

        return $this->sendResponse(null, null, 200);
    }
}
