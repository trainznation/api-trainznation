<?php

namespace App\Http\Controllers\Api\Admin\User;

use App\Http\Controllers\Api\BaseController;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\RegisterRequest;
use App\Http\Requests\User\StoreUserRequest;
use App\Http\Resources\Admin\User\UserGet;
use App\Http\Resources\Admin\User\UsersList;
use App\Http\Resources\Auth\Me;
use App\User;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

/**
 * Class UserController
 * @package App\Http\Controllers\Api\Admin\User
 */

class UserController extends BaseController
{
    /**
     * @var User
     */
    private $user;

    /**
     * UserController constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }


    /**
     * @return JsonResponse
     *
     * @OA\Get(
     *     path="/api/admin/user/list",
     *     tags={"Admin User"},
     *     security={{"api_key": {}}},
     *     summary="Liste des utilisateurs",
     *      @OA\Response(
     *          response="200",
     *          description="Retourne la liste des utilisateurs avec les informations correspondantes",
     *          @OA\JsonContent(
     *              type="array",
     *              @OA\Items(ref="#/components/schemas/AdminUsersList")
     *          )
     *      )
     * )
     */
    public function list()
    {
        $format = new UsersList();
        $users = $this->user->newQuery()->get();

        return $this->sendResponse($format->toArray($users), null, 200);
    }

    /**
     * @param $user_id
     * @return JsonResponse
     *
     * @OA\Get(
     *     path="/api/admin/user/{user_id}",
     *     tags={"Admin User"},
     *     security={{"api_key": {}}},
     *     summary="Information sur un utilisateur",
     *      @OA\Parameter(name="user_id", in="path", description="ID de l'utilisateur", required=true),
     *      @OA\Response(
     *          response="200",
     *          description="Tableau de l'utilisateur",
     *          @OA\JsonContent(
     *              type="array",
     *              @OA\Items(ref="#/components/schemas/AdminUserGet")
     *          )
     *      )
     * )
     */
    public function get($user_id)
    {
        $format = new UserGet();
        try {
            $user = $this->user->newQuery()->findOrFail($user_id);
        }catch (Exception $exception) {
            return $this->sendError($exception->getMessage(), null, 500);
        }

        return $this->sendResponse($format->toArray($user->id), null, 200);
    }


    /**
     * @param RegisterRequest $request
     * @return JsonResponse
     *
     * @OA\Post(
     *     tags={"Admin User"},
     *     path="/api/admin/user/store",
     *     security={{"api_key": {}}},
     *     summary="Création d'un utilisateur",
     *     @OA\Response(
     *      response="200",
     *      description="Retourne l'utilisateur créer",
     *      @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref="#/components/schemas/AdminUserGet")
     *      )
     *     ),
     *     @OA\RequestBody(
     *          description="Création de l'utilisateur",
     *          required=true,
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(ref="#/components/schemas/RegisterRequest")
     *          )
     *     )
     * )
     */
    public function store(RegisterRequest $request)
    {
        $format = new UserGet();
        $user = $this->user->create([
            "name" => $request->get('name'),
            "email" => $request->get('email'),
            "password" => Hash::make($request->get('password')),
            "admin" => $request->get('admin', 0),
        ]);

        return $this->sendResponse($format->toArray($user->id), null, 200);
    }
}
