<?php

namespace App\Http\Controllers\Api\Admin\Wiki;

use App\Http\Controllers\Api\BaseController;
use App\Http\Controllers\Controller;
use App\Http\Requests\Wiki\WikiCategoryRequest;
use App\Http\Resources\Admin\Wiki\WikiCategoryGet;
use App\Http\Resources\Admin\Wiki\WikiCategoryList;
use App\Model\WikiCategory;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class WikiCategoryController extends BaseController
{
    /**
     * @var WikiCategory
     */
    private $wikiCategory;

    /**
     * WikiCategoryController constructor.
     * @param WikiCategory $wikiCategory
     */
    public function __construct(WikiCategory $wikiCategory)
    {
        $this->wikiCategory = $wikiCategory;
    }

    public function list()
    {
        $format = new WikiCategoryList();
        try {
            $lists = $this->wikiCategory->newQuery()->get();
        }catch (Exception $exception) {
            Log::error($exception);
            return $this->sendError($exception->getMessage());
        }

        return $this->sendResponse($format->toArray($lists), null,200);
    }

    public function search(Request $request)
    {
        $format = new WikiCategoryList();
        try {
            $lists = $this->wikiCategory->newQuery()->where('name', 'LIKE', '%'.$request->get('q').'%')
                ->orderBy('id', 'asc')
                ->get();
        }catch (Exception $exception) {
            Log::error($exception);
            return $this->sendError($exception->getMessage());
        }

        return $this->sendResponse($format->toArray($lists), null, 200);
    }

    public function store(WikiCategoryRequest $request)
    {
        $format = new WikiCategoryGet();
        try {
            $category = $this->wikiCategory->newQuery()->create([
                "name" => $request->get('name')
            ]);
        }catch (Exception $exception) {
            Log::error($exception);
            return $this->sendError($exception->getMessage());
        }

        return $this->sendResponse($format->toArray($category), null, 200);
    }

    public function delete($category_id)
    {
        try {
            $this->wikiCategory->newQuery()->find($category_id)->delete();
        }catch (Exception $exception) {
            Log::error($exception);
            return $this->sendError($exception->getMessage());
        }

        return $this->sendResponse(null, null, 200);
    }
}
