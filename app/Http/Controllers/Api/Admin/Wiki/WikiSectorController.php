<?php

namespace App\Http\Controllers\Api\Admin\Wiki;

use App\Http\Controllers\Api\BaseController;
use App\Http\Controllers\Controller;
use App\Http\Requests\Wiki\WikiSectorRequest;
use App\Http\Resources\Admin\Wiki\WikiSectorGet;
use App\Http\Resources\Admin\Wiki\WikiSectorList;
use App\Model\WikiSector;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class WikiSectorController extends BaseController
{
    /**
     * @var WikiSector
     */
    private $sector;

    /**
     * WikiSectorController constructor.
     * @param WikiSector $sector
     */
    public function __construct(WikiSector $sector)
    {
        $this->sector = $sector;
    }

    public function list()
    {
        $format = new WikiSectorList();

        try {
            $lists = $this->sector->newQuery()->get();
        } catch (Exception $exception) {
            Log::error($exception);
            return $this->sendError($exception->getMessage());
        }

        return $this->sendResponse($format->toArray($lists), null, 200);
    }

    public function search(Request $request)
    {
        $format = new WikiSectorList();
        try {
            if ($request->has('q')) {
                $lists = $this->sector->newQuery()->where('name', 'LIKE', '%'.$request->get('q').'%')
                    ->orderBy('id', 'asc')
                    ->get();
            } elseif ($request->has('category')) {
                $lists = $this->sector->newQuery()->where('wiki_category_id', $request->get('category'))
                    ->orderBy('id', 'asc')
                    ->get();
            }else{
                $lists = $this->sector->newQuery()
                    ->orderBy('id', 'asc')
                    ->get();
            }
        } catch (Exception $exception) {
            Log::error($exception);
            return $this->sendError($exception->getMessage());
        }

        return $this->sendResponse($format->toArray($lists), null, 200);
    }

    public function store(WikiSectorRequest $request)
    {
        $format = new WikiSectorGet();

        try {
            $sector = $this->sector->newQuery()->create([
                "name" => $request->get('name'),
                "wiki_category_id" => $request->get('wiki_category_id')
            ]);
        } catch (Exception $exception) {
            Log::error($exception);
            return $this->sendError($exception->getMessage());
        }

        return $this->sendResponse($format->toArray($sector), null, 200);
    }

    public function delete($sector_id)
    {
        try {
            $this->sector->newQuery()->find($sector_id)->delete();
        } catch (Exception $exception) {
            Log::error($exception);
            return $this->sendError($exception->getMessage());
        }

        return $this->sendResponse(null, null, 200);
    }
}
