<?php

namespace App\Http\Controllers\Api\Admin\Wiki;

use App\Helpers\Notificator;
use App\Http\Controllers\Api\BaseController;
use App\Http\Controllers\Controller;
use App\Http\Requests\Wiki\WikiArticleRequest;
use App\Http\Resources\Admin\Wiki\WikiArticleGet;
use App\Http\Resources\Admin\Wiki\WikiArticleList;
use App\Model\WikiArticle;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class WikiController extends BaseController
{
    /**
     * @var WikiArticle
     */
    private $article;
    /**
     * @var Notificator
     */
    private $notificator;

    /**
     * WikiController constructor.
     * @param WikiArticle $article
     * @param Notificator $notificator
     */
    public function __construct(WikiArticle $article, Notificator $notificator)
    {
        $this->article = $article;
        $this->notificator = $notificator;
    }

    public function list()
    {
        $format = new WikiArticleList();

        try {
            $lists = $this->article->newQuery()->get();
        } catch (Exception $exception) {
            Log::error($exception);
            return $this->sendError($exception->getMessage());
        }

        return $this->sendResponse($format->toArray($lists), null, 200);
    }

    public function search(Request $request)
    {
        $format = new WikiArticleList();
        if ($request->has('q') == true) {
            $lists = $this->article->newQuery()->where('title', 'LIKE', '%' . $request->get('q') . '%')
                ->orderBy('id', 'asc')
                ->get();
        } elseif ($request->has('category')) {
            $lists = $this->article->newQuery()->where('wiki_category_id', $request->get('category'))
                ->orderBy('id', 'asc')
                ->get();
        } elseif ($request->has('sector')) {
            $lists = $this->article->newQuery()->where('wiki_sector_id', $request->get('sector'))
                ->orderBy('id', 'asc')
                ->get();
        } elseif ($request->has('published')) {
            $lists = $this->article->newQuery()->where('published', $request->get('published'))
                ->orderBy('id', 'asc')
                ->get();
        } else {
            $lists = $this->article->newQuery()->get();
        }

        return $this->sendResponse($format->toArray($lists), null, 200);

    }

    public function store(WikiArticleRequest $request)
    {
        $format = new WikiArticleGet();
        try {
            $article = $this->article->newQuery()->create([
                "title" => $request->get('title'),
                "content" => $request->get('content'),
                "wiki_category_id" => $request->get('wiki_category_id'),
                "wiki_sector_id" => $request->get('wiki_sector_id')
            ]);
        } catch (Exception $exception) {
            Log::error($exception);
            return $this->sendError($exception->getMessage());
        }

        return $this->sendResponse($format->toArray($article), null, 200);
    }

    public function get($article_id)
    {
        $format = new WikiArticleGet();

        try {
            $article = $this->article->newQuery()->find($article_id);
        } catch (Exception $exception) {
            Log::error($exception);
            return $this->sendError($exception->getMessage());
        }

        return $this->sendResponse($format->toArray($article), null, 200);
    }

    public function update(Request $request, $article_id)
    {
        $format = new WikiArticleGet();

        try {
            $article = $this->article->newQuery()->find($article_id);
            $article->update([
                "title" => $request->get('title'),
                "content" => $request->get('content'),
                "updated_at" => now()
            ]);
        } catch (Exception $exception) {
            Log::error($exception);
            return $this->sendError($exception->getMessage());
        }

        return $this->sendResponse($format->toArray($article), null, 200);
    }

    public function delete($article_id)
    {
        try {
            $this->article->newQuery()->find($article_id)->delete();
        } catch (Exception $exception) {
            Log::error($exception);
            return $this->sendError($exception->getMessage());
        }

        return $this->sendResponse(null, null, 200);
    }

    public function publish($article_id)
    {
        try {
            $article = $this->article->newQuery()->find($article_id);
            $article->update([
                "published" => 1,
                "published_at" => now()
            ]);
        } catch (Exception $exception) {
            Log::error($exception);
            return $this->sendError($exception->getMessage());
        }

        if($article->notification == 0) {
            $this->notificator->publishAsAdmin("Publication d'un article du wiki", 'success', null, 'fas fa-check-circle');
            $this->notificator->publish("Publication d'un article", 'success', "L'article ".$article->title." à été publier", "fas fa-check-circle");
            $article->update([
                "notification" => 1
            ]);
        }
        return $this->sendResponse(null, null, 200);
    }

    public function unpublish($article_id)
    {
        try {
            $this->article->newQuery()->find($article_id)->update([
                "published" => 0,
                "published_at" => null
            ]);
        } catch (Exception $exception) {
            Log::error($exception);
            return $this->sendError($exception->getMessage());
        }

        return $this->sendResponse(null, null, 200);
    }
}
