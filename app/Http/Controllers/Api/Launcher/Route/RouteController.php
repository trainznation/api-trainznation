<?php

namespace App\Http\Controllers\Api\Launcher\Route;

use App\Http\Controllers\Api\BaseController;
use App\Http\Resources\Launcher\Route\RouteGetFormated;
use App\Http\Resources\Launcher\Route\RouteListFormated;
use App\Http\Resources\Launcher\Route\RouteSessionGetFormated;
use App\Model\Route;
use App\Model\RouteSession;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;

class RouteController extends BaseController
{
    /**
     * @var Route
     */
    private $route;
    /**
     * @var RouteSession
     */
    private $routeSession;

    /**
     * RouteController constructor.
     * @param Route $route
     * @param RouteSession $routeSession
     */
    public function __construct(Route $route, RouteSession $routeSession)
    {
        $this->route = $route;
        Carbon::setLocale('fr');
        $this->routeSession = $routeSession;
    }

    /**
     * @group Launcher Route
     *
     * Affiche la liste des routes
     *
     * @return JsonResponse
     */
    public function list()
    {
        $formated = new RouteListFormated();
        $routes = $this->route->newQuery()->isPublished()->get();

        return $this->sendResponse($formated->toArray($routes), null, 200);
    }

    /**
     * @group Launcher Route
     *
     * Affiche toutes les sessions de toutes les routes publier
     *
     * @return JsonResponse
     */
    public function allSession()
    {
        $formated = new RouteSessionGetFormated();
        $sessions = $this->routeSession->newQuery()
            ->where('published', 1)
            ->get();

        return $this->sendResponse($formated->toArray($sessions), null, 200);
    }

    /**
     * @group Launcher Route
     *
     * Show Route
     * Affiche les informations d'une route particulière
     *
     * @urlParam id required int ID de la route
     *
     * @param $route_id
     * @return JsonResponse
     */
    public function get($route_id)
    {
        $formated = new RouteGetFormated();
        $route = $this->route->newQuery()->findOrFail($route_id)->load('roadmap', 'sessions');

        return $this->sendResponse($formated->toArray($route), null, 200);
    }
}
