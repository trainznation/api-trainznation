<?php

namespace App\Http\Controllers\Api\Launcher\Asset;

use App\Http\Controllers\Api\BaseController;
use App\Http\Controllers\Controller;
use App\Http\Resources\Launcher\Asset\GetAssetCategoryFormated;
use App\Model\Asset;
use App\Model\AssetCategory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class AssetCategoryController extends BaseController
{
    /**
     * @var AssetCategory
     */
    private $assetCategory;
    /**
     * @var Asset
     */
    private $asset;

    /**
     * AssetCategoryController constructor.
     * @param AssetCategory $assetCategory
     * @param Asset $asset
     */
    public function __construct(AssetCategory $assetCategory, Asset $asset)
    {
        $this->assetCategory = $assetCategory;
        $this->asset = $asset;
    }

    /**
     * @group Launcher Asset Category
     *
     * Affiche la liste des catégories
     *
     * @return JsonResponse
     */
    public function list()
    {
        $categories = $this->assetCategory->newQuery()->get();

        return $this->sendResponse($categories->toArray(), null, 200);
    }

    /**
     * @group Launcher Asset Category
     *
     * Affiche les informations d'une catégorie avec la liste des objets
     *
     * @urlParam asset_category_id int required ID de la catégorie
     *
     * @param $category_id
     * @return JsonResponse
     */
    public function get($category_id) {
        $formated = new GetAssetCategoryFormated();

        $category = $this->assetCategory->newQuery()->findOrFail($category_id);
        $assets = $this->asset->newQuery()->where('asset_category_id', $category_id)
            ->isPublished()
            ->get();

        $data = [
            "category" => $category,
            "assets" => $assets
        ];

        return $this->sendResponse($formated->toArray((object)$data), null, 200);
    }
}
