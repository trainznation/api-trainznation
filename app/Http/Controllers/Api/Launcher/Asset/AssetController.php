<?php

namespace App\Http\Controllers\Api\Launcher\Asset;

use App\Http\Controllers\Api\BaseController;
use App\Http\Controllers\Controller;
use App\Http\Resources\Launcher\Asset\GetAssetFormated;
use App\Model\Asset;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class AssetController extends BaseController
{
    /**
     * @var Asset
     */
    private $asset;

    /**
     * AssetController constructor.
     * @param Asset $asset
     */
    public function __construct(Asset $asset)
    {
        $this->asset = $asset;
        Carbon::setLocale('fr');
    }

    /**
     * @group Launcher Asset
     *
     * Liste des objets
     *
     * @return JsonResponse
     */
    public function list()
    {
        $assets = $this->asset->newQuery()->isPublished()->get()->load('category', 'kuids');

        return $this->sendResponse($assets->toArray(), null, 200);
    }

    /**
     * @group Launcher Asset
     *
     * Affiche les informations d'un objet
     *
     * @urlParam asset_id required int ID de l'objet à récupérer
     *
     * @param $asset_id
     * @return JsonResponse
     */
    public function get($asset_id)
    {
        $format = new GetAssetFormated();
        $asset = $this->asset->newQuery()->findOrFail($asset_id);

        return $this->sendResponse($format->toArray($asset), null, 200);
    }

    /**
     * @group Launcher Asset
     *
     * Ajout 1 au compteur de téléchargement
     *
     * @urlParam id required int ID de l'objet
     *
     * @param $asset_id
     * @return JsonResponse
     */
    public function addCounter($asset_id)
    {
        $asset = $this->asset->newQuery()->findOrFail($asset_id);
        $asset->update([
            'count_download' => $asset->count_download + 1
        ]);

        return $this->sendResponse(null, null, 200);
    }
}
