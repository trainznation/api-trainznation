<?php

namespace App\Http\Controllers\Api\Launcher\Notice;

use App\Http\Controllers\Api\BaseController;
use App\Http\Controllers\Controller;
use App\Model\BugTacker;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class NoticeController extends BaseController
{
    /**
     * @var BugTacker
     */
    private $bugTacker;

    /**
     * NoticeController constructor.
     * @param BugTacker $bugTacker
     */
    public function __construct(BugTacker $bugTacker)
    {
        $this->bugTacker = $bugTacker;
    }

    /**
     * @group Launcher Notice
     *
     * Poste un nouveau ticket
     *
     * @bodyParam ip string required IP emmetrice
     * @bodyParam channel string required Type de cannal par lequel le ticket à été ouvert
     * @bodyParam categorie string required Catégorie de la demande (Plu, Déplu, Suggestion, Bugs)
     * @bodyParam sector string required Quelle est la source concerner (Launcher, Site, Téléchargement, etc...)
     * @bodyParam message string required Message de description
     *
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function noticing(Request $request)
    {
        $bug = $this->bugTacker->newQuery()->create([
            "ip" => $request->ip(),
            "channel" => $request->get('channel'),
            "categorie" => $request->get('categorie'),
            "sector" => $request->get('sector'),
            "message" => $request->get('message')
        ]);

        return $this->sendResponse($bug->toArray(), null, 200);
    }
}
