<?php

namespace App\Http\Controllers\Api\Launcher\Blog;

use App\Http\Controllers\Api\BaseController;
use App\Http\Controllers\Controller;
use App\Http\Resources\Launcher\Blog\GetArticleFormated;
use App\Http\Resources\Launcher\Blog\ListArticleFormated;
use App\Model\Blog;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class BlogController extends BaseController
{
    /**
     * @var Blog
     */
    private $blog;

    /**
     * BlogController constructor.
     * @param Blog $blog
     */
    public function __construct(Blog $blog)
    {
        Carbon::setLocale('fr');
        $this->blog = $blog;
    }

    /**
     * @group Launcher Blog
     *
     * Affiche la liste suivant des paramètres
     *
     * @bodyParam limit int Le nombre d'enregistrement afficher
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function list(Request $request)
    {
        //dd($request->all());
        $list = new ListArticleFormated();
        $calls = $this->blog->newQuery()
            ->isPublished()
            ->limit($request->get('limit'))
            ->orderBy('published_at', 'DESC')
            ->get()->load('category');

        return $this->sendResponse($list->toArray($calls), null, 200);
    }

    /**
     * @group Launcher Blog
     *
     * Affiche les information d'un article
     *
     * @urlParam id int required ID de l'article
     *
     * @param $id
     * @return JsonResponse
     */
    public function show($id)
    {
        $formated = new GetArticleFormated();
        $call = $this->blog->newQuery()->findOrFail($id)->load('category');

        return $this->sendResponse($formated->toArray($call), null, 200);
    }
}
