<?php

namespace App\Http\Controllers\Automate;

use App\Helpers\Notificator;
use App\Http\Controllers\Controller;
use App\Model\Tutoriel;
use Illuminate\Http\Request;

class AutomateController extends Controller
{
    /**
     * @var Tutoriel
     */
    private $tutoriel;
    /**
     * @var Notificator
     */
    private $notificator;

    /**
     * AutomateController constructor.
     * @param Tutoriel $tutoriel
     * @param Notificator $notificator
     */
    public function __construct(Tutoriel $tutoriel, Notificator $notificator)
    {
        $this->tutoriel = $tutoriel;
        $this->notificator = $notificator;
    }

    public function publishingTutoriel()
    {
        $tutoriels = $this->tutoriel->newQuery()->where('published', 1)->get();
        foreach ($tutoriels as $tutoriel) {
            if($tutoriel->published_at == now()) {
                $this->notificator->publish('Nouveau tutoriel publier', 'success', "Le tutoriel <strong>".$tutoriel->title."</strong> à été publier", "fas fa-camera-video");
            }
        }
    }
}
