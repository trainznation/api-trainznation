<?php

namespace App\Events\Route;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class RoutePublishVersionEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private $version;
    private $route;

    /**
     * Create a new event instance.
     *
     * @param $version
     * @param $route
     */
    public function __construct($version, $route)
    {
        //
        $this->version = $version;
        $this->route = $route;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
