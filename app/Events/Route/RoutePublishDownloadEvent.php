<?php

namespace App\Events\Route;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class RoutePublishDownloadEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private $download;

    /**
     * Create a new event instance.
     *
     * @param $download
     */
    public function __construct($download)
    {
        //
        $this->download = $download;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
