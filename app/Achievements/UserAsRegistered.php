<?php
declare(strict_types=1);

namespace App\Achievements;

use Assada\Achievements\Achievement;

/**
 * Class Registered
 *
 * @package App\Achievements
 */
class UserAsRegistered extends Achievement
{
    /*
     * The achievement name
     */
    public $name = 'UserAsRegistered';


    /*
     * A small description for the achievement
     */
    public $description = 'Félicitation, vous êtes inscrit sur Trainznation';
}
