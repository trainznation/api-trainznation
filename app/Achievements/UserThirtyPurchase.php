<?php
declare(strict_types=1);

namespace App\Achievements;

use Assada\Achievements\Achievement;

/**
 * Class Registered
 *
 * @package App\Achievements
 */
class UserThirtyPurchase extends Achievement
{
    /*
     * The achievement name
     */
    public $name = 'UserThirtyPurchase';

    /*
     * A small description for the achievement
     */
    public $description = 'Vous avez effectuer 30 achats';
}
