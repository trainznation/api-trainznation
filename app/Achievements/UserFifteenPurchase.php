<?php
declare(strict_types=1);

namespace App\Achievements;

use Assada\Achievements\Achievement;

/**
 * Class Registered
 *
 * @package App\Achievements
 */
class UserFifteenPurchase extends Achievement
{
    /*
     * The achievement name
     */
    public $name = 'UserFifteenPurchase';

    /*
     * A small description for the achievement
     */
    public $description = 'Vous avez effectuer 15 Achats';
}
