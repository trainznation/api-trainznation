<?php
declare(strict_types=1);

namespace App\Achievements;

use Assada\Achievements\Achievement;

/**
 * Class Registered
 *
 * @package App\Achievements
 */
class UserFirstPurchase extends Achievement
{
    /*
     * The achievement name
     */
    public $name = 'UserFirstPurchase';

    /*
     * A small description for the achievement
     */
    public $description = 'Vous avez effectuer votre premier achat';
}
