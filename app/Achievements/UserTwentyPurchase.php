<?php
declare(strict_types=1);

namespace App\Achievements;

use Assada\Achievements\Achievement;

/**
 * Class Registered
 *
 * @package App\Achievements
 */
class UserTwentyPurchase extends Achievement
{
    /*
     * The achievement name
     */
    public $name = 'UserTwentyPurchase';

    /*
     * A small description for the achievement
     */
    public $description = 'Vous avez effectuer 20 Achats';
}
