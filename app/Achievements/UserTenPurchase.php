<?php
declare(strict_types=1);

namespace App\Achievements;

use Assada\Achievements\Achievement;

/**
 * Class Registered
 *
 * @package App\Achievements
 */
class UserTenPurchase extends Achievement
{
    /*
     * The achievement name
     */
    public $name = 'UserTenPurchase';

    /*
     * A small description for the achievement
     */
    public $description = 'Vous commençez à être un habituer';
}
