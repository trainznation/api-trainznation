<?php
declare(strict_types=1);

namespace App\Achievements;

use Assada\Achievements\Achievement;

/**
 * Class Registered
 *
 * @package App\Achievements
 */
class UserPremium extends Achievement
{
    /*
     * The achievement name
     */
    public $name = 'UserPremium';

    /*
     * A small description for the achievement
     */
    public $description = 'Vous êtes maintenant un membre premium';
}
