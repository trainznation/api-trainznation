<?php


namespace App\Helpers;


use App\Model\UserNotification;
use App\User;

class Notificator
{
    /**
     * @var UserNotification
     */
    private $notification;

    /**
     * Notificator constructor.
     * @param UserNotification $notification
     */
    public function __construct(UserNotification $notification)
    {
        $this->notification = $notification;
    }

    public function publish($title, $status, $description = null, $icon = null, $image = null, $unit = false, $user_id = null)
    {
        if($unit == true) {
            $this->notification->newQuery()->create([
                "title" => $title,
                "description" => $description,
                "icon" => $icon,
                "image" => $image,
                "status" => $status,
                "user_id" => $user_id
            ]);
        } else {
            $users = User::all();
            foreach ($users as $user) {
                $this->notification->newQuery()->create([
                    "title" => $title,
                    "description" => $description,
                    "icon" => $icon,
                    "image" => $image,
                    "status" => $status,
                    "user_id" => $user->id
                ]);
            }
        }
    }

    public function publishAsAdmin($title, $status, $description = null, $icon = null, $image = null)
    {
        $users = User::where('admin', 1)->get();
        foreach ($users as $user) {
            $this->notification->newQuery()->create([
                "title" => $title,
                "description" => $description,
                "icon" => $icon,
                "image" => $image,
                "status" => $status,
                "user_id" => $user->id
            ]);
        }
    }
}
