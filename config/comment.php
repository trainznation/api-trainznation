<?php

use Actuallymab\LaravelComment\Models\Comment;

return [
    /**
     * This is the default comment model of the application.
     * If you create another comment class with extending this one, you should update this field with that.
     */
    'model' => Comment::class,
];
