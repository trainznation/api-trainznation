<?php

return [
    'priority' => [
        0 => [
            "name" => "Basse",
            "point" => "1"
        ],
        1 => [
            "name" => "Moyenne",
            "point" => 5,
        ],
        2 => [
            "name" => "Haute",
            "point" => 10
        ],
        3 => [
            "name" => "Critique",
            "point" => 20
        ]
    ]
];
