<?php
namespace Deployer;

require 'recipe/laravel.php';

// Project name
set('application', 'API Trainznation');

// Project repository
set('repository', 'git@gitlab.com:trainznation/api-trainznation.git');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', true);

// Shared files/dirs between deploys
add('shared_files', []);
add('shared_dirs', []);

// Writable dirs by web server
add('writable_dirs', []);


// Hosts

host('192.168.0.18')
    ->user('deployer')
    ->identityFile('~/.ssh/deployerkey')
    ->set('deploy_path', '/www/wwwroot/api.trainznation.tk')
    ->set('writable_mode', 'chmod');

// Tasks

task('build', function () {
    run('cd {{release_path}} && npm install && npm run dev');
});

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

// Migrate database before symlink new release.

before('deploy:symlink', 'artisan:migrate');

