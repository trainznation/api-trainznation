<?php

namespace Tests\Feature\Admin\Blog;

use App\Model\BlogCategory;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class BlogCategoryTest extends TestCase
{
    use RefreshDatabase;

    public function getUser() {
        return factory(User::class)->create(["admin" => 1]);
    }

    public function testListCategories()
    {
        $user = factory(User::class)->create(["admin" => 1]);
        $this->withoutExceptionHandling();
        $categories = factory(BlogCategory::class, 5)->create();

        $response = $this->actingAs($user, 'api')->getJson('api/admin/blog/category/list');

        $response
            ->assertStatus(200)
            ->assertJson([
                'success' => 200
            ]);
    }

    public function testListCategoriesWithSearch()
    {
        $user = factory(User::class)->create(["admin" => 1]);
        $this->withoutExceptionHandling();
        $categories = factory(BlogCategory::class, 5)->create();

        $response = $this->actingAs($user, 'api')->getJson('api/admin/blog/category/list?q=serch');

        $response
            ->assertStatus(200)
            ->assertJson([
                'success' => 200
            ]);
    }

    public function testCreateCategory()
    {
        $user = factory(User::class)->create(["admin" => 1]);
        $response = $this->actingAs($user, 'api')->postJson('api/admin/blog/category', ["name" => "Test"]);

        $response->assertStatus(200)->assertJson([
            "success" => 200,
        ]);
    }

    public function testCreateCategoryWithoutField()
    {
        $response = $this->actingAs($this->getUser(), 'api')->postJson('api/admin/blog/category', []);

        $response->assertStatus(422);
    }

    public function testDeleteCategory()
    {
        $category = factory(BlogCategory::class)->create();

        $call = $this->actingAs($this->getUser(), 'api')->deleteJson('api/admin/blog/category/'.$category->id);

        $call->assertStatus(200)->assertJson([
            "success" => 200,
        ]);
    }

    public function testErrorDeleteCategory()
    {
        $category = factory(BlogCategory::class)->create();

        $call = $this->actingAs($this->getUser(), 'api')->deleteJson('api/admin/blog/category/2');

        $call->assertStatus(404);
    }
}
