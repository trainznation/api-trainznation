<?php

namespace Tests\Feature\Admin\Blog;

use App\Model\Blog;
use App\Model\BlogCategory;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Str;
use Tests\TestCase;

class BlogTest extends TestCase
{
    public function getUser() {
        return factory(User::class)->create(["admin" => 1]);
    }

    public function testListBlog()
    {
        factory(BlogCategory::class)->create();
        factory(Blog::class, 5)
            ->create();

        $call = $this->actingAs($this->getUser(), 'api')->getJson('api/admin/blog/list')
            ->assertStatus(200);
    }

    public function testListBlogWithSearch()
    {
        factory(BlogCategory::class)->create();
        factory(Blog::class, 5)
            ->create();

        $call = $this->actingAs($this->getUser(), 'api')->getJson('api/admin/blog/list?q=test')
            ->assertStatus(200);
    }

    public function testCreateArticle()
    {
        $this->withoutExceptionHandling();
        $category = factory(BlogCategory::class)->create();
        $data = [
            "title" => "Test d'un titre",
            "short_content" => "Courte description à 120 caractères",
            "content" => "Description",
            "blog_category_id" => $category->id,
            "published" => 0,
            "social" => 0
        ];

        $this->actingAs($this->getUser(), 'api')->postJson('api/admin/blog', $data)
            ->assertStatus(200);
    }

    public function testCreateArticleWithoutBreak()
    {
        //$this->withoutExceptionHandling();
        $category = factory(BlogCategory::class)->create();
        $data = [];

        $this->actingAs($this->getUser(), 'api')->postJson('api/admin/blog', $data)
            ->assertStatus(422);
    }

    public function testGetBlog()
    {
        factory(BlogCategory::class)->create();
        factory(Blog::class)
            ->create();

        $call = $this->actingAs($this->getUser(), 'api')->getJson('api/admin/blog/1')
            ->assertStatus(200);
    }

    public function testUpdateBlog()
    {
        //$this->withoutExceptionHandling();
        factory(BlogCategory::class)->create();
        factory(Blog::class)
            ->create();

        $data = [
            "blog_category_id" => 1,
            "title" => "Testing",
            "short_content" => "testing",
            "slug" => Str::slug("Testi"),
            "content" => "Testing",
            "published" => 0,
            "social" => 0
        ];

        $call = $this->actingAs($this->getUser(), 'api')->putJson('api/admin/blog/1', $data)
            ->assertStatus(200);
    }

    public function testDeleteBlog()
    {
        factory(BlogCategory::class)->create();
        factory(Blog::class)
            ->create();

        $this->actingAs($this->getUser(), 'api')->deleteJson('api/admin/blog/1')
            ->assertStatus(200);
    }

    public function testDeleteBlogWithErrorHandling()
    {
        factory(BlogCategory::class)->create();
        factory(Blog::class)
            ->create();

        $this->actingAs($this->getUser(), 'api')->deleteJson('api/admin/blog/2')
            ->assertStatus(404);
    }

    public function testBlogPublish()
    {
        factory(BlogCategory::class)->create();
        factory(Blog::class)
            ->create();

        $this->actingAs($this->getUser(), 'api')->getJson('api/admin/blog/1/publish')
            ->assertStatus(200);
    }

    public function testBlogDispublish()
    {
        factory(BlogCategory::class)->create();
        factory(Blog::class)
            ->create();

        $this->actingAs($this->getUser(), 'api')->getJson('api/admin/blog/1/dispublish')
            ->assertStatus(200);
    }
}
