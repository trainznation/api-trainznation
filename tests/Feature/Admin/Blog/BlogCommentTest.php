<?php

namespace Tests\Feature\Admin\Blog;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class BlogCommentTest extends TestCase
{
    public function getUser() {
        return factory(User::class)->create(["admin" => 1]);
    }
}
