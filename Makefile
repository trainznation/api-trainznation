sy := php artisan

.DEFAULT_GOAL := help
.PHONY: help
help: ## Affiche cette aide
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.PHONY: tf
tf: ## Migrate Fresh avec seedingFactory
	$(sy) migrate:fresh
	$(sy) db:seed --class="TestingSeeder"

.PHONY: t
t: ## Migrate Fresh
	$(sy) migrate:fresh

.PHONY: tp
tp: ## Migrate Fresh Production
	$(sy) migrate:fresh
	$(sy) db:seed --class="ProductionSeeder"

.PHONY: clear
clear: ## Nettoie toues les caches du système
	$(sy) down
	$(sy) clear-compiled
	$(sy) optimize
	$(sy) auth:clear-resets
	$(sy) cache:clear
	$(sy) config:clear
	$(sy) event:clear
	$(sy) queue:flush
	$(sy) route:clear
	$(sy) view:clear
	$(sy) up

.PHONY: coverage
test: ## Effectue un test par artisan avec coverage
	$(sy) test --coverage-html=coverage
