<?php
use OpenApi\Annotations as OA;
/**
 * @OA\Info(title="API TRAINZNATION", version="1.0")
 * @OA\Server(
 *     url="https://api.trainznation.test/api",
 *     description="Api du site Trainznation (Dev)"
 * )
 * @OA\Server(
 *     url="https://api.trainznation.tk/api",
 *     description="Api du site Trainznation"
 * )
 */
